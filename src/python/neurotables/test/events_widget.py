import sys

import numpy as np

from PyQt4.QtGui import QMainWindow,QApplication,QHBoxLayout,QScrollArea

from neurotables.test.generated.test_window import Ui_TestWindow
from neurotables.ui.events_widget import EventsWidget,create_events_widget_layout


class TestWindow(QMainWindow, Ui_TestWindow):

    def __init__(self, parent=None):
        QMainWindow.__init__(self, parent)
        self.setupUi(self)


def testEventsWidget():
    app = QApplication([])

    test_window = TestWindow()

    img = np.random.rand(60, 1000)

    events = [{'id':1, 'start':1.0, 'end':1.5, 'background-color':'#fff'},
              {'id':2, 'start':2.25, 'end':3.5, 'background-color':'#fff', 'image':img},
              {'id':3, 'start':4.0, 'end':5.7, 'background-color':'#fff'}
             ]

    eventsWidget = EventsWidget(events, duration=7.0, resolution=300.0)
    eventsWidget.selectionChanged.connect(selectionChangedEvent)
    layout = create_events_widget_layout(eventsWidget)

    test_window.centralwidget.setLayout(layout)

    test_window.show()
    sys.exit(app.exec_())


def selectionChangedEvent(start, end):
    print 'Selection changed: start=%0.3f, end=%0.3f' % (start, end)

if __name__ == '__main__':
    testEventsWidget()