import os
import time
import unittest
import datetime

import numpy as np
import tables

from neurotables.objects.cells import *


class TestCells(unittest.TestCase):

    H5_FILE = '/tmp/TestCells.h5'

    def setUp(self):
        self.f = tables.open_file(TestCells.H5_FILE, 'a')

    def tearDown(self):
        self.f.close()
        os.remove(TestCells.H5_FILE)

    def test_framework(self):

        s1 = Sound(self.f)
        s1.sample_rate = 441000
        s1.md5 = '81351238583288'
        s1.stim_class = 'Con'
        s1.stim_type = 'Something'
        s1.stim_number = 1
        s1.waveform = np.random.randn(2000)
        s1.save()

        subject = Subject(self.f)
        subject.name = 'Frank'
        subject.age = 10
        subject.sex = 'M'
        subject.save()

        experiment = Experiment(self.f)
        experiment.subject_id = subject.id
        experiment.name = 'Awesome'
        experiment.save()

        site1 = Site(self.f)
        site1.experiment_id = experiment.id
        site1.ldepth = 500
        site1.rdepth = 800
        site1.save()

        geom = ElectrodeGeometry(self.f)
        geom.name = 'MEA'
        geom.save()

        e1data = ElectrodeData(self.f)
        e1data.site_id = site1.id
        e1data.number = 1
        e1data.geometry_id = geom.id
        e1data.save()

        e2data = ElectrodeData(self.f)
        e2data.site_id = site1.id
        e2data.number = 2
        e2data.geometry_id = geom.id
        e2data.save()

        proto1a = Protocol(self.f)
        proto1a.name = 'Calls1'
        proto1a.site_id = site1.id
        proto1a.timezone = 'PST'
        proto1a.start_time = time.mktime(datetime.datetime.now().timetuple())
        proto1a.end_time = time.mktime(datetime.datetime.now().timetuple())
        proto1a.save()

        p = Protocol(self.f)
        p.from_id(proto1a.id)

        assert p.site.id == site1.id
        assert len(site1.protocols) == 1
        assert site1.protocols[0].id == proto1a.id

        assert e1data.site.id == site1.id
        assert len(site1.electrodes) == 2
        assert site1.electrodes[0].id == e1data.id
        assert site1.electrodes[1].id == e2data.id


class TestPlots(unittest.TestCase):

    def setUp(self):
        if not hasattr(self, 'f'):
            self.f = tables.open_file('/auto/k8/fdata/mschachter/tdt_continuous/GreBlu9508M/GreBlu9508M_tabled.h5', 'r')
        self.s = Site(self.f)
        assert self.s.from_id(1)

    def tearDown(self):
        pass

    def test_protocol(self):
        assert len(self.s.protocols) > 0
        p = self.s.protocols[0]
        p.plot_spectrograms(169.0, 170.0, threshold=None, colormap=cm.jet)


if __name__ == '__main__':
    unittest.main()








