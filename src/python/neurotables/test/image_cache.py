import os
import unittest

import numpy as np

from neurotables.ui.image_cache import ImageCache,get_qimage_from_raw_data


class TestImageCache(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_cache(self):
        ic = ImageCache()
        assert os.path.exists(ic.cache_dir)

        #generate a random qimage
        noise = np.random.rand(100, 1000)
        img = get_qimage_from_raw_data(noise, 1000, 100)

        #store an image that is not in the cache
        image_id = 'test1234.png'
        assert ic.get_qimage(image_id) == None
        assert ic.store_qimage(image_id, img)

        #test the cache to make sure things are stored
        assert image_id in ic.cache
        assert os.path.exists(os.path.join(ic.cache_dir, image_id))

        img2 = ic.get_qimage(image_id)
        assert img2 is not None



if __name__ == '__main__':
    unittest.main()



