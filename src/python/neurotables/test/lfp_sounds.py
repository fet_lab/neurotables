from scipy.interpolate import interp1d
import unittest

import numpy as np

import matplotlib.pyplot as plt

import tables
from lasp.hht import HHT
from lasp.signal import resample_signal
from lasp.sound import WavFile

from neurotables.objects.cells import Site


class TestLFPSounds(unittest.TestCase):

    def setUp(self):
        if not hasattr(self, 'f'):
            self.f = tables.open_file('/auto/k8/fdata/mschachter/tdt_continuous/GreBlu9508M/GreBlu9508M_tabled.h5', mode='a')

    def tearDown(self):
        pass

    def test_am(self):

        s = Site(self.f)
        assert s.from_id(1)
        assert len(s.electrodes) > 0

        e = s.electrodes[0]
        assert len(e.lfp.protocol_responses) > 0

        #get the LFP waveform from the electrode
        lfppr = e.lfp.protocol_responses[0]

        #get a segment of the LFP
        sr = lfppr.lfp.sample_rate
        dt = 1.0 / sr
        t1 = 100.0
        t2 = 125.0
        i1 = int(t1*sr)
        i2 = int(t2*sr)
        s = lfppr.waveform[i1:i2]
        t = np.arange(0, len(s))*dt

        #create carrier at high sampling rate
        csr = 44100.0
        cdt = 1.0 / csr
        cn = int((t2 - t1 - dt) / cdt)
        ct = np.arange(0, cn)*cdt
        cf = 1000.0 # carrier frequency
        cwave = np.zeros_like(ct)
        #freqs = [600, 1200, 2400]
        freqs = [500, 1000, 2000, 4000]
        for freq in freqs:
            cwave += np.sin(2*np.pi*ct*freq)

        print 'len(t)=%d, len(ct)=%d, t=(%0.6f, %0.6f), ct=(%0.6f, %0.6f)' % (len(t), len(ct), t.min(), t.max(), ct.min(), ct.max())

        #do EMD on LFP
        print 'Computing EMD...'
        hht = HHT(s, sr, emd_resid_tol=1e-6, sift_max_iter=100)
        assert len(hht.imfs) > 0

        for k,imf in enumerate(hht.imfs):
            #get lowest frequency IMF, upsample to carrier sampling rate
            print 'Building IMF %d spline for resampling...' % k
            func = interp1d(t, imf, kind='linear')
            print 'Evaluating spline...'
            imf_rs = func(ct)
            #imf_rs = resample_signal(imf, sr, csr)
            imf_rs /= np.abs(imf_rs).max()

            #modulate carrier signal with upsampled IMF
            print 'Creating wavefile...'
            x = imf_rs*cwave

            #write the sound to a wavefile
            fname = '/tmp/test_am_imf%d.wav' % k
            wf = WavFile()
            wf.data = x
            wf.sample_rate = csr
            wf.depth = 1
            wf.to_wav(fname, normalize=True)

        """
        plt.figure()
        plt.subplot(3, 1, 1)
        plt.plot(t, s, 'k-')
        plt.axis('tight')
        plt.title('Signal')
        plt.subplot(3, 1, 2)
        plt.plot(ct, imf_rs, 'b-')
        plt.axis('tight')
        plt.title('IMF')
        plt.subplot(3, 1, 3)
        plt.plot(ct, x, 'g-')
        plt.title('Modulated Signal')
        plt.axis('tight')
        plt.show()
        """


    def test_fm(self):

        s = Site(self.f)
        assert s.from_id(1)
        assert len(s.electrodes) > 0

        e = s.electrodes[0]
        assert len(e.lfp.protocol_responses) > 0

        #get the LFP waveform from the electrode
        lfppr = e.lfp.protocol_responses[0]

        #get a segment of the LFP
        sr = lfppr.lfp.sample_rate
        dt = 1.0 / sr
        t1 = 100.0
        t2 = 125.0
        i1 = int(t1*sr)
        i2 = int(t2*sr)
        s = lfppr.waveform[i1:i2]
        t = np.arange(0, len(s))*dt

        #set carrier wave properties
        csr = 44100.0
        cdt = 1.0 / csr
        cn = int((t2 - t1 - dt) / cdt)
        ct = np.arange(0, cn)*cdt

        print 'len(t)=%d, len(ct)=%d, t=(%0.6f, %0.6f), ct=(%0.6f, %0.6f)' % (len(t), len(ct), t.min(), t.max(), ct.min(), ct.max())

        #do EMD on LFP
        print 'Computing EMD...'
        hht = HHT(s, sr, emd_resid_tol=1e-6, sift_max_iter=100)
        assert len(hht.imfs) > 0

        for k,imf in enumerate(hht.imfs):
            #get lowest frequency IMF, upsample to carrier sampling rate
            print 'Building IMF %d spline for resampling...' % k
            func = interp1d(t, imf, kind='linear')
            print 'Evaluating spline...'
            imf_rs = func(ct)
            #imf_rs = resample_signal(imf, sr, csr)
            imf_rs /= np.abs(imf_rs).max()

            #create frequency-modulated signal
            cf = 1000.0
            bw = 400.0
            #cwave = np.zeros_like(ct)
            fsum = np.cumsum(imf_rs)*dt
            cwave = np.sin(2*np.pi*ct*cf + bw*fsum)

            #modulate carrier signal with upsampled IMF
            print 'Creating wavefile...'
            x = imf_rs*cwave

            #write the sound to a wavefile
            fname = '/tmp/test_fm_imf%d.wav' % k
            wf = WavFile()
            wf.data = x
            wf.sample_rate = csr
            wf.depth = 1
            wf.to_wav(fname, normalize=True)

        """
        plt.figure()
        plt.subplot(3, 1, 1)
        plt.plot(t, s, 'k-')
        plt.axis('tight')
        plt.title('Signal')
        plt.subplot(3, 1, 2)
        plt.plot(ct, imf_rs, 'b-')
        plt.axis('tight')
        plt.title('IMF')
        plt.subplot(3, 1, 3)
        plt.plot(ct, x, 'g-')
        plt.title('Modulated Signal')
        plt.axis('tight')
        plt.show()
        """
