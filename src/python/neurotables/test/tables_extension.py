import os
import unittest

import numpy as np
import tables


from neurotables.objects.tables_extension import TablesBackedObject,DatasetCol,TablesBackedRow,tj_relations,IdCol


class AClass(TablesBackedObject):

    class Row(TablesBackedRow):
        x1 = tables.Float64Col()
        x2 = tables.Int64Col()
        x3 = DatasetCol()


class BClass(TablesBackedObject):

    class Row(TablesBackedRow):
        xx = DatasetCol({'compute':'get_xx'})

    def get_xx(self):
        return np.array([22, 23])

class CClass(TablesBackedObject):

    class Row(TablesBackedRow):
        b_id = IdCol()
        xxx = tables.Float64Col()

tj_relations.one2many(BClass, 'cvals', CClass, 'b_id', 'b')

class TestConstruction(unittest.TestCase):

    H5_FILE = '/tmp/TestSimpleConstruction.h5'

    def setUp(self):
        self.f = tables.open_file(TestConstruction.H5_FILE, 'a')

    def tearDown(self):
        self.f.close()
        os.remove(TestConstruction.H5_FILE)

    def test_get_all(self):
        pass

    def test_computable_datacol(self):
        #make a simple class and set some data
        b = BClass(self.f)
        b.save()
        y = b.xx
        assert len(y) == 2
        assert y[0] == 22
        assert y[1] == 23

        c = BClass(self.f)
        c.from_id(b.id)
        assert len(c.xx) == 2
        assert c.xx[0] == 22
        assert c.xx[1] == 23

    def test_simple_construction(self):
        #make a simple class and set some data
        a = AClass(self.f)
        a.x1 = 5352.35
        a.x2 = 8
        a.save()
        a.x3 = np.random.randn(4, 4)

        #pull the same class by id from the h5 file and check for equality
        b = AClass(self.f)
        b.from_id(a.id)
        assert a.id == b.id
        assert a.x1 == b.x1
        assert a.x2 == b.x2
        mdiff = np.max(a.x3 - b.x3)
        assert mdiff == 0.0

    def test_simple_relation(self):

        b = BClass(self.f)
        b.save()
        y = b.xx
        assert y is not None

        c = CClass(self.f)
        c.b_id = b.id
        c.xxx = 33.27
        c.save()

        assert c.b.id == b.id
        diff = np.linalg.norm(c.b.xx - b.xx)
        assert diff == 0.0

        assert len(b.cvals) == 1
        assert b.cvals[0].id == c.id
        assert b.cvals[0].xxx == c.xxx

if __name__ == '__main__':
    unittest.main()






