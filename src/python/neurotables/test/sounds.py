import unittest

import numpy as np

import tables

import matplotlib.pyplot as plt

from neurotables.objects.cells import Site


class TestSounds(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_sounds(self):

        f = tables.open_file('/auto/k8/fdata/mschachter/tdt_continuous/GreBlu9508M/GreBlu9508M_tabled.h5', 'r')

        s = Site(f)
        assert s.from_id(1)

        p = s.protocols[0]

        sounds = dict()
        for e in p.events:
            snd = e.sound
            if snd.id not in sounds and snd.stim_type == 'song':
                sounds[s.id] = snd

        #make plot of single song
        skeys = sounds.keys()
        sid0 = skeys[0]
        #sounds[sid0].plot(db_rectify=-100)
        plt.rcParams.update({'font.size': 20})

        #make plot of 10 different songs
        db_rectify = -100
        """
        plt.figure()
        plt.subplots_adjust(bottom=0.06, top=0.99, right=0.99, left=0.06)
        for k in range(10):

            ax = plt.subplot(5, 2, k+1)

            s = sounds[skeys[k]]
            spec = s.spectrogram
            sdata = spec.data
            if db_rectify is not None:
                sdata += db_rectify
                sdata[sdata < 0.0] = 0.0

            spec_t = np.arange(sdata.shape[1])*(1.0 / spec.sample_rate)
            plot_spectrogram(spec_t, spec.frequency, sdata, ax=ax, ticks=False, colorbar=False, colormap=cm.afmhot_r)
            plt.axis('tight')
        """

        #plot protocol sound specs and LFPs
        #p.plot_spectrograms(168.0, 180.0, threshold=0.0, min_freq=300.0, max_freq=8000.0, colormap=cm.afmhot_r, electrode_num=10, lfp_min_freq=5.0, lfp_max_freq=100.0)

        enum = 15
        pr = s.get_electrode(enum).get_lfp_protocol_response(p.name)

        #plot IMFs
        nimfs = 5
        start_time = 168.0
        end_time = 180.0
        e1 = int(start_time*pr.lfp.sample_rate)
        e2 = int(end_time*pr.lfp.sample_rate)
        lfp = pr.waveform[e1:e2]*1e6
        t = np.arange(e1, e2) / pr.lfp.sample_rate
        plt.figure()
        plt.subplots_adjust(bottom=0.06, top=0.99, right=0.99, left=0.06)
        plt.subplot(nimfs+1, 1, 1)
        plt.plot(t, lfp, 'k-', linewidth=2.0)
        plt.axis('tight')

        for k in range(nimfs):
            plt.subplot(nimfs+1, 1, k+2)
            plt.plot(t, pr.emd.emd[k, e1:e2]*1e6, 'b-', linewidth=2.0, alpha=0.75)
            if k == (nimfs-1):
                plt.xlabel('Time (s)')
            else:
                plt.xticks([])
            plt.axis('tight')

        plt.show()


