import numpy as np
import os

import tables

from PyQt4.QtGui import QMainWindow,QApplication,QHBoxLayout,QTabWidget,QScrollArea,QWidget
import sys
from neurotables.objects.cells import Site
from neurotables.test.generated.test_window import Ui_TestWindow
from neurotables.ui.controller import AppController
from neurotables.ui.events_widget import EventsWidget,create_events_widget_layout
from neurotables.ui.protocol_panel import ProtocolPanel


class TestWindow(QMainWindow, Ui_TestWindow):

    def __init__(self, parent=None):
        QMainWindow.__init__(self, parent)
        self.setupUi(self)


def testProtocolPanelWidget(site, protocol):

    app = QApplication([])

    test_window = TestWindow()

    controller = AppController()

    #create a site panel
    item = FakeItem()
    item.protocol = protocol
    item.site = site

    #create protocol panel
    protocolPanel = ProtocolPanel(controller=controller, item=item, parent=test_window)
    ewl = EventsWidgetListener(protocolPanel.eventsWidget)
    protocolPanel.eventsWidget.selectionChanged.connect(ewl.selectionChangedEvent)

    #create tab view to contain site panel
    tabView = QTabWidget()
    tabView.insertTab(0, protocolPanel, 'Protocol Panel')

    #create a layout to contain the tab view and add it to the main window
    main_layout = QHBoxLayout()
    main_layout.addWidget(tabView)

    test_window.centralwidget.setLayout(main_layout)

    test_window.show()
    sys.exit(app.exec_())


class FakeItem(object):
    def __init__(self):
        pass


class EventsWidgetListener(object):

    def __init__(self, eventsWidget):
        self.eventsWidget = eventsWidget

    def selectionChangedEvent(self, start, end):
        print 'Selection changed: start=%0.3f, end=%0.3f' % (start, end)
        img = self.eventsWidget.get_selection_image()
        ipath = '/tmp/events_widget_image.png'
        if os.path.exists(ipath):
            os.remove(ipath)
        img.save(ipath)
        print 'Image saved to %s' % ipath

if __name__ == '__main__':

    f = tables.open_file('/auto/k8/fdata/mschachter/tdt_continuous/GreBlu9508M/GreBlu9508M_tabled.h5', 'a')
    site = Site(f)
    assert site.from_id(1)

    protocol = site.protocols[0]

    testProtocolPanelWidget(site, protocol)
