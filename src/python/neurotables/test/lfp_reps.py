import copy
import unittest

import numpy as np

from matplotlib import cm

import matplotlib.pyplot as plt

import tables
import operator

from lasp.sound import plot_spectrogram
from lasp.timefreq import GaussianSpectrumEstimator,timefreq, gaussian_stft, bandpass_timefreq, resample_spectrogram, postprocess_spectrogram, compute_mean_spectrogram
from neurotables.objects.cells import Site, Sound


class TestLFPReps(unittest.TestCase):

    def setUp(self):
        if not hasattr(self, 'f'):
            self.f = tables.open_file('/auto/k8/fdata/mschachter/tdt_continuous/GreBlu9508M/GreBlu9508M_tabled.h5', mode='r')

            s = Site(self.f)
            assert s.from_id(1)
            assert len(s.electrodes) > 0
            self.site = s

            #get an electrode
            e = s.electrodes[0]
            self.lfp_pr = e.get_lfp_protocol_response('Call1')
            assert self.lfp_pr is not None

            #get electrode geometry
            self.geom = e.geometry
            self.coords = self.geom.coordinates

            self.protocol = self.lfp_pr.protocol
            self.protocol_sounds = np.unique([e.sound for e in self.protocol.events])

            #get list of sounds that occurred over course of protocol
            sounds = list()
            psounds = np.unique([e.sound.id for e in self.protocol.events])
            for sid in psounds:
                s = Sound(self.f)
                s.from_id(sid)
                sounds.append(s)

            stypes = np.unique([s.stim_type for s in sounds])
            natural_sounds = [s for s in sounds if s.stim_type in ['call', 'song']]
            slist = [(s.id, s.stim_type, s.call_id) for s in natural_sounds]
            slist.sort(key=operator.itemgetter(-1))
            slist.sort(key=operator.itemgetter(-2))
            #for s in slist:
            #    print s

            self.sr = self.lfp_pr.lfp.sample_rate

            #zscore the LFP
            lfp = self.lfp_pr.waveform
            lfp -= lfp.mean()
            lfp /= lfp.std(ddof=1)

            self.lfp = lfp
            self.t = np.arange(len(self.lfp)) / self.sr

    def tearDown(self):
        pass

    """
    def test_stim_conditioning(self):

        sound_id = 174
        #sound_id = 208

        #get the event times associated with the given sound
        s = Sound(self.f)
        assert s.from_id(sound_id)
        etimes = np.array([(e.start_time, e.end_time) for e in self.protocol.events if e.sound.id == sound_id])
        duration = np.min(etimes[:, 1] - etimes[:, 0])

        #sort electrodes by hemisphere
        ehemi = [(electrode, electrode.hemisphere) for electrode in self.site.electrodes]
        ehemi.sort(key=operator.itemgetter(-1))

        #get the multi-electrode LFPs that correspond to the given repeats of the stimulus
        ntrials = len(s.events)
        nduration = int(duration*self.sr)
        num_electrodes = len(ehemi)
        lfps = np.zeros([num_electrodes, ntrials, nduration])

        for k,(electrode,hemi) in enumerate(ehemi):
            lfp_pr = electrode.get_lfp_protocol_response('Call1')
            lfp = lfp_pr.waveform
            lfp -= lfp.mean()
            lfp /= lfp.std(ddof=1)
            for j,(stime, etime) in enumerate(etimes):
                si = int(stime*self.sr)
                ei = si + nduration
                lfps[k, j, :] = lfp[si:ei]

        lfps_mean = lfps.mean(axis=1)
        lfps_std = lfps.std(axis=1, ddof=1)

        t = np.arange(nduration)*(1.0/self.sr)
        spec = s.spectrogram
        sdata = copy.copy(spec.data)
        db_rectify = 0.0
        sdata += db_rectify
        sdata[sdata < 0.0] = 0.0
        spec_t = np.arange(sdata.shape[1])*(1.0 / spec.sample_rate)
        spec_f = spec.frequency

        #make plots
        egroups = [range(1, 17), range(17, 33)]
        for egrp in egroups:

            wv_fig = plt.figure()
            spec_fig = plt.figure()
            nrows = 9
            ncols = 2

            #plot the stimulus spectrogram in each column
            ax = wv_fig.add_subplot(nrows, ncols, 1)
            plot_spectrogram(spec_t, spec_f, sdata, ax=ax, ticks=False, colorbar=False, colormap=cm.gist_yarg)
            ax = wv_fig.add_subplot(nrows, ncols, 2)
            plot_spectrogram(spec_t, spec_f, sdata, ax=ax, ticks=False, colorbar=False, colormap=cm.gist_yarg)

            ax = spec_fig.add_subplot(nrows, ncols, 1)
            plot_spectrogram(spec_t, spec_f, sdata, ax=ax, ticks=False, colorbar=False, colormap=cm.gist_yarg)
            ax = spec_fig.add_subplot(nrows, ncols, 2)
            plot_spectrogram(spec_t, spec_f, sdata, ax=ax, ticks=False, colorbar=False, colormap=cm.gist_yarg)

            for k,enumber in enumerate(egrp):
                #get the grid coordinates for this LFP
                row,col = np.nonzero(self.coords == enumber)

                print 'enumber=%d, row=%d, col=%d' % (enumber, row, col)
                sp = 2 + row*2 + (col % 2) + 1

                #plot waveform
                ax = wv_fig.add_subplot(nrows, ncols, sp)
                plt.sca(ax)
                si = enumber - 1
                y = lfps_mean[si, :]
                yerr = lfps_std[si, :]
                plt.errorbar(t, y, yerr=yerr, ecolor='r')
                if row < (nrows - 2):
                    plt.xticks([])
                plt.ylabel('E%d' % enumber)
                plt.axis('tight')

                #plot spectrogram
                ax = spec_fig.add_subplot(nrows, ncols, sp)
                plt.sca(ax)
                lfp_spec_t,lfp_spec_f,lfp_spec,lfp_rms = gaussian_stft(y, self.sr, 0.075, 0.025)
                lfp_spec = postprocess_spectrogram(np.abs(lfp_spec))
                plot_spectrogram(lfp_spec_t, lfp_spec_f, lfp_spec, ax=ax, ticks=False, colormap=cm.afmhot_r, colorbar=False)
                plt.ylabel('E%d' % enumber)

        plt.show()
    """

    def test_stft_timefreq(self):

        #set up spectrum estimator
        win_sizes = np.arange(0.050, 0.800, 0.050)

        #set segment in time to be examined
        t1 = 500
        t2 = 560
        i1 = int(t1*self.sr)
        i2 = int(t2*self.sr)

        num_freqs = 75
        t,freq,tf = compute_mean_spectrogram(self.lfp[i1:i2], self.sr, win_sizes, 0.025, mask=False, num_freq_bands=num_freqs)
        t,freq,tf_mask = compute_mean_spectrogram(self.lfp[i1:i2], self.sr, win_sizes, 0.025, mask=True, mask_gain=0.5, num_freq_bands=num_freqs)

        #get stimulus spectrogram
        stim_t, stim_freq, stim_spec = self.protocol.compute_stimulus_spectrogram(t1, t2)

        #get stimulus amplitude envelope
        stim_env_sr = 400.0
        stim_tenv,stim_env = self.protocol.compute_stimulus_envelope(sample_rate=stim_env_sr, t1=t1, t2=t2)
        #compute spectrogram of amplitude envelope
        gaussian_est = GaussianSpectrumEstimator(nstd=6)
        stim_env_spec_t,stim_env_spec_f,stim_env_spec = timefreq(stim_env, stim_env_sr, win_sizes[0], win_sizes[0]/2.0, gaussian_est)

        nrows = 4
        ncols = 1

        plt.figure()
        plt.subplots_adjust(bottom=0.06, top=0.97, right=0.99, left=0.06)

        plt.subplot(nrows, ncols, 1)
        print 'stim_spec.shape=',stim_spec.shape
        plot_spectrogram(stim_t, stim_freq, stim_spec, ticks=True, fmin=300.0, fmax=8000.0, colorbar=False)
        plt.xticks([])
        plt.xlabel('')
        #stim_env_spec_log = 20*np.log10(np.abs(stim_env_spec))
        #thresh = np.percentile(stim_env_spec_log.ravel(), 10)
        #stim_env_spec_log[stim_env_spec_log < thresh] = thresh
        #plot_spectrogram(stim_env_spec_t, stim_env_spec_f, stim_env_spec_log, ticks=True, colorbar=False, colormap=cm.afmhot_r)

        plt.subplot(nrows, ncols, 2)
        plt.plot(self.t[i1:i2], self.lfp[i1:i2], 'k-', linewidth=2.0)
        plt.ylabel('Raw LFP')
        plt.axis('tight')
        plt.xticks([])
        plt.xlabel('')

        #plot the mean spectrogram
        ax = plt.subplot(nrows, ncols, 3)
        plot_spectrogram(t, freq, tf, ticks=True, colorbar=False, ax=ax, fmin=0.0, colormap=cm.afmhot_r)
        plt.xticks([])
        plt.xlabel('')
        plt.ylabel('Mean (unmasked)')

        ax = plt.subplot(nrows, ncols, 4)
        plot_spectrogram(t, freq, tf_mask, ticks=True, colorbar=False, ax=ax, fmin=0.0, colormap=cm.afmhot_r)
        plt.xticks([])
        plt.xlabel('')
        plt.ylabel('Mean (masked)')

        plt.show()

    """
    def test_bandpass_timefreq(self):

        bpfreqs = [4.0, 8.0, 15.0, 30.0, 80.0, 120.0]

        tf_freqs,tf_raw,tf = bandpass_timefreq(self.lfp, bpfreqs, self.sr)

        t1 = 600.0
        t2 = 605.0
        i1 = int(t1*self.sr)
        i2 = int(t2*self.sr)
        t = (np.arange(i2-i1) / self.sr) + t1

        plt.figure()
        plt.subplots_adjust(bottom=0.06, top=0.97, right=0.99, left=0.06)
        plt.subplot(len(tf_freqs)+1, 1, 1)
        plt.plot(t, self.lfp[i1:i2], 'k-', linewidth=2.0)
        plt.title('Raw LFP')
        plt.axis('tight')
        plt.xticks([])

        for k,(lowf, highf) in enumerate(tf_freqs):
            plt.subplot(len(tf_freqs)+1, 1, k+2)
            plt.plot(t, tf_raw[k, i1:i2], 'b-', linewidth=2.0)
            plt.title('%d - %d Hz' % (lowf, highf))
            plt.axis('tight')
            if k < len(tf_freqs)-1:
                plt.xticks([])

        tf_ps = np.abs(tf)
        tf_phase = np.angle(tf)

        plt.figure()
        plt.subplot(2, 1, 1)
        plt.imshow(20*np.log10(tf_ps), interpolation='nearest', aspect='auto', origin='lower', extent=(t.min(), t.max(), tf_freqs.min(), tf_freqs.max()))
        plt.title('Amplitude')
        plt.axis('tight')
        plt.colorbar()
        plt.ylabel('Frequency (Hz)')

        plt.subplot(2, 1, 2)
        plt.imshow(tf_phase, interpolation='nearest', aspect='auto', origin='lower', extent=(t.min(), t.max(), tf_freqs.min(), tf_freqs.max()))
        plt.title('Phase')
        plt.axis('tight')
        plt.colorbar()
        plt.ylabel('Frequency (Hz)')

        plt.show()
    """