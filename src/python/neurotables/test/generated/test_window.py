# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'test_window.ui'
#
# Created: Sat Sep  7 12:51:48 2013
#      by: PyQt4 UI code generator 4.9.3
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

class Ui_TestWindow(object):
    def setupUi(self, TestWindow):
        TestWindow.setObjectName(_fromUtf8("TestWindow"))
        TestWindow.resize(800, 600)
        self.centralwidget = QtGui.QWidget(TestWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        TestWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtGui.QMenuBar(TestWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 800, 25))
        self.menubar.setObjectName(_fromUtf8("menubar"))
        TestWindow.setMenuBar(self.menubar)
        self.statusbar = QtGui.QStatusBar(TestWindow)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        TestWindow.setStatusBar(self.statusbar)

        self.retranslateUi(TestWindow)
        QtCore.QMetaObject.connectSlotsByName(TestWindow)

    def retranslateUi(self, TestWindow):
        TestWindow.setWindowTitle(QtGui.QApplication.translate("TestWindow", "MainWindow", None, QtGui.QApplication.UnicodeUTF8))

