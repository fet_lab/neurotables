import numpy as np
import unittest

import tables
from neurotables.objects.cells import Site
from neurotables.objects.site_filter import SiteFilter


class TestSiteFilter(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_basic(self):

        f = tables.open_file('/auto/k8/fdata/mschachter/tdt_continuous/GreBlu9508M_tabled.h5', 'a')

        site = Site(f)
        assert site.from_id(1)

        protocol = site.protocols[0]

        sf = SiteFilter(site, protocol)

        #do a simple search
        crit = {'electrodes':'2,3-5,11', 'hemisphere':['L'], 'sort':['single', 'multi']}
        good_electrodes = [2, 3, 4, 5, 11]

        found_objects = sf.find(criteria=crit)

        assert len(found_objects) == 2
        assert 'LFP' in found_objects
        assert 'Cell' in found_objects

        lfps = found_objects['LFP']
        assert len(lfps) == len(good_electrodes)
        assert np.sum([lfp.lfp.electrode.hemisphere != 'L' for lfp in lfps]) == 0
        assert np.sum([lfp.lfp.electrode.number not in good_electrodes for lfp in lfps]) == 0

        cells = found_objects['Cell']
        assert len(cells) >= len(lfps)
        assert np.sum([not c.cell.sort_type not in ['noise', 'unsorted'] for c in cells]) == 0

        #do another simple search
        crit = {'electrodes':'ALL', 'hemisphere':['L', 'R'], 'sort':['single', 'noise', 'unsorted']}
        good_electrodes = np.arange(32)+1

        found_objects = sf.find(criteria=crit)

        assert len(found_objects) == 2
        assert 'LFP' in found_objects
        assert 'Cell' in found_objects

        lfps = found_objects['LFP']
        assert len(lfps) == len(good_electrodes)
        assert np.sum([lfp.lfp.electrode.number not in good_electrodes for lfp in lfps]) == 0

        cells = found_objects['Cell']
        assert len(cells) >= len(lfps)
        assert np.sum([c.cell.sort_type == 'multi' for c in cells]) == 0
        assert np.sum([c.cell.electrode.number not in good_electrodes for c in cells]) == 0

        f.close()
