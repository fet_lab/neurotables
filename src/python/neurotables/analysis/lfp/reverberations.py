from lasp.signal import bandpass_filter
from neurotables.objects.cells import *
from neurotables.analysis.lfp.objects import *

def low_power_during_silence(f, electrode_number, site_id=1, protocol_name='Call1', low_freq=3, high_freq=20, stim_type='song', ax=None):

    #load up site
    site = Site(f)
    assert site.from_id(site_id)

    #load up protocol
    pcalls = [p for p in site.protocols if p.name == protocol_name]
    assert len(pcalls) == 1
    protocol = pcalls[0]

    #get electrode
    elist = [e for e in site.electrodes if e.number == electrode_number]
    assert len(elist) == 1
    electrode = elist[0]

    #get the LFP
    lfp_pr = electrode.get_lfp_protocol_response(protocol.name)
    lfp = lfp_pr.waveform
    sample_rate = lfp_pr.lfp.sample_rate
    lfp_end_time = len(lfp) / sample_rate

    #zscore and low-pass filter the LFP
    lfp -= lfp.mean()
    lfp /= lfp.std(ddof=1)
    lfp = bandpass_filter(lfp, sample_rate, low_freq, high_freq)

    pre_next_stim_time = 0.025

    #get stim indices and silence periods that follow them
    stim_times = list()
    silence_times = list()
    all_events = protocol.events
    for k,event in enumerate(all_events):
        if event.sound.stim_type == stim_type:
            stim_times.append( (event.start_time, event.end_time))
            if k < len(all_events)-1:
                next_event = all_events[k+1]
                silence_times.append( (event.end_time, next_event.start_time-pre_next_stim_time) )
            else:
                silence_times.append( (event.end_time, lfp_end_time))

    #get RMS power during and after stimuli
    rms_power = list()
    for (stim_start,stim_end),(silence_start,silence_end) in zip(stim_times, silence_times):
        stim_si = int(stim_start*sample_rate)
        stim_ei = int(stim_end*sample_rate)
        silence_ei = int(silence_end*sample_rate)

        stim_lfp = lfp[stim_si:stim_ei]
        silence_lfp = lfp[stim_ei:silence_ei]

        rms_power.append( (stim_lfp.std(ddof=1), silence_lfp.std(ddof=1)) )

    rms_power = np.array(rms_power)

    if ax is None:
        plt.figure()
        ax = plt.subplot(111)
    plt.sca(ax)
    plt.hist(rms_power[:, 0], bins=30, color='r')
    plt.hist(rms_power[:, 1], bins=30, color='b', alpha=0.60)
    plt.legend(['Stim', 'Silence'])
    plt.xlabel('RMS Power')
    plt.axis('tight')


def plot_all_stim_vs_silence_rms(f, site_id=1, protocol_name='Call1', stim_type='song'):

    #load up site
    site = Site(f)
    assert site.from_id(site_id)

    #load up protocol
    pcalls = [p for p in site.protocols if p.name == protocol_name]
    assert len(pcalls) == 1
    protocol = pcalls[0]

    #get electrode geometry and coordinates
    geom = site.electrodes[0].geometry
    coords = geom.coordinates

    nrows = 8
    ncols = 4

    fig = plt.figure()
    plt.subplots_adjust(bottom=0.05, top=0.95, right=0.95, left=0.05, wspace=0.25, hspace=0.25)

    for e in site.electrodes:

        row,col = np.nonzero(coords == e.number)
        sp = ncols*row + col + 1
        ax= fig.add_subplot(nrows, ncols, sp)
        low_power_during_silence(f, e.number, site_id=site_id, protocol_name=protocol_name, stim_type=stim_type, ax=ax)

    plt.suptitle('Site %d, %s, %s' % (site_id, protocol_name, stim_type))


if __name__ == '__main__':

    f = tables.open_file('/auto/k8/fdata/mschachter/tdt_continuous/GreBlu9508M/GreBlu9508M_tabled.h5', 'r')

    #plot_all_stim_vs_silence_rms(f, site_id=1, protocol_name='Call1', stim_type='song')
    #plot_all_stim_vs_silence_rms(f, site_id=1, protocol_name='Call1', stim_type='call')

    plot_all_stim_vs_silence_rms(f, site_id=2, protocol_name='Call2', stim_type='song')
    plot_all_stim_vs_silence_rms(f, site_id=2, protocol_name='Call2', stim_type='call')

    plot_all_stim_vs_silence_rms(f, site_id=3, protocol_name='Call3', stim_type='song')
    plot_all_stim_vs_silence_rms(f, site_id=3, protocol_name='Call3', stim_type='call')

    plot_all_stim_vs_silence_rms(f, site_id=4, protocol_name='Call1', stim_type='song')
    plot_all_stim_vs_silence_rms(f, site_id=4, protocol_name='Call1', stim_type='call')

    plot_all_stim_vs_silence_rms(f, site_id=5, protocol_name='Call2', stim_type='song')
    plot_all_stim_vs_silence_rms(f, site_id=5, protocol_name='Call2', stim_type='call')

    plt.show()

    f.close()
