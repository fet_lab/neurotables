import os
import re
import h5py
from matplotlib import rcParams
from matplotlib.gridspec import GridSpec
import matplotlib.cm as cm

import numpy as np
import operator
import tables

import matplotlib.pyplot as plt

from lasp.incrowd import fast_conv_grouped
from lasp.sound import plot_spectrogram

from neurotables.analysis.lfp.fit_trial_envelope_models import get_decoder_data,get_labeled_stimuli, \
    get_stim_representations,get_envelope_data_matrix,get_lfps_for_decoder
from neurotables.analysis.lfp.fit_trial_spec_models import get_spectrogram_data_matrix
from neurotables.analysis.lfp.stim_cond import find_common_stims
from neurotables.objects.cells import Sound, Site


class AggregateElectrodeData(object):

    def __init__(self, f, stim_type='song', load_data=False):
        self.f = f
        self.property_map = dict()

        sounds = find_common_stims(self.f)
        self.stim_ids = [s.id for s in sounds if s.stim_type == stim_type]

        #load up sites
        self.sites = dict()
        s = Site(f)
        self.sites = {site.id:site for site in s.get_all()}

        #get electrodes
        self.electrodes = self.sites[1].electrodes

        #get geometry
        site1 = self.sites[self.sites.keys()[0]]
        self.electrode_geometry = site1.electrodes[0].geometry
        self.electrode_coords = self.electrode_geometry.coordinates

        if not load_data:
            return

        #load up the LFPs
        post_stim_time = 0.050
        self.stim_cond_lfps_by_site_and_protocol = dict()
        self.sample_rate = None
        for site in self.sites.values():
            for protocol in site.protocols:
                electrode_nums = [e.number for e in self.electrodes]
                stim_cond_lfps,self.sample_rate = get_lfps_for_decoder(f, electrode_nums, self.stim_ids, site_id=site.id,
                                                                       protocol_name=protocol.name, filter=None, cutoff_freq=None,
                                                                       post_stim_time=post_stim_time)
                key = (site.id, protocol.name)
                self.stim_cond_lfps_by_site_and_protocol[key] = stim_cond_lfps

        #load up the sound amplitude envelopes
        #pick a random site and protocol
        s = self.sites[1]
        p = s.protocols[0]
        stim_envelopes,sample_rate = get_stim_representations(f, site_id=s.id, protocol_name=p.name,
                                                              stim_type=stim_type, stim_ids=self.stim_ids,
                                                              gain_tau=0.0, type='envelope')
        self.stim_envelopes = stim_envelopes

        #load up the spectrograms
        stim_specs,spec_t,spec_freq,sample_rate = get_stim_representations(f, site_id=s.id, protocol_name=p.name,
                                                                           stim_type=stim_type, type='spectrogram')
        self.stim_specs = stim_specs
        self.spec_freq = spec_freq

        #make sure envelopes and spectrograms have the same length as the LFPs they're associated with
        lfps_by_stim = self.stim_cond_lfps_by_site_and_protocol[(s.id, p.name)] #a random stimulus conditioned LFP tensor
        self.lfp_lens = dict()
        for stim_id in self.stim_ids:
            nt = lfps_by_stim[stim_id].shape[-1]
            self.lfp_lens[stim_id] = nt

            #pad envelope
            env = self.stim_envelopes[stim_id]
            new_env = np.zeros([nt])
            env_len = min(len(env), nt)
            new_env[:env_len] = env[:env_len]
            self.stim_envelopes[stim_id] = new_env

            #pad spectrogram
            spec = self.stim_specs[stim_id]
            new_spec = np.zeros([spec.shape[0], nt])
            spec_len = min(spec.shape[1], nt)
            new_spec[:, :spec_len] = spec[:, :spec_len]
            self.stim_specs[stim_id] = new_spec

    def add_encoder(self, bird_name, site_id, protocol, electrode,
                    env_dir='/auto/k8/fdata/mschachter/analysis/envelope_models',
                    spec_dir='/auto/k8/fdata/mschachter/analysis/spec_models',
                    stim_type='song'):

        key = (bird_name, site_id, protocol, electrode)

        if key not in self.property_map:
            self.property_map[key] = dict()

        env_file = os.path.join(env_dir, 'encoder_decoder_Site%d_%s_e%d.h5' % (site_id, protocol, electrode))
        print 'Reading %s...' % env_file

        #read properties from the envelope encoders/decoders file
        hf = h5py.File(env_file, 'r')
        grp = hf['none']

        self.property_map[key]['env_encoder_cc'] = np.array(grp['encoder']['ccs']).mean()
        self.property_map[key]['env_decoder_cc'] = np.array(grp['decoder']['ccs']).mean()

        self.property_map[key]['env_encoder_lags'] = np.array(grp['encoder']['lags'])

        self.property_map[key]['env_encoder_bias'] = np.array(grp['encoder']['biases']).mean(axis=0)
        self.property_map[key]['env_encoder_filter'] = np.array(grp['encoder']['filters']).mean(axis=0)
        self.property_map[key]['env_encoder_filter_std'] = np.array(grp['encoder']['filters']).std(axis=0, ddof=1)

        self.property_map[key]['env_decoder_bias'] = np.array(grp['decoder']['biases']).mean(axis=0)
        self.property_map[key]['env_decoder_filter'] = np.array(grp['decoder']['filters']).mean(axis=0)
        self.property_map[key]['env_decoder_filter_std'] = np.array(grp['decoder']['filters']).std(axis=0, ddof=1)

        hf.close()

        #read properties from the spectrogram encoder file
        spec_file = os.path.join(spec_dir, 'encoder_spec_Site%d_%s_e%d.h5' % (site_id, protocol, electrode))
        print 'Reading %s...' % spec_file

        hf = h5py.File(spec_file, 'r')

        self.property_map[key]['spec_encoder_cc'] = np.array(hf['ccs']).mean()
        self.property_map[key]['spec_encoder_lags'] = np.array(hf['lags'])
        self.property_map[key]['spec_encoder_bias'] = np.array(hf['biases']).mean()
        self.property_map[key]['spec_encoder_filter'] = np.array(hf['filters']).mean(axis=0)
        self.property_map[key]['spec_encoder_filter_std'] = np.array(hf['filters']).std(axis=0, ddof=1)

        hf.close()

        #get LFPs for this electrode
        lfps = self.stim_cond_lfps_by_site_and_protocol[(site_id, protocol)]
        enumber2index = {e.number:k for k,e in enumerate(self.electrodes)}
        eindex = enumber2index[electrode]
        stim_cond_lfps = {stim_id:lfps[stim_id][:, eindex, :].squeeze() for stim_id in self.stim_ids}

        #assemble a matrix of envelope stimuli and responses
        Xenv,y,group_index,group_identity = get_envelope_data_matrix(self.stim_envelopes, stim_cond_lfps,
                                                                     group_identities_to_use=None, encoder=True)

        #compute prediction for each stimulus/trial combination
        env_filter = self.property_map[key]['env_encoder_filter']
        env_filter = env_filter.reshape([1, len(env_filter)])
        env_time_lags = self.property_map[key]['env_encoder_lags']
        env_encoder_bias = self.property_map[key]['env_encoder_bias']
        #print 'Xenv.shape=',Xenv.shape
        #print 'y.shape=',y.shape
        #print 'env_filter.shape=',env_filter.shape
        #print 'group_index.shape=',group_index.shape
        #print 'env_time_lags.shape=',env_time_lags.shape
        env_resp = fast_conv_grouped(Xenv, env_filter, env_time_lags, group_index, bias=env_encoder_bias)

        #delete the envelopes
        del Xenv

        #assemble a matrix of spectrogram stimuli and responses
        unique_groups = np.unique(group_index)
        gident = [group_identity[g] for g in unique_groups]
        Xspec,y,group_index,group_identity = get_spectrogram_data_matrix(self.stim_specs, self.spec_freq, stim_cond_lfps,
                                                                         group_identities_to_use=gident,
                                                                         pad_length=0)
        #print 'Xspec.shape=',Xspec.shape
        #print 'y.shape=',y.shape

        #delete the LFP dictionary
        del stim_cond_lfps

        #compute prediction for each stimulus/trial combination
        spec_filter = self.property_map[key]['spec_encoder_filter']
        spec_time_lags = self.property_map[key]['spec_encoder_lags']
        spec_encoder_bias = self.property_map[key]['spec_encoder_bias']
        #print 'spec_filter.shape=',spec_filter.shape
        #print 'spec_time_lags.shape=',spec_time_lags.shape
        spec_resp = fast_conv_grouped(Xspec, spec_filter, spec_time_lags, group_index, bias=spec_encoder_bias)

        #delete the spectrograms
        del Xspec

        #compute the performance for each stimulus/trial pair
        groups = np.unique(group_index)
        performance_matrix = np.zeros([len(groups), 4])
        for k,group in enumerate(groups):
            stim_id,trial = group_identity[group]
            performance_matrix[k, 0] = stim_id
            performance_matrix[k, 1] = trial

            index = group_index == group
            C = np.corrcoef(y[index], env_resp[index])
            performance_matrix[k, 2] = C[0, 1]

            C = np.corrcoef(y[index], spec_resp[index])
            performance_matrix[k, 3] = C[0, 1]

        #compute the overall performance across stimulus/trial pairs
        self.property_map[key]['env_cc_overall'] = performance_matrix[:, 2].mean()
        self.property_map[key]['env_cc_overall_std'] = performance_matrix[:, 2].std(ddof=1)

        self.property_map[key]['spec_cc_overall'] = performance_matrix[:, 3].mean()
        self.property_map[key]['spec_cc_overall_std'] = performance_matrix[:, 3].std(ddof=1)

        #compute the performance per stimulus
        unique_stim_ids = np.unique(performance_matrix[:, 0])
        for stim_id in unique_stim_ids:
            index = performance_matrix[:, 0] == stim_id
            self.property_map[key]['env_cc_stim%d' % stim_id] = performance_matrix[index, 2].mean()
            self.property_map[key]['env_cc_stim%d_std' % stim_id] = performance_matrix[index, 2].std(ddof=1)
            self.property_map[key]['spec_cc_stim%d' % stim_id] = performance_matrix[index, 3].mean()
            self.property_map[key]['spec_cc_stim%d_std' % stim_id] = performance_matrix[index, 3].std(ddof=1)

        #compute cc between envelope and spectrogram prediction
        C = np.corrcoef(env_resp, spec_resp)
        self.property_map[key]['env_vs_spec_cc'] = C[0, 1]

        #get anatomical region
        reg = '?'
        site = Site(self.f)
        assert site.from_id(site_id)
        elist = [e for e in site.electrodes if e.number == electrode]
        assert len(elist) == 1

        electrode_obj = elist[0]

        if electrode_obj.anatomy.__class__.__name__ == 'ElectrodeAnatomicalLocation':
            reg = electrode_obj.anatomy.region

        self.property_map[key]['region'] = reg

    def to_file(self, output_file):

        hf = h5py.File(output_file, 'w')
        for key,value_dict in self.property_map.iteritems():
            hfkey = ','.join([str(x) for x in key])
            grp = hf.create_group(hfkey)
            grp.attrs['bird'] = key[0]
            grp.attrs['site'] = key[1]
            grp.attrs['protocol'] = key[2]
            grp.attrs['electrode'] = key[3]

            for prop_name,prop_val in value_dict.iteritems():
                grp[prop_name] = prop_val

        hf.close()

    @classmethod
    def from_file(cls, f, fname, load_data=False):

        agg = AggregateElectrodeData(f, load_data=load_data)

        hf = h5py.File(fname, 'r')
        for key in hf.keys():
            bird = hf[key].attrs['bird']
            site = hf[key].attrs['site']
            protocol = hf[key].attrs['protocol']
            electrode = hf[key].attrs['electrode']

            pkey = (bird, site, protocol, electrode)
            agg.property_map[pkey] = dict()

            for pname in hf[key].keys():
                pval = np.array(hf[key][pname])
                if str(pval.dtype)[1] == 'S':
                    pval = str(pval)
                elif len(pval.shape) == 1 and pval.shape[0] == 1:
                    pval = pval[0]
                agg.property_map[pkey][pname] = pval

        hf.close()

        return agg

    def plot(self):
        pass

    def plot_multi_site(self, bird_name, protocol_regex='Call', prop_name='spec_encoder_cc'):

        nrows = 8
        ncols = 4

        #first, generate a matrix of property values for each site
        prop_mats = dict()
        text_mats = dict()
        site_ids = self.sites.keys()
        site_ids.sort()

        for site_id in site_ids:
            site = self.sites[site_id]
            plist = [p for p in site.protocols if re.match(protocol_regex, p.name)]
            assert len(plist) > 0
            protocol = plist[0]

            #make a matrix for this site
            prop_mat = np.zeros([nrows, ncols])
            text_mat = np.zeros([nrows, ncols], dtype='S40')
            enumbers = [e.number for e in site.electrodes]
            for enumber in enumbers:
                #get coordinates
                row,col = np.nonzero(self.electrode_coords == enumber)
                key = (bird_name, site.id, protocol.name, enumber)
                val = self.property_map[key][prop_name]
                prop_mat[row, col] = val
                reg = self.property_map[key]['region']
                text_mat[row, col] = '%d\n%s\n%0.2f' % (enumber, reg, val)

            text_mats[site.id] = text_mat
            prop_mats[site.id] = prop_mat

        #plot the site matrices
        all_sites_fig = plt.figure(figsize=(28, 16))
        plt.subplots_adjust(bottom=0.04, top=0.97, right=0.99, left=0.04, wspace=0.10)

        gsres = 2
        ncols = len(self.sites)*gsres
        gs = GridSpec(1, ncols)
        rcParams.update({'font.size': 18})

        for k,site_id in enumerate(site_ids):
            site = self.sites[site_id]
            #ax = plt.subplot(1, len(site_ids), k+1)
            i1 = k*gsres
            i2  = i1+gsres
            ax = plt.subplot(gs[0, i1:i2])
            im = plt.imshow(prop_mats[site.id], interpolation='nearest', aspect='auto', vmin=0, vmax=1, extent=[0, 4, 0, 8], origin='upper')
            for row in range(8):
                for col in range(4):
                    txt = text_mats[site.id][row, col]
                    plt.text(col+0.50, (7-row)+0.25, txt, horizontalalignment='center')
            plt.axvline(2.0, color='k')
            plt.xticks([])
            plt.yticks([])
            #plt.colorbar()
            plt.title('Site %d' % site.id)

        #method that's executed when an electrode is clicked on
        def onclick(event):
            rcParams.update({'font.size': 10})
            width_px = all_sites_fig.get_figwidth()*all_sites_fig.get_dpi()
            site_index = int((event.x / width_px)*4.0)

            #get site and protocol
            site_id = site_ids[site_index]
            site = self.sites[site_id]
            plist = [p for p in site.protocols if re.match(protocol_regex, p.name)]
            assert len(plist) > 0
            protocol = plist[0]

            row = int(np.floor(event.ydata))
            col = int(np.floor(event.xdata))
            enumber = self.electrode_coords[7-row, col]

            print 'clicked site %d, electrode %d, row,col=(%d,%d)' % (site_id, enumber, row, col)

            #get predictions organized by stimulus
            key = (bird_name, int(site_id), protocol.name, enumber)
            stim_ids, lfps_by_stim,preds_by_stim,perfs_by_stim, specs_by_stim = self.get_preds_by_stim(key)

            plt.figure()
            plt.suptitle('%s, Site %d, %s, Electrode %d' % (bird_name, site_id, protocol.name, enumber))
            plt.subplots_adjust(bottom=0.04, top=0.97, right=0.99, left=0.04, wspace=0.10)

            trial_indices = [0, 2, 5, 8]

            ncols = len(stim_ids)
            nrows = len(trial_indices) + 1

            for k,stim_id in enumerate(stim_ids):

                #plot spectrogram
                spec = specs_by_stim[stim_id]
                spec_t = np.arange(spec.shape[0])
                spec_freq = np.arange(spec.shape[1])
                ax = plt.subplot(nrows, ncols, k+1)
                plot_spectrogram(spec_t, spec_freq, spec, ax=ax, ticks=False, colormap=cm.gist_yarg, colorbar=False)

                #get max and min LFP values across trials
                lfp_min = np.inf
                lfp_max = -np.inf
                for j,trial in enumerate(trial_indices):
                    lfp = lfps_by_stim[stim_id][trial]
                    lfp_min = min(lfp_min, lfp.min())
                    lfp_max = max(lfp_max, lfp.max())

                #plot trials
                for j,trial in enumerate(trial_indices):
                    lfp = lfps_by_stim[stim_id][trial]
                    env_pred = preds_by_stim[stim_id][trial][0]
                    spec_pred = preds_by_stim[stim_id][trial][1]
                    t = np.arange(len(lfp)) / self.sample_rate
                    env_cc = perfs_by_stim[stim_id][trial][0]
                    spec_cc = perfs_by_stim[stim_id][trial][1]

                    sp = (j+1)*ncols + k + 1
                    ax = plt.subplot(nrows, ncols, sp)
                    plt.plot(t, lfp, 'k-', linewidth=3.0)
                    plt.plot(t, env_pred, 'b-', linewidth=2.0, alpha=0.75)
                    plt.plot(t, spec_pred, 'r-', linewidth=2.0, alpha=0.65)
                    plt.xticks([])
                    plt.yticks([])
                    #plt.legend(['LFP', 'ENV', 'SPEC'])
                    plt.title('env_cc=%0.2f, spec_cc=%0.2f' % (env_cc, spec_cc))
                    plt.axis('tight')
                    plt.ylim(lfp_min, lfp_max)

            plt.show()

        cid = all_sites_fig.canvas.mpl_connect('button_press_event', onclick)

    def get_preds_by_stim(self, key):
        """
            Get spectrogram encoder performance by stimulus, get the stimulus with the highest performance,
            lowest performance, and medium performance.
        """

        bird_name,site_id,protocol,electrode = key
        print key
        print self.property_map.keys()

        stim_perfs = list()
        for stim_id in self.stim_ids:
            spec_cc = self.property_map[key]['spec_encoder_cc']
            stim_perfs.append( (stim_id, spec_cc ))

        stim_perfs.sort(key=operator.itemgetter(1))
        nstims = len(self.stim_ids)

        stim_ids_to_use = [stim_perfs[0][0], stim_perfs[nstims / 2][0], stim_perfs[-1][0]]

        #get LFPs for this electrode
        lfps = self.stim_cond_lfps_by_site_and_protocol[(site_id, protocol)]
        enumber2index = {e.number:k for k,e in enumerate(self.electrodes)}
        eindex = enumber2index[electrode]
        lens_to_use = dict()
        for stim_id in stim_ids_to_use:
            if lfps[stim_id].shape[-1] != self.lfp_lens[stim_id]:
                nt = self.lfp_lens[stim_id]
                min_nt = min(nt, lfps[stim_id].shape[-1])
                new_lfps = np.zeros([lfps[stim_id].shape[0], lfps[stim_id].shape[1], nt])
                new_lfps[:, :, :min_nt] = lfps[stim_id][:, :, :min_nt]
                lfps[stim_id] = new_lfps

            print 'stim_id=%d, lfp_len=%d, lfp_len_real=%d, env_len=%d, spec_len=%d' % \
                  (stim_id, self.lfp_lens[stim_id], lfps[stim_id].shape[-1], len(self.stim_envelopes[stim_id]), self.stim_specs[stim_id].shape[1])

            lens_to_use[stim_id] = np.min([self.lfp_lens[stim_id], len(self.stim_envelopes[stim_id]), self.stim_specs[stim_id].shape[1]])
        stim_cond_lfps = {stim_id:lfps[stim_id][:, eindex, :].squeeze() for stim_id in stim_ids_to_use}
        stim_envs = {stim_id:self.stim_envelopes[stim_id][:] for stim_id in stim_ids_to_use}
        stim_specs = {stim_id:self.stim_specs[stim_id][:, :] for stim_id in stim_ids_to_use}

        #assemble a matrix of envelope stimuli and responses
        Xenv,y,group_index,group_identity = get_envelope_data_matrix(stim_envs, stim_cond_lfps,
                                                                     group_identities_to_use=None, encoder=True)

        #compute prediction for each stimulus/trial combination
        env_filter = self.property_map[key]['env_encoder_filter']
        env_filter = env_filter.reshape([1, len(env_filter)])
        env_time_lags = self.property_map[key]['env_encoder_lags']
        env_encoder_bias = self.property_map[key]['env_encoder_bias']
        env_resp = fast_conv_grouped(Xenv, env_filter, env_time_lags, group_index, bias=env_encoder_bias)

        #delete the envelopes
        del Xenv

        #assemble a matrix of spectrogram stimuli and responses
        unique_groups = np.unique(group_index)
        gident = [group_identity[g] for g in unique_groups]
        Xspec,y,group_index,group_identity = get_spectrogram_data_matrix(stim_specs, self.spec_freq, stim_cond_lfps,
                                                                         group_identities_to_use=gident,
                                                                         pad_length=0)

        #delete the LFP dictionary
        del stim_cond_lfps

        #compute prediction for each stimulus/trial combination
        spec_filter = self.property_map[key]['spec_encoder_filter']
        spec_time_lags = self.property_map[key]['spec_encoder_lags']
        spec_encoder_bias = self.property_map[key]['spec_encoder_bias']
        spec_resp = fast_conv_grouped(Xspec, spec_filter, spec_time_lags, group_index, bias=spec_encoder_bias)

        #compute the performance for each stimulus/trial pair
        groups = np.unique(group_index)

        specs_by_stim = dict()
        preds_by_stim = dict()
        lfps_by_stim = dict()
        perfs_by_stim = dict()
        for k,group in enumerate(groups):

            stim_id,trial = group_identity[group]

            index = group_index == group
            if stim_id not in specs_by_stim:
                specs_by_stim[stim_id] = Xspec[index, :].T

            if stim_id not in preds_by_stim:
                preds_by_stim[stim_id] = list()
            preds_by_stim[stim_id].append((env_resp[index], spec_resp[index]))

            if stim_id not in lfps_by_stim:
                lfps_by_stim[stim_id] = list()
            lfps_by_stim[stim_id].append(y[index])

            Cenv = np.corrcoef(y[index], env_resp[index])
            Cspec = np.corrcoef(y[index], spec_resp[index])

            if stim_id not in perfs_by_stim:
                perfs_by_stim[stim_id] = list()
            perfs_by_stim[stim_id].append( (Cenv[0, 1], Cspec[0, 1]) )

        return stim_ids_to_use, lfps_by_stim, preds_by_stim, perfs_by_stim, specs_by_stim

    def plot_site(self, bird_name, site_id, protocol_name):

        fig_names = ['filters', 'strfs', 'per stim']

        figs = dict()
        for figname in fig_names:
            fig = plt.figure()
            plt.suptitle('%s: Site %d, %s' % (figname.capitalize(), site_id, protocol_name))
            plt.subplots_adjust(bottom=0.05, top=0.95, right=0.95, left=0.05, wspace=0.25, hspace=0.25)
            figs[figname] = fig

        nrows = 8
        ncols = 4

        site = self.sites[site_id]
        enumbers = [e.number for e in site.electrodes]
        for enumber in enumbers:
            #get coordinates
            row,col = np.nonzero(self.electrode_coords == enumber)
            sp = ncols*row + col + 1
            key = (bird_name, site_id, protocol_name, enumber)

            #get filter properties
            lags = self.property_map[key]['env_encoder_lags']
            env_filter = self.property_map[key]['env_encoder_filter']
            env_cc = self.property_map[key]['env_encoder_cc']

            #create subplot for filter
            fig = figs['filters']
            ax = fig.add_subplot(nrows, ncols, sp)
            plt.sca(ax)
            plt.plot(lags, env_filter, 'k-', linewidth=2.0)
            plt.xticks([])
            plt.yticks([])
            plt.title('E%d: %0.2f' % (enumber, env_cc))
            plt.axis('tight')

            #get STRF properties
            spec_cc = self.property_map[key]['spec_encoder_cc']
            spec_lags = self.property_map[key]['spec_encoder_lags']
            spec_strf = self.property_map[key]['spec_encoder_filter']
            spec_freq = np.arange(spec_strf.shape[0])
            strf_absmax = np.abs(spec_strf).max()

            #create subplot for STRF
            fig = figs['strfs']
            ax = fig.add_subplot(nrows, ncols, sp)
            plt.sca(ax)
            plot_spectrogram(spec_lags, spec_freq, spec_strf, ax=ax, ticks=False, colorbar=False,
                             vmin=-strf_absmax, vmax=strf_absmax)
            plt.title('E%d: %0.2f' % (enumber, spec_cc))
            plt.axis('tight')

            #get per-stim performance properties
            stim_perfs = np.zeros([len(self.stim_ids), 2])
            stim_stds = np.zeros([len(self.stim_ids), 2])
            for k,stim_id in enumerate(self.stim_ids):
                stim_perfs[k, 0] = self.property_map[key]['env_cc_stim%d' % stim_id]
                stim_perfs[k, 1] = self.property_map[key]['spec_cc_stim%d' % stim_id]
                stim_stds[k, 0] = self.property_map[key]['env_cc_stim%d_std' % stim_id]
                stim_stds[k, 1] = self.property_map[key]['spec_cc_stim%d_std' % stim_id]

            #plot per-stim bar plots
            fig = figs['per stim']
            ax = fig.add_subplot(nrows, ncols, sp)
            plt.sca(ax)

            ind = np.arange(len(self.stim_ids))
            w = 0.35
            #env_rects = ax.bar(ind, stim_perfs[:, 0], w, color='b', yerr=stim_stds[:, 0])
            spec_rects = ax.bar(ind+w, stim_perfs[:, 1], w, color='y', yerr=stim_stds[:, 1])
            #ax.legend(['env', 'spec'])
            plt.xticks([])
            plt.axis('tight')
            plt.ylim(0.0, 1.0)


def create_aggregator(f, envelope_dir='/auto/k8/fdata/mschachter/analysis/envelope_models',
                      spec_dir='/auto/k8/fdata/mschachter/analysis/spec_models',
                      stim_type='song'):

    agg = AggregateElectrodeData(f)

    s = Site(f)
    for site in s.get_all():
        bird_name = site.experiment.subject.name
        for protocol in site.protocols:
            for electrode in site.electrodes:
                agg.add_encoder(bird_name, site.id, protocol.name, electrode.number,
                                env_dir=envelope_dir, spec_dir=spec_dir, stim_type=stim_type)

    return agg


if __name__ == '__main__':

    f = tables.open_file('/auto/k8/fdata/mschachter/tdt_continuous/GreBlu9508M/GreBlu9508M_tabled.h5', 'r')


    odir = '/auto/k8/fdata/mschachter/analysis/envelope_models/'
    #agg = create_aggregator(f, load_data=True)
    #agg.to_file(os.path.join('GreBlu9508M_electrode_data.h5'))

    bird_name = 'GreBlu9508'

    agg = AggregateElectrodeData.from_file(f, os.path.join(odir, 'GreBlu9508M_electrode_data.h5'), load_data=True)
    #agg.plot_site(bird_name, 1, 'Call1')
    #agg.plot_site(bird_name, 2, 'Call2')
    agg.plot_site(bird_name, 3, 'Call3')
    #agg.plot_site(bird_name, 4, 'Call1')
    #agg.plot_site(bird_name, 5, 'Call2')

    #agg.plot_multi_site(bird_name, protocol_regex='Call', prop_name='spec_encoder_cc')

    plt.show()
