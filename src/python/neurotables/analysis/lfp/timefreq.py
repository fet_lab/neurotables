import numpy as np

import matplotlib.pyplot as plt
from sklearn.decomposition import PCA,FastICA
import time
from lasp.timefreq import compute_mean_spectrogram, postprocess_spectrogram
from neurotables.analysis.lfp.figures import plot_electrodes_2figs

from neurotables.objects.cells import Site
from neurotables.analysis.lfp.objects import *

def compute_spectrum_stats(f, site_id, protocol_name):
    """
        Compute the mean and covariance matrix for each LFP's spectrogram.
    """

    s = Site(f)
    assert s.from_id(site_id)

    spectrum_means = dict()
    spectrum_covs = dict()
    spectrum_pcs = dict()
    spectrum_ics = dict()

    spec_f = None
    geom = None
    coords = None
    for electrode in s.electrodes:
        lfp_pr = electrode.get_lfp_protocol_response(protocol_name)

        geom = electrode.geometry
        coords = geom.coordinates

        #compute mean spectrum
        print 'Electrode %d' % electrode.number
        spec = lfp_pr.spectrogram
        spec_f = spec.freq
        spectrum_means[electrode.number] = spec.tf.mean(axis=1)

        #compute spectrum covariance matrix
        C = np.corrcoef(spec.tf)
        spectrum_covs[electrode.number] = C

        #do PCA on spectrum
        pca = PCA(n_components=0.95)
        pca.fit(spec.tf.transpose())
        print 'Explained variance:',pca.explained_variance_ratio_
        print '# of freq components: %d' % len(spec_f)

        print 'pca.components_.shape=',pca.components_.shape
        spectrum_pcs[electrode.number] = pca.components_

        #do ICA on spectrum
        ica = FastICA(n_components=5, whiten=True)
        ica.fit(spec.tf.transpose())
        spectrum_ics[electrode.number] = ica.components_
        print 'ica.components_.shape=',ica.components_.shape

    #make plots
    egroups = [range(1, 17), range(17, 33)]
    edesc = ['Left', 'Right']
    for i,egrp in enumerate(egroups):

        mean_fig = plt.figure()
        mean_fig.suptitle(edesc[i])
        cov_fig = plt.figure()
        cov_fig.suptitle(edesc[i])
        pca_fig = plt.figure()
        pca_fig.suptitle(edesc[i])
        ica_fig = plt.figure()
        ica_fig.suptitle(edesc[i])
        nrows = 8
        ncols = 2

        for k,enumber in enumerate(egrp):

            C = spectrum_covs[enumber]
            std = np.diag(C)
            for i in range(C.shape[0]):
                C[i, i] = 0.0

            #get the grid coordinates for this LFP
            row,col = np.nonzero(coords == enumber)

            print 'enumber=%d, row=%d, col=%d' % (enumber, row, col)
            sp = row*2 + (col % 2) + 1

            #plot mean and standard deviation of frequencies across time
            ax = mean_fig.add_subplot(nrows, ncols, sp)
            plt.sca(ax)
            si = enumber - 1
            y = spectrum_means[enumber]
            yerr = std
            plt.errorbar(spec_f, y, yerr=yerr, ecolor='r', linewidth=2)
            if row < (nrows - 2):
                plt.xticks([])
            plt.ylabel('E%d' % enumber)
            plt.axis('tight')

            #plot the covariance matrix
            ax = cov_fig.add_subplot(nrows, ncols, sp)
            plt.sca(ax)
            ax.imshow(C, interpolation='nearest', aspect='auto', vmin=-1.0, vmax=1.0)
            plt.xticks([])
            plt.yticks([])
            plt.ylabel('E%d' % enumber)
            plt.axis('tight')

            #plot the first 5 principle components
            pcs = spectrum_pcs[enumber]
            ax = pca_fig.add_subplot(nrows, ncols, sp)
            plt.sca(ax)
            for j in range(5):
                plt.plot(spec_f, pcs[j, :], linewidth=1.5)
            plt.ylabel('E%d' % enumber)
            plt.axis('tight')

            #plot the first 5 independent components
            ics = spectrum_ics[enumber]
            ax = ica_fig.add_subplot(nrows, ncols, sp)
            plt.sca(ax)
            for j in range(5):
                plt.plot(spec_f, ics[j, :], linewidth=1.5)
            plt.ylabel('E%d' % enumber)
            plt.axis('tight')

    plt.show()


def plot_lfp(f, site_id, protocol_name, t1, t2):
    #get the site by id
    site = Site(f)
    assert site.from_id(site_id)

    #get the protocol by name
    pcalls = [p for p in site.protocols if p.name == protocol_name]
    assert len(pcalls) == 1
    protocol = pcalls[0]

    #get some LFP properties
    e = site.electrodes[0]
    lfp_pr = e.get_lfp_protocol_response(protocol_name)
    sr = lfp_pr.lfp.sample_rate
    geom = e.geometry
    coords = geom.coordinates

    si = int(t1*sr)
    ei = int(t2*sr)
    nduration = ei - si

    #get the LFPs
    lfps = dict()
    for electrode in site.electrodes:
        lfp_pr = electrode.get_lfp_protocol_response(protocol_name)
        lfp = lfp_pr.waveform[si:ei]
        lfp -= lfp.mean()
        lfp /= lfp.std(ddof=1)
        lfps[electrode.number] = lfp

    #get the stimulus spectrogram and threshold out noise
    t = np.arange(nduration) / sr
    stim_spec_t,stim_spec_f,stim_spec = protocol.compute_stimulus_spectrogram(t1, t2)

    #get anatomical region for each electrode
    electrode_regions = dict()
    for e in site.electrodes:
        reg = '?'
        if e.anatomy.__class__.__name__ == 'ElectrodeAnatomicalLocation':
            reg = e.anatomy.region
        electrode_regions[e.number] = reg

    #get spikes for each cell on each electrode
    sort_type = 'single'
    spikes_by_electrode = dict()
    for electrode in site.electrodes:
        spikes_by_electrode[electrode.number] = list()
        for cell in electrode.cells:
            if cell.sort_type != sort_type:
                continue
            resps = [cpr for cpr in cell.protocol_responses if cpr.protocol.name == protocol_name]
            assert len(resps) == 1
            all_spikes = resps[0].spikes
            spike_index = (all_spikes >= t1) & (all_spikes <= t2)
            spikes = all_spikes[spike_index]
            spikes_by_electrode[electrode.number].append(spikes)

    #group neurograms together by hemisphere and column
    electrode_data = list()
    for e in site.electrodes:
        row,col = np.nonzero(coords == e.number)
        electrode_data.append( (e.number, e.hemisphere, row, col % 2) )
    electrode_data.sort(key=operator.itemgetter(3))
    electrode_data.sort(key=operator.itemgetter(2))
    electrode_data.sort(key=operator.itemgetter(1))

    spikes_by_hemicol = dict()
    spike_groups_by_hemicol = dict()
    for hemi,col in [('L', 0), ('L', 1), ('R', 0), ('R', 1)]:
        enumbers = [enumber for enumber,ehemi,erow,ecol in electrode_data if ecol == col and ehemi == hemi]
        spike_list = list()
        spike_groups = dict()
        index = 0
        for k,enumber in enumerate(enumbers):
            espikes = spikes_by_electrode[enumber]
            spike_list.extend(espikes)
            eslen = max(1, len(espikes))
            spike_groups[enumber] = range(index, index+eslen)
            index += eslen

        spikes_by_hemicol[(hemi, col)] = spike_list
        spike_groups_by_hemicol[(hemi, col)] = spike_groups

    #make plots
    def spec_header_func(ax, hemisphere, col):
        plot_spectrogram(stim_spec_t, stim_spec_f, stim_spec, ax=ax, ticks=False, colorbar=False, colormap=cm.gist_yarg)

    def raster_footer_func(ax, hemisphere, col):
        key = (hemisphere, col)
        plot_raster(spikes_by_hemicol[key], ax=ax, duration=t2-t1, bin_size=0.001, time_offset=t1, groups=spike_groups_by_hemicol[key], ylabel='Electrode')
        plt.xlabel('Time (ms)')

    def electrode_waveform_func(ax, enumber, row, col):
        plt.sca(ax)
        y = lfps[enumber]
        plt.errorbar(t, y, color='k', linewidth=2)
        #print 'enumber=%d, row=%d, col=%d' % (enumber, row, col)
        plt.xticks([])
        plt.ylabel('E%d (%s)' % (enumber, electrode_regions[enumber]))
        plt.axis('tight')

    def electrode_spectrogram_func(ax, enumber, row, col):
        plt.sca(ax)
        y = lfps[enumber]
        #lfp_spec_t,lfp_spec_f,lfp_spec = compute_mean_spectrogram(y, sr, win_sizes=[0.050, 0.075, 0.150, 0.250, 0.400, 0.600], num_freq_bands=75)
        lfp_spec_t,lfp_spec_f,lfp_spec,lfp_spec_rms = gaussian_stft(y, sr, 0.075, 1.0/sr)
        lfp_spec = np.abs(lfp_spec)
        lfp_mean = lfp_spec.mean(axis=1)
        lfp_std = lfp_spec.std(ddof=1)
        lfp_spec = (lfp_spec.T - lfp_mean).T
        lfp_spec = (lfp_spec.T / lfp_std).T
        #lfp_spec = postprocess_spectrogram(np.abs(lfp_spec))

        plot_spectrogram(lfp_spec_t, lfp_spec_f, lfp_spec, ax=ax, ticks=False, colormap=cm.afmhot_r, colorbar=False)
        plt.ylabel('%d (%s)' % (enumber, electrode_regions[enumber]))

    hemi_groups = {'L':list(), 'R':list()}
    for e in site.electrodes:
        hemi_groups[e.hemisphere].append(e.number)
    #plot_electrodes_2figs(electrode_waveform_func, coords, hemi_groups, header_func=spec_header_func, footer_func=raster_footer_func)
    plot_electrodes_2figs(electrode_spectrogram_func, coords, hemi_groups, header_func=spec_header_func, footer_func=raster_footer_func)


def spectral_pca(f, enumber, site_id=1, protocol_name='Call1'):

    site = Site(f)
    assert site.from_id(site_id)
    pcalls = [p for p in site.protocols if p.name == protocol_name]
    protocol = pcalls[0]

    elist = [e for e in site.electrodes if e.number == enumber]
    assert len(elist) == 1
    electrode = elist[0]

    lfp_pr = electrode.get_lfp_protocol_response(protocol_name)
    lfp = lfp_pr.waveform
    lfp -= lfp.mean()
    lfp /= lfp.std(ddof=1)

    win_size = 0.100
    stime = time.time()
    sr = electrode.lfp.sample_rate
    lfp_spec_t,lfp_spec_f,lfp_spec,lfp_spec_rms = gaussian_stft(lfp, sr, win_size, 1.0/sr)
    lfp_spec = postprocess_spectrogram(np.abs(lfp_spec))
    etime = time.time() - stime
    print 'Time to compute spectrogram: %d s' % int(etime)

    #keep track of stimulus and silence indices
    silence_indices = list()
    last_index = 0
    offset = int(win_size*sr)
    for k,event in enumerate(protocol.events):
        start_index = int(event.start_time*sr)
        silence_indices.append( range(last_index+offset, start_index-offset))
        last_index = int(event.end_time*sr)

    #keep track of song indices
    song_indices = list()
    last_index = 0
    offset = int(win_size*sr)
    for k,event in enumerate([e for e in protocol.events if e.sound.stim_type == 'song']):
        start_index = int(event.start_time*sr)
        end_index = int(event.end_time*sr)
        song_indices.append( range(start_index-offset, end_index+offset))

    def cov_and_pca(X, desc=''):

        stime = time.time()
        #estimate covariance matrix
        C = np.cov(X)
        for k in range(C.shape[0]):
            C[k, k] = 0.0
        etime = time.time() - stime
        print 'Time to compute %s covariance: %d s' % (desc, int(etime))

        #do PCA
        stime = time.time()
        ncomp = 8
        pca = PCA(n_components=ncomp)
        pca.fit(X.T)
        etime = time.time() - stime
        print 'Time to do %s PCA: %d s' % (desc, int(etime))

        #plot covariance matrix
        fmin = lfp_spec_f.min()
        fmax = lfp_spec_f.max()
        plt.figure()
        plt.imshow(C, interpolation='nearest', origin='lower', aspect='auto', extent=[fmin, fmax, fmin, fmax])
        plt.xlabel('Frequency')
        plt.ylabel('Frequency')
        plt.title('Covariance Matrix (%s)' % desc)

        #plot principle components
        plt.figure()
        for k in range(ncomp):
            plt.subplot(ncomp, 1, k+1)
            plt.plot(lfp_spec_f, pca.components_[k, :], 'k-')
            plt.title('PC %d (%s): var=%0.2f' % (k, desc, pca.explained_variance_ratio_[k]))
            plt.axis('tight')

    i = np.concatenate(silence_indices)
    stime = time.time()
    X = lfp_spec[:, i]
    etime = time.time() - stime
    print 'Time to index lfp_spec: %d s' % int(etime)
    cov_and_pca(X, 'Silence')

    i = np.concatenate(song_indices)
    X = lfp_spec[:, i]
    cov_and_pca(X, 'Song')


if __name__ == '__main__':

    f = tables.open_file('/auto/k8/fdata/mschachter/tdt_continuous/GreBlu9508M/GreBlu9508M_tabled.h5', 'r')

    #t1 = 455.0
    #t2 = 465.0
    #plot_lfp(f, site_id=3, protocol_name='Call3', t1=t1, t2=t2)

    spectral_pca(f, 7, site_id=1, protocol_name='Call1')
    plt.show()
    f.close()

