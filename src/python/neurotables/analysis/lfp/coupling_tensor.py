import copy
import os
import time
import glob

import numpy as np

from scipy.interpolate import splrep,splev
import h5py
from matplotlib import cm
from matplotlib.gridspec import GridSpec

import matplotlib.pyplot as plt
from lasp.sound import WavFile

from neurotables.analysis.lfp.cross_coherence import ElectrodeComparator, get_all_pair_files
from neurotables.analysis.lfp.figures import plot_nmi_matrix
from neurotables.objects.cells import *


band_params = {'delta':{'window_size':5.00, 'increment':2.5, 'bandwidth':3.0},
               'theta':{'window_size':1.25, 'increment':0.625, 'bandwidth':9.0},
               'alpha':{'window_size':0.385, 'increment':0.1925, 'bandwidth':17.0},
               'low_gamma':{'window_size':0.167, 'increment':0.083, 'bandwidth':40.0},
               'high_gamma':{'window_size':0.071, 'increment':0.036, 'bandwidth':75.0},
              }

#set up anatomical groupings
region_groups = {'HP':'HP', 'CML':'CML', 'CMM':'CMM', 'L':'L2', 'L2A':'L2', 'L2B':'L2', 'L3':'L1/L3', 'L1':'L1/L3', 'NCM':'NCM', '?':'?'}

default_group_order = ['NCM', 'L2', 'HP', 'CML', 'CMM', 'L1/L3']


class LFPCouplingTensor(object):

    def __init__(self):
        self.f = None
        self.t = None
        self.num_electrodes = None
        self.nmi = None
        self.hemispheres = None
        self.regions = None
        self.distances = None
        self.protocol = None
        self.site_id = None
        self.site = None
        self.t1 = None
        self.t2 = None
        self.edesc = "all"
        self.nmi_stat_by_stim = None

    @classmethod
    def from_files(cls, f, root_dir='/auto/k8/fdata/mschachter/analysis/cc', site_id=1, protocol_name='Call1', edesc=None, num_electrodes=32):

        cc_files = get_all_pair_files(site_id=site_id, protocol_name=protocol_name, edesc=edesc, num_electrodes=num_electrodes)

        #peek into the first file to get some basic information
        ec = ElectrodeComparator.from_file(f, cc_files[0][-1], root_dir=root_dir)
        ct = ec.ct
        cfreq = ec.cfreq
        high_freq = cfreq.max()
        low_freq = cfreq.min()
        if ec.filter is not None:
            low_freq,high_freq = ec.get_filter_freqs()
        protocol = ec.protocol
        del ec
        df = cfreq[1] - cfreq[0]

        #create a map of electrodes
        site = Site(f)
        assert site.from_id(site_id)
        emap = {e.number:e for e in site.electrodes}

        freq_index = (cfreq >= low_freq) & (cfreq <= high_freq)

        #go through all files and construct a tensor of dimensionality (num_electrodes, num_electrodes, coherence frequency, time)
        hemi = np.zeros([num_electrodes, num_electrodes, 2], dtype='S1')
        region = np.zeros([num_electrodes, num_electrodes, 2], dtype='S16')
        nmi = dict()
        print "Creating ElectrodeComparator objects for %d files" % len(cc_files)
        for k,(enum1,enum2,fname) in enumerate(cc_files):

            try:
                full_fname = os.path.join(root_dir, fname)
                print 'Reading %s (%d of %d)...' % (full_fname, (k+1), len(cc_files))

                hf = h5py.File(full_fname, 'r')
                cgrp = hf['cross_coherence']
                cc_nmi = np.array(cgrp['nmi'])
                hf.close()

            except:
                print 'Problem opening file %s' % fname
                continue

            e1 = emap[enum1]
            e2 = emap[enum2]

            i = e1.number - 1
            j = e2.number - 1
            p = [i+1, j+1]
            p.sort()
            nmi[tuple(p)] = cc_nmi

            hemi[i, j, 0] = e1.hemisphere
            hemi[i, j, 1] = e2.hemisphere
            hemi[j, i, 0] = hemi[i, j, 1]
            hemi[j, i, 1] = hemi[i, j, 0]

            a1 = e1.anatomy
            r1 = '?'
            if type(a1) is not list:
                r1 = a1.region

            a2 = e2.anatomy
            r2 = '?'
            if type(a2) is not list:
                r2 = a2.region

            region[i, j, 0] = r1
            region[i, j, 1] = r2
            region[j, i, 0] = r2
            region[j, i, 1] = r1

        print "Creating LFPCouplingTensor object"
        lct = LFPCouplingTensor()
        lct.num_electrodes = num_electrodes
        lct.t = ct
        lct.freq = cfreq[freq_index]
        lct.nmi = nmi
        lct.hemispheres = hemi
        lct.region = region
        lct.protocol = protocol
        lct.site_id = site_id
        lct.t1 = ct.min()
        lct.t2 = ct.max()
        if edesc is not None:
            lct.edesc = edesc
        lct.f = f

        return lct

    def compute_stats(self, t1=None, t2=None):
        """
            Compute mean and std of the nmi for the tensor between time pts t1 and t2
            Things to add:
            -Compute significance statistics using noise floor
        """

        if t1 is None:
            t1 = [self.t1]
        if type(t1) is not list:
            t1 = list(t1)

        if t2 is None:
            t2 = [self.t2]
        if type(t2) is not list:
            t2 = list(t2)

        #compute the mean and std NMI over time
        nmi_mean = np.zeros([self.num_electrodes, self.num_electrodes])
        nmi_std = np.zeros([self.num_electrodes, self.num_electrodes])

        t_index = lambda start, end: (self.t >= start) & (self.t <= end)

        inds = np.any([t_index(start, end) for start, end in zip(t1, t2)], axis=0)

        for (e1,e2),nmi in self.nmi.iteritems():
            i = e1 - 1
            j = e2 - 1
            nmi_mean[i, j] = nmi[inds].mean()
            nmi_mean[j, i] = nmi_mean[i, j]
            nmi_std[i, j] = nmi[inds].std(ddof=1)
            nmi_std[j, i] = nmi_std[i, j]

        self.nmi_mean = nmi_mean
        self.nmi_std = nmi_std

        return self.nmi_mean, self.nmi_std

    def per_stim_stats(self):

        st = time.time()
        print 'Aggregating events...'
        #aggregate events by sound
        events = dict()
        for se in self.protocol.events:
            sound_id = se.sound.id
            sid = se.id
            stime = se.start_time
            etime = se.end_time
            if sound_id not in events:
                events[sound_id] = list()
            events[sound_id].append({'id':sid, 'start':stime, 'end':etime})

        print 'Interpolating NMI over time...'

        #interpolate each NMI time series by a cubic spline
        spnmi = dict()
        for ekey,nmi in self.nmi.iteritems():
            nmi_ms = copy.copy(nmi)
            nmi_ms[nmi_ms < 55.0] = 0.0
            spnmi[ekey] = splrep(self.t, nmi)

        #condition on sound and get tensor values that span the duration
        dt = band_params[self.edesc]['increment']
        min_duration = 1.5
        nmi_stat_by_stim = dict()

        for sid,elist in events.iteritems():
            print 'Interpolating for stimulus %d' % sid

            #settle on a single time interval to describe the stimulus
            stim_dur = np.max([sedict['end']-sedict['start'] for sedict in elist])
            if stim_dur < min_duration:
                stim_dur = min_duration

            #go through each presentation of this sound
            nmi_per_trial = dict()
            for sedict in elist:
                #ensure a minimum amount of time for computing mean from start of stimulus
                t1 = sedict['start']
                t2 = t1 + stim_dur
                ti = np.arange(t1, t2, dt)

                print '\tTrial (%0.1f, %0.1f) len(ti)=%d' % (t1, t2, len(ti))
                #evaluate the NMI from t1 to t2
                for ekey in self.nmi.keys():
                    ival = splev(ti, spnmi[ekey])
                    if ekey not in nmi_per_trial:
                        nmi_per_trial[ekey] = list()
                    nmi_per_trial[ekey].append(ival)

            print '\tComputing stats'
            #compute mean and std of nmi across trials per pair for this stimulus
            nmi_stat_by_stim[sid] = dict()
            for ekey,nmiarr in nmi_per_trial.iteritems():
                nmiarr = np.array(nmiarr)
                nmi_stat_by_stim[sid][ekey] = (nmiarr.mean(), nmiarr.std(ddof=1))
        etime = time.time() - st
        print 'Elapsed time: %0.0fs' % etime

        #turn each mean and std into a matrix for further computations
        nmi_mat_by_stim = dict()
        for sid,estats in nmi_stat_by_stim.iteritems():
            nmi_mean = np.zeros([self.num_electrodes, self.num_electrodes])
            nmi_std = np.zeros([self.num_electrodes, self.num_electrodes])
            for (e1,e2),(m,s) in estats.iteritems():
                i = e1 - 1
                j = e2 - 1
                nmi_mean[i, j] = m
                nmi_mean[j, i] = m
                nmi_std[i, j] = s
                nmi_std[j, i] = s
            nmi_mat_by_stim[sid] = (nmi_mean, nmi_std)

        self.nmi_stat_by_stim = nmi_mat_by_stim

        #break up NMI by stimulus type
        sounds = dict()
        for sid in self.nmi_stat_by_stim.keys():
            s = Sound(self.f)
            assert s.from_id(sid)
            sounds[sid] = s

        means_by_type = dict()
        means_by_fam = dict()
        for sid,s in sounds.iteritems():
            stype = s.stim_type
            src = s.stim_source
            if stype not in means_by_type:
                means_by_type[stype] = list()
            if src not in means_by_fam:
                means_by_fam[src] = list()
            M,S = self.nmi_stat_by_stim[sid]
            means_by_type[stype].append(M)
            means_by_fam[src].append(M)

        #compute grand means and stds
        mean_by_type = dict()
        mean_by_fam = dict()
        for stype,mlist in means_by_type.iteritems():
            print '# of stimuli of type %s: %d' % (stype, len(mlist))
            mlist = np.array(mlist)
            M = mlist.mean(axis=0)
            M -= self.nmi_mean
            #nz = M > 0.0
            #M[nz] = np.log(M[nz])
            mean_by_type[stype] = M
        for ftype,mlist in means_by_fam.iteritems():
            mlist = np.array(mlist)
            M = mlist.mean(axis=0)
            M -= self.nmi_mean
            #nz = S > 0.0
            #S[nz] = np.log(S[nz])
            mean_by_fam[ftype] = M

        self.stim_nmi_mean_by_type = mean_by_type
        self.stim_nmi_mean_by_fam = mean_by_fam

    def plot_perstim_stats(self):

        if self.nmi_stat_by_stim is None:
            self.per_stim_stats()

        #make plots
        column_names = ['%d' % (k+1) for k in range(self.num_electrodes)]

        plt.figure()
        ax = plt.subplot(1, 1, 1)
        im = ax.imshow(self.stim_nmi_mean_by_type['call'], interpolation='nearest', aspect='auto', origin='lower', cmap=cm.jet)
        plt.colorbar(im)
        plt.title('Deviation from Mean for Calls')
        ax.set_yticks(range(len(column_names)))
        ax.set_yticklabels(column_names)
        ax.set_xticks(range(len(column_names)))
        ax.set_xticklabels(column_names)
        plt.axhline(self.num_electrodes/2 - 0.5, color='k')
        plt.axvline(self.num_electrodes/2 - 0.5, color='k')
        plt.xlabel('Electrode')
        plt.ylabel('Electrode')

        plt.figure()
        ax = plt.subplot(1, 1, 1)
        im = ax.imshow(self.stim_nmi_mean_by_type['song'], interpolation='nearest', aspect='auto', origin='lower', cmap=cm.jet)
        plt.colorbar(im)
        plt.title('Deviation from Mean for Song')
        ax.set_yticks(range(len(column_names)))
        ax.set_yticklabels(column_names)
        ax.set_xticks(range(len(column_names)))
        ax.set_xticklabels(column_names)
        plt.axhline(self.num_electrodes/2 - 0.5, color='k')
        plt.axvline(self.num_electrodes/2 - 0.5, color='k')
        plt.xlabel('Electrode')
        plt.ylabel('Electrode')

        plt.figure()
        ax = plt.subplot(1, 1, 1)
        im = ax.imshow(self.stim_nmi_mean_by_type['mlnoise'], interpolation='nearest', aspect='auto', origin='lower', cmap=cm.jet)
        plt.colorbar(im)
        plt.title('Deviation from Mean for Noise')
        plt.axis('tight')
        ax.set_yticks(range(len(column_names)))
        ax.set_yticklabels(column_names)
        ax.set_xticks(range(len(column_names)))
        ax.set_xticklabels(column_names)
        plt.axhline(self.num_electrodes/2 - 0.5, color='k')
        plt.axvline(self.num_electrodes/2 - 0.5, color='k')
        plt.xlabel('Electrode')
        plt.ylabel('Electrode')

        plt.figure()
        ax = plt.subplot(2, 1, 1)
        im = ax.imshow(self.stim_nmi_mean_by_fam['familiar'], interpolation='nearest', aspect='auto', origin='lower', cmap=cm.jet)
        plt.colorbar(im)
        ax.set_yticks(range(len(column_names)))
        plt.title('Mean Across Familiar')
        plt.axis('tight')

        ax = plt.subplot(2, 1, 2)
        im = ax.imshow(self.stim_nmi_mean_by_fam['unfamiliar'], interpolation='nearest', aspect='auto', origin='lower', cmap=cm.jet)
        plt.colorbar(im)
        ax.set_yticks(range(len(column_names)))
        plt.title('Mean Across Unfamiliar')
        plt.axis('tight')

    def to_file(self, output_file):
        """
            Write the aggregated cross coherence data to an hdf5 file.
        """
        hf = h5py.File(output_file, 'a')

        grp = hf.create_group(self.edesc)

        nmigrp = grp.create_group('nmi')
        for (e1,e2),nmi in self.nmi.iteritems():
            key = '%d,%d'  % (e1, e2)
            nmigrp[key] = nmi
        grp['t'] = self.t
        grp['hemispheres'] = self.hemispheres
        grp.attrs['protocol_id'] = self.protocol.id
        grp.attrs['site_id'] = self.site_id
        grp.attrs['t1'] = self.t1
        grp.attrs['t2'] = self.t2
        grp.attrs['num_electrodes'] = self.num_electrodes
        if self.f is not None:
            grp.attrs['f'] = self.f.filename
        hf.close()

    @classmethod
    def from_file(cls, f, file_name='/auto/k8/fdata/mschachter/analysis/cc/lfp_tensor.h5', edesc='all', t1=None, t2=None,
                  return_summary=False, zscore=False, zscore_time=None, verbose=False, compute_stats=True,
                  log=False):

        stime = time.time()
        L = LFPCouplingTensor()

        hf = h5py.File(file_name, 'r')
        if edesc not in hf.keys():
            hf.close()
            return

        grp = hf[edesc]

        # Load time points
        if t1 is None:
            L.t = np.array(grp['t'])
            L.t1 = grp.attrs['t1']
            L.t2 = grp.attrs['t2']
            t_inds = range(len(L.t))
            if verbose:
                print "No time intervals specified. Using all %d time points" % len(L.t)
        else:
            if type(t1) is not list:
                L.t1 = [t1]
            else:
                L.t1 = t1
            if type(t2) is not list:
                L.t2 = [t2]
            else:
                L.t2 = t2

            if verbose:
                print "%d time intervals specified" % len(L.t1)

            assert len(L.t1) == len(L.t2)

            if L.t1[0] < grp.attrs['t1']:
                print "Specified t1 is less than the t1 from file: %3.2f < %3.2f" % (L.t1[0], grp.attrs['t1'])
                return
            if L.t2[-1] > grp.attrs['t2']:
                print "Specified t2 is greater than the t2 from file: %3.2f > %3.2f" % (L.t2[-1], grp.attrs['t2'])
                return

            t = np.array(grp['t'])
            t_inds = list()
            for start, stop in zip(L.t1, L.t2):
                t_inds.extend(np.nonzero(np.logical_and(t >= start, t <= stop))[0])
            L.t = t[t_inds]
            if verbose:
                print "Using %d of %d total time points. %3.2f%%" % (len(t_inds), len(t), 100 * float(len(t_inds)) / len(t))


        L.hemispheres = np.array(grp['hemispheres'])

        protocol_id = grp.attrs['protocol_id']
        protocol = Protocol(f)
        assert protocol.from_id(protocol_id)
        L.protocol = protocol

        L.site_id = grp.attrs['site_id']
        site = Site(f)
        assert site.from_id(L.site_id)
        L.site = site

        L.num_electrodes = grp.attrs['num_electrodes']
        L.edesc = edesc

        # #get sample rate of LFP from electrode sample rate
        # #TODO: save the sample rate in the tensor file so it doesn't have to be inferred
        # site = Site(f)
        # assert site.from_id(L.site_id)
        # sr = site.electrodes[0].lfp.sample_rate

        # if t1 is not None:
        #     t1_index = int(t1*sr)
        # if t2 is not None:
        #     t2_index = int(t2*sr)

        nmi_max = -np.inf
        nmi_min = np.inf
        L.nmi = dict()
        for key in grp['nmi'].keys():

            nmi = np.array(grp['nmi'][key])
            if log:
                nz = nmi > 0.0
                nmi[nz] = np.log(nmi[nz])
            e1, e2 = [int(x) for x in key.split(',')]
            if zscore is True:
                if verbose:
                    print "Z-scoring the nmi for electrode pair %d,%d." % (e1, e2)
                if zscore_time is not None:
                    nmi_mean = nmi[zscore_time].mean()
                    nmi_std = nmi[zscore_time].std()
                    if verbose:
                        print "Computing z-score on a subset of the time points: %d of %d" % (len(zscore_time), len(nmi))
                        print "Mean: %4.3f, Std: %4.3f, Mean Total: %4.3f, Std Total: %4.3f" % (nmi_mean, nmi_std, nmi.mean(), nmi.std())
                    nmi -= nmi_mean
                    nmi /= nmi_std

                else:
                    nmi -= nmi.mean()
                    nmi /= nmi.std()

            nmi_max = max(nmi_max, nmi.max())
            nmi_min = min(nmi_min, nmi.min())

            if t1 is None:
                if return_summary is False:
                    if verbose:
                        print "Returning entire nmi timecourse of %d points" % len(nmi)
                    L.nmi[(e1, e2)] = nmi
                else:
                    nmi_mean = nmi.mean()
                    if verbose:
                        print "Returning mean of nmi timecourse of %d points: %4.3f" % (len(nmi), nmi_mean)
                    L.nmi[(e1, e2)] = nmi_mean
            else:
                if return_summary is False:
                    if verbose:
                        print "Returning entire nmi timecourse of %d points" % len(t_inds)
                    L.nmi[(e1, e2)] = nmi[t_inds]
                else:
                    nmi_mean = nmi[t_inds].mean()
                    if verbose:
                        print "Returning mean of nmi timecourse of %d points: %4.3f" % (len(t_inds), nmi_mean)
                    L.nmi[(e1, e2)] = nmi_mean

        L.nmi_max = nmi_max
        L.nmi_min = nmi_min
        L.f = f
        hf.close()

        if (t1 is None) and (return_summary is False) and compute_stats:
            L.compute_stats()

        etime = time.time() - stime
        print 'LFPCouplingTensor.from_file: took %d seconds to load' % int(etime)

        return L

    def plot(self, start_time=None, end_time=None):

        #plot mean and std of NMI over time
        fig = plt.figure()
        plt.subplots_adjust(top=0.95, bottom=0.05, left=0.03, right=0.99, hspace=0.03)

        ax = fig.add_subplot(1, 2, 1)
        plot_nmi_matrix(self.nmi_mean, ax, colorbar=True)
        plt.title('NMI Mean')

        ax = fig.add_subplot(1, 2, 2)
        plot_nmi_matrix(self.nmi_std, ax, colorbar=True)
        plt.title('NMI Std')

        if start_time is not None and end_time is not None:
            C,mgindex = self.get_time_matrix(start_time, end_time)

            #get stimulus spectrogram
            spec_t,spec_f,spec = self.protocol.compute_stimulus_spectrogram(start_time, end_time)

            plt.figure()

            #plot stimulus spectrogram
            gs = GridSpec(8, 1)
            ax = plt.subplot(gs[:2, 0])
            plot_spectrogram(spec_t, spec_f, spec, ax=ax, fmin=300.0, fmax=8000.0, colormap=cm.gist_yarg, colorbar=False)

            #plot time matrix
            ax = plt.subplot(gs[2:, 0])
            im = plt.imshow(C, interpolation='nearest', aspect='auto', cmap=cm.jet, extent=[spec_t.min(), spec_t.max(), C.shape[0], 1])
            for mg,(si,ei) in mgindex.iteritems():
                plt.axhline(ei-0.5, color=[1.0, 1.0, 1.0])
            plt.axis('tight')
            plt.xlabel('Time (s)')

    def export_matrix2d_to_file(self, output_dir='/auto/k8/fdata/mschachter/analysis/cc'):
        """
            Export a 2D over time representation of the NMI to a file.
        """

        C,mgindex = self.get_time_matrix(self.t1, self.t2)

        mglist = [(mgname,si,ei) for mgname,(si,ei) in mgindex.iteritems()]
        mglist.sort(key=operator.itemgetter(1))

        ofile = os.path.join(output_dir, 'nmi2d_Site%d_%s.h5' % (self.site_id, self.protocol.name))
        hf = h5py.File(ofile, 'w')
        hf['matrix'] = C
        hf['mgnames'] = [x[0] for x in mglist]
        hf['mg_index'] = [x[1] for x in mglist]
        hf.close()

    def get_epairs_by_group(self):

        #initialize the dictionary that will contain electrode pairs grouped by anatomical group connectivity
        epairs_by_group = dict()
        hemis = ['L', 'R']

        #map all the electrodes to their group/hemisphere
        electrode_groups = dict()
        for e in self.site.electrodes:
            reg = e.anatomy.region
            sstrs = reg.split('-')
            reg = sstrs[0]
            group = region_groups[reg]
            hemi = e.hemisphere
            electrode_groups[e.number] = (group, hemi)

        #get each electrode pair and put it with the group it belongs to
        for e1,e2 in self.nmi.keys():
            g1,h1 = electrode_groups[e1]
            g2,h2 = electrode_groups[e2]
            #print 'e1=%d, e2=%d, g1=%s, h1=%s, g2=%s, h2=%s' % (e1, e2, g1, h1, g2, h2)

            pair = [(e1, g1, h1, default_group_order.index(g1), hemis.index(h1)), (e2, g2, h2, default_group_order.index(g2), hemis.index(h2))]
            if h1 == h2:
                #sort by region
                pair.sort(key=operator.itemgetter(3))
            else:
                #sort by hemisphere
                pair.sort(key=operator.itemgetter(4))

            key1 = (pair[0][1], pair[0][2])
            key2 = (pair[1][1], pair[1][2])
            #print 'key1=%s, key2=%s' % (str(key1), str(key2))

            key = (key1, key2)
            if key not in epairs_by_group:
                epairs_by_group[key] = list()

            epairs_by_group[key].append( (e1, e2) )

        return epairs_by_group

    def get_time_matrix(self, t1, t2):

        #get the generic group orderings
        group_ordering = get_pair_ordering_by_group()

        epairs_by_group = self.get_epairs_by_group()
        npairs = len(self.nmi)

        ti1 = np.min(np.where(self.t >= t1)[0])
        ti2 = np.max(np.where(self.t <= t2)[0])

        #construct a matrix of NMI values
        major_groups = ['L-within', 'L-between',
                        'R-within', 'R-between',
                        'LR-within', 'LR-between']
        C = np.zeros([npairs, ti2-ti1])
        index = 0

        major_group_indices = dict()

        for mgrp in major_groups:
            si = index
            for (g1,h1),(g2,h2) in group_ordering[mgrp]:
                key = ((g1,h1),(g2,h2))
                if key not in epairs_by_group:
                    continue

                epairs = epairs_by_group[key]
                for e1,e2 in epairs:
                    C[index, :] = self.nmi[(e1, e2)][ti1:ti2]
                    index += 1
            major_group_indices[mgrp] = (si, index)
        return C,major_group_indices

    def dense_nmi(self, nmi_dict=None):
        """
            Construct a tensor with dimensions num_electrodes X num_electrodes X len(t).
        """
        NMI = np.zeros([self.num_electrodes, self.num_electrodes, len(self.t)])
        for (e1,e2),nmi in self.nmi.iteritems():
            i = e1-1
            j = e2-1
            NMI[i, j] = nmi
            NMI[j, i] = NMI[i, j]
        return NMI

    def sound_track(self, t1, t2, output_file):
        """
            Make a sound track of stimuli to go along with the electrode activity.
        """

        duration = t2 - t1
        sr = 44100.0
        n = int(duration*sr)
        wave = np.zeros([n])

        for event in self.protocol.events:
            #get sound corresponding ot event
            if event.start_time >= t1 and event.start_time <= t2:
                si = int((event.start_time - t1)*sr)
                w = event.sound.waveform
                if event.sound.sample_rate != sr:
                    tw,w = resample_signal(w, event.sound.sample_rate, sr)
                ei = min(si + len(w), n)
                print 'si=%d, ei=%d, len(wave)=%d, len(w)=%d' % (si, ei, len(wave), len(w))

                #normalize amplitude
                w /= np.abs(w).max()

                l = ei - si
                #put into the sound track
                wave[si:ei] = w[:l]

        wf = WavFile()
        wf.data = wave
        wf.sample_rate = sr
        wf.num_channels = 1
        wf.to_wav(output_file, normalize=True)

    @classmethod
    def merge_audio_with_video(cls, audio_file, video_file, output_file):

        cmd = 'avconv -i %s -i %s -c copy -c:a libmp3lame %s' % (video_file, audio_file, output_file)
        os.system(cmd)

def get_pair_ordering_by_group():
    """
        Return a list of group/hemisphere pairs [ (grp1, hemi1), (grp2, hemi2) ] that are ordered such that they are grouped by anatomical
        region.
    """

    hemis = ['L', 'R']

    group_indices = {'L-within':list(), 'L-between':list(),
                     'R-within':list(), 'R-between':list(),
                     'LR-within':list(), 'LR-between':list()}

    #within hemisphere connections
    for h in hemis:
        #within hemisphere within region connections

        for grp in default_group_order:
            group_indices['%s-within' % h].append( ((grp, h), (grp, h)) )

        #within hemisphere between region connections
        for k,grp1 in enumerate(default_group_order):
            for j,grp2 in enumerate(default_group_order[(k+1):]):
                group_indices['%s-between' % h].append( ((grp1, h), (grp2, h)) )

    #between hemisphere within region connections
    for grp in default_group_order:
        group_indices['LR-within'].append( ((grp, hemis[0]), (grp, hemis[1])) )

    #between hemisphere between region connections
    for k,grp1 in enumerate(default_group_order):
            for j,grp2 in enumerate(default_group_order):
                if grp1 != grp2:
                    group_indices['LR-between'].append( ((grp1, hemis[0]), (grp2, hemis[1])) )

    return group_indices


if __name__ == '__main__':

    f = tables.open_file('/auto/k8/fdata/mschachter/tdt_continuous/GreBlu9508M/GreBlu9508M_tabled.h5', 'r')

    ct = LFPCouplingTensor.from_files(f, site_id=1, protocol_name='Call1')
    ct.to_file('/tmp/lfp_tensor_Site1_Call1.h5')

    f.close()
