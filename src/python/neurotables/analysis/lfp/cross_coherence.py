import copy
import glob
import os, h5py, time, re, tables

import numpy as np
from scipy.interpolate import splrep, splev
from scipy.ndimage import convolve1d
from matplotlib import cm,gridspec,rcParams
import matplotlib.pyplot as plt

try:
    from sklearn.decomposition import PCA
    from sklearn.cluster import k_means

except ImportError:
    from scikits.learn.decomposition import PCA
    from scikits.learn.cluster import KMeans as k_means

from lasp.signal import lowpass_filter,bandpass_filter,highpass_filter,resample_signal,gaussian_window,power_spectrum
from lasp.timefreq import gaussian_stft,postprocess_spectrogram
from lasp.sound import plot_spectrogram,WavFile

from neurotables.objects.cells import Site, Protocol, Sound
from neurotables.analysis.lfp.objects import *


class ElectrodeComparator(object):

    def __init__(self, f, protocol_name, electrode1, electrode2, filter=None, filter_freqs=None):
        self.f = f
        self.e1 = electrode1
        self.e2 = electrode2
        self.protocol_name = protocol_name
        self.pr1 = self.e1.get_lfp_protocol_response(self.protocol_name)
        self.pr2 = self.e2.get_lfp_protocol_response(self.protocol_name)
        self.protocol = self.pr1.protocol
        self.lfp1 = self.pr1.waveform
        self.lfp2 = self.pr2.waveform
        self.sample_rate = self.pr1.lfp.sample_rate
        self.dt = 1.0 / self.sample_rate
        self.t1 = None
        self.t2 = None
        self.noise_floor = False
        self.file_name = None

        self.filter = filter
        self.filter_freqs = filter_freqs

        self.window_size = None

        assert len(self.lfp1) == len(self.lfp2)
        self.iduration = len(self.lfp1)
        self.duration = self.iduration * self.dt

        self.ct = None
        self.cfreq = None
        self.ctimefreq = None
        self.ctimefreq_floor = None
        self.nmi = None

        self.stim_envelope = None

        self.tf1 = None
        self.tf2 = None

        self.principle_components = None
        self.explained_variance_ratio = None

        self.phase_diff = None

    def compute_coherence_from_timefreq(self, tf1, tf2, coherence_window_size, increment, nstd, use_window):

        N = tf1.shape[1]

        #compute the power spectrum of each individual spectrogram
        tf1_conj = np.conj(tf1)
        tf2_conj = np.conj(tf2)
        tf1_ps = (tf1 * tf1_conj).real
        tf2_ps = (tf2 * tf2_conj).real
        del tf1_conj

        #compute the sufficient statistics for the cross spectrum, i.e. the stuff that will be averaged
        #when computing the coherence
        cross_spec12 = tf1 * tf2_conj
        del tf2_conj

        #print 'len(s1)=%d, sample_rate=%0.2f, increment=%0.6f, window_size=%0.3f' % (len(s1), self.sample_rate, increment, window_size)
        nincrement = int(np.round(self.sample_rate*increment))
        nwindows = N / nincrement
        t_xcoh = np.arange(0, nwindows, 1.0) * increment

        #nwinlen = max(np.unique(windows[:, 2] - windows[:, 1]))
        nwinlen = int(self.sample_rate*coherence_window_size)

        #print 'nwindows=%d, nwinlen=%d' % (len(windows), nwinlen)
        #generate a normalized window for computing the weighted mean around a point in time
        if use_window:
            gauss_t, average_window = gaussian_window(nwinlen, nstd)
            average_window /= np.abs(average_window).sum()
        else:
            average_window = np.ones(nwinlen) / float(nwinlen)
            #print 'len(average_window)=%d, average_window.sum()=%0.6f' % (len(average_window), average_window.sum())
        nfreqs = tf1.shape[0]
        #compute the coherence at each frequency
        coherence = np.zeros([nfreqs, nwindows])
        for k in range(nfreqs):
            #convolve the window function with each frequency band
            tf1_mean = convolve1d(tf1_ps[k, :], average_window, mode='mirror')
            tf2_mean = convolve1d(tf2_ps[k, :], average_window, mode='mirror')
            denom = np.real(tf1_mean * tf2_mean)
            del tf1_mean
            del tf2_mean

            cs12_mean_r = convolve1d(cross_spec12[k, :].real, average_window, mode='mirror')
            cs12_mean_i = convolve1d(cross_spec12[k, :].imag, average_window, mode='mirror')

            cs12_mean = cs12_mean_r**2 + cs12_mean_i**2
            del cs12_mean_r
            del cs12_mean_i

            coherence[k, :] = cs12_mean / denom

        return t_xcoh,coherence

    def compute_phase_diff(self, spec_window_size=0.100, coherence_window_size=0.200, increment=None, t1=None, t2=None,
                            nstd=6, use_window=True, num_floor_shuffles=0, max_freq=120.0):

        stime = time.time()
        if t1 is None:
            t1 = 0
        if t2 is None:
            t2 = len(self.lfp2) / self.sample_rate
        if increment is None:
            increment = 1.0 / self.sample_rate
        self.t1 = t1
        self.t2 = t2
        self.window_size = spec_window_size
        i1 = int(self.t1*self.sample_rate)
        i2 = int(self.t2*self.sample_rate)

        #zscore and potentially filter the LFPs
        s1 = self.zscore_and_filter(self.lfp1[i1:i2])
        s2 = self.zscore_and_filter(self.lfp2[i1:i2])

        #compute the complex spectrogram for each signal
        spec_stime = time.time()
        spec_t1,spec_freq1,tf1,spec_rms1 = gaussian_stft(s1, self.sample_rate, spec_window_size, increment)

        freq_index = spec_freq1 < max_freq
        print 'Retaining %d frequency bands out of %d' % (freq_index.sum(), len(spec_freq1))
        spec_freq1 = spec_freq1[freq_index]
        tf1 = tf1[freq_index, :]

        spec_etime = time.time() - spec_stime
        print 'Time for spectrogram 1: %0.2f seconds' % spec_etime
        del spec_rms1
        del s1

        spec_stime = time.time()
        spec_t2,spec_freq2,tf2,spec_rms2 = gaussian_stft(s2, self.sample_rate, spec_window_size, increment)

        freq_index = spec_freq2 < max_freq
        spec_freq2 = spec_freq2[freq_index]
        tf2 = tf2[freq_index, :]

        spec_etime = time.time() - spec_stime
        print 'Time for spectrogram 2: %0.2f seconds' % spec_etime
        del spec_rms2
        del s2

        #compute the phase difference between spectrograms
        self.phase_diff = np.cos(np.angle(tf1) - np.angle(tf2))

        #discard the spectrograms
        del tf1
        del tf2

        #smooth the phases across time for each frequency


    def plot_phase_diff(self, start_time=None, end_time=None):
        t1 = self.t1
        if start_time is not None:
            t1 = start_time

        t2 = self.t2
        if end_time is not None:
            t2 = end_time

        i1 = int(t1 * self.sample_rate)
        i2 = int(t2 * self.sample_rate)
        d = i2 - i1

        t = (np.arange(0, d) / self.sample_rate) + t1

        #get the stimulus envelope
        t_stim_env,stim_env = self.protocol.compute_stimulus_envelope(sample_rate=100.0, t1=t1, t2=t2)

        #preprocess the LPF
        zlfp1 = self.lfp1[i1:i2]*1e6
        zlfp1 = self.zscore_and_filter(zlfp1)

        zlfp2 = self.lfp2[i1:i2]*1e6
        zlfp2 = self.zscore_and_filter(zlfp2)

        #compute LFP spectrograms if it hasn't been done already
        if self.tf1 is None:
            self.spec_t1,self.spec_freq1,self.tf1,spec_rms1 = \
                gaussian_stft(zlfp1, self.sample_rate, 0.050, 1.0 / self.sample_rate)
        if self.tf2 is None:
            self.spec_t2,self.spec_freq2,self.tf2,spec_rms2 = \
                gaussian_stft(zlfp2, self.sample_rate, 0.050, 1.0 / self.sample_rate)

        #get properties of stuff
        min_freq = 3.0
        max_freq = 120.0
        if self.filter is not None:
            min_freq, max_freq = self.get_filter_freqs()
        ## first figure, raw LFPs and their cross coherence
        #plot stimulus amplitude envelope
        fig = plt.figure()
        plt.subplots_adjust(bottom=0.06, top=0.99, right=0.99, left=0.06)
        title = '%d and %d' % (self.e1.number, self.e2.number)
        if self.file_name is not None:
            title += ' %s' % self.file_name
        fig.canvas.set_window_title(title)

        gs = gridspec.GridSpec(5, 1)

        plt.subplot(gs[0, 0])
        plt.plot(t_stim_env, stim_env, 'k-')
        plt.ylabel('Stim Env.')
        plt.axis('tight')

        #plot LFPs
        ax = plt.subplot(gs[1, 0])
        #plt.plot(t, zlfp1, 'k-', linewidth=2)
        lfp_spec1 = postprocess_spectrogram(np.abs(self.tf1))
        plot_spectrogram(self.spec_t1, self.spec_freq1, lfp_spec1, ax=ax, colormap=cm.afmhot_r, colorbar=False)
        plt.ylabel('LFP ($\mu$V)')
        plt.axis('tight')
        plt.legend(['E%d' % self.e1.number])

        ax = plt.subplot(gs[2, 0])
        #plt.plot(t, zlfp2, 'k-', linewidth=2)
        lfp_spec2 = postprocess_spectrogram(np.abs(self.tf2))
        plot_spectrogram(self.spec_t2, self.spec_freq2, lfp_spec2, ax=ax, colormap=cm.afmhot_r, colorbar=False)
        plt.ylabel('LFP ($\mu$V)')
        plt.axis('tight')
        plt.legend(['E%d' % self.e2.number])

        ax = plt.subplot(gs[3:, 0])
        ex = (self.spec_t1.min(), self.spec_t2.max(), self.spec_freq1.min(), self.spec_freq1.max())
        plt.imshow(self.phase_diff, cmap=cm.jet, interpolation='nearest', aspect='auto', origin='lower', extent=ex) #vmin=-90.0, vmax=90.0)
        #plot_spectrogram(self.spec_t1, self.spec_freq1, np.unwrap(self.phase_diff), colormap=cm.jet, colorbar=True, )
        plt.ylabel('Phase Diff')
        plt.axis('tight')


    def compute_cross_coherence(self, spec_window_size=0.100, coherence_window_size=0.200, increment=None, t1=None, t2=None,
                                nstd=6, use_window=True, num_floor_shuffles=0, save_memory=False, max_freq=120.0):
        stime = time.time()
        if t1 is None:
            t1 = 0
        if t2 is None:
            t2 = len(self.lfp2) / self.sample_rate
        if increment is None:
            increment = 1.0 / self.sample_rate
        self.t1 = t1
        self.t2 = t2
        self.window_size = spec_window_size
        i1 = int(self.t1*self.sample_rate)
        i2 = int(self.t2*self.sample_rate)

        #zscore and potentially filter the LFPs
        s1 = self.zscore_and_filter(self.lfp1[i1:i2])
        s2 = self.zscore_and_filter(self.lfp2[i1:i2])
        if save_memory:
            del self.lfp1
            del self.lfp2

        #compute the complex spectrogram for each signal
        spec_stime = time.time()
        spec_t1,spec_freq1,tf1,spec_rms1 = gaussian_stft(s1, self.sample_rate, spec_window_size, increment)

        freq_index = spec_freq1 < max_freq
        print 'Retaining %d frequency bands out of %d' % (freq_index.sum(), len(spec_freq1))
        spec_freq1 = spec_freq1[freq_index]
        tf1 = tf1[freq_index, :]

        spec_etime = time.time() - spec_stime
        print 'Time for spectrogram 1: %0.2f seconds' % spec_etime
        del spec_rms1
        del s1
        if save_memory:
            del spec_t1

        spec_stime = time.time()
        spec_t2,spec_freq2,tf2,spec_rms2 = gaussian_stft(s2, self.sample_rate, spec_window_size, increment)

        freq_index = spec_freq2 < max_freq
        spec_freq2 = spec_freq2[freq_index]
        tf2 = tf2[freq_index, :]

        spec_etime = time.time() - spec_stime
        print 'Time for spectrogram 2: %0.2f seconds' % spec_etime
        del spec_rms2
        del s2
        if save_memory:
            del spec_t2

        #compute the mean coherence
        xcoh_stime = time.time()
        t_xcoh, self.ctimefreq = self.compute_coherence_from_timefreq(tf1, tf2, coherence_window_size, increment, nstd, use_window)
        self.cfreq = spec_freq1
        self.ct = t_xcoh + t1
        xcoh_etime = time.time() - xcoh_stime
        print 'Mean Coherence elapsed time: %0.2fs' % xcoh_etime

        #partition the time axis into chunks for computing the coherence floor
        chunk_size = int(1.0 / increment)
        nchunks = int(np.ceil(len(self.ct) / float(chunk_size)))
        print 'chunk_size=%d, nchunks=%d' % (chunk_size, nchunks)

        chunk_indices = list()
        for k in range(nchunks):
            si = k*chunk_size
            ei = min(si+chunk_size, len(self.ct))
            chunk_indices.append( (si, ei) )

        #compute the coherence floor
        chunk_index = np.arange(nchunks)
        nshuffles = 0

        cfloor_mean = np.zeros([tf1.shape[0], tf1.shape[1]])
        cfloor_std = np.zeros([tf1.shape[0], tf1.shape[1]])

        for k in range(num_floor_shuffles):
            xcoh_stime = time.time()
            #shuffle the chunk indices
            np.random.shuffle(chunk_index)
            #generate indices from the shuffled chunks
            rand_index = list()
            for ci in chunk_index:
                rand_index.extend(range(chunk_indices[ci][0], chunk_indices[ci][1]))

            #shuffle the second electrode and recompute the coherence
            t_xcoh_floor, ctimefreq_floor1 = self.compute_coherence_from_timefreq(tf1, tf2[:, rand_index],
                                                                                  coherence_window_size,
                                                                                  increment, nstd,
                                                                                  use_window)
            del t_xcoh_floor

            nshuffles += 1
            delta1 = ctimefreq_floor1 - cfloor_mean
            cfloor_mean += delta1 / nshuffles
            cfloor_std += delta1*(ctimefreq_floor1 - cfloor_mean)
            del ctimefreq_floor1
            del delta1

            #shuffle the first electrode and recompute the coherence
            t_xcoh_floor, ctimefreq_floor2 = self.compute_coherence_from_timefreq(tf1[:, rand_index], tf2,
                                                                                  coherence_window_size,
                                                                                  increment, nstd,
                                                                                  use_window)
            del t_xcoh_floor

            nshuffles += 1
            delta2 = ctimefreq_floor2 - cfloor_mean
            cfloor_mean += delta2 / nshuffles
            cfloor_std += delta2*(ctimefreq_floor2 - cfloor_mean)
            del ctimefreq_floor2
            del delta2

            xcoh_etime = time.time() - xcoh_stime
            print 'Shuffle %d elapsed time: %0.2fs' % (nshuffles, xcoh_etime)

        if num_floor_shuffles > 0:
            cfloor_std /= (nshuffles - 1)
            cfloor_std = np.sqrt(cfloor_std)

            #subtract the floor off from the cross coheregram
            self.ctimefreq -= cfloor_mean
            for k in range(tf1.shape[0]):
                #zero out any elements of the cross coheregram that are below two standard deviations of the floor
                zindx = self.ctimefreq[k, :] < 2.0*cfloor_std[k, :]
                self.ctimefreq[k, zindx] = 0.0

        if save_memory:
            del tf1
            del tf2
            del cfloor_mean
            del cfloor_std
        else:
            self.spec_t1,self.spec_freq1,self.tf1 = spec_t1,spec_freq1,tf1
            self.spec_t2,self.spec_freq2,self.tf2 = spec_t2,spec_freq2,tf2
            self.ctimefreq_floor = cfloor_mean
            self.ctimefreq_floor_std = cfloor_std

        self.compute_nmi()

        etime = time.time() - stime
        print '[compute_cross_coherence] elapsed time: %0.2f seconds' % etime

    def to_file(self, fname):
        hf = h5py.File(fname, 'w')
        cgrp = hf.create_group('cross_coherence')
        cgrp['t'] = self.ct
        cgrp['freq'] = self.cfreq
        cgrp['timefreq'] = self.ctimefreq
        cgrp.attrs['t1'] = self.t1
        cgrp.attrs['t2'] = self.t2
        cgrp.attrs['window_size'] = self.window_size
        #cgrp['timefreq_floor'] = self.ctimefreq_floor
        if self.nmi is not None:
            cgrp['nmi'] = self.nmi
        if self.filter is not None:
            cgrp.attrs['filter_freqs'] = self.filter_freqs
            cgrp.attrs['filter'] = self.filter
        if self.principle_components is not None:
            cgrp['principle_components'] = self.principle_components
            cgrp['explained_variance_ratio'] = self.explained_variance_ratio

        hf.close()

    def compute_nmi(self, min_freq=None, max_freq=None):
        if min_freq is None:
            min_freq = 0.0
        if max_freq is None:
            max_freq = self.sample_rate / 2.0        
        
        if self.filter is not None:
            min_freq,max_freq = self.get_filter_freqs()

        if self.ctimefreq is not None:
            df = self.cfreq[1] - self.cfreq[0]
            freq_index = (self.cfreq >= min_freq) & (self.cfreq <= max_freq)
            y = 1.0 - self.ctimefreq[freq_index, :]
            nzy = y > 0.0
            y[nzy] = np.log2(y[nzy])
            self.nmi = -df*y.sum(axis=0)
            del y

    def zscore_and_filter(self, s):
        """
            Z-scores and potentially filters the signal s, depending on self.filter and self.filter_freqs
        """
        sfilt = s - s.mean()
        sfilt /= sfilt.std(ddof=1)
        if self.filter is not None:
            if self.filter.startswith('band'):
                sfilt = bandpass_filter(s, self.sample_rate, low_freq=self.filter_freqs[0], high_freq=self.filter_freqs[1])
            elif self.filter.startswith('low'):
                sfilt = lowpass_filter(s, self.sample_rate, self.filter_freqs[0])
            elif self.filter.startswith('high'):
                sfilt = highpass_filter(s, self.sample_rate, self.filter_freqs[0])
        return sfilt

    def get_filter_freqs(self):
        min_freq = 0.0
        max_freq = self.sample_rate / 2.0
        if self.filter is not None:
            if self.filter.startswith('band'):
                min_freq = self.filter_freqs[0]
                max_freq = self.filter_freqs[1]
            elif self.filter.startswith('high'):
                min_freq = self.filter_freqs[0]
            elif self.filter.startswith('low'):
                max_freq = self.filter_freqs[0]
        return min_freq, max_freq

    def compute_pca(self):

        ctf = self.ctimefreq
        nzctf = ctf > 0.0
        ctf[nzctf] = np.log(ctf[nzctf])
        ctf[~nzctf] = ctf[nzctf].min()

        n_components = ctf.shape[0]

        pca = PCA(n_components=n_components)
        pca.fit(ctf.transpose())

        self.explained_variance_ratio = pca.explained_variance_ratio_
        self.principle_components = pca.components_

    def plot_pca(self, n_components=6):

        plt.figure()
        plt.subplots_adjust(bottom=0.02, top=0.95, right=0.99, left=0.06, hspace=0.30)
        for k in range(n_components):
            evar = self.explained_variance_ratio[k]
            pc = self.principle_components[k, :]

            plt.subplot(n_components, 1, k+1)
            plt.plot(self.cfreq, pc, 'r-', linewidth=2.0)
            plt.axhline(0.0, color='k')
            #plt.xlabel('Frequency (Hz)')
            plt.title('PC %d, explained variance=%0.3f' % (k, evar))
            plt.axis('tight')
            plt.ylim([-1.0, 1.0])

    def plot(self, start_time=None, end_time=None, colormap=cm.jet, hide_xaxis=False, font_size=None, nmi_max=None, swap=False):

        if font_size is not None:
            rcParams.update({'font.size': font_size})

        t1 = self.t1
        if start_time is not None:
            t1 = start_time

        t2 = self.t2
        if end_time is not None:
            t2 = end_time

        i1 = int(t1 * self.sample_rate)
        i2 = int(t2 * self.sample_rate)
        d = i2 - i1

        t = (np.arange(0, d) / self.sample_rate) + t1

        #get the stimulus spectrogram
        stim_spec_t,stim_spec_f,stim_spec = self.protocol.compute_stimulus_spectrogram(t1, t2)

        #preprocess the LPF
        zlfp1 = self.lfp1[i1:i2]*1e6
        zlfp1 = self.zscore_and_filter(zlfp1)

        zlfp2 = self.lfp2[i1:i2]*1e6
        zlfp2 = self.zscore_and_filter(zlfp2)

        if swap:
            zcpy = copy.copy(zlfp1)
            zlfp1 = zlfp2
            zlfp2 = zcpy

        #compute LFP spectrograms if it hasn't been done already
        if self.tf1 is None:
            self.spec_t1,self.spec_freq1,self.tf1,spec_rms1 = \
                gaussian_stft(zlfp1, self.sample_rate, 0.050, 1.0 / self.sample_rate)
        if self.tf2 is None:
            self.spec_t2,self.spec_freq2,self.tf2,spec_rms2 = \
                gaussian_stft(zlfp2, self.sample_rate, 0.050, 1.0 / self.sample_rate)

        #get properties of stuff
        min_freq = 3.0
        max_freq = 120.0
        if self.filter is not None:
            min_freq, max_freq = self.get_filter_freqs()
        ## first figure, raw LFPs and their cross coherence
        #plot stimulus amplitude envelope
        fig = plt.figure()
        plt.subplots_adjust(bottom=0.06, top=0.99, right=0.99, left=0.06)
        title = '%d and %d' % (self.e1.number, self.e2.number)
        if self.file_name is not None:
            title += ' %s' % self.file_name
        fig.canvas.set_window_title(title)

        gs = gridspec.GridSpec(6, 1)

        ax = plt.subplot(gs[0, 0])
        plot_spectrogram(stim_spec_t, stim_spec_f/1000.0, stim_spec, ax=ax, colormap=cm.gist_yarg, colorbar=False, fmin=0.3, fmax=8.0)
        plt.ylabel('Sound\nFreq (kHz)', multialignment='center')
        plt.axis('tight')
        plt.xlabel('')
        if hide_xaxis:
            plt.xticks([])

        enum1 = self.e1.number
        enum2 = self.e2.number
        if swap:
            enum2 = self.e1.number
            enum1 = self.e2.number

        #plot LFPs
        ax = plt.subplot(gs[1, 0])
        #plt.plot(t, zlfp1, 'k-', linewidth=2)
        lfp_spec1 = postprocess_spectrogram(np.abs(self.tf1))
        plot_spectrogram(self.spec_t1 + start_time, self.spec_freq1, lfp_spec1, ax=ax, colormap=cm.afmhot_r, colorbar=False)
        plt.ylabel('LFP\nFreq (Hz)', multialignment='center')
        plt.axis('tight')
        plt.xlabel('')
        dur = end_time - start_time
        plt.text(start_time + 0.90*dur, 150.0, 'Electrode %d' % enum1, color='k')
        if hide_xaxis:
            plt.xticks([])

        ax = plt.subplot(gs[2, 0])
        #plt.plot(t, zlfp2, 'k-', linewidth=2)
        lfp_spec2 = postprocess_spectrogram(np.abs(self.tf2))
        plot_spectrogram(self.spec_t2 + start_time, self.spec_freq2, lfp_spec2, ax=ax, colormap=cm.afmhot_r, colorbar=False)
        plt.ylabel('LFP\nFreq (Hz)', multialignment='center')
        plt.axis('tight')
        plt.text(start_time + 0.90*dur, 150.0, 'Electrode %d' % enum2, color='k')
        plt.xlabel('')
        if hide_xaxis:
            plt.xticks([])

        #plot the cross-coherence
        plt.subplot(gs[3:5, 0])
        cdt = self.ct[1] - self.ct[0]
        dur = t2 - t1
        ci1 = np.where(self.ct >= start_time)[0].min()
        ci2 = np.where(self.ct <= end_time)[0].max()
        if self.filter is None:
            findex = self.cfreq >= 0.0
        else:
            findex = (self.cfreq >= min_freq) & (self.cfreq <= max_freq)
        fmin = self.cfreq[findex].min()
        fmax = self.cfreq[findex].max()
        print 'fmin=%0.3f, fmax=%0.3f' % (fmin, fmax)
        print 'cdt=%0.6f, ci1=%d, ci2=%d, findex.sum()=%d' % (cdt, ci1, ci2, findex.sum())
        print 'self.ctimefreq.shape=',self.ctimefreq.shape

        ctf = self.ctimefreq[findex, ci1:ci2]
        plt.imshow(ctf, interpolation='nearest', aspect='auto', extent=[start_time, end_time, fmin, fmax],
                   origin='lower', cmap=cm.jet, vmin=0.0, vmax=1.0)
                   #origin='lower', cmap=cm.jet, vmax=0.0)
        plt.ylabel('Coherence\nFreq (Hz)', multialignment='center')
        plt.axis('tight')
        #plt.colorbar()
        plt.xlabel('')
        if hide_xaxis:
            plt.xticks([])

        #plot the principle components
        """
        topn = 6
        proj_pcs = np.dot(ctf.transpose(), self.principle_components[:, :topn])
        t_proj = np.arange(proj_pcs.shape[0])*cdt
        plt.subplot(gs[6:8, 0])
        plt.axhline(0.0, color='k')
        for k in range(topn):
            plt.plot(t_proj, proj_pcs[:, k], linewidth=2.0)
        plt.legend(['%d' % x for x in range(topn)])
        plt.ylabel('PC Projections')
        plt.axis('tight')
        """

        """
        #plot the noise floor
        plt.subplot(gs[5:7, 0])
        ctf_floor = self.ctimefreq_floor[findex, ci1:ci2]
        #nzctf = ctf > 0.0
        #ctf[nzctf] = 20*np.log10(ctf[nzctf])
        #ctf[~nzctf] = ctf[nzctf].min()
        plt.imshow(ctf_floor, interpolation='nearest', aspect='auto', extent=[0, dur, fmin, fmax],
                   origin='lower', cmap=cm.jet, vmin=0.0, vmax=1.0)
        plt.ylabel('Frequency (Hz)')
        plt.axis('tight')
        """

        #plot the NMI
        t_nmi = np.arange(len(self.nmi))*cdt
        plt.subplot(gs[5, 0])
        plt.plot(t_nmi[ci1:ci2], self.nmi[ci1:ci2], 'k-', linewidth=2.0)
        plt.ylabel('NMI (bits/s)')
        plt.xlabel('Time (s)')
        plt.axis('tight')
        if nmi_max is not None:
            plt.ylim([0.0, nmi_max])

    @classmethod
    def from_file(cls, f, fname, root_dir='/auto/k8/fdata/mschachter/analysis/cc'):

        fpath = os.path.join(root_dir, fname)
        if not os.path.exists(fpath):
            print 'electrode file not found: %s' % fpath
            return None

        m = re.match(r'cc_(\w+)_(\w+)_(\d+)_(\d+)_?(.*).h5', fname)
        site_name, protocol_name, e1, e2 = m.group(1), m.group(2), int(m.group(3)), int(m.group(4))
        #print 'protocol_name=%s, site_name=%s, site_id=%d, e1=%d, e2=%d' % (protocol_name, site_name, site_id, e1, e2)

        s = Site(f)
        assert s.from_name(site_name)
        emap = {key: value for (key, value) in [(e.number,e) for e in s.electrodes]}
        assert e1 in emap
        assert e2 in emap
        electrode1 = emap[e1]
        electrode2 = emap[e2]

        ec = ElectrodeComparator(f, protocol_name, electrode1, electrode2)

        #print 'Opening file %s' % fpath
        hf = h5py.File(fpath, 'r')
        cgrp = hf['cross_coherence']
        ec.ct = np.array(cgrp['t'])
        ec.cfreq = np.array(cgrp['freq'])
        ec.ctimefreq = np.array(cgrp['timefreq'])
        if 'timefreq_floor' in cgrp:
            ec.ctimefreq_floor = np.array(cgrp['timefreq_floor'])
        ec.t1 = float(cgrp.attrs['t1'][()])
        ec.t2 = float(cgrp.attrs['t2'][()])
        ec.window_size = float(cgrp.attrs['window_size'][()])
        ec.file_name = fname
        if 'filter' in cgrp.attrs:
            ec.filter = cgrp.attrs['filter']
            ec.filter_freqs = np.array(cgrp.attrs['filter_freqs'])
        else:
            ec.filter = None
            ec.filter_freqs = None

        if 'principle_components' in cgrp:
            ec.principle_components = np.array(cgrp['principle_components'])
            ec.explained_variance_ratio = np.array(cgrp['explained_variance_ratio'])

        hf.close()

        ec.compute_nmi()

        return ec


def get_all_pair_files(site_id=1, protocol_name='Call1', num_electrodes=32, edesc=None):
    pairs = dict()
    for i in range(num_electrodes):
        for j in range(num_electrodes):
            if i == j:
                continue
            p = [i+1, j+1]
            p.sort()
            pairs[tuple(p)] = True

    all_files = list()
    for e1,e2 in pairs.keys():
        ofile = get_cc_output_file_name(e1, e2, site_id, protocol_name, edesc)
        all_files.append( (e1, e2, ofile) )
    return all_files


def cluster_pcs(site_id, protocol_name, output_dir='/auto/k8/fdata/mschachter/analysis/cc', topn=5):
    """
        Examine PCs across all pairs and plot their statistics.
    """

    all_pcs = list()

    all_pairs = get_all_pair_files(site_id, protocol_name)

    #read the principle components and explained variance from each file
    for e1,e2,pfile in all_pairs:

        ppath = os.path.join(output_dir, pfile)
        hf = h5py.File(ppath, 'r')
        cgrp = hf['cross_coherence']
        pcs = np.array(cgrp['principle_components'])
        evar = np.array(cgrp['explained_variance_ratio'])
        hf.close()

        for k in range(topn):
            all_pcs.append( ((e1, e2), k, evar[k], pcs[k, :].squeeze()) )

    print 'Read a total of %d PCs' % len(all_pcs)

    #create a matrix of principle components
    X = np.array([x[3] for x in all_pcs])

    #compute a graph between all the principle components with weights between nodes set by the absolute value of
    #the correlation coefficient
    tot_num_pcs = X.shape[0]
    print 'Computing distance graph for %d PCs...' % tot_num_pcs
    D = np.zeros([tot_num_pcs, tot_num_pcs])
    #[g.add_node(k) for k in range(tot_num_pcs)]
    for k in range(tot_num_pcs):
        for j in range(k):
            C = np.corrcoef(X[k, :], X[j, :])
            #print 'k=%d, j=%d, D.shape=%s' % (k, j, str(D.shape))
            #print 'C.shape=',C.shape
            #g.add_edge(k, j, weight=np.abs(C[0, 1]))
            D[k, j] = np.abs(C[0, 1])
            D[j, k] = D[k, j]

    #plot a histogram of explained variance for each group
    plt.figure()
    plt.suptitle('Explained Variance by PC')
    for k in range(topn):
        explained_variance = np.array([x[2] for x in all_pcs if x[1] == k])

        #plt.subplot(topn, 1, k+1)
        plt.hist(explained_variance, bins=50)
        plt.xlim([0.0, 1.0])
        #plt.ylabel('PC %d' % k)
    plt.legend(['%d' % k for k in range(topn)])

    #plot some samples from each group
    plt.figure()
    plt.suptitle('PC Samples')
    for k in range(topn):
        #plot a random sample of 50 pcs
        grp_pcs = np.array([x[3] for x in all_pcs if x[1] == k])
        rand_index = range(len(grp_pcs))
        np.random.shuffle(rand_index)

        #plot PC
        plt.subplot(topn, 2, 2*k+1)
        df = (381.0 / 2.0) / grp_pcs.shape[1]
        freq = np.arange(grp_pcs.shape[1])*df #TODO: this is a hack
        plt.axhline(0.0, color='k')
        for pci in rand_index[:50]:
            pc = grp_pcs[pci, :]
            plt.plot(freq, pc)
        plt.axis('tight')

        #plot FFT of PC
        plt.subplot(topn, 2, 2*k+2)
        for pci in rand_index[:50]:
            pc = grp_pcs[pci, :]
            pc_freq,pc_ps = power_spectrum(pc, 1.0, log=True)
            findex = pc_freq > 0.0
            plt.plot(pc_freq[findex], pc_ps[findex])
        plt.ylabel('|FT{PC %d}|' % k)
        plt.axis('tight')

    #plot correlation coefficients histograms per group
    for k in range(topn):

        #get the indices into D for PCs that capture the kth most variance
        kgrp = [m for m,x in enumerate(all_pcs) if x[1] == k]

        plt.figure()
        plt.suptitle('Waveform Distance for PC %d' % k)
        for j in range(topn):

            dvals = list()
            jgrp = [m for m,x in enumerate(all_pcs) if x[1] == j]
            for kk in kgrp:
                for jj in jgrp:
                    if kk != jj:
                        dvals.append(D[kk, jj])
            dvals = np.array(dvals)

            #plot histogram for distance between groups k and j
            plt.subplot(topn, 1, j+1)
            plt.hist(dvals, bins=50, color='r')
            plt.ylabel('%d vs %d' % (k, j))
            plt.xlim([0.0, 1.0])


def compute_cc(f, site_id, protocol_name, electrode1, electrode2, output_dir,
               spec_window_size=0.100, coherence_window_size=0.150,
               t1=None, t2=None, edesc=None, num_shuffles=200):

    stime = time.time()
    ofile = get_cc_output_file_name(electrode1, electrode2, site_id, protocol_name, edesc)
    output_file = os.path.join(output_dir, ofile)

    if os.path.exists(output_file):
        print 'Quitting, file %s already exists!' % output_file
        return

    s = Site(f)
    assert s.from_id(site_id)

    emap = {key: value for (key, value) in [(e.number,e) for e in s.electrodes]}

    assert electrode1 in emap
    assert electrode2 in emap

    ec = ElectrodeComparator(f, protocol_name, emap[electrode1], emap[electrode2])
    ec.compute_cross_coherence(spec_window_size=spec_window_size, coherence_window_size=coherence_window_size,
                               increment=None, t1=t1, t2=t2, use_window=True, num_floor_shuffles=num_shuffles,
                               save_memory=True)
    ec.compute_nmi()

    etime = time.time() - stime
    print 'Elapsed time for coherence: %d seconds' % etime

    """
    print 'Computing principle components'
    stime = time.time()
    ec.compute_pca()
    etime = time.time() - stime
    print 'Elapsed time for PCA: %d seconds' % etime
    """

    print 'Writing output to %s' % output_file
    ec.to_file(output_file)


def get_electrode_cross_coherence(f, site_id, protocol_name, output_dir, window_size=1.0, increment=0.250, bandwidth=10.0, t1=None, t2=None, edesc=None, filter=None, filter_freqs=None, overwrite_ccs=False):

    '''Retrieves objects containing the cross-coherograms between all pairs of electrodes from the specified site.

    Important Inputs:
    overwrite_ccs - If False, cross-coherence data will be loaded if they have already been computed

    Output:
    electrode_comparisons - dictionary of ElectrodeComparator objects, with each comparison between electrodes e1 and e2. The keys of the dictionary are 'e1.number_e2.number'.

    ElectrodeComparator - Important values include: ct - the time indicies, cfreq - the frequencies being correlated, ctimefreq - the cross-coherogram, nmi - the normal mutual information. '''


    # Grab the requested site
    s = Site(f)
    assert s.from_id(site_id)

    electrode_comparisons = dict()
    for ii, e1 in enumerate(s.electrodes):

        for jj, e2 in enumerate(s.electrodes[ii+1:]):

            # Check if file exists
            output_file = get_cc_output_file_name(e1.number, e2.number, site_id, protocol_name, edesc)

            if os.path.exists(os.path.join(output_dir, output_file)):
                if overwrite_ccs:
                    print "Removing file from %s" % os.path.join(output_dir, output_file)
                    os.remove(os.path.join(output_dir, output_file))

                    print "Computing cross-coherence for electrodes %d and %d" % (e1.number, e2.number)
                    compute_cc(f, site_id, protocol_name, e1.number, e2.number, output_dir, window_size, increment, bandwidth, t1, t2, edesc, filter, filter_freqs)

            else:
                print "Computing cross-coherence for electrodes %d and %d" % (e1.number, e2.number)
                compute_cc(f, site_id, protocol_name, e1.number, e2.number, output_dir, window_size, increment, bandwidth, t1, t2, edesc, filter, filter_freqs)

            # load the cross-coherence data
            print "Loading cross-coherence data from %s" % os.path.join(output_dir, output_file)
            ec = ElectrodeComparator.from_file(f, output_file, root_dir=output_dir)
            ec.compute_nmi()

            dict_key = "%d_%d" % (e1.number, e2.number)
            electrode_comparisons[dict_key] = ec

    return electrode_comparisons


def get_cc_output_file_name(electrode1, electrode2, site_id, protocol_name, edesc):
    evals = [electrode1, electrode2]
    evals.sort()
    if edesc is None:
        gname = 'cc_Site%d_%s_%d_%d.h5' % (site_id, protocol_name, evals[0], evals[1])
    else:
        gname = 'cc_Site%d_%s_%d_%d_%s.h5' % (site_id, protocol_name, evals[0], evals[1], edesc)
    return gname


if __name__ == '__main__':

    f = tables.open_file('/auto/k8/fdata/mschachter/tdt_continuous/GreBlu9508M/GreBlu9508M_tabled.h5', 'r')

    site = Site(f)
    assert site.from_id(2)

    emap = {e.number:e for e in site.electrodes}
    protocol = 'Call2'

    e1 = emap[21]
    e2 = emap[22]
    fname = 'cc_Site%d_%s_%d_%d.h5' % (site.id, protocol, e1.number, e2.number)

    t1 = 733.0
    t2 = 745.0
    #t1 = 1000.0
    #t2 = 2000.0
    #t1 = None
    #t2 = None

    #code to compute the cross coherence from scratch:

    #ec = ElectrodeComparator(f, protocol, e1, e2)
    #ec.compute_cross_coherence(spec_window_size=0.100, coherence_window_size=0.200, increment=None,
    #                           use_window=True, t1=t1, t2=t2, num_floor_shuffles=150, save_memory=False)
    #ec.plot(start_time=t1, end_time=t2)
    #plt.show()
    #ec.to_file(os.path.join('/tmp', fname))

    #code to compute the phase difference from scratch:

    #ec.compute_phase_diff(spec_window_size=0.100, coherence_window_size=0.200, increment=None,
    #                       use_window=True, t1=t1, t2=t2, num_floor_shuffles=150)
    #ec.plot_phase_diff(start_time=t1, end_time=t2)
    #plt.show()

    #code to read and plot the coherence data from a file:

    ec = ElectrodeComparator.from_file(f, fname, root_dir='/auto/k8/fdata/mschachter/analysis/cc')
    ec.plot(start_time=t1, end_time=t2, font_size=18, nmi_max=160.0, swap=True)
    plt.show()

    #code to compute the cross coherence and write it to a file:

    #compute_cc(f, site_id=1, protocol_name=protocol, electrode1=e1.number, electrode2=e2.number, output_dir='/tmp',
    #           spec_window_size=0.100, coherence_window_size=0.200, num_shuffles=10)

    #f.close()

    #cluster_pcs(1, 'Call1', topn=6)
    #plt.show()
