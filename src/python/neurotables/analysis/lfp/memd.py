import os
import time
import h5py
import husl
from matplotlib.gridspec import GridSpec,rcParams
from sklearn.decomposition import PCA
from scipy.fftpack import fft, fftfreq
from scipy.interpolate import interp1d
from scipy.ndimage import gaussian_filter1d
from sklearn.covariance import GraphLassoCV
from lasp.memd import memd
from lasp.signal import another_hilbert, lowpass_filter, compute_instantaneous_frequency,demodulate

from neurotables.analysis.lfp.decomposition import get_bandpassed_lfp
from neurotables.analysis.lfp.objects import *


def run_memd(f, site_id, protocol_name, hemispheres=['L'], start_time=None, end_time=None, output_file=None,
             nimfs=6, num_noise_channels=0, num_samps=100, resolution=1.0, max_iterations=30, rseed=None):
    """Run the multivariate empirical mode decomposition (MEMD) on multi-electrode LFPs.

       Args:
        f (pytables file object): the pytables file to work with

        site_id (int): the site idd /au k

        protocol_name (str): the name of the protocol used within the site

        hemishperes (array): the hemispheres to include in the MEMD, possible values are ['L', 'R']

        start_time (float): the start time within the protocol of the LFP (seconds). If no time is specified, defaults to 0.0.

        end_time (float): the end time within the protocol of the LFP (seconds). If no time is given, defaults to end time of the protocol.

        output_file (str): absolute file path of the hdf5 that the resultant IMFs are written to.

        nimfs (int): the number of IMFs to find. Defaults to 6.

        num_noise_channels (int): if nonzero, the number of white noise channels to add to the signal before finding the IMFs. This
        method is called "Noise assisted MEMD" and is documented in Rehman and Mandic (2011). Defaults to zero.

        num_samps (int): the number of samples used in computing the mean envelope for the sifting process. Defaults to 100.

       Returns:

        imfs (np.ndarray): The IMFs, with dimensions NxT, where N is the number of electrodes, T is the number of time points.
        index2electrode (list): maps an electrode index from 0 to N-1 to an electrode number.
        sample_rate (float): the sample rate of the LFP in Hz.
    """

    #get site
    site = Site(f)
    assert site.from_id(site_id)
    site = site

    #get protocol
    plist = [p for p in site.protocols if p.name == protocol_name]
    assert len(plist) == 1
    protocol = plist[0]

    #get electrode
    elist = [e for e in site.electrodes if e.hemisphere in hemispheres]
    elist.sort(key=operator.attrgetter('number'))
    electrodes = elist

    #get coordinates
    electrode_geometry = site.electrodes[0].geometry
    coords = electrode_geometry.coordinates
    nrows,ncols = coords.shape

    #peek into the first LFP to get the movie end time
    lfp_pr = electrodes[0].get_lfp_protocol_response(protocol.name)
    sample_rate = lfp_pr.lfp.sample_rate
    lfp = lfp_pr.waveform
    duration = len(lfp) / sample_rate
    del lfp

    #set start and end times of the LFP
    t1 = 0.0
    t2 = duration
    if start_time is not None:
        t1 = start_time
    if end_time is not None:
        t2 = end_time

    #get LFPs
    start_index = int(t1*sample_rate)
    end_index = int(t2*sample_rate)
    lfps = np.zeros([len(electrodes), end_index-start_index])
    index2electrode = list()
    for k,electrode in enumerate(electrodes):
        index2electrode.append(electrode.number)
        lfp,sample_rate = get_bandpassed_lfp(f, electrode.number,
                                             site_id=site_id, protocol_name=protocol_name,
                                             low_freq=None, high_freq=None)
        lfps[k, :] = lfp[start_index:end_index]

    #set random seed
    if rseed is not None:
        np.random.seed(rseed)

    #perform MEMD on the electrodes
    stime = time.time()
    imfs = memd(lfps, nimfs, nsamps=num_samps, resolution=resolution,
                max_iterations=max_iterations, num_noise_channels=num_noise_channels)
    etime = time.time() - stime
    print 'MEMD elapsed time: %d seconds' % int(etime)

    if output_file is not None:
        hf = h5py.File(output_file, 'w')
        hf['imfs'] = imfs
        hf.attrs['num_samples'] = num_samps
        hf.attrs['num_noise_channels'] = num_noise_channels
        hf.attrs['nimfs'] = nimfs
        hf.attrs['hemispheres'] = hemispheres
        hf.attrs['index2electrode'] = index2electrode
        hf.attrs['site_id'] = site_id
        hf.attrs['protocol_name'] = protocol_name
        hf.attrs['start_time'] = start_time
        hf.attrs['end_time'] = end_time
        hf.attrs['resolution'] = resolution
        hf.attrs['max_iterations'] = max_iterations
        hf.attrs['sample_rate'] = sample_rate
        hf.close()

    return imfs, index2electrode, sample_rate


class IMFPlotter(object):

    def __init__(self, f):

        self.f = f

        self.imfs = None
        self.site_id = None
        self.protocol_name = None

        self.num_samples = None
        self.num_noise_channels = None
        self.nimfs = None
        self.hemispheres = None
        self.index2electrode = None
        self.start_time = None
        self.end_time = None
        self.resolution = None
        self.max_iterations = None
        self.sample_rate = None

    def load_data(self):
        if self.site_id is None or self.protocol_name is None:
            return

        self.site = Site(self.f)
        assert self.site.from_id(self.site_id)

        #get protocol
        plist = [p for p in self.site.protocols if p.name == self.protocol_name]
        assert len(plist) == 1
        self.protocol = plist[0]

        #get electrode
        elist = [e for e in self.site.electrodes if e.hemisphere in self.hemispheres]
        elist.sort(key=operator.attrgetter('number'))
        self.electrodes = elist

        #get coordinates
        electrode_geometry = self.site.electrodes[0].geometry
        self.coords = electrode_geometry.coordinates

        #peak into first electrode to determine length of recording
        lfp_pr = elist[0].get_lfp_protocol_response(self.protocol.name)
        self.sample_rate = lfp_pr.lfp.sample_rate

        lfp = lfp_pr.waveform
        self.lfp_len = len(lfp)
        self.duration = self.lfp_len / self.sample_rate
        del lfp

        #get stimulus times and properties
        events = [e for e in self.protocol.events if e.start_time >= self.start_time and e.end_time <= self.end_time]
        self.stim_list = [(e.sound.id, e.sound.stim_type, e.sound.call_id, e.start_time, e.end_time) for e in events]


    @classmethod
    def from_file(cls, f, file_path, load_data=True, phase_file=None):

        p = IMFPlotter(f)

        hf = h5py.File(file_path, 'r')
        p.site_id = hf.attrs['site_id']
        p.protocol_name = hf.attrs['protocol_name']
        p.num_samples = hf.attrs['num_samples']
        p.num_noise_channels = hf.attrs['num_noise_channels']
        p.nimfs = hf.attrs['nimfs']
        p.hemispheres = hf.attrs['hemispheres']
        p.index2electrode = hf.attrs['index2electrode']
        p.start_time = hf.attrs['start_time']
        p.end_time = hf.attrs['end_time']
        p.resolution = hf.attrs['resolution']
        p.max_iterations = hf.attrs['max_iterations']
        p.sample_rate = hf.attrs['sample_rate']

        if load_data:
            p.imfs = np.array(hf['imfs'])
            p.load_data()
        hf.close()

        if phase_file is not None:
            hf = h5py.File(phase_file, 'r')
            p.imf_phase_resids = np.array(hf['phase_resids'])
            p.imf_amplitudes = np.array(hf['amplitudes'])
            p.imf_pcs = np.array(hf['pcs'])
            p.imf_projections = np.array(hf['projections'])
            hf.close()

        return p


    def compute_complex_imfs(self, start_time=None, end_time=None, output_file=None):
        """
            Compute the instantaneous frequencies of each IMF.
        """

        if start_time is None:
            start_time = self.start_time
        if end_time is None:
            end_time = self.end_time

        t1 = int((start_time-self.start_time)*self.sample_rate)
        t2 = int((end_time-self.start_time)*self.sample_rate)
        d = t2 - t1
        print 'self.start_time=%0.3f, self.end_time=%0.3f, start_time=%0.3f, end_time=%0.3f, t1=%d, t2=%d' % \
              (self.start_time, self.end_time, start_time, end_time, t1, t2)

        nimfs,nelectrodes,nt = self.imfs.shape

        imf_analytic = np.zeros([nimfs, nelectrodes, d], dtype='complex')
        imf_amplitudes = np.zeros([nimfs, nelectrodes, d])
        imf_phases = np.zeros([nimfs, nelectrodes, d])
        imf_ifreqs = np.zeros([nimfs, nelectrodes, d])

        for k in range(nimfs):
            for j in range(nelectrodes):
                print 'Computing analytic signal for IMF %d, electrode %d' % (k+1, j)
                imf = self.imfs[k, j, t1:t2].squeeze()
                z = another_hilbert(imf)
                imf_amplitudes[k, j, :] = np.abs(z)
                imf_phases[k, j, :] = np.angle(z)
                imf_analytic[k, j, :] = z

                #identify all the zero crossings
                #imf_sign = np.sign(imf)
                #zci = np.r_[False, np.abs(imf_sign[:-1] - imf_sign[1:]) > 0]

                #unwrap the phase of the analytic signal
                #uw_phase = np.unwrap(np.angle(z))

                #compute the raw instantaneous frequency
                #ifreq = (np.diff(uw_phase) * self.sample_rate) / (2*np.pi)
                #ifreq_avg = ifreq.mean()
                #ifreq = np.r_[ifreq_avg, ifreq]

                ifreq = compute_instantaneous_frequency(z, self.sample_rate)

                #rectify the inst freq
                ifreq[ifreq < 0.0] = 0.0

                #smooth the inst freq
                ifreq = lowpass_filter(ifreq, self.sample_rate, 50.0)

                imf_ifreqs[k, j, :] = ifreq

                """
                plt.figure()
                plt.suptitle('IMF %d, Electrode %d' % (k+1, self.index2electrode[j]))

                plt.subplot(4, 1, 1)
                plt.plot(t, imf, 'k-', linewidth=2.0)
                plt.ylabel('IMF')
                plt.axis('tight')

                plt.subplot(4, 1, 2)
                plt.plot(t, imf_amplitudes[k, j, :].squeeze(), 'b-', linewidth=2.0)
                plt.ylabel('Amplitude')
                plt.axis('tight')

                plt.subplot(4, 1, 3)
                plt.plot(t, imf_phases[k, j, :].squeeze(), 'r-', linewidth=2.0)
                plt.ylabel('Phase')
                plt.axis('tight')

                plt.subplot(4, 1, 4)
                plt.plot(t, imf_ifreqs[k, j, :].squeeze(), 'g-', linewidth=2.0)
                plt.ylabel('Inst. Freq.')
                plt.axis('tight')

                plt.show()
                """

        self.imf_analytic = imf_analytic
        self.imf_ifreqs = imf_ifreqs
        self.imf_phases = imf_phases
        self.imf_amplitudes = imf_amplitudes

        #compute PCA across space
        imf_phase_resids = list()
        imf_projections = list()
        imf_pcs = list()
        for k in range(nimfs):
            phase_resids,pcs = demodulate(self.imf_analytic[k, :, :].squeeze(), over_space=True)

            imf_phase_resids.append(phase_resids)
            imf_pcs.append(pcs)

            proj = np.dot(self.imf_analytic[k, :, :].squeeze().T, pcs[0, :].squeeze())
            imf_projections.append(proj)

        del self.imf_analytic

        self.imf_phase_resids = np.array(imf_phase_resids)
        self.imf_pcs = np.array(imf_pcs)

        if output_file is not None:
            hf = h5py.File(output_file, 'w')
            hf.attrs['num_samples'] = d
            hf.attrs['num_noise_channels'] = self.num_noise_channels
            hf.attrs['nimfs'] = self.nimfs
            hf.attrs['hemispheres'] = self.hemispheres
            hf.attrs['index2electrode'] = self.index2electrode
            hf.attrs['site_id'] = self.site_id
            hf.attrs['protocol_name'] = self.protocol_name
            hf.attrs['start_time'] = self.start_time
            hf.attrs['end_time'] = self.end_time
            hf.attrs['resolution'] = self.resolution
            hf.attrs['max_iterations'] = self.max_iterations
            hf.attrs['sample_rate'] = self.sample_rate

            hf['phase_resids'] = self.imf_phase_resids
            hf['pcs'] = self.imf_pcs
            hf['amplitudes'] = self.imf_amplitudes
            hf['projections'] = np.array(imf_projections)

            hf.close()

        #self.imf_phase_resids2 = np.array(imf_phase_resids2)
        #self.imf_pcs2 = np.array(imf_pcs2)


    def plot_complex_imfs(self, start_time, end_time, output_dir=None):
        """
        #zscore the matrix
        Xmean = X.mean(axis=1)
        Xstd = X.std(axis=1, ddof=1)
        X = (X.T - Xmean).T
        X = (X.T / Xstd).T

        cov_est = GraphLassoCV()
        cov_est.fit(X.T)

        C = cov_est.covariance_

        plt.figure()
        im = plt.imshow(C, interpolation='nearest', aspect='auto', vmin=-1.0, vmax=1.0, cmap=cm.seismic)
        plt.colorbar(im)
        plt.title('Covariance Matrix for IMF %d' % (k+1))
        plt.axis('tight')
        """

        #phase residual histogram
        #pr = self.imf_phase_resids.ravel()
        #pr[pr < 0.0] += 2*np.pi
        #plt.figure()
        #plt.hist(pr, bins=20)
        #plt.title('Phase Residuals')
        #plt.show()

        nimfs,nelectrodes,nt = self.imfs.shape

        t1 = int((start_time-self.start_time)*self.sample_rate)
        t2 = int((end_time-self.start_time)*self.sample_rate)
        d = t2 - t1
        t = (np.arange(d)/self.sample_rate) + start_time

        colors = np.array([[244.0, 244.0, 244.0], #white
                           [241.0, 37.0, 9.0],    #red
                           [238.0, 113.0, 25.0],  #orange
                           [255.0, 200.0, 8.0],   #yellow
                           [19.0, 166.0, 50.0],   #green
                           [1.0, 134.0, 141.0],   #blue
                           [244.0, 244.0, 244.0], #white
                          ])
        colors /= 255.0

        #get stimulus spectrogram
        stim_spec_t,stim_spec_freq,stim_spec = self.protocol.compute_stimulus_spectrogram(start_time, end_time)

        ##################
        ## make plots of the joint activity per IMF
        ##################
        rcParams.update({'font.size':10})
        plt.figure(figsize=(24.0, 13.5))
        plt.subplots_adjust(top=0.98, bottom=0.01, left=0.03, right=0.99, hspace=0.10)
        nsubplots = nimfs + 1

        #plot the stimulus spectrogram
        ax = plt.subplot(nsubplots, 1, 1)
        plot_spectrogram(stim_spec_t, stim_spec_freq, stim_spec, ax=ax, colormap=cm.gist_yarg, colorbar=False)
        plt.ylabel('')
        plt.yticks([])

        """
        #plot the instantaneous frequency along with it's amplitude
        for k in range(nimfs):

            img = np.zeros([nelectrodes, d, 4], dtype='float32')

            flat_ifreqs = self.imf_ifreqs[k, :, :].ravel()
            ifreq_min = np.percentile(flat_ifreqs, 5)
            ifreq_max = np.percentile(flat_ifreqs, 95)
            ifreq_dist = ifreq_max - ifreq_min
            print 'ifreq_max=%0.3f, ifreq_min=%0.3f' % (ifreq_max, ifreq_min)

            electrode_order = np.zeros([nelectrodes])
            for j in range(nelectrodes):
                enumber = self.index2electrode[j]
                row,col = np.nonzero(self.coords == enumber)
                remapped_index = row*2 + col
                if 'R' in self.hemispheres:
                    remapped_index -= 16
                electrode_order[remapped_index] = enumber

                max_amp = np.percentile(self.imf_amplitudes[k, j, :], 85)

                #get the instantaneous frequency and amplitude
                ifreq = self.imf_ifreqs[k, j, :].squeeze()
                amp = self.imf_amplitudes[k, j, :].squeeze()

                #set the alpha and color for the bins
                alpha = amp / max_amp
                alpha[alpha > 1.0] = 1.0 #saturate
                alpha[alpha < 0.05] = 0.0 #nonlinear threshold

                cnorm = (ifreq - ifreq_min) / ifreq_dist
                cnorm[cnorm > 1.0] = 1.0
                cnorm[cnorm < 0.0] = 0.0
                img[remapped_index, :, 0] = 1.0 - cnorm
                img[remapped_index, :, 1] = 0.0
                img[remapped_index, :, 2] = cnorm
                img[remapped_index, :, 3] = alpha

            ax = plt.subplot(nsubplots, 1, k+2)
            ax.set_axis_bgcolor('black')
            im = plt.imshow(img, interpolation='nearest', aspect='auto', origin='upper', extent=[t.min(), t.max(), 1, nelectrodes])
            lbls = ['%d' % e for e in electrode_order]
            lbls.reverse()
            plt.yticks(range(nelectrodes), lbls)
            plt.axis('tight')
            plt.ylabel('Electrode')
            plt.title('IMF %d' % (k+1))
        plt.suptitle('Instantaneous Frequency')
        """

        ##################
        ## make plots of the joint demodulated phase per IMF
        ##################
        rcParams.update({'font.size':10})
        plt.figure(figsize=(24.0, 13.5))
        plt.subplots_adjust(top=0.98, bottom=0.01, left=0.03, right=0.99, hspace=0.10)
        nsubplots = nimfs + 1

        #plot the stimulus spectrogram
        ax = plt.subplot(nsubplots, 1, 1)
        plot_spectrogram(stim_spec_t, stim_spec_freq, stim_spec, ax=ax, colormap=cm.gist_yarg, colorbar=False)
        plt.ylabel('')
        plt.yticks([])

        #plot the instantaneous frequency along with it's amplitude
        for k in range(nimfs):

            img = np.zeros([nelectrodes, d, 4], dtype='float32')
            phase_resids = self.imf_phase_resids[k, :, :].squeeze()

            phase_min = phase_resids.min()
            phase_max = phase_resids.max()
            phase_dist = phase_max - phase_min
            print 'k=%d, phase_max=%0.3f, phase_min=%0.3f' % (k, phase_max, phase_min)

            electrode_order = np.zeros([nelectrodes])
            for j in range(nelectrodes):
                enumber = self.index2electrode[j]
                row,col = np.nonzero(self.coords == enumber)
                remapped_index = row*2 + col
                if 'R' in self.hemispheres:
                    remapped_index -= 16
                electrode_order[remapped_index] = enumber

                max_amp = np.percentile(self.imf_amplitudes[k, j, :], 85)

                #get the instantaneous phase residual and amplitude
                amp = self.imf_amplitudes[k, j, t1:t2].squeeze()
                phase = phase_resids[j, t1:t2].squeeze()

                #set the alpha and color for the bins
                alpha = amp / max_amp
                alpha[alpha > 1.0] = 1.0 #saturate
                alpha[alpha < 0.05] = 0.0 #nonlinear threshold

                cnorm = ((180.0 / np.pi) * phase).astype('int')
                for ti in range(d):
                    img[remapped_index, ti, :3] = husl.husl_to_rgb(cnorm[ti], 75.0, 50.0) #use HUSL color space: https://github.com/boronine/pyhusl/tree/v2.1.0
                img[remapped_index, :, 3] = alpha

            ax = plt.subplot(nsubplots, 1, k+2)
            ax.set_axis_bgcolor('black')
            im = plt.imshow(img, interpolation='nearest', aspect='auto', origin='upper', extent=[t.min(), t.max(), 1, nelectrodes])
            lbls = ['%d' % e for e in electrode_order]
            lbls.reverse()
            plt.yticks(range(nelectrodes), lbls)
            plt.axis('tight')
            plt.ylabel('Electrode')
            plt.title('IMF %d' % (k+1))

        plt.suptitle('Demodulated Phase')

        """
        ##################
        ## make plots of the joint demodulated phase per IMF (over time)
        ##################
        rcParams.update({'font.size':10})
        plt.figure(figsize=(24.0, 13.5))
        plt.subplots_adjust(top=0.98, bottom=0.01, left=0.03, right=0.99, hspace=0.10)
        nsubplots = nimfs + 1

        #plot the stimulus spectrogram
        ax = plt.subplot(nsubplots, 1, 1)
        plot_spectrogram(stim_spec_t, stim_spec_freq, stim_spec, ax=ax, colormap=cm.gist_yarg, colorbar=False)
        plt.ylabel('')
        plt.yticks([])

        #plot the instantaneous frequency along with it's amplitude
        for k in range(nimfs):

            img = np.zeros([nelectrodes, d, 4], dtype='float32')
            phase_resids = self.imf_phase_resids2[k, :, :].squeeze()

            phase_min = phase_resids.min()
            phase_max = phase_resids.max()
            phase_dist = phase_max - phase_min
            print 'k=%d, phase_max=%0.3f, phase_min=%0.3f' % (k, phase_max, phase_min)

            electrode_order = np.zeros([nelectrodes])
            for j in range(nelectrodes):
                enumber = self.index2electrode[j]
                row,col = np.nonzero(self.coords == enumber)
                remapped_index = row*2 + col
                if 'R' in self.hemispheres:
                    remapped_index -= 16
                electrode_order[remapped_index] = enumber

                max_amp = np.percentile(self.imf_amplitudes[k, j, :], 85)

                #get the instantaneous phase residual and amplitude
                amp = self.imf_amplitudes[k, j, :].squeeze()
                phase = phase_resids[j, :].squeeze()

                #set the alpha and color for the bins
                alpha = amp / max_amp
                alpha[alpha > 1.0] = 1.0 #saturate
                alpha[alpha < 0.05] = 0.0 #nonlinear threshold

                cnorm = ((180.0 / np.pi) * phase).astype('int')
                for ti in range(d):
                    img[remapped_index, ti, :3] = husl.husl_to_rgb(cnorm[ti], 75.0, 50.0) #use HUSL color space: https://github.com/boronine/pyhusl/tree/v2.1.0
                img[remapped_index, :, 3] = alpha

            ax = plt.subplot(nsubplots, 1, k+2)
            ax.set_axis_bgcolor('black')
            im = plt.imshow(img, interpolation='nearest', aspect='auto', origin='upper', extent=[t.min(), t.max(), 1, nelectrodes])
            lbls = ['%d' % e for e in electrode_order]
            lbls.reverse()
            plt.yticks(range(nelectrodes), lbls)
            plt.axis('tight')
            plt.ylabel('Electrode')
            plt.title('IMF %d' % (k+1))

        plt.suptitle('Demodulated Phase (over time)')
        """
        plt.show()

        if output_dir is not None:
            fname = 'imf_multi_Site%d_%s_%s_start%0.6f_end%0.6f.png' % (self.site_id, self.protocol_name, ','.join(self.hemispheres), start_time, end_time)
            plt.savefig(os.path.join(output_dir, fname))
        else:
            plt.show()

        #make plots of the IMF spectrograms
        subplot_nrows = 8 + 1
        subplot_ncols = len(self.hemispheres)*2
        #plt.figure()
        #plt.subplots_adjust(top=0.95, bottom=0.05, left=0.03, right=0.99, hspace=0.10)

        """
        for k in range(subplot_ncols):
            ax = plt.subplot(subplot_nrows, subplot_ncols, k+1)
            plot_spectrogram(stim_spec_t, stim_spec_freq, stim_spec, ax=ax, colormap=cm.gist_yarg, colorbar=False)
            plt.ylabel('')
            plt.yticks([])
        """

        """
        for j in range(nelectrodes):

            plt.figure(figsize=(24.0, 13.5))
            #plt.subplots_adjust(top=0.95, bottom=0.05, left=0.03, right=0.99, hspace=0.10)

            enumber = self.index2electrode[j]
            row,col = np.nonzero(self.coords == enumber)
            if len(self.hemispheres) > 1:
                sp = (row+1)*subplot_ncols + col + 1
            else:
                sp = (row+1)*subplot_ncols + (col % 2) + 1

            #ax = plt.subplot(subplot_nrows, subplot_ncols, sp)
            gs = GridSpec(100, 1)
            ax = plt.subplot(gs[:20])
            plot_spectrogram(stim_spec_t, stim_spec_freq, stim_spec, ax=ax, colormap=cm.gist_yarg, colorbar=False)
            plt.ylabel('')
            plt.yticks([])

            ax = plt.subplot(gs[20:])
            ax.set_axis_bgcolor('black')

            #get the maximum frequency and set the resolution
            max_freq = self.imf_ifreqs[:, j, :].max().max()
            nf = 150
            df = max_freq / nf

            #create an image to hold the frequencies
            img = np.zeros([nf, self.imf_ifreqs.shape[-1], 4], dtype='float32')

            #fill in the image for each IMF
            for k in range(nimfs):
                max_amp = np.percentile(self.imf_amplitudes[k, j, :], 85)

                #get the instantaneous frequency and amplitude
                ifreq = self.imf_ifreqs[k, j, :].squeeze()
                amp = self.imf_amplitudes[k, j, :].squeeze()

                freq_bin = (ifreq / df).astype('int') - 1
                freq_bin[freq_bin < 0] = 0

                #set the color and alpha for the bins
                alpha = amp / max_amp
                alpha[alpha > 1.0] = 1.0 #saturate
                alpha[alpha < 0.05] = 0.0 #nonlinear threshold

                for m,fbin in enumerate(freq_bin):
                    #print 'm=%d, fbin=%d, colors[k, :].shape=%s' % (m, fbin, str(colors[k, :].shape))
                    img[fbin, m, :3] = colors[k, :]
                    img[fbin, m, 3] = alpha[m]

            #plot the image
            im = plt.imshow(img, interpolation='nearest', aspect='auto', origin='lower', extent=[t.min(), t.max(), 0.0, max_freq])
            plt.ylabel('E%d' % enumber)
            plt.axis('tight')
            plt.ylim(0.0, 140.0)

            if output_dir is not None:
                fname = 'imf_spec_e%d_Site%d_%s_%s_start%0.6f_end%0.6f.png' % (enumber, self.site_id, self.protocol_name, ','.join(self.hemispheres), start_time, end_time)
                plt.savefig(os.path.join(output_dir, fname))
                plt.close('all')
        """

        if output_dir is None:
            plt.show()

    def make_phase_image(self, amp, phase):
        """
            Turns a phase matrix into an image to be plotted with imshow.
        """

        nelectrodes,d = amp.shape
        max_amp = np.percentile(amp, 97)

        img = np.zeros([nelectrodes, d, 4], dtype='float32')

        #set the alpha and color for the bins
        alpha = amp / max_amp
        alpha[alpha > 1.0] = 1.0 #saturate
        alpha[alpha < 0.05] = 0.0 #nonlinear threshold

        cnorm = ((180.0 / np.pi) * phase).astype('int')
        for j in range(nelectrodes):
            for ti in range(d):
                img[j, ti, :3] = husl.husl_to_rgb(cnorm[j, ti], 75.0, 50.0) #use HUSL color space: https://github.com/boronine/pyhusl/tree/v2.1.0

        img[:, :, 3] = alpha

        return img

    def plot_raw_imfs(self, start_time, end_time, imfs_to_plot=None, output_dir=None):

        for sprops in self.stim_list:
            print sprops

        if imfs_to_plot is None:
            imfs_to_plot = range(self.nimfs)

        stim_spec_t,stim_spec_freq,stim_spec = self.protocol.compute_stimulus_spectrogram(start_time, end_time)
        t1 = int((start_time-self.start_time)*self.sample_rate)
        t2 = int((end_time-self.start_time)*self.sample_rate)

        #compute the average power spectrum of each IMF across electrodes
        imf_ps_list = list()
        imf_ps_freq = None
        for n in imfs_to_plot:
            ps = list()
            for k,enumber in enumerate(self.index2electrode):
                imf = self.imfs[n, k, t1:t2].squeeze()
                imf_fft = fft(imf)
                freq = fftfreq(len(imf), d=1.0/self.sample_rate)
                findex = freq > 0.0
                imf_ps = np.real(imf_fft[findex]*np.conj(imf_fft[findex]))
                ps.append(imf_ps)
                imf_ps_freq = freq[findex]
                ps.append(imf_ps)

            imf_ps_list.append(np.array(ps))

        imf_ps = np.array(imf_ps_list)

        #plot the average power spectrums
        plt.figure(figsize=(24.0, 13.5))
        for k,n in enumerate(imfs_to_plot):
            #compute mean across electrodes
            imf_ps_mean = imf_ps[k, :].mean(axis=0)
            imf_ps_mean_filt = gaussian_filter1d(imf_ps_mean, 10)
            nzindex = imf_ps_mean_filt > 0.0
            imf_ps_mean_filt[nzindex] = np.log10(imf_ps_mean_filt)
            #plt.subplot(len(imfs_to_plot), 1, k+1)

            cnorm = float(k) / len(imfs_to_plot)
            a = 0.75*cnorm + 0.25
            c = [cnorm, 1.0 - cnorm, 1.0 - cnorm]
            plt.plot(imf_ps_freq, imf_ps_mean_filt, '-', c=c, linewidth=3.0)
        plt.legend(['%d' % (n+1) for n in imfs_to_plot])
        plt.title("IMF Avgerage Power Spectrum Across Electrodes")
        plt.xlabel('Freq (Hz)')
        plt.ylabel('Power (dB)')
        plt.axis('tight')
        if output_dir is not None:
            fname = 'imf_ps_Site%d_%s_%s_start%0.6f_end%0.6f.png' % (self.site_id, self.protocol_name, ','.join(self.hemispheres), start_time, end_time)
            plt.savefig(os.path.join(output_dir, fname))
            plt.close('all')

        #plot the IMFs
        t = np.arange(t2-t1) / self.sample_rate

        subplot_nrows = 8 + 1
        subplot_ncols = len(self.hemispheres)*2

        for n in imfs_to_plot:
            plt.figure(figsize=(24.0, 13.5))
            plt.subplots_adjust(top=0.95, bottom=0.05, left=0.03, right=0.99, hspace=0.10)
            plt.suptitle('IMF %d' % (n+1))

            for k in range(subplot_ncols):
                ax = plt.subplot(subplot_nrows, subplot_ncols, k+1)
                plot_spectrogram(stim_spec_t, stim_spec_freq, stim_spec, ax=ax, colormap=cm.gist_yarg, colorbar=False)
                plt.ylabel('')
                plt.yticks([])

            for k,enumber in enumerate(self.index2electrode):
                electrode = self.electrodes[k]
                assert electrode.number == enumber

                row,col = np.nonzero(self.coords == enumber)
                if len(self.hemispheres) > 1:
                    sp = (row+1)*subplot_ncols + col + 1
                else:
                    sp = (row+1)*subplot_ncols + (col % 2) + 1

                plt.subplot(subplot_nrows, subplot_ncols, sp)

                imf = self.imfs[n, k, t1:t2].squeeze()
                plt.plot(t, imf, 'k-', linewidth=2.0)
                plt.ylabel('E%d' % enumber)
                plt.axis('tight')

            if output_dir is not None:
                fname = 'imf_raw_Site%d_%s_%s_%d_start%0.6f_end%0.6f.png' % (self.site_id, self.protocol_name, ','.join(self.hemispheres), n+1, start_time, end_time)
                plt.savefig(os.path.join(output_dir, fname))
                plt.close('all')

        if output_dir is None:
            plt.show()


    def plot_stim_cond(self, stim_id, post_stim_time=0.500, demodulated=True, output_dir=None):

        stim_events = [x for x in self.stim_list if x[0] == stim_id]

        nimfs,nelectrodes,nt = self.imfs.shape

        stim_times = np.array([(start_time, end_time) for stim_id, stim_type, call_id, start_time, end_time in stim_events])
        max_stim_time = np.max(stim_times[:, 1] - stim_times[:, 0])
        stim_len = int(max_stim_time*self.sample_rate) + int(post_stim_time*self.sample_rate)

        stim_spec_t,stim_spec_freq,stim_spec = self.protocol.compute_stimulus_spectrogram(stim_times[0][0], stim_times[0][1]+post_stim_time)

        ntrials = len(stim_times)
        phase_by_imf_and_trial = np.zeros([nimfs, nelectrodes, ntrials, stim_len])
        amp_by_imf_and_trial = np.zeros([nimfs, nelectrodes, ntrials, stim_len])
        for k in range(nimfs):

            first_pc_phase = np.angle(self.imf_projections[k, :].squeeze())

            for m,(stim_id, stim_type, call_id, start_time, end_time) in enumerate(stim_events):

                si = int(start_time*self.sample_rate)
                ei = si + stim_len

                for j in range(nelectrodes):
                    enumber = self.index2electrode[j]
                    row,col = np.nonzero(self.coords == enumber)
                    remapped_index = row*2 + col
                    if 'R' in self.hemispheres:
                        remapped_index -= 16

                    phase_by_imf_and_trial[k, remapped_index, m, :] = self.imf_phase_resids[k, j, si:ei]
                    amp_by_imf_and_trial[k, remapped_index, m, :] = self.imf_amplitudes[k, j, si:ei]

                    if not demodulated:
                        phase_by_imf_and_trial[k, remapped_index, m, :] += first_pc_phase[si:ei]

        print 'phase_by_imf_and_trial.shape=',phase_by_imf_and_trial.shape

        tmin = 0.0
        tmax = stim_len / self.sample_rate

        sid = stim_events[0][0]
        stype = stim_events[0][1]

        callid = ''
        if stim_events[0][2] is not None:
            callid = '_%s' % stim_events[0][2]

        ofile_prefix = '%s%s_stim%d' % (stype, callid, sid)

        #plot per trial activity
        for k in range(nimfs):
            nrows = ntrials + 1
            plt.figure(figsize=(24.0, 13.5))
            plt.subplots_adjust(top=0.95, bottom=0.05, left=0.03, right=0.99, hspace=0.10)
            plt.suptitle('IMF %d' % (k+1))

            ax = plt.subplot(nrows, 1, 1)
            plot_spectrogram(stim_spec_t, stim_spec_freq, stim_spec, ax=ax, colormap=cm.gist_yarg, colorbar=False)

            for m in range(ntrials):
                phase = phase_by_imf_and_trial[k, :, m, :].squeeze()
                amp = amp_by_imf_and_trial[k, :, m, :].squeeze()

                img = self.make_phase_image(amp, phase)

                ax = plt.subplot(nrows, 1, m+2)
                ax.set_axis_bgcolor('black')
                im = plt.imshow(img, interpolation='nearest', aspect='auto', origin='upper', extent=[tmin, tmax, 1, nelectrodes])
                plt.axis('tight')
                plt.ylabel('Trial %d' % (m+1))

            if output_dir is not None:
                ofile = os.path.join(output_dir, '%s_imf%d.png' % (ofile_prefix, k+1))
                plt.savefig(ofile)

        #plot trial-averaged activity
        plt.figure(figsize=(24.0, 13.5))
        plt.subplots_adjust(top=0.95, bottom=0.05, left=0.03, right=0.99, hspace=0.10)

        nrows = nimfs + 1

        ax = plt.subplot(nrows, 1, 1)
        plot_spectrogram(stim_spec_t, stim_spec_freq, stim_spec, ax=ax, colormap=cm.gist_yarg, colorbar=False)

        for k in range(nimfs):

            #compute trial-averaged phase and amplitude
            mean_phase = phase_by_imf_and_trial[k, :, :, :].squeeze().mean(axis=1)
            mean_amp = amp_by_imf_and_trial[k, :, :, :].squeeze().mean(axis=1)

            img = self.make_phase_image(mean_amp, mean_phase)

            ax = plt.subplot(nrows, 1, k+2)
            ax.set_axis_bgcolor('black')
            im = plt.imshow(img, interpolation='nearest', aspect='auto', origin='upper', extent=[tmin, tmax, 1, nelectrodes])
            plt.axis('tight')
            plt.ylabel('IMF %d' % (k+1))

        if output_dir is not None:
            ofile = os.path.join(output_dir, '%s_trial_average.png' % ofile_prefix)
            plt.savefig(ofile)
        else:
            plt.show()
        plt.close('all')


if __name__ == '__main__':

    f = tables.open_file('/auto/k8/fdata/mschachter/tdt_continuous/GreBlu9508M/GreBlu9508M_tabled.h5', 'r')

    imfp = IMFPlotter.from_file(f, '/auto/k8/fdata/mschachter/analysis/memd/imfs_Site4_Call1_L.h5',
                                phase_file='/auto/k8/fdata/mschachter/analysis/memd/imfs_Site4_Call1_L_phase.h5')

    t1 = 620.0
    t2 = 625.0
    #t1 = None
    #t2 = None

    #imfp.compute_complex_imfs(t1, t2, output_file='/tmp/imfs_Site4_Call1_R_phase.h5')
    #imfp.plot_complex_imfs(start_time=t1, end_time=t2, output_dir='/tmp/memd')
    #imfp.plot_raw_imfs(t1, t2, output_dir='/tmp/memd')

    for stim_id, stim_type, call_id, start_time, end_time in imfp.stim_list:
        imfp.plot_stim_cond(stim_id, demodulated=True, output_dir='/tmp/memd')

    f.close()
