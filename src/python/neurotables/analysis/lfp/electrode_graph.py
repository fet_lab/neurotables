import operator
import os
from scipy.interpolate import interp1d
from matplotlib.gridspec import GridSpec
from matplotlib.patches import FancyArrowPatch
from mpl_toolkits.mplot3d import proj3d

import networkx as nx
import numpy as np

import matplotlib.pyplot as plt
import matplotlib.cm as cm

import tables
from lasp.sound import WavFile
from neurotables.analysis.lfp.fit_envelope import get_lfp_tensors_by_stim

from neurotables.objects.cells import Site


class ElectrodeGraph(object):

    def __init__(self, num_arrays, num_sites_per_shank, num_rows, num_cols, array_locations, site_depths, row_spacing = 125.0, col_spacing = 250.0):

        self.num_electrodes = num_arrays*num_sites_per_shank*num_rows*num_cols
        self.array_locations = array_locations
        self.site_depths = site_depths
        self.row_spacing = row_spacing
        self.col_spacing = col_spacing
        self.num_arrays,self.num_sites_per_shank,self.num_rows,self.num_cols = num_arrays, num_sites_per_shank, num_rows, num_cols

        #set up the nodes of an undirected graph representing the electrode grid
        self.g = nx.Graph()

        #initialize the 3D location of each node
        self.init_nodes_and_locations()
        
        #initialize the connections in the graph
        self.init_connections()
                
    def init_nodes_and_locations(self):
        self.X = np.zeros([self.num_arrays, self.num_sites_per_shank, self.num_rows, self.num_cols])
        self.Y = np.zeros([self.num_arrays, self.num_sites_per_shank, self.num_rows, self.num_cols])
        self.Z = np.zeros([self.num_arrays, self.num_sites_per_shank, self.num_rows, self.num_cols])
        for k in range(self.num_arrays):
            for j in range(self.num_sites_per_shank):
                for row in range(self.num_rows):
                    for col in range(self.num_cols):

                        self.X[k, j, row, col] = self.array_locations[k] + self.col_spacing*col
                        self.Y[k, j, row, col] = row*self.row_spacing
                        self.Z[k, j, row, col] = site_depths[j][k]

                        self.g.add_node( (k, j, row, col), location=[self.X[k, j, row, col], self.Y[k, j, row, col], self.Z[k, j, row, col]])

    def init_connections(self):
        #connect adjacent nodes on the grid
        for arrnum, site, row, col in self.g.nodes():
            #make a list of adjacent neighbors on same site
            neighbors = list()
            if row > 0:
                neighbors.append( (arrnum, site, row-1, col) )
            if row < 7:
                neighbors.append( (arrnum, site, row+1, col) )
            if col % 2 == 0:
                neighbors.append( (arrnum, site, row, col+1) )
            else:
                neighbors.append( (arrnum, site, row, col-1) )

            #find neighbors on sites above and below
            if site > 0:
                neighbors.append( (arrnum, site-1, row, col) )
            if site < self.num_sites_per_shank-1:
                neighbors.append( (arrnum, site+1, row, col) )

            #connect the electrode to it's adjacent neighbors on the same site
            for nbarrnum,nbsite,nbrow,nbcol in neighbors:
                #connect nodes
                self.g.add_edge( (arrnum, site, row, col), (nbarrnum, nbsite, nbrow, nbcol))

    def get_node_value_extrema(self, node_value_name, percentile=99):

        node_vals = np.array([self.g.node[node_id][node_value_name] for node_id in self.g.nodes()])
        absmax = np.percentile(np.abs(node_vals), percentile)
        node_max = absmax
        node_min = -absmax
        return node_min,node_max

    def get_node_locations_and_values(self, array_index, node_value_name):

        #fill the node value matrix with values
        L = np.zeros([self.num_arrays, self.num_sites_per_shank, self.num_rows, self.num_cols])

        for k in range(self.num_arrays):
            for j in range(self.num_sites_per_shank):
                for row in range(self.num_rows):
                    for col in range(self.num_cols):
                        if node_value_name in self.g.node[(k, j, row, col)]:
                            L[k, j, row, col] = self.g.node[(k, j, row, col)][node_value_name]

        X = self.X[array_index, :, :, :].ravel()
        Y = self.Y[array_index, :, :, :].ravel()
        Z = self.Z[array_index, :, :, :].ravel()
        V = L[array_index, :, :, :].ravel()

        return X,Y,Z,V

    def get_edge_locations_and_values(self, array_index, edge_value_name='weight'):

        X1 = list()
        Y1 = list()
        Z1 = list()
        X2 = list()
        Y2 = list()
        Z2 = list()
        W = list()

        edge_list = [((k1, j1, row1, col1),(k2, j2, row2, col2)) for (k1, j1, row1, col1),(k2, j2, row2, col2) in self.g.edges() if k1 == k2 and k1 == array_index]
        for (k1, j1, row1, col1),(k2, j2, row2, col2) in edge_list:
            node1 = (k1, j1, row1, col1)
            node2 = (k2, j2, row2, col2)
            if edge_value_name in self.g[node1][node2]:

                w = self.g[node1][node2][edge_value_name]
                x1,y1,z1 = self.X[k1, j1, row1, col1], self.Y[k1, j1, row1, col1], self.Z[k1, j1, row1, col1]
                x2,y2,z2 = self.X[k2, j2, row2, col2], self.Y[k2, j2, row2, col2], self.Z[k2, j2, row2, col2]

                X1.append(x1)
                Y1.append(y1)
                Z1.append(z1)

                X2.append(x2)
                Y2.append(y2)
                Z2.append(z2)

                W.append(w)

        return X1,Y1,Z1,X2,Y2,Z2,W


class ElectrodeGraphRenderer(object):

    def __init__(self):
        pass

    def render(self, eg, ax_list, node_value_name='v', node_value_min=None, node_value_max=None,
               edge_value_name='weight', edge_value_min=None, edge_value_max=None,
               output_file=None, fig=None, titles=None, colorbar=False,
               draw_edges=True, edge_value_threshold=None):
        """
            eg: an ElectrodeGraph instance
            ax: must be a list of matplotlib Axes object with projection='3d', with len(ax_list) == g.num_arrays
        """

        #for x,y,z,l in  zip(self.X.ravel(), self.Y.ravel(), self.Y.ravel(), L.ravel()):
        #    print '%d, %d, %d, %0.6f' % (x, y, z, l)


        node_val_extrema = eg.get_node_value_extrema(node_value_name)
        if node_value_min is None:
            node_value_min = node_val_extrema[0]
        if node_value_max is None:
            node_value_max = node_val_extrema[1]

        if draw_edges:
            all_weights = np.array([eg.g[node1][node2][edge_value_name] for node1,node2 in eg.g.edges() if edge_value_name in eg.g[node1][node2]])
            if edge_value_min is None:
                edge_value_min = all_weights.min()
            if edge_value_max is None:
                edge_value_max = all_weights.max()
        if edge_value_threshold is None:
            edge_value_threshold = 0.0

        for k,ax in enumerate(ax_list):

            #orient the 3D plot so that x axis is rostral-caudal, y axis goes from left to right, and z axis is depth
            ax.view_init(-175.0, -73.0)
            ax.grid(False)
            ax.set_xticks([])
            ax.set_yticks([])
            ax.set_zlabel('Depth ($\mu$m)')

            #create matrix of data
            X,Y,Z,V = eg.get_node_locations_and_values(array_index=k, node_value_name=node_value_name)
            data = np.array(zip(X, Y, Z, V))

            #figure out the dot size for each electrode
            dot_size_max = 400
            dot_size_min = 10
            dot_sizes = np.abs(data[:, 3])
            dmax = dot_sizes.max()
            if dmax > 0:
                dot_sizes /= dmax
                dot_sizes *= dot_size_max
            dot_sizes += dot_size_min

            #create a 3d scatterplot
            p = ax.scatter(data[:, 0], data[:, 1], data[:, 2], c=data[:, 3], s=dot_sizes, cmap=cm.bwr_r,
                           vmin=node_value_min, vmax=node_value_max)

            #create colorbar
            if colorbar:
                if fig is None:
                    fig = plt.gcf()
                fig.colorbar(p)

            #set title
            if titles is not None:
                plt.title(titles[k])

            #draw connections between points on the grid
            if draw_edges:

                X1,Y1,Z1,X2,Y2,Z2,W = eg.get_edge_locations_and_values(array_index=k, edge_value_name=edge_value_name)

                for x1,y1,z1,x2,y2,z2,w in zip(X1,Y1,Z1,X2,Y2,Z2,W):
                    if np.abs(w) > edge_value_threshold:
                        alpha = (w - edge_value_min) / (edge_value_max - edge_value_min)
                        a = Arrow3D([x1,x2],[y1,y2],[z1,z2], mutation_scale=20, lw=1, arrowstyle="-", color='k', alpha=alpha)
                        ax.add_artist(a)

        #plt.show()
        #write to file if specified
        if output_file is not None:
            plt.savefig(output_file)

        #clean up
        #plt.show()
        plt.clf()
        plt.close('all')


class Arrow3D(FancyArrowPatch):
    """
        Copied from:
        http://stackoverflow.com/questions/11140163/python-matplotlib-plotting-a-3d-cube-a-sphere-and-a-vector
    """
    def __init__(self, xs, ys, zs, *args, **kwargs):
        FancyArrowPatch.__init__(self, (0,0), (0,0), *args, **kwargs)
        self._verts3d = xs, ys, zs

    def draw(self, renderer):
        xs3d, ys3d, zs3d = self._verts3d
        #print xs3d,ys3d,zs3d
        xs, ys, zs = proj3d.proj_transform(xs3d, ys3d, zs3d, renderer.M)
        #print xs,ys,zs
        self.set_positions((xs[0],ys[0]),(xs[1],ys[1]))
        FancyArrowPatch.draw(self, renderer)


class LFPMovieRenderer(object):

    def __init__(self, f, avg_file='/auto/k8/fdata/mschachter/analysis/stim_avg/GreBlu9508M_stim_cond_lfp.h5', stim_type='song'):

        self.f = f

        #load up sites
        s = Site(self.f)
        self.sites = [site for site in s.get_all()]
        self.site_depths = [(site.ldepth, site.rdepth) for site in self.sites]

        self.sites.sort(key=operator.attrgetter('ldepth'))

        #load up stimulus envelopes and stimulus-averaged LFPs
        self.avg_file = avg_file
        assert os.path.exists(self.avg_file)
        self.site_ids,self.sample_rate,self.stim_envelopes,self.lfp_tensors_by_stim,self.electrode_indices = \
            get_lfp_tensors_by_stim(f, avg_file, stim_type=stim_type)

        self.electrode2index = {enumber:k for k,enumber in enumerate(self.electrode_indices)}

        some_stim_id = self.lfp_tensors_by_stim.keys()[0]
        nsites,nelectrodes,nt = self.lfp_tensors_by_stim[some_stim_id].shape
        hemispheres = np.unique([e.hemisphere for e in self.sites[0].electrodes])
        nhemi = len(hemispheres)
        self.array_locations = np.arange(nhemi)*400.0

        #get electrode geometry and coordinates matrix
        e = self.sites[0].electrodes[0]
        self.geometry = e.geometry
        self.coords = self.geometry.coordinates
        
        #create an electrode graph
        nrows,ncols = self.geometry.shape
        ncols = int(ncols / nhemi)
        print 'nhemi=%d, nsites=%d, nrows=%d, ncols=%d' % (nhemi, nsites, nrows, ncols)
        self.egraph = ElectrodeGraph(nhemi, nsites, nrows, ncols, self.array_locations, self.site_depths)
        self.renderer = ElectrodeGraphRenderer()

    def make_movie(self, stim_id, output_dir='/tmp', t=None):

        #get amplitude envelope
        stim_env = self.stim_envelopes[stim_id]

        #get the LFP tensor across time, sites, and electrodes
        lfp = self.lfp_tensors_by_stim[stim_id]

        vmin = -np.percentile(np.abs(lfp[lfp < 0.0]).ravel(), 99)
        vmin2 = lfp[lfp < 0.0].ravel().min()
        vmax = lfp[lfp > 0.0].ravel().max()
        absmax = max(abs(vmin), abs(vmax))
        print 'vmax=%0.3f, vmin=%0.3f, vmin_old=%0.3f, absmax=%0.3f' % (vmax, vmin, vmin2, absmax)

        #plot a frame for each segment of time, write to a file
        if t is None:
            nt = min(len(stim_env), lfp.shape[-1])
            t = range(nt)
        for ti in t:
            #set the node value of each graph equal to the LFP magnitude
            for k, j, row, col in self.egraph.g.nodes():
                #self.coords contains both hemispheres, but the graph is indexed by hemisphere,
                #so we need to adjust the column index corresponding to the electrode to
                #get the right hemisphere
                ccol = col + 2*k
                #print 'k=%d, j=%d, row=%d, col=%d, ccol=%d' % (k, j, row, col, ccol)
                enumber = self.coords[row, ccol]
                eindex = self.electrode2index[enumber]
                #print 'enumber=%d, eindex=%d' % (enumber, eindex)
                self.egraph.g.node[(k, j, row, col)]['v'] = lfp[j, eindex, ti]

            fig = plt.figure()
            gs = GridSpec(2, 2)
            ax_list = list()
            narrays = len(self.array_locations)
            for k,loc in enumerate(self.array_locations):
                ax = fig.add_subplot(gs[1, k], projection='3d')
                ax_list.append(ax)

            ofile = os.path.join(output_dir, 'stim_%d_frame%04d.png' % (stim_id, ti))
            print 'Rendering %s' % ofile
            self.renderer.render(self.egraph, ax_list, node_value_min=vmin, node_value_max=vmax,
                                 output_file=ofile, draw_edges=False, fig=fig)
            del fig
            del ax_list

        #merge the frames into a movie
        mp4_file = os.path.join(output_dir, 'stim_%d.mp4' % stim_id)
        input_fmt = os.path.join(output_dir, 'stim_%d_frame%%04d.png' % stim_id)
        fps = 10.0
        cmd = 'avconv -f image2 -r %d -i %s -c:v libx264 -preset slow -vf "scale=640:trunc(ow/a/2)*2" -crf 0 %s' % (fps, input_fmt, mp4_file)
        print cmd
        os.system(cmd)

        #create a .wav file for the amplitude envelope
        wav_file = os.path.join(output_dir, 'stim_%d.wav' % stim_id)
        self.envelope_to_sound(stim_id, output_file=wav_file, movie_fps=fps)

        #merge the video and audio
        ofile = os.path.join(output_dir, 'stim_%d+audio.mp4' % stim_id)
        cmd = 'avconv -i %s -i %s -c copy -c:a libmp3lame %s' % (mp4_file, wav_file, ofile)
        os.system(cmd)

    def envelope_to_sound(self, stim_id, desired_duration=None, output_file=None, movie_fps=None):
        """
            Creates a frequency-modulated sound from the amplitude envelope of a sound.
        """

        stim_env = self.stim_envelopes[stim_id]
        dur = len(stim_env) / self.sample_rate
        dt = 1.0 / self.sample_rate
        if movie_fps is not None:
            tps = movie_fps*dt
            desired_duration = dur / tps
        if desired_duration is None:
            desired_duration = dur

        #interpolate the stimulus envelope
        t = np.arange(len(stim_env)) / self.sample_rate
        func = interp1d(t, stim_env, kind='linear')

        #set properties of carrier signal
        csr = 44100.0
        cdt = 1.0 / csr
        cn = int(desired_duration / cdt)

        #expand or contract the signal according to the desired duration
        scaled_dt = dur / cn
        op = cn*scaled_dt - t.max()
        cn -= int(op/scaled_dt) #do this so we don't exceed the bounds for interpolation

        teval = np.arange(cn)*scaled_dt

        stim_env_rs = func(teval)
        stim_env_rs += np.abs(stim_env_rs.min())
        stim_env_rs /= np.abs(stim_env_rs).max()

        #create FM modulated signal
        cf = 300.0  #carrier frequency
        bw = 100.0   #bandwidth
        #stim_env_rs_normalized = stim_env_rs + stim_env_rs.min()
        #stim_env_rs_normalized /= np.abs(stim_env_rs_normalized).max()
        fsum = np.cumsum(stim_env_rs)*dt

        ct = np.arange(cn)*cdt
        cwave = np.sin(2*np.pi*ct*cf + bw*fsum)

        if output_file is not None:
            print 'Creating wav file at %s' % output_file
            wf = WavFile()
            wf.data = cwave
            wf.sample_rate = csr
            wf.depth = 1
            wf.to_wav(output_file, normalize=True)


if __name__ == '__main__':

    f = tables.open_file('/auto/k8/fdata/mschachter/tdt_continuous/GreBlu9508M/GreBlu9508M_tabled.h5', mode='r')
    #load up sites
    s = Site(f)
    sites = [site for site in s.get_all()]
    sites.sort(key=operator.attrgetter('ldepth'))
    site_depths = [[s.ldepth, s.rdepth] for s in sites]
    print site_depths

    eg = ElectrodeGraph(2, len(sites), 8, 2, array_locations=[0.0, 400.0], site_depths=site_depths, row_spacing=125.0, col_spacing=200.0)
    #for k,j,row,col in eg.g.nodes():
        #print '%d, %d, %d, %d' % (k, j, row, col)
    for e in eg.g.edges():
        print e
    print '# of combos: %d' % len(eg.g.edges())

    mrend = LFPMovieRenderer(f)
    mrend.make_movie(277, output_dir='/tmp/stim_movies')

    f.close()
