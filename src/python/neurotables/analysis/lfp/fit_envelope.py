import os
import time

import h5py
import numpy as np
from matplotlib import rcParams
from matplotlib.gridspec import GridSpec
import matplotlib.pyplot as plt
import operator

from lasp.incrowd import InCrowd, ConvolutionalInCrowdModel, fast_conv_grouped,fast_conv
from lasp.signal import power_spectrum
from neurotables.analysis.lfp.coupling_tensor import region_groups
from neurotables.analysis.lfp.objects import ElectrodeAnatomicalLocation
from neurotables.analysis.lfp.stim_cond import get_lfp_tensors_by_stim
from neurotables.objects.cells import *


class LinearFilter(object):

    def __init__(self, X=None, y=None, biases=None, sample_rate=None, lags=None, group_index=None, cv_sets=None, filters=None):
        self.X = X
        self.y = y

        self.sample_rate = sample_rate
        self.lags = lags
        self.group_index = group_index
        self.cv_sets = cv_sets

        self.biases = biases
        self.filters = filters

        self.errs = None
        self.etimes = None
        self.ccs = None

    def plot(self):
        #get average filter
        filter_mean = self.filters.mean(axis=0)
        filter_std = self.filters.std(axis=0)
        bias = self.biases.mean()

        #plot the mean filter and it's std
        lagdt = 1.0 / self.sample_rate
        plt.figure()
        plt.subplot(1, 2, 1)
        nfilts = min(5, filter_mean.shape[0])
        for k in range(nfilts):
            #plt.errorbar(self.lags*lagdt, filter_mean[k, :], yerr=filter_std[k, :], ecolor='#aaaaaa', capsize=0, color='k', linewidth=2, alpha=0.75)
            plt.plot(self.lags*lagdt, self.filters[k, :], linewidth=2.0)
        plt.plot(self.lags*lagdt, filter_mean.squeeze(), 'k-', linewidth=2.0)
        plt.xlabel('Lag')
        plt.axis('tight')

        if self.X is None:
            return

        mresp_mean = fast_conv_grouped(self.X, filter_mean, self.lags, self.group_index, bias=bias)

        #make a scatterplot that shows the nonlinear relationship between linear response and actual response
        plt.subplot(1, 2, 2)
        plt.plot(mresp_mean, self.y, 'go')
        plt.xlabel('Linear Response')
        plt.ylabel('Actual Response')
        plt.axis('tight')

        #plot each response
        groups = np.unique(self.group_index)

        for group in groups:
            gi = self.group_index == group
            input = self.X[gi]
            output = self.y[gi]
            mresp = mresp_mean[gi]
            t = np.arange(len(mresp))*lagdt

            plt.figure()
            plt.subplot(2, 1, 1)
            plt.plot(t*1000.0, input)
            plt.xlabel('Time (ms)')
            plt.ylabel('Input')
            plt.axis('tight')

            plt.subplot(2, 1, 2)
            plt.plot(output, 'k-', linewidth=2.0)
            plt.plot(mresp, 'r-', linewidth=2.0)
            plt.axis('tight')

    def to_file(self, output_file=None, output_group=None, include_dataset=True):
        of = None
        if output_group is None:
            of = h5py.File(output_file, 'w')
            output_group = of['/']

        if include_dataset:
            output_group['X'] = self.X
            output_group['y'] = self.y
            output_group['group_index'] = self.group_index

        output_group['filters'] = self.filters
        output_group['biases'] = self.biases
        output_group['ccs'] = self.ccs

        output_group['lags'] = self.lags
        output_group['errs'] = self.errs
        output_group['etimes'] = self.etimes

        cvgrp = output_group.create_group('cv_sets')
        for k,(training_indices,validation_indices) in enumerate(self.cv_sets):
            grpgrp = cvgrp.create_group('group%d' % k)
            grpgrp['training_indices'] = np.array(training_indices)
            grpgrp['validation_indices'] = np.array(validation_indices)

        output_group.attrs['sample_rate'] = self.sample_rate

        if of is not None:
            of.close()

    @classmethod
    def from_file(cls, output_file=None, output_group=None):

        lf = LinearFilter(None, None, None, None, None, None, None)

        hf = None
        if output_file is not None:
            hf = h5py.File(output_file, 'r')
            output_group = hf['/']
            
        if 'X' in output_group.keys():
            lf.X = np.array(output_group['X'])
            lf.y = np.array(output_group['y'])
            lf.group_index = np.array(output_group['group_index'])
        else:
            lf.X = None
            lf.y = None
            lf.group_index = None

        lf.biases = np.array(output_group['biases'])
        lf.filters = np.array(output_group['filters'])
        lf.lags = np.array(output_group['lags'])
        lf.errs = np.array(output_group['errs'])
        lf.ccs = np.array(output_group['ccs'])
        lf.etimes = np.array(output_group['etimes'])
        lf.sample_rate = output_group.attrs['sample_rate']

        cvgrp = output_group['cv_sets']
        nfolds = len(cvgrp.keys())
        cv_sets = list()
        for k in range(nfolds):
            gname = 'group%d' % k
            ti = np.array(cvgrp[gname]['training_indices'])
            vi = np.array(cvgrp[gname]['validation_indices'])
            cv_sets.append( (ti, vi) )
        lf.cv_sets = cv_sets

        if hf is not None:
            hf.close()

        return lf


def get_stim_averaged_lfps(lfp_tensors_by_stim, site_indices, electrode_indices):
    """
        Gets a list of stimulus averaged LFP matrices from a list of stimulus-averaged LFP tensors.
    """

    lfp_matrices = dict()
    index2indices = dict()
    for sid,lfp_tensor in lfp_tensors_by_stim.iteritems():

        nt = lfp_tensor.shape[-1]
        lfp_matrix = np.zeros([nt, len(site_indices)*len(electrode_indices)])
        for k,si in enumerate(site_indices):
            for j,ei in enumerate(electrode_indices):
                i = k*len(electrode_indices) + j
                lfp_matrix[:, i] = lfp_tensor[si, ei, :]
                index2indices[i] = (si, ei)
        lfp_matrices[sid] = lfp_matrix

    return index2indices,lfp_matrices


def fit_envelope_lfp_model(stim_envelopes, stim_averaged_lfps, sample_rate, lags, lambda1, lambda2, threshold=0.0, niters=25, nfolds=3, encoder=True):
    """
        Fits an linear filter encoding model that maps the stimulus amplitude envelope to an LFP.

        stim_envelopes: a list of numpy arrays, each array is a different stimulus amplitude envelope.

        stim_averaged_lfps: a list of numpy arrays, each array is a local field potential.

        lags: an array of indices representing the filter lags. For example, to create a causal FIR filter,
            one could specify lags=np.array([0, 1, 2, 3]). Negative values for the lags indicate times in
            the future, for acausal filters.

        The sampling rate and length of stim_envelopes[i] should match stim_averaged_lfps[i].

        Returns a LinearFilter object.
    """

    print 'len(stim_envelopes)=%d, len(stim_averaged_lfps)=%d' % (len(stim_envelopes), len(stim_averaged_lfps))
    print 'stim_envelopes[0].shape=',stim_envelopes[0].shape
    print 'stim_averaged_lfps[0].shape=',stim_averaged_lfps[0].shape

    for stim_env,lfp in zip(stim_envelopes, stim_averaged_lfps):
        print 'len(stim_env)=%d, len(lfp)=%d' % (len(stim_env), len(lfp))
        assert len(stim_env) == len(lfp)

    #concatenate the series together
    nstims = len(stim_envelopes)
    stimlens = [len(stim_env) for stim_env in stim_envelopes]

    X = np.concatenate(stim_envelopes)
    if len(X.shape) == 1:
        X.resize(len(X), 1)
    y = np.concatenate(stim_averaged_lfps)
    if len(y.shape) > 1:
        y = y.squeeze()
    if not encoder:
        #swap input and output
        ycpy = y
        y = X
        X = ycpy

    print 'X.shape=',X.shape
    print 'y.shape=',y.shape

    group_index = np.concatenate([np.ones(stimlens[k])*k for k in range(nstims)])

    """
    plt.figure()
    plt.subplot(2, 1, 1)
    plt.plot(X, 'k-')
    plt.axis('tight')

    plt.subplot(2, 1, 2)
    plt.plot(y, 'b-')
    plt.axis('tight')

    plt.show()
    """

    assert len(X) == len(y)
    assert len(group_index) == len(y)

    #generate training indicies
    cv_sets = get_cv_groups(group_index, nfolds)

    strfs = list()
    errs = list()
    etimes = list()
    biases = list()
    ccs = list()

    #train each model serially
    for k, (training_indices, holdout_indices) in enumerate(cv_sets):
        fm = ConvolutionalInCrowdModel(X[training_indices], y[training_indices], lags=lags, bias=0.0, group_index=group_index[training_indices])

        stime = time.time()
        ico = InCrowd(fm, solver_params={'lambda1': lambda1, 'lambda2': lambda2}, max_additions_fraction=threshold)
        while not ico.converged and ico.num_iter < niters:
            ico.iterate()

        etime = time.time() - stime

        holdout_resp = y[holdout_indices]
        holdout_fm = ConvolutionalInCrowdModel(X[holdout_indices, :], y[holdout_indices], lags=lags, bias=fm.bias, group_index=group_index[holdout_indices])
        model_holdout_resp = holdout_fm.forward(ico.x)

        holdout_err = ((holdout_resp - model_holdout_resp) ** 2).sum()
        holdout_cc_mat = np.corrcoef(holdout_resp, model_holdout_resp)
        holdout_cc = holdout_cc_mat[0, 1]

        print 'lambda1=%0.1f, lambda2=%0.1f, threshold=%0.2f, k=%d, holdout_err=%0.1f, etime=%ds, len(active_set)=%d, cc=%0.2f' %\
              (lambda1, lambda2, threshold, k, holdout_err, int(etime), len(ico.active_set), holdout_cc)
        print ''

        strf = fm.get_filter(ico.x)
        errs.append(holdout_err)
        etimes.append(etime)
        strfs.append(strf)
        biases.append(fm.bias)
        ccs.append(holdout_cc)

    lf = LinearFilter(X, y, np.array(biases), sample_rate, lags, group_index, cv_sets, filters=np.array(strfs))
    lf.etimes = etimes
    lf.errs = errs
    lf.ccs = ccs

    return lf


def fit_spec_and_envelope_lfp_encoder(spectrograms, stim_envelopes, stim_averaged_lfps, sample_rate, lags, lambda1, lambda2, threshold=0.0, niters=25, nfolds=5):
    """
        Fits an linear filter encoding model that maps the stimulus amplitude envelope to an LFP.

        spectrograms: a list of spectrograms, each with shape (num_freqs, num_time_points)

        stim_envelopes: a list of numpy arrays, each array is a different stimulus amplitude envelope.

        stim_averaged_lfps: a list of numpy arrays, each array is a local field potential.

        lags: an array of indices representing the filter lags. For example, to create a causal FIR filter,
            one could specify lags=np.array([0, 1, 2, 3]). Negative values for the lags indicate times in
            the future, for acausal filters.

        The sampling rate and length of stim_envelopes[i] should match stim_averaged_lfps[i].

        Returns a LinearFilter object.
    """

    print 'len(stim_envelopes)=%d, len(stim_averaged_lfps)=%d' % (len(stim_envelopes), len(stim_averaged_lfps))
    print 'stim_envelopes[0].shape=',stim_envelopes[0].shape
    print 'stim_averaged_lfps[0].shape=',stim_averaged_lfps[0].shape

    for stim_env,lfp in zip(stim_envelopes, stim_averaged_lfps):
        print 'len(stim_env)=%d, len(lfp)=%d' % (len(stim_env), len(lfp))
        assert len(stim_env) == len(lfp)

    #normalize all spectrograms by their own absolute maximum, removing envelope information
    spectrograms = [spec / np.abs(spec).max() for spec in spectrograms]

    env_absmax = [np.abs(env).max() for env in stim_envelopes]
    stim_envelopes = [np.abs(env) / env_absmax for env in stim_envelopes]

    #concatenate the series together
    nstims = len(stim_envelopes)
    stimlens = [len(stim_env) for stim_env in stim_envelopes]
    total_len = np.sum(stimlens)

    #put the envelope and spectrogram information together
    num_freqs = spectrograms[0].shape[0]

    X = np.zeros([total_len, num_freqs+1])
    X[:, 0] = np.concatenate(stim_envelopes)
    X[:, 1:] = np.concatenate(spectrograms, axis=1)

    y = np.concatenate(stim_averaged_lfps)
    if len(y.shape) > 1:
        y = y.squeeze()

    print 'X.shape=',X.shape
    print 'y.shape=',y.shape

    group_index = np.concatenate([np.ones(stimlens[k])*k for k in range(nstims)])

    assert X.shape[0] == len(y)
    assert len(group_index) == len(y)

    #generate training indicies
    cv_sets = get_cv_groups(group_index, nfolds)

    strfs = list()
    errs = list()
    etimes = list()
    biases = list()
    ccs = list()

    #train each model serially
    for k, (training_indices, holdout_indices) in enumerate(cv_sets):
        fm = ConvolutionalInCrowdModel(X[training_indices, :], y[training_indices], lags=lags, bias=0.0, group_index=group_index[training_indices])

        stime = time.time()
        ico = InCrowd(fm, solver_params={'lambda1': lambda1, 'lambda2': lambda2}, max_additions_fraction=threshold)
        while not ico.converged and ico.num_iter < niters:
            ico.iterate()

        etime = time.time() - stime

        holdout_resp = y[holdout_indices]
        holdout_fm = ConvolutionalInCrowdModel(X[holdout_indices, :], y[holdout_indices], lags=lags, bias=fm.bias, group_index=group_index[holdout_indices])
        model_holdout_resp = holdout_fm.forward(ico.x)

        holdout_err = ((holdout_resp - model_holdout_resp) ** 2).sum()
        holdout_cc_mat = np.corrcoef(holdout_resp, model_holdout_resp)
        holdout_cc = holdout_cc_mat[0, 1]

        print 'lambda1=%0.1f, lambda2=%0.1f, threshold=%0.2f, k=%d, holdout_err=%0.1f, etime=%ds, len(active_set)=%d, cc=%0.2f' %\
              (lambda1, lambda2, threshold, k, holdout_err, int(etime), len(ico.active_set), holdout_cc)
        print ''

        strf = fm.get_filter(ico.x)
        errs.append(holdout_err)
        etimes.append(etime)
        strfs.append(strf)
        biases.append(fm.bias)
        ccs.append(holdout_cc)

    lf = LinearFilter(X, y, np.array(biases), sample_rate, lags, group_index, cv_sets, filters=np.array(strfs))
    lf.etimes = etimes
    lf.errs = errs
    lf.ccs = ccs

    return lf

def find_match(arr, vals):

    marr = np.zeros(arr.shape, dtype='bool')
    for v in vals:
        marr |= arr == v
    return (marr.nonzero()[0]).tolist()


def get_cv_groups(group_index, nfolds=5):

    group_index = np.array(group_index)
    ugroups = np.unique(group_index)
    grps_per_holdout = int(len(ugroups) / float(nfolds))

    cv_sets = list()
    for k in range(nfolds):
        ustart = k*grps_per_holdout
        #print 'k=%d, ustart=%d, ustart+%d=%d' % (k, ustart, grps_per_holdout, (ustart+grps_per_holdout))
        hgrps = ugroups[ustart:(ustart+grps_per_holdout)]
        tgrps = np.setdiff1d(ugroups, hgrps)

        training_indices = find_match(group_index, tgrps)
        holdout_indices = find_match(group_index, hgrps)

        cv_sets.append( (training_indices, holdout_indices) )

    return cv_sets

def plot_encoders(f, site_id_electrode_combos, output_dir='/auto/k8/fdata/mschachter/analysis/stim_env_encoder', edesc='',
                  avg_file='/auto/k8/fdata/mschachter/analysis/stim_cond/GreBlu9508M_stim_cond_lfp.h5', stim_ids=None):

    """
        One plot for each stim - spectrogram on top, then amplitude envelope, and the LFP/predicted LFP for each
        (site_id, electrode) pair.
    """

    rcParams.update({'font.size': 18})
    site_ids2,sample_rate,stim_envelopes,lfp_tensors_by_stim,electrode_indices = get_lfp_tensors_by_stim(f, avg_file=avg_file)

    if stim_ids is None:
        stim_ids = stim_envelopes.keys()

    for sound_id in stim_ids:
        stim_env = stim_envelopes[sound_id]
        stim_env.resize([len(stim_env), 1])

        #get data about LFPs and predictions for each site_enectrode combo
        resps = dict()
        lfp_abs_max = -np.inf
        for site_id,protocol_name,electrode in site_id_electrode_combos:

            #load the filter file
            output_file = os.path.join(output_dir, 'env_encoder_site%d_electrode%d.h5' % (site_id, electrode))
            lf = LinearFilter.from_file(output_file)
            assert lf is not None
            mean_filter = lf.filters.mean(axis=0)
            mean_bias = lf.biases.mean()

            #load the stimulus-averaged LFP data
            lfp_tensor = lfp_tensors_by_stim[sound_id]
            site_index = site_ids2.index(site_id)
            eindex = electrode_indices[electrode]
            lfp = lfp_tensor[site_index, eindex, :]

            mresp = fast_conv(stim_env, mean_filter, lf.lags, bias=mean_bias)
            C = np.corrcoef(lfp, mresp)
            cc = C[0, 1]
            resps[(site_id,protocol_name,electrode)] = (lfp, mresp, cc)

            lfp_abs_max = np.max([np.abs(mresp).max(), np.abs(lfp).max(), lfp_abs_max])

        #load sound
        sound = Sound(f)
        assert sound.from_id(sound_id)

        stim_env = stim_envelopes[sound_id]
        t_env = np.arange(len(stim_env)) / sample_rate

        #compute spectrogram
        spec_t,spec_f,spec = sound.compute_spectrogram(thresh_percentile=20)

        #make plots
        nsp = 2 + len(site_id_electrode_combos)
        plt.figure()

        #plot the spectrogram
        ax = plt.subplot(nsp, 1, 1)
        plot_spectrogram(spec_t, spec_f, spec, ax=ax, colormap=cm.gist_yarg, colorbar=False)
        plt.xlabel('')

        #plot the amplitude envelope
        ax = plt.subplot(nsp, 1, 2)
        plt.plot(t_env, stim_env, 'g-', linewidth=3.0)
        plt.xlabel('')

        for k,(site_id,protocol_name,electrode) in enumerate(site_id_electrode_combos):
            lfp, mresp, cc = resps[(site_id,protocol_name,electrode)]
            t = np.arange(len(lfp)) / sample_rate

            plt.subplot(nsp, 1, 2+k+1)
            plt.plot(t, lfp, 'k-', linewidth=3)
            plt.plot(t, mresp, 'r-', linewidth=3)
            plt.ylim([-lfp_abs_max, lfp_abs_max])
            plt.legend(['Real', 'Model'], loc=4)
            plt.axis('tight')
            plt.ylabel('Site %d, E%d' % (site_id, electrode))
            if k == len(site_id_electrode_combos) - 1:
                plt.xlabel('Time (s)')
            plt.setp(plt.gca().get_legend().get_texts(), fontsize='16')


def boxplot_from_dict(d, key_order, ax=None):

    if ax is None:
        ax = plt.gca()

    vals = [d[key] for key in key_order]

    bp = plt.boxplot(vals)
    plt.setp(bp['boxes'], lw=3.0, color='k')
    plt.setp(bp['whiskers'], lw=3.0, color='k')
    plt.xticks(range(1, len(key_order)+1), key_order)


def get_encoder_data(f, site_ids=[1, 2, 3, 4], electrode_ids=range(32),
                    output_dir='/auto/k8/fdata/mschachter/analysis/stim_env_encoder', edesc='',
                    avg_file='/auto/k8/fdata/mschachter/analysis/stim_cond/GreBlu9508M_stim_cond_lfp.h5',
                    csv_output_file=None):

    #get stimulus-averaged data from file
    site_ids2,sample_rate,stim_envelopes,lfp_tensors_by_stim,electrode_indices = get_lfp_tensors_by_stim(f, avg_file=avg_file)

    #aggregate data across sites
    filter_list = list()
    site_hemi_depths = dict()
    for site_id in site_ids:

        site = Site(f)
        assert site.from_id(site_id)
        emap = {e.number:e for e in site.electrodes}

        #record the depths for the site/hemisphere combinations
        site_hemi_depths[(site_id, 'L')] = site.ldepth
        site_hemi_depths[(site_id, 'R')] = site.rdepth

        #record the anatomical region for each electrode
        electrode_regions = dict()
        for e in site.electrodes:
            geom = e.geometry
            coords = geom.coordinates
            reg = '?'
            if type(e.anatomy) == ElectrodeAnatomicalLocation:
                reg = e.anatomy.region
            electrode_regions[e.number] = reg

        for electrode_id in electrode_ids:

            enumber = electrode_id+1

            #read encoder data from the file
            output_file = os.path.join(output_dir, 'env_encoder_site%d_electrode%d%s.h5' % (site_id, enumber, edesc))
            print 'Opening file %s' % output_file
            lf = LinearFilter.from_file(output_file)

            #get the electrode for this site
            electrode = emap[enumber]

            #get grid coordinates
            row,col = np.nonzero(coords == electrode.number)
            #print 'e%d, hemi=%s, row=%d, col=%d' % (electrode.number, electrode.hemisphere, row, col)

            #compute some filter properties
            filt = lf.filters.mean(axis=0).squeeze()
            tmin,fmin,tmax,fmax = compute_filter_props(filt, lf.lags / sample_rate)

            tmin *= 1e3
            tmax *= 1e3
            filt_width = tmax - tmin

            #get depth
            depth = site_hemi_depths[(site_id, electrode.hemisphere)]

            #get the anatomical grouping
            full_reg = electrode_regions[electrode.number]
            sstrs = full_reg.split('-')
            reg = sstrs[0]
            group = region_groups[reg]

            #save the data in a dictionary
            fdata = {'site_id':site_id,
                     'electrode':electrode.number,
                     'hemisphere':electrode.hemisphere,
                     'region':full_reg,
                     'group':group,
                     'loc':(row, col),
                     'lf':lf,
                     'cc':lf.ccs.mean(),
                     'tmin':tmin,
                     'tmax':tmax,
                     'filt_width':filt_width,
                     'depth':depth,
                     'filter':lf.filters.mean(axis=0)}

            #add the dictionary to the list
            filter_list.append(fdata)

    #write out to a csv file if requested
    if csv_output_file is not None:
        print 'Writing output to %s' % csv_output_file
        col_names = [key for key in filter_list[0].keys() if key not in ['lf', 'loc']]
        f = open(csv_output_file, 'w')
        f.write('%s\n' % ','.join(col_names))
        for fdata in filter_list:
            f.write('%s\n' % ','.join([str(fdata[key]) for key in col_names]))
        f.close()

    return filter_list,lfp_tensors_by_stim,stim_envelopes,electrode_indices,coords,sample_rate


def aggregate_encoders(f, site_ids=[1, 2, 3, 4], electrode_ids=range(32),
                       output_dir='/auto/k8/fdata/mschachter/analysis/stim_env_encoder', edesc='',
                       avg_file='/auto/k8/fdata/mschachter/analysis/stim_cond/GreBlu9508M_stim_cond_lfp.h5',
                       figure_dir='/auto/k8/fdata/mschachter/analysis/figures/encoder'):

    #get filter data across sites and electrodes
    filter_list,lfp_tensors_by_stim,stim_envelopes,electrode_indices,coords,sample_rate = \
        get_encoder_data(f, site_ids=site_ids, electrode_ids=electrode_ids, output_dir=output_dir, avg_file=avg_file)

    #map site+(row,col) for each site,electrode pair to a string for display
    site_cc_mats = dict()
    site_peak_mats = dict()
    site_cc_regions = dict()
    for site_id in site_ids:
        site_cc_mats[site_id] = np.zeros([8, 4])
        site_peak_mats[site_id] = np.zeros([8, 4])
        site_cc_regions[site_id] = np.zeros([8, 4], dtype='S40')

    #fill the matrices with data
    for fdata in filter_list:
        cc = fdata['cc']
        row,col = fdata['loc']
        tmax = fdata['tmax']
        enumber = fdata['electrode']
        reg = fdata['region']
        site_id = fdata['site_id']

        #print '%d, %d, %s, %s, %0.2f' % (site_id, enumber, hemi, reg, cc)
        site_cc_mats[site_id][row, col] = fdata['cc']
        site_peak_mats[site_id][row, col] = fdata['tmax']
        site_cc_regions[site_id][row, col] = '%d\n%s\n%0.2f' % (enumber, reg, cc)

    #aggregate filters by region
    filter_by_region = dict()
    models_by_site_and_electrode = dict()
    for fdata in filter_list:
        group = fdata['group']
        if group not in filter_by_region:
            filter_by_region[group] = list()
        filter_by_region[group].append(fdata)
        models_by_site_and_electrode[(fdata['site_id'], fdata['electrode'])] = fdata['lf']

    #compute filter peak times and performance by region
    print ''
    peak_time_and_cc = list()
    filt_peak_times_by_region = dict()
    filt_cc_by_region = dict()
    for reg,flist in filter_by_region.iteritems():
        ptimes = list()
        fccs = list()
        for fdata in flist:
            ptimes.append(fdata['tmax'])
            fccs.append(fdata['cc'])
            peak_time_and_cc.append( (fdata['tmax'], fdata['cc']) )

        filt_peak_times_by_region[reg] = np.array(ptimes)
        print '%s: %d' % (reg, len(ptimes))
        filt_cc_by_region[reg] = np.array(fccs)

    #plot peak time vs correlation coefficient
    peak_time_and_cc = np.array(peak_time_and_cc)
    plt.figure(figsize=(16, 9))
    plt.plot(peak_time_and_cc[:, 0], peak_time_and_cc[:, 1], 'ro', ms=10.0)
    plt.xlabel('Peak Filter Time (ms)')
    plt.ylabel('Prediction Correlation Coefficient')
    plt.axis('tight')
    plt.savefig(os.path.join(figure_dir, 'peak_time_vs_cc.svg'))

    #plot all the sites and electrodes, along with their correlation coefficient and latency
    #plt.ion()
    all_sites_fig = plt.figure(figsize=(28, 16))
    plt.subplots_adjust(bottom=0.04, top=0.97, right=0.99, left=0.04, wspace=0.10)
    gsres = 2
    ncols = len(site_ids)*gsres
    gs = GridSpec(1, ncols)
    rcParams.update({'font.size': 18})
    im = None
    for k,site_id in enumerate(site_ids):
        #ax = plt.subplot(1, len(site_ids), k+1)
        i1 = k*gsres
        i2  = i1+gsres
        print 'i1=%d, i2=%d' % (i1, i2)
        ax = plt.subplot(gs[0, i1:i2])
        im = plt.imshow(site_cc_mats[site_id], interpolation='nearest', aspect='auto', vmin=0, vmax=1, extent=[0, 4, 0, 8], origin='upper')
        for row in range(8):
            for col in range(4):
                txt = site_cc_regions[site_id][row, col]
                plt.text(col+0.50, (7-row)+0.25, txt, horizontalalignment='center')
        plt.axvline(2.0, color='k')
        plt.xticks([])
        plt.yticks([])
        #plt.colorbar()
        plt.title('Site %d' % site_id)

    plt.savefig(os.path.join(figure_dir, 'all_sites.svg'))

    #make figure for just colorbar
    plt.figure()
    ax = plt.subplot(111)
    plt.colorbar(im)
    plt.savefig(os.path.join(figure_dir, 'all_sites_colorbar.svg'))

    #get max and min of all filters
    filt_max = -np.inf
    filt_min = np.inf
    for fdata in filter_list:
        filt = fdata['filter'].squeeze()
        filt_max = max(filt_max, filt.max())
        filt_min = min(filt_min, filt.min())
    filt_abs_max = max(abs(filt_min), abs(filt_max))

    #print out peak times by region
    print ''
    print 'Average Filter Peak Times By Region:'
    fptimes = [(reg, ptimes.mean(), ptimes.std(ddof=1)) for reg,ptimes in filt_peak_times_by_region.iteritems()]
    fptimes.sort(key=operator.itemgetter(1))
    for reg,tmean,tstd in fptimes:
        print '%s: %0.3f +/- %0.3f' % (reg, tmean, tstd)

    #print out avg performance by region
    print ''
    print 'Average Performance By Region:'
    fperf = [(reg, fcc.mean(), fcc.std(ddof=1)) for reg,fcc in filt_cc_by_region.iteritems()]
    fperf.sort(key=operator.itemgetter(1))
    for reg,perf_mean,perf_std in fperf:
        print '%s: %0.3f +/- %0.3f' % (reg, perf_mean, perf_std)

    regs_to_use = [reg for reg,pmean,pstd in fptimes if reg != '?']

    #make a boxplot of peak filter time by region
    max_peak_time = np.max([ptimes.max() for ptimes in filt_peak_times_by_region.values()])
    plt.figure(figsize=(16, 9))
    boxplot_from_dict(filt_peak_times_by_region, regs_to_use)
    plt.ylim([0.0, max_peak_time])
    plt.title('Peak Filter Time by Region')
    plt.xlabel('Region')
    plt.ylabel('Filter Time (ms)')
    plt.savefig(os.path.join(figure_dir, 'peak_time_by_region.svg'))

    #make a boxplot of encoder performance by region
    regs_to_use = [reg for reg,ccmean,ccstd in fperf if reg != '?']
    plt.figure(figsize=(16, 9))
    boxplot_from_dict(filt_cc_by_region, regs_to_use)
    plt.title('Encoder Performance by Region')
    plt.xlabel('Region')
    plt.ylabel('Correlation Coefficient')
    plt.savefig(os.path.join(figure_dir, 'cc_by_region.svg'))

    #plot all the filters by region
    plt.figure(figsize=(6, 12))
    #rcParams.update({'font.size': 22})
    plt.subplots_adjust(bottom=0.10, top=0.98, right=0.95, left=0.05, wspace=0.0)

    nsp = len(regs_to_use)

    for k,reg in enumerate(regs_to_use):
        plt.subplot(nsp, 1, k+1)
        for fdata in filter_by_region[reg]:
            lags = fdata['lf'].lags
            filt = fdata['filter'].squeeze()
            tfilt = (lags / sample_rate)*1e3
            ex = int(0.100*sample_rate)
            plt.plot(tfilt[:ex], filt[:ex], alpha=0.70, linewidth=2.5)
        plt.axis('tight')
        plt.ylim([-filt_abs_max, filt_abs_max])
        plt.yticks([])
        if k < nsp-1:
            plt.xticks([])
        else:
            plt.xlabel('Lag (ms)')
        plt.text(80, filt_abs_max-(0.50*filt_abs_max), reg)
    plt.axis('tight')
    plt.savefig(os.path.join(figure_dir, 'filters_by_region.svg'))

    #method that's executed when an electrode is clicked on
    def onclick(event):
        width_px = all_sites_fig.get_figwidth()*all_sites_fig.get_dpi()
        site_index = int((event.x / width_px)*4.0)
        site_id = site_ids[site_index]

        row = int(np.floor(event.ydata))
        col = int(np.floor(event.xdata))
        electrode = coords[7-row, col]
        lf = models_by_site_and_electrode[(site_id, electrode)]
        mean_filter = lf.filters.mean(axis=0)
        mean_bias = lf.biases.mean()
        sr = lf.sample_rate
        lags = (lf.lags / sr)*1e3
        peak_time,ymin,tmax,ymax = compute_filter_props(mean_filter.squeeze(), lags)

        plt.figure()
        plt.plot(lags, mean_filter.squeeze(), 'k-', linewidth=2.0)
        plt.plot(tmax, ymax, 'ro')
        plt.plot(peak_time, ymin, 'bo')
        plt.axis('tight')
        plt.title('Filter Site %d, electrode %d' % (site_id, electrode))
        plt.xlabel('Lag (ms)')

        resps = list()
        lfp_ps = dict()
        env_ps = dict()
        model_ps = dict()
        for sid,stim_env in stim_envelopes.iteritems():

            lfp_tensor = lfp_tensors_by_stim[sid]
            lfp = lfp_tensor[site_index, electrode-1, :]
            stim_env.resize([len(stim_env), 1])

            mresp = fast_conv(stim_env, mean_filter, lf.lags, bias=mean_bias)
            C = np.corrcoef(lfp, mresp)
            cc = C[0, 1]
            resps.append( (stim_env, sid, lfp, mresp, cc, np.abs(cc)) )

            max_val = np.max([np.abs(lfp).max(), np.abs(mresp).max()])
            lfreq,lpspec = power_spectrum(lfp.squeeze(), sample_rate, log=True, max_val=max_val)
            mfreq,mpspec = power_spectrum(mresp.squeeze(), sample_rate, log=True, max_val=max_val)
            efreq,epspec = power_spectrum(stim_env.squeeze(), sample_rate, log=True)

            lfp_ps[sid] = (lfreq, lpspec)
            model_ps[sid] = (mfreq, mpspec)
            env_ps[sid] = (efreq, epspec)

        resps.sort(key=operator.itemgetter(-1))

        for k in [0, 12, -1]:
            stim_env, sid, lfp, mresp, cc, cc_abs = resps[k]
            t = np.arange(len(lfp)) / sr

            plt.figure()
            plt.subplot(3, 1, 1)
            plt.plot(t, stim_env[:, 0], 'b-', linewidth=2)
            plt.axis('tight')
            #plt.xlabel('Time (s)')
            plt.ylabel('Amplitude (dB)')

            plt.subplot(3, 1, 2)
            plt.plot(t, lfp, 'k-', linewidth=2)
            plt.plot(t, mresp, 'r-', linewidth=2)
            plt.legend(['Real', 'Model'])
            plt.axis('tight')
            plt.title('cc=%0.2f' % cc)
            plt.xlabel('Time (s)')
            plt.ylabel('Z-score')

            lfreq,lpspec = lfp_ps[sid]
            mfreq,mpspec = model_ps[sid]
            efreq,epspec = env_ps[sid]

            C = np.corrcoef(lpspec, mpspec)
            pscc = C[0, 1]
            plt.subplot(3, 1, 3)
            plt.plot(efreq, epspec, 'b-', linewidth=2)
            plt.plot(lfreq, lpspec, 'k-', linewidth=2, alpha=0.75)
            plt.plot(mfreq, mpspec, 'r-', linewidth=2, alpha=0.75)
            plt.legend(['Stim', 'Real', 'Model'])
            plt.axis('tight')
            #plt.title('cc=%0.2f' % pscc)
            plt.xlabel('Frequency (Hz)')
            plt.ylabel('Power (dB)')

            plt.suptitle('Site %d, elec %d' % (site_id, electrode))
        plt.show()

        print 'site_id=%d, (%f, %f), row=%d, col=%d, electrode=%d' % \
              (site_id, event.xdata, event.ydata, row, col, electrode)
    cid = all_sites_fig.canvas.mpl_connect('button_press_event', onclick)

    plt.show()


def compute_filter_props(filt, lags):

    imin = filt.argmin()
    imax = filt.argmax()

    if imax < imin:
        #definitely not what we're looking for
        imax = filt[imin:].argmax() + imin

    tmax = lags[imax]
    tmin = lags[imin]

    return tmin,filt[imin],tmax,filt[imax]


if __name__ == '__main__':


    f = tables.open_file('/auto/k8/fdata/mschachter/tdt_continuous/GreBlu9508M/GreBlu9508M_tabled.h5', mode='r')

    """
    secombos = ([3, 'Call1', 20], [3, 'Call2', 29])
    stim_ids = [277]

    plot_encoders(f, secombos, output_dir='/auto/k8/fdata/mschachter/analysis/stim_env_encoder', edesc='',
                  avg_file='/auto/k8/fdata/mschachter/analysis/stim_cond/GreBlu9508M_stim_cond_lfp.h5', stim_ids=stim_ids)

    plt.show()
    """

    aggregate_encoders(f)

    #filter_list = get_encoder_data(f, csv_output_file='/auto/k8/fdata/mschachter/analysis/stim_env_encoder/filter_list.csv')

    f.close()