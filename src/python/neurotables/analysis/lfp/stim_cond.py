from matplotlib import rcParams
import numpy as np
import operator
import h5py
from scipy.ndimage import convolve1d
from lasp.signal import lowpass_filter
from lasp.spikes import compute_psth
from lasp.timefreq import compute_mean_spectrogram, postprocess_spectrogram
from neurotables.analysis.lfp.figures import plot_electrodes_2figs
from neurotables.objects.cells import *
from neurotables.analysis.lfp.objects import *


def get_stim_conditioned_lfps(site, protocol, sound, post_stim_time=1.0):
    #get some LFP properties
    e = site.electrodes[0]
    lfp_pr = e.get_lfp_protocol_response(protocol.name)
    sr = lfp_pr.lfp.sample_rate

    #get the event times associated with the given sound
    etimes = np.array([(se.start_time, se.end_time+post_stim_time) for se in protocol.events if se.sound.id == sound.id])
    print 'Sound id %d has %d presentations for site.id=%d, protocol.name=%s' % (sound.id, len(etimes), site.id, protocol.name)
    if len(etimes) == 0:
        return None,None,None
    #duration = np.min(etimes[:, 1] - etimes[:, 0])
    duration = np.max(etimes[:, 1] - etimes[:, 0])

    #sort electrodes by hemisphere
    ehemi = [(electrode, electrode.hemisphere) for electrode in site.electrodes]
    ehemi.sort(key=operator.itemgetter(-1))

    #get the multi-electrode LFPs that correspond to the given repeats of the stimulus
    electrode_indices = dict()
    num_electrodes = len(ehemi)
    ntrials = etimes.shape[0]
    nduration = int(duration*sr)
    lfps = np.zeros([ntrials, num_electrodes, nduration])
    for k,(electrode,hemi) in enumerate(ehemi):
        eindex = electrode.number - 1
        lfp_pr = electrode.get_lfp_protocol_response(protocol.name)
        lfp = lfp_pr.waveform
        lfp -= lfp.mean()
        lfp /= lfp.std(ddof=1)
        for j,(stime, etime) in enumerate(etimes):
            si = int(stime*sr)
            ei = si + nduration
            lfps[j, eindex, :] = lfp[si:ei]
        electrode_indices[electrode.number] = eindex
    electrode_indices = electrode_indices

    return electrode_indices,lfps,sr


def get_stim_avg_neurograms(site, protocol, sound, post_stim_time=1.0, sort_type='single'):

    #get event times for sound
    etimes = np.array([(e.start_time, e.end_time+post_stim_time) for e in protocol.events if e.sound.id == sound.id])
    duration = np.min(etimes[:, 1] - etimes[:, 0])

    #compute PSTH for each cell on each electrode
    neurograms_by_electrode = dict()
    for electrode in site.electrodes:
        neurogram = list()
        for cell in electrode.cells:
            if cell.sort_type != sort_type:
                continue
            resps = [cpr for cpr in cell.protocol_responses if cpr.protocol.name == protocol.name]
            assert len(resps) == 1
            spikes = resps[0].spikes

            trials = list()
            for stime,etime in etimes:
                #get the spike times that fall within the given stimulus presentation
                ti = (spikes >= stime) & (spikes <= etime)
                trials.append( spikes[ti] - stime )

            spike_t,spike_counts = compute_psth(trials, duration, bin_size=0.001)
            neurogram.append(spike_counts / len(trials))
        if len(neurogram) > 0:
            neurograms_by_electrode[electrode.number] = np.array(neurogram)

    #get electrode geometry and coordinates
    geom = site.electrodes[0].geometry
    coords = geom.coordinates

    #group neurograms together by hemisphere and column
    neurograms = dict()
    electrode_data = list()
    for e in site.electrodes:
        row,col = np.nonzero(coords == e.number)
        electrode_data.append( (e.number, e.hemisphere, row, col % 2) )
    electrode_data.sort(key=operator.itemgetter(3))
    electrode_data.sort(key=operator.itemgetter(2))
    electrode_data.sort(key=operator.itemgetter(1))
    #for enumber,ehemi,erow,ecol in electrode_data:
        #print 'electrode %d, %s, (%d, %d)' % (enumber, ehemi, erow, ecol)

    for hemi,col in [('L', 0), ('L', 1), ('R', 0), ('R', 1)]:
        enumbers = [enumber for enumber,ehemi,erow,ecol in electrode_data if ecol == col and ehemi == hemi]
        if len(enumbers) > 0:
            total_neurogram = np.concatenate([neurograms_by_electrode[enumber] for enumber in enumbers if enumber in neurograms_by_electrode])
            neurograms[(hemi, col)] = total_neurogram

    return neurograms


class StimAveragedData(object):

    def __init__(self, f, site, protocol, sound_id, sort_type='single'):

        self.site = site
        #get some LFP properties
        e = site.electrodes[0]
        lfp_pr = e.get_lfp_protocol_response(protocol.name)
        sr = lfp_pr.lfp.sample_rate
        geom = e.geometry
        coords = geom.coordinates
        sr = sr
        self.coords = coords

        #get the sound object
        sound = Sound(f)
        assert sound.from_id(sound_id)

        #get the LFPs
        post_stim_time = 1.0
        self.electrode_indices,lfps,lfp_sample_rate = \
            get_stim_conditioned_lfps(site, protocol, sound, post_stim_time=post_stim_time)
        self.lfp_means = lfps.mean(axis=0).squeeze()
        self.lfp_stds = lfps.std(axis=0, ddof=1).squeeze()
        del lfps
        nduration = self.lfp_means.shape[-1]

        #get the stimulus spectrogram
        spec = sound.spectrogram
        sdata = copy.copy(spec.data)
        thresh = np.percentile(sdata.ravel(), 20)
        sdata[sdata < thresh] = thresh

        nbins_post = int(post_stim_time / 0.001)
        sdata_pad = np.ones( (sdata.shape[0], sdata.shape[1]+nbins_post) )*thresh
        sdata_pad[:, :sdata.shape[1]] = sdata

        spec_t = np.arange(sdata_pad.shape[1])*(1.0 / spec.sample_rate)
        spec_f = spec.frequency

        self.stim_spec_t = spec_t
        self.stim_spec_f = spec_f
        self.stim_spec = sdata_pad

        #get the stimulus amplitude envelope
        self.sr = lfp_sample_rate
        stim_env_sr = lfp_sample_rate # sampling rate of stimulus amplitude envelope
        stim_env = spectral_envelope(sound.waveform, sound.sample_rate, cutoff_freq=self.sr/2.0)
        stim_env_t,stim_env_rs = resample_signal(stim_env, sound.sample_rate, stim_env_sr)
        npad = int(post_stim_time*stim_env_sr)
        stim_env_pad = np.zeros([len(stim_env_rs)+npad])
        stim_env_pad[:len(stim_env_rs)] = stim_env_rs
        stim_env_t = np.arange(len(stim_env_pad)) / stim_env_sr
        self.stim_env = stim_env_pad
        self.stim_env_t = stim_env_t

        #get neurograms
        self.neurograms = get_stim_avg_neurograms(site, protocol, sound, post_stim_time=post_stim_time, sort_type=sort_type)

        self.lfp_t = np.arange(nduration) / sr

        #get hemispheres
        self.hemispheres = np.unique([e.hemisphere for e in site.electrodes])

        #get anatomical region for each electrode
        electrode_regions = dict()
        for e in site.electrodes:
            reg = '?'
            if e.anatomy.__class__.__name__ == 'ElectrodeAnatomicalLocation':
                reg = e.anatomy.region
            electrode_regions[e.number] = reg
        self.electrode_regions = electrode_regions

    def plot(self, lfp_waveform_bounds=[-1.0, 1.0]):

        def spec_header_func(ax, hemisphere, col):
            plot_spectrogram(self.stim_spec_t, self.stim_spec_f, self.stim_spec, ax=ax, ticks=False, colorbar=False, colormap=cm.gist_yarg)

        def neurogram_footer_func(ax, hemisphere, col):
            key = (hemisphere, col)
            if key in self.neurograms:
                ax.imshow(self.neurograms[key], aspect='auto', interpolation='nearest', cmap=cm.gist_yarg)
                plt.yticks([])
                plt.xlabel('Time (ms)')

        def electrode_waveform_func(ax, enumber, row, col):
            plt.sca(ax)
            si = self.electrode_indices[enumber]
            y = self.lfp_means[si, :]
            yerr = self.lfp_stds[si, :]
            plt.errorbar(self.lfp_t, y, yerr=yerr, ecolor='#aaaaaa', capsize=0, color='k', linewidth=2, alpha=0.75)
            #print 'enumber=%d, row=%d, col=%d' % (enumber, row, col)
            plt.xticks([])

            plt.ylabel('E%d (%s)' % (enumber, self.electrode_regions[enumber]))
            plt.axis('tight')
            plt.ylim(lfp_waveform_bounds)

        def electrode_waveform_with_stimulus_envelope(ax, enumber, row, col):
            plt.sca(ax)
            si = self.electrode_indices[enumber]
            y = self.lfp_means[si, :]
            yerr = self.lfp_stds[si, :]
            plt.errorbar(self.lfp_t, y, yerr=yerr, ecolor='#aaaaaa', capsize=0, color='k', linewidth=2, alpha=0.75)
            senv = (self.stim_env / np.abs(self.stim_env).max())*2.0 - 1.0
            plt.plot(self.stim_env_t, senv, 'g-', linewidth=2.0, alpha=0.6)
            #print 'enumber=%d, row=%d, col=%d' % (enumber, row, col)
            plt.xticks([])

            plt.ylabel('E%d (%s)' % (enumber, self.electrode_regions[enumber]))
            plt.axis('tight')
            plt.ylim(lfp_waveform_bounds)

        def electrode_spectrogram_func(ax, enumber, row, col):
            plt.sca(ax)
            si = self.electrode_indices[enumber]
            y = self.lfp_means[si, :]
            lfp_spec_t,lfp_spec_f,lfp_spec = compute_mean_spectrogram(y, self.sr, win_sizes=[0.050, 0.075, 0.150, 0.250, 0.400, 0.600], num_freq_bands=75)
            plot_spectrogram(lfp_spec_t, lfp_spec_f, lfp_spec, ax=ax, ticks=False, colormap=cm.afmhot_r, colorbar=False)
            plt.ylabel('%d (%s)' % (enumber, self.electrode_regions[enumber]))

        #group electrodes by hemisphere
        hemi_groups = dict()
        for e in self.site.electrodes:
            hemi = e.hemisphere
            if hemi not in hemi_groups:
                hemi_groups[hemi] = list()
            hemi_groups[hemi].append(e.number)

        plot_electrodes_2figs(electrode_waveform_func, self.coords, header_func=spec_header_func,
                              footer_func=neurogram_footer_func, hemisphere_groups=hemi_groups)
        #plot_electrodes_2figs(electrode_spectrogram_func, coords, header_func=spec_header_func, footer_func=neurogram_footer_func)


def plot_stim_conditioned_lfp(f, site_id, protocol_name, sound_id, sort_type='single', lfp_waveform_bounds=[-1.75, 1.75]):

    #get the sound
    s = Sound(f)
    assert s.from_id(sound_id)

    #get the site by id
    site = Site(f)
    assert site.from_id(site_id)

    #get the protocol by name
    pcalls = [p for p in site.protocols if p.name == protocol_name]
    assert len(pcalls) == 1
    protocol = pcalls[0]

    stim_data = StimAveragedData(f, site, protocol, sound_id, sort_type=sort_type)
    stim_data.plot(lfp_waveform_bounds=lfp_waveform_bounds)

def find_common_stims(f):

    sound = Sound(f)
    all_stims = [s.id for s in sound.get_all()]

    site = Site(f)
    for s in site.get_all():
        for p in s.protocols:
            sids = np.unique([e.sound.id for e in p.events])
            all_stims = np.intersect1d(all_stims, sids)

    all_sounds = list()
    #print '# of common stims across sites: %d' % len(all_stims)
    for sid in all_stims:
        s = Sound(f)
        assert s.from_id(sid)
        #print 'sid=%d: type=%s, call_id=%s' % (sid, s.stim_type, s.call_id)
        all_sounds.append(s)

    return all_sounds


def export_stim_cond_lfp_and_envelope(f, output_file, stim_type='song', common_only=False):

    #find common stimuli across sites
    sound = Sound(f)
    all_sounds = sound.get_all()
    if common_only:
        all_sounds = find_common_stims(f)
        print 'Found %d common stims across sites.' % len(all_sounds)
    if stim_type is not None:
        sounds = [s for s in all_sounds if s.stim_type == stim_type]
    else:
        sounds = all_sounds
    if len(sounds) == 0:
        print 'Cannot find any sounds!'
        return

    print 'Using %d stims of type %s' % (len(sounds), stim_type)

    #aggregate stimulus-averaged LFP data across sites for common stimuli
    hf = h5py.File(output_file, 'w')

    s = Site(f)
    all_sites = s.get_all()

    #obtain the sample rate from one of the LFP objects
    lfp_sample_rate = all_sites[0].electrodes[0].lfp.sample_rate

    #go through each sound, obtain the envelope, and the LFPs that responded to that sound
    for sound in sounds:

        #get sound amplitude envelope
        stim_env = spectral_envelope(sound.waveform, sound.sample_rate, cutoff_freq=lfp_sample_rate / 2.0)
        stim_env_t,stim_env_rs = resample_signal(stim_env, sound.sample_rate, lfp_sample_rate)
        stim_spec_t,stim_spec_f,stim_spec,stim_spec_rms = gaussian_stft(sound.waveform, sound.sample_rate,
                                                                        window_length=0.007, increment=1.0 / lfp_sample_rate,
                                                                        min_freq=300.0, max_freq=8000.0)
        stim_spec = postprocess_spectrogram(np.abs(stim_spec))

        nt = min(len(stim_spec_t), len(stim_env_t))
        stim_spec_t = stim_spec_t[:nt]
        stim_spec = stim_spec[:, :nt]

        electrode_indices = None

        #keep track of what site/protocol each index in the first dimension of the tensor refers to
        sound_site_ids = list()
        sound_protocol_names = list()

        all_lfps = list()
        for site in all_sites:

            for protocol in site.protocols:
                print 'Getting stimulus conditioned data for sound %d, site %d, protocol %s' % (sound.id, site.id, protocol.name)

                post_stim_time = 0.500
                electrode_indices2,lfps,lfp_sample_rate2 = \
                    get_stim_conditioned_lfps(site, protocol, sound, post_stim_time=post_stim_time)

                if electrode_indices2 is None:
                    print 'No data found!'
                    continue
                else:
                    electrode_indices = electrode_indices2

                ntrials = lfps.shape[0]
                for k in range(ntrials):
                    all_lfps.append(lfps[k, :, :nt])
                    #keep track of the site and protocol associate with these
                    sound_site_ids.append(site.id)
                    sound_protocol_names.append(protocol.name)

        if len(sound_site_ids) == 0:
            continue

        #write the lfp tensors to a file
        print 'writing averaged data for sound %d' % sound.id

        sgrp = hf.create_group('sound%d' % sound.id)
        sgrp.attrs['id'] = sound.id
        sgrp.attrs['original_wavfile'] = sound.original_wavfile
        sgrp.attrs['md5'] = sound.md5
        sgrp.attrs['sample_rate'] = lfp_sample_rate
        sgrp.attrs['stim_type'] = sound.stim_type
        sgrp['envelope'] = stim_env_rs
        sgrp['spectrogram'] = stim_spec
        sgrp['frequencies'] = stim_spec_f
        sgrp['lfps'] = np.array(all_lfps)
        sgrp['lfp_site_ids'] = sound_site_ids
        sgrp['lfp_protocol_names'] = sound_protocol_names

        #make note of what electrode numbers each index refers to
        ei = [(enumber, eindex) for enumber,eindex in electrode_indices.iteritems()]
        ei.sort(key=operator.itemgetter(1))
        sgrp['electrode_indices'] = [e[0] for e in ei]

        hf.flush()

        del all_lfps

    hf.close()


def get_lfp_tensors_by_stim(f, avg_file='/auto/k8/fdata/mschachter/analysis/stim_cond/GreBlu9508M_stim_cond_lfp.h5',
                            stim_type='song'):

    hf = h5py.File(avg_file, 'r')
    #get information about sounds in stim avg file
    sound_ids = [hf[key].attrs['id'] for key in hf.keys()]
    sounds = dict()
    for sid in sound_ids:
        s = Sound(f)
        assert s.from_id(sid)
        sounds[sid] = s

    #restrict fitting by stim type
    sounds = {s.id: s for sid, s in sounds.iteritems() if s.stim_type == stim_type}
    print '[get_lfp_tensors_by_stim] len(sounds)=%d' % len(sounds)

    #variables that will be initialized within the for loop
    sample_rate = None
    site_ids = None
    lfp_protocol_names = None
    electrode_indices = None

    #variables to keep track of stimulus envelope and LFP tensors per sound
    stim_envelopes = dict()
    lfp_tensors_by_stim = dict()

    #for each sound, get the trial-averaged LFPs on each electrode, across sites
    for sid, sound in sounds.iteritems():
        key = 'sound%d' % sid
        sample_rate = hf[key].attrs['sample_rate']
        envelope = np.array(hf[key]['envelope'])

        lfp_site_ids = np.array(hf[key]['lfp_site_ids'])
        lfp_protocol_names = np.array(hf[key]['lfp_protocol_names'])
        electrode_indices = np.array(hf[key]['electrode_indices'])
        lfps = np.array(hf[key]['lfps'])

        stim_envelopes[sid] = envelope

        #create a tensor of LFPs, where the dimensionality is # of sites x # of electrodes x # of time points
        lfps_by_site = list()
        if site_ids is None:
            site_ids = sorted(np.unique(lfp_site_ids))
        for site_id in site_ids:
            sindex = lfp_site_ids == site_id
            #average across protocols within a site
            lfp_site_mean = lfps[sindex, :, :].mean(axis=0)
            lfps_by_site.append(lfp_site_mean)
        lfp_tensors_by_stim[sid] = np.array(lfps_by_site)

    hf.close()

    stim_envelopes = preprocess_envelopes(stim_envelopes, sample_rate)

    return site_ids,sample_rate,stim_envelopes,lfp_tensors_by_stim,electrode_indices


def compute_running_std(x, sample_rate, tau):
    """
        Computes the running standard deviation using a sliding exponential window with decay constant tau.
    """

    ws = int(tau*sample_rate)*10
    t = np.arange(ws) / sample_rate
    win = np.exp(-t / tau)
    win = win[::-1]
    win /= win.sum()

    #compute time-varying mean with exponential window
    mean = convolve1d(x, win, mode='mirror')

    #compute time-varying std with exponential window
    sq = (x - mean)**2
    var = convolve1d(sq, win, mode='mirror')
    std = np.sqrt(var)

    return mean,std


def preprocess_envelope(env, sample_rate, cutoff_freq=None, env_max=None, thresh=-35.0, gain_tau=0.0):
    if env_max is None:
        env_max = env.max()
    if cutoff_freq is not None:
        env = lowpass_filter(env, sample_rate, cutoff_freq)

    env /= env_max
    nz = env > 0.0
    env[nz] = 20*np.log10(env[nz]) - thresh
    env[env < 0.0] = 0.0

    if gain_tau > 0.0:
        rmean,rstd = compute_running_std(env, sample_rate, gain_tau)

        """
        t = np.arange(len(env)) / sample_rate

        plt.figure()
        plt.subplot(2, 1, 1)
        plt.plot(t, env, 'b-')
        plt.plot(t, rmean, 'k-')
        plt.plot(t, rmean+rstd, 'k--', alpha=0.75)
        plt.plot(t, rmean-rstd, 'k--', alpha=0.75)
        plt.axis('tight')

        plt.subplot(2, 1, 2)
        plt.plot(t, env / rstd, 'b-')
        plt.axis('tight')

        plt.suptitle('gain_tau=%0.3f' % gain_tau)

        plt.show()
        """
        nz = rstd > 0.0
        env[nz] /= rstd[nz]

    return env


def preprocess_envelopes(stim_envelopes, sample_rate, cutoff_freq=None, gain_control=False):

    max_val = np.max([env.max() for env in stim_envelopes.values()])
    stim_envelopes2 = dict()
    for sid,env in stim_envelopes.iteritems():
        stim_envelopes2[sid] = preprocess_envelope(env, sample_rate, cutoff_freq=cutoff_freq, env_max=max_val, gain_control=False)
    return stim_envelopes2


def plot_lfp_trials(f, stim_id, electrode, site_id=1, protocol_name='Call1', cutoff_freq=180.0):

    site = Site(f)
    assert site.from_id(site_id)

    pcalls = [p for p in site.protocols if p.name == protocol_name]
    assert len(pcalls) == 1
    protocol = pcalls[0]

    sound = Sound(f)
    assert sound.from_id(stim_id)

    electrode_indices,lfps,sr = get_stim_conditioned_lfps(site, protocol, sound, post_stim_time=0.500)

    eindex = electrode_indices[electrode]

    ntrials = lfps.shape[0]

    stim_env = spectral_envelope(sound.waveform, sound.sample_rate, cutoff_freq=sr / 2.0)
    stim_env_t,stim_env_rs = resample_signal(stim_env, sound.sample_rate, sr)
    stim_env_rs = preprocess_envelope(stim_env_rs, sr)

    nt = min(len(stim_env_rs), lfps.shape[-1])
    t = np.arange(nt) / sr

    lfp_mean = lfps[:, eindex, :nt].mean(axis=0).squeeze()

    plt.figure()
    plt.subplot(2, 1, 1)
    plt.plot(t, stim_env_rs[:nt], 'k-', linewidth=2.0)
    plt.axis('tight')

    plt.subplot(2, 1, 2)
    for k in range(ntrials):
        blue = 1.0 - (float(k)/ntrials)
        red = float(k)/ntrials
        c = [red, 0.0, blue]
        a = 1.0 - (float(k)/ntrials)*0.6

        x = lfps[k, eindex, :nt]
        x = lowpass_filter(x, sr, cutoff_freq)

        plt.plot(t, x, linewidth=2.0, alpha=a, c=c)
    plt.plot(t, lfp_mean[:nt], 'k-', linewidth=3.0)
    plt.axis('tight')

    plt.suptitle('Site %d, E%d, sound=%d' % (site_id, electrode, stim_id))

def plot_spec_and_envelope(f, stim_id):

    sound = Sound(f)
    assert sound.from_id(stim_id)

    sample_rate = 381.0

    stim_env = spectral_envelope(sound.waveform, sound.sample_rate, cutoff_freq=sample_rate / 2.0)
    stim_env_t,stim_env_rs = resample_signal(stim_env, sound.sample_rate, sample_rate)
    stim_env = preprocess_envelope(stim_env_rs, sample_rate)

    spec_t,spec_freq,spec,spec_rms = gaussian_stft(sound.waveform, sound.sample_rate, 0.007, 0.001, min_freq=300.0, max_freq=8000.0)
    log_spec = postprocess_spectrogram(np.abs(spec))

    plt.figure()
    ax = plt.subplot(2, 1, 1)
    plot_spectrogram(spec_t, spec_freq, log_spec, ax=ax, colormap=cm.gist_yarg, colorbar=False)
    plt.axis('tight')

    ax = plt.subplot(2, 1, 2)
    plt.plot(stim_env_t, stim_env, 'k-', linewidth=3.0)
    plt.ylabel('Power')
    plt.xlabel('Time (s)')
    plt.axis('tight')


if __name__ == '__main__':

    f = tables.open_file('/auto/k8/fdata/mschachter/tdt_continuous/GreBlu9508M/GreBlu9508M_tabled.h5', mode='r')
    #f = tables.open_file('/auto/k8/fdata/tlee/tdt_continuous/LbBla4218M/LbBla4218M_tabled.h5', mode='r')

    ##### Tyler's data
    #plot_stim_conditioned_lfp(f, 1, 'Mask1', 218, sort_type='multi', lfp_waveform_bounds=[-1.75, 1.75])
    #plot_stim_conditioned_lfp(f, 1, 'Mask2', 74, sort_type='multi', lfp_waveform_bounds=[-1.75, 1.75])

    ##### Julie's data
    #compute_spectrum_stats(f, 1, 'Call1')
    #compute_spectrum_stats(f, 2, 'Call2')
    #compute_spectrum_stats(f, 3, 'Call3')

    #plot_stim_conditioned_lfp(f, 1, 'Call1', 17)
    #plot_stim_conditioned_lfp(f, 1, 'Call1', 177)

    #plot_stim_conditioned_lfp(f, 1, 'Call1', 196)

    #plot_stim_conditioned_lfp(f, 2, 'Call2', 196)

    #plot_stim_conditioned_lfp(f, 1, 'Call1', 272)
    #plot_stim_conditioned_lfp(f, 2, 'Call2', 272)
    #plot_stim_conditioned_lfp(f, 3, 'Call3', 272)
    #plot_stim_conditioned_lfp(f, 4, 'Call1', 272)
    #plot_stim_conditioned_lfp(f, 5, 'Call2', 272)

    #plot_stim_conditioned_lfp(f, 3, 'Call3', 196) #song
    #plot_stim_conditioned_lfp(f, 3, 'Call3', 17)  #DC
    #plot_stim_conditioned_lfp(f, 3, 'Call3', 277) #song
    #plot_stim_conditioned_lfp(f, 3, 'Call3', 253) #LT
    #plot_stim_conditioned_lfp(f, 3, 'Call3', 192) #Di
    #plot_stim_conditioned_lfp(f, 3, 'Call3', 287) #song

    #plot_lfp(f, 3, 'Call3', 1333.5, 1337) # song
    #plot_lfp(f, 3, 'Call3', 1428.0, 1432.0)
    #plot_lfp(f, 3, 'Call3', 1428.75, 1429.5)
    #plot_lfp(f, 3, 'Call3', 1530.0, 1533.0)
    #plot_lfp(f, 3, 'Call3', 1524.0, 1530.0)

    #plot_lfp(f, 4, 'Call1', 235.0, 242.0)
    #plot_lfp(f, 4, 'Call1', 424.0, 429.0)

    #plot_lfp(f, 5, 'Call2', 902.0, 909.0)
    #plot_lfp(f, 5, 'Call2', 914.0, 921.0)

    #plt.show()

    #export_stim_cond_lfp_and_envelope(f, '/auto/k8/fdata/mschachter/analysis/stim_cond/GreBlu9508M_stim_cond_lfp.h5', stim_type='song', common_only=True)
    #export_stim_cond_lfp_and_envelope(f, '/tmp/LbBla4218M_stim_avg_lfp.h5', stim_type='song')

    #plot_lfp_trials(f, 277, 23, site_id=2, protocol_name='Call2', cutoff_freq=20.0)

    rcParams.update({'font.size':22})
    plot_spec_and_envelope(f, 277)
    plt.show()

    f.close()
