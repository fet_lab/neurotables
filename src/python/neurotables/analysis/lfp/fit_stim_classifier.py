from neurotables.analysis.lfp.fit_trial_envelope_models import get_labeled_stimuli
from neurotables.objects.cells import Site


def get_spike_data(f, site_id=1, protocol_name='Call1'):

    site = Site(f)
    assert site.from_id(site_id)

    plist = [p for p in site.protocols if p.name == protocol_name]
    assert len(plist) == 1
    protocol = plist[0]

    stim_ids,stim_times,num_repeats,stim_blocks,block_ends = get_labeled_stimuli(f, site_id=site_id, protocol_name=protocol_name)










