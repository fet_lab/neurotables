import numpy as np
import os
import sys
import tables
import time

from lasp.fit_strf import fit_strf_lasso, fit_strf_ridge
from lasp.incrowd import fast_conv

from neurotables.analysis.lfp.fit_envelope import LinearFilter
from neurotables.analysis.lfp.fit_trial_envelope_models import get_decoder_data, get_random_cv_groups, get_temporal_cv_groups, get_stim_representations, \
    get_lfps_for_decoder
from neurotables.analysis.lfp.stim_cond import find_common_stims


def get_spectrogram_data_matrix(stim_specs, spec_freq, stim_cond_lfps, group_identities_to_use=None, pad_length=0):

    if group_identities_to_use is None:
        #a group identity is a tuple that looks like (stim_id, trial_number), indicating that a given
        #trial number for a given stimulus should be used as data. If a list of group identities is
        #not specified, then we'll assume all stimuli and their trials should be used

        stim_ids = stim_specs.keys()
        ntrials = {stim_id:stim_cond_lfps[stim_id].shape[0] for stim_id in stim_ids}

        group_identities_to_use = list()
        for stim_id in stim_ids:
            for trial_num in ntrials[stim_id]:
                group_identities_to_use.append( (stim_id, trial_num) )
    else:
        stim_ids = np.unique([x[0] for x in group_identities_to_use])

    #compute total length of data vectors
    stim_lens = {stim_id:stim_cond_lfps[stim_id].shape[-1] for stim_id in stim_ids}
    total_lens = [stim_lens[stim_id] for stim_id,trial_num in group_identities_to_use]
    total_pad_len = len(total_lens)*pad_length
    total_len = np.sum(total_lens) + total_pad_len

    #construct empty data vectors
    X = np.zeros([total_len, len(spec_freq)])
    y = np.zeros([total_len])

    #populate the data vectors with envelopes and LFPs
    group_index = np.zeros([total_len])
    group_identity = dict()
    index = 0
    group = 0
    for stim_id,trial_num in group_identities_to_use:
        nt = stim_lens[stim_id]
        end_index = index + nt

        spec = stim_specs[stim_id]
        spec_end_index = index + spec.shape[1]
        print 'spec.shape=',spec.shape
        print 'nt=%d, end_index-start_index=%d, spec_end_index-index=%d' % (nt, end_index-index, spec_end_index-index)
        X[index:spec_end_index, :] = spec.T
        y[index:end_index] = stim_cond_lfps[stim_id][trial_num, :]
        group_index[index:end_index] = group
        index = end_index + pad_length
        group_identity[group] = (stim_id, trial_num)
        group += 1

    return X,y,group_index,group_identity


def fit_spectrogram_encoder(f, electrode, site_id=1, protocol_name='Call1',
                            stim_type='song', cv_type='random',
                            opt_params={'solver':'lasso', 'lambda1':1.0, 'lambda2':1.0}):



    #get LFP data
    """
    stim_envelopes,stim_cond_lfps,sample_rate = get_decoder_data(f, electrode,
                                                                 site_id=site_id, protocol_name=protocol_name,
                                                                 stim_ids=None, stim_type=stim_type)
    """

    sounds = find_common_stims(f)
    stim_ids = [s.id for s in sounds if s.stim_type == stim_type]

    #get LFPs
    stim_cond_lfps,sample_rate = get_lfps_for_decoder(f, [electrode], stim_ids, site_id=site_id, protocol_name=protocol,
                                                      filter=None, cutoff_freq=None, post_stim_time=0.050)
    #squeeze the LFPs
    for stim_id in stim_ids:
        lfp = stim_cond_lfps[stim_id]
        stim_cond_lfps[stim_id] = lfp.squeeze()

    #get cross validation data
    cv_sets = None
    if cv_type == 'random':
        cv_sets = get_random_cv_groups(f, site_id, protocol_name, stim_types=[stim_type])
    elif cv_type == 'temporal':
        cv_sets = get_temporal_cv_groups(f, site_id, protocol_name, num_training_blocks=3, stim_types=[stim_type])
    else:
        print 'No such cross validation type \'%s\', valid options are \'random\' and \'temporal\'.' % cv_type

    nfolds = len(cv_sets)

    #get stimulus spectrograms
    stim_specs,stim_t,spec_freq,sample_rate = get_stim_representations(f, site_id=site_id, protocol_name=protocol_name,
                                                                       stim_type=stim_type, type='spectrogram')

    #make sure spectrograms and LFPs are the same size
    for stim_id in stim_ids:
        spec = stim_specs[stim_id]
        lfp = stim_cond_lfps[stim_id]
        nt = lfp.shape[-1]
        if spec.shape[-1] != nt:
            new_spec = np.zeros([spec.shape[0], nt])
            new_spec[:, :spec.shape[1]] = spec
            stim_specs[stim_id] = new_spec

    #initialize data structure to store encoder
    encoder_length = 0.100
    encoder_lags = range(0, int(encoder_length*sample_rate))

    encoder_filt = LinearFilter()
    encoder_filt.sample_rate = sample_rate
    encoder_filt.cv_sets = cv_sets
    encoder_filt.lags = encoder_lags
    encoder_filt.biases = np.zeros([nfolds])
    encoder_filt.filters = np.zeros([nfolds, len(spec_freq), len(encoder_lags)])
    encoder_filt.errs = np.zeros([nfolds])
    encoder_filt.etimes = np.zeros([nfolds])
    encoder_filt.ccs = np.zeros([nfolds])

    #train the models on each holdout set
    for k,(training_group,validation_group) in enumerate(cv_sets):

        filt,bias,err,cc,etime = optimize(stim_specs, stim_t, spec_freq, stim_cond_lfps,
                                          training_group, validation_group, encoder_lags,
                                          opt_params)

        #save encoder data to data structure
        encoder_filt.biases[k] = bias
        encoder_filt.filters[k, :] = filt
        encoder_filt.errs[k] = err
        encoder_filt.ccs[k] = cc
        encoder_filt.etimes[k] = etime

        print 'ENCODER: k=%d, holdout_err=%0.1f, etime=%ds, cc=%0.2f' % (k, err, int(etime), cc)
        print ''
        print '--------------'
        print ''

    return encoder_filt


def optimize(stim_specs, stim_t, spec_freq, stim_cond_lfps, training_group, validation_group,
             encoder_lags, opt_params):

    #calculate padding between stimuli
    pad_length = len(encoder_lags)

    #get training data
    X,y,group_index,group_identity = get_spectrogram_data_matrix(stim_specs, spec_freq, stim_cond_lfps,
                                                                 group_identities_to_use=training_group,
                                                                 pad_length=pad_length)

    #train encoder
    stime = time.time()
    if opt_params['solver'] == 'lasso':
        strf,bias = fit_strf_lasso(X, y, encoder_lags, lambda1=opt_params['lambda1'], lambda2=opt_params['lambda2'])
    elif opt_params['solver'] == 'ridge':
        strf,bias = fit_strf_ridge(X, y, encoder_lags, alpha=opt_params['alpha'])
    else:
        raise ValueError('[fit_trial_spec_model.optimize] No such solver named %s, valid values are lasso, ridge' % opt_params['solver'])
    etime = time.time() - stime

    #get holdout data
    X,y,group_index,group_identity = get_spectrogram_data_matrix(stim_specs, spec_freq, stim_cond_lfps,
                                                                 group_identities_to_use=validation_group,
                                                                 pad_length=pad_length)

    #estimate performance on holdout set
    model_holdout_resp,holdout_err,holdout_cc = compute_model_performance(X, y, strf, bias, encoder_lags)

    return strf, bias, holdout_err, holdout_cc, etime


def compute_model_performance(X, y, strf, bias, lags):

    model_holdout_resp = fast_conv(X, strf, lags, bias=bias)

    holdout_err = ((y - model_holdout_resp) ** 2).sum()
    holdout_cc_mat = np.corrcoef(y, model_holdout_resp)
    holdout_cc = holdout_cc_mat[0, 1]

    return model_holdout_resp,holdout_err,holdout_cc


if __name__ == '__main__':

    f = tables.open_file('/auto/k8/fdata/mschachter/tdt_continuous/GreBlu9508M/GreBlu9508M_tabled.h5', 'r')

    site_id = 4
    protocol = 'Call1'
    electrode = 22
    output_dir = '/auto/k8/fdata/mschachter/analysis/envelope_models/'
    ofile = os.path.join(output_dir, 'encoder_decoder_Site%d_%s_e%d.h5' % (site_id, protocol, electrode))

    """
    stim_specs,stim_t,spec_freq,sample_rate = get_stim_representations(f, site_id=site_id, protocol_name=protocol, stim_type='song', type='spectrogram')
    dlist = list()
    for stim_id,stim_spec in stim_specs.iteritems():
        dlist.append({'stim_id':stim_id, 'freq':spec_freq, 't':stim_t[stim_id], 'spec':stim_spec, 'sample_rate':sample_rate})

    def plot_func(pdata, ax):
        plot_spectrogram(pdata['t'], pdata['freq']/1e3, pdata['spec'], ax=ax, fmin=0.3, fmax=8.0, colormap=cm.gist_yarg, colorbar=True, vmin=0.0, vmax=1.0)
        plt.title('%d' % pdata['stim_id'])
        plt.xlabel('')
        plt.ylabel('')

    multi_plot(dlist, plot_func, 'Site %d, %s' % (site_id, protocol), nrows=4, ncols=4)
    """

    #encoder_filt = fit_spectrogram_encoder(f, electrode, site_id=site_id, protocol_name=protocol, cv_type='random',
    #                                       opt_params={'solver':'lasso', 'lambda1':1.0, 'lambda2':1.0})

    encoder_filt = fit_spectrogram_encoder(f, electrode, site_id=site_id, protocol_name=protocol, cv_type='random',
                                           opt_params={'solver':'ridge', 'alpha':1.0})

    encoder_filt.to_file('/tmp/encoder_spec_e%d.h5' % electrode, include_dataset=False)

    #encoder_filt = fit_spectrogram_encoder(f, electrode, site_id=site_id, protocol_name=protocol, cv_type='random',
    #                                       threshold=1.0, niters=100, opt_type='gd')

