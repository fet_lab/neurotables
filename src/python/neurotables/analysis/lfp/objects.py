
import numpy as np
from neurotables.objects.cells import *


class ElectrodeLocation(TablesBackedObject):

    class Row(TablesBackedRow):
        # These three should be moved to ElectrodeGeometry
        angle = tables.Float64Col() # Angle of entrance for the array (radians)
        row_spacing = tables.Float64Col() # spacing between rows of a 2-d array (um)
        col_spacing = tables.Float64Col() # Spacing between columns of a 2-d array (um)

        row_spacing = 250
        col_spacing = 500

        x = tables.Float64Col() # The medial-lateral distance from the Y-sinus
        y = tables.Float64Col() # The rostral-caudal distance from the Y-sinus
        z = tables.Float64Col() # The dorsal-ventral distance from the Y-sinus

    # Might not need this if the anatomy supplies these coordinates
    def compute_location(self):

        e = self.electrode
        s = e.site
        depth = s.depth
        coords = np.where(np.flipud(e.geometry.coordinates) == e.number)


class ElectrodeAnatomicalLocation(TablesBackedObject):

    class Row(TablesBackedRow):
        electrode_id = IdCol()
        region = tables.StringCol(10, dflt='')
        dist_from_midline = tables.Float64Col()
        dist_from_l2a = tables.Float64Col()
        hemisphere = tables.StringCol(1, dflt='')
        headstage_type = tables.StringCol(1, dflt='')


class LFPEMD(TablesBackedObject):

    class Row(TablesBackedRow):
        protocol_response_id = IdCol()
        emd = DatasetCol()


class LFPSpectrogram(TablesBackedObject):

    class Row(TablesBackedRow):
        protocol_response_id = IdCol()
        t = DatasetCol()
        freq = DatasetCol()
        tf = DatasetCol()


tj_relations.one2many(LFPProtocolResponse, 'emd', LFPEMD, 'protocol_response_id', 'protocol_response', one2one=True)
tj_relations.one2many(LFPProtocolResponse, 'spectrogram', LFPSpectrogram, 'protocol_response_id', 'protocol_response', one2one=True)

tj_relations.one2many(ElectrodeData, 'anatomy', ElectrodeAnatomicalLocation, 'electrode_id', 'electrode', one2one=True)
tj_relations.one2many(ElectrodeData, 'location', ElectrodeLocation, 'electrode_id', 'electrode')