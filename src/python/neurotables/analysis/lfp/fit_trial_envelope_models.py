import os
from scipy.fftpack import fft,fftfreq
from scipy.interpolate import interp1d
import time
import h5py
from matplotlib import rcParams
from lasp.incrowd import ConvolutionalInCrowdModel, InCrowd
from lasp.optimization import ThresholdGradientDescent, ConvolutionalLinearModel, find_index
from lasp.plots import multi_plot
from lasp.signal import lowpass_filter, highpass_filter
from lasp.timefreq import postprocess_spectrogram
from neurotables.analysis.lfp.fit_envelope import LinearFilter, boxplot_from_dict
from neurotables.analysis.lfp.stim_cond import find_common_stims, get_stim_conditioned_lfps, preprocess_envelope
from neurotables.objects.cells import *


def get_stim_representations(f, site_id=1, protocol_name='Call1', stim_type='song', stim_ids=None, gain_tau=0.0,
                             type='envelope', sample_rate=None):

    if stim_ids is None and site_id is not None and protocol_name is not None:
        #load up site
        site = Site(f)
        assert site.from_id(site_id)

        #load up protocol
        pcalls = [p for p in site.protocols if p.name == protocol_name]
        assert len(pcalls) == 1
        protocol = pcalls[0]

        #get some LFP properties
        e = site.electrodes[0]
        lfp_pr = e.get_lfp_protocol_response(protocol.name)
        sample_rate = lfp_pr.lfp.sample_rate

        #get stimuli
        stim_ids = np.unique([e.sound.id for e in protocol.events if e.sound.stim_type == stim_type])
        print 'Identified %d stimuli of type %s to use for encoding/decoding' % (len(stim_ids), stim_type)

    assert sample_rate is not None
    assert stim_ids is not None

    stim_reps = dict()
    stim_t = dict()
    max_amp = -np.inf
    spec_freq = None
    for stim_id in stim_ids:
        sound = Sound(f)
        assert sound.from_id(stim_id)

        if type == 'envelope':
            #construct the stimulus amplitude envelope
            stim_env = spectral_envelope(sound.waveform, sound.sample_rate, cutoff_freq=None)
            stim_env_t,stim_env_rs = resample_signal(stim_env, sound.sample_rate, sample_rate)
            del stim_env_t
            del stim_env
            stim_reps[stim_id] = stim_env_rs

            max_amp = max(stim_env_rs.max(), max_amp)

        elif type == 'spectrogram':
            spec_inc = 1.0 / sample_rate
            win_len = 0.007
            spec_t,spec_freq,spec,spec_rms = gaussian_stft(sound.waveform, sound.sample_rate,
                                                           window_length=win_len, increment=spec_inc,
                                                           min_freq=300.0, max_freq=8000.0)
            spec = postprocess_spectrogram(np.abs(spec))
            stim_reps[stim_id] = spec
            stim_t[stim_id] = spec_t

            max_amp = max(max_amp, spec.max())

    for stim_id in stim_reps.keys():
        if type == 'envelope':
            stim_reps[stim_id] = preprocess_envelope(stim_reps[stim_id], sample_rate, thresh=-35.0, env_max=max_amp, gain_tau=gain_tau)
        elif type == 'spectrogram':
            stim_reps[stim_id] = stim_reps[stim_id] / max_amp

    if type == 'envelope':
        return stim_reps,sample_rate
    elif type == 'spectrogram':
        return stim_reps,stim_t,spec_freq,sample_rate


def get_lfps_for_decoder(f, electrodes, stim_ids, site_id=1, protocol_name='Call1', filter=None, cutoff_freq=None, post_stim_time=0.200):

    #load site
    site = Site(f)
    assert site.from_id(site_id)

    #load protocol
    plist = [p for p in site.protocols if p.name == protocol_name]
    assert len(plist) == 1
    protocol = plist[0]

    #get the LFPs associated with each stimulus
    sample_rate = None
    stim_cond_lfps = dict()
    for stim_id in stim_ids:
        sound = Sound(f)
        assert sound.from_id(stim_id)

        #get the LFPs associated with this stimulus
        electrode_indices,lfps,sample_rate = get_stim_conditioned_lfps(site, protocol, sound, post_stim_time=post_stim_time)
        eindex = [electrode_indices[e] for e in electrodes]

        nt = lfps.shape[-1]
        ntrials = lfps.shape[0]
        lfps = lfps[:, eindex, :]

        #low-pass the LFPs if requested
        if filter is not None and cutoff_freq is not None:
            for j in range(len(electrodes)):
                for k in range(ntrials):
                    if filter == 'high':
                        lfps[k, j, :] = highpass_filter(lfps[k, j, :], sample_rate, cutoff_freq)
                    elif filter == 'low':
                        lfps[k, j, :] = lowpass_filter(lfps[k, j, :], sample_rate, cutoff_freq)
        stim_cond_lfps[stim_id] = lfps

    return stim_cond_lfps,sample_rate


def get_decoder_data(f, electrode, site_id=1, protocol_name='Call1', filter=None, cutoff_freq=None,
                     stim_ids=None, stim_type='song', gain_tau=0.0):
    """
        Get data adequate for fitting linear models that decode the stimulus amplitude envelope from the trial-to-trial
        LFP.
    """

    #load up site
    site = Site(f)
    assert site.from_id(site_id)

    #load up protocol
    pcalls = [p for p in site.protocols if p.name == protocol_name]
    assert len(pcalls) == 1
    protocol = pcalls[0]

    #get stimuli to decode
    if stim_ids is None:
        stim_ids = np.unique([e.sound.id for e in protocol.events if e.sound.stim_type == stim_type])
        print 'Identified %d stimuli of type %s to use for encoding/decoding' % (len(stim_ids), stim_type)

    stim_envelopes,sample_rate = get_stim_representations(f, site_id=site_id, protocol_name=protocol_name,
                                                          stim_type=stim_type, stim_ids=stim_ids,
                                                          gain_tau=gain_tau, type='envelope')

    #get the LFPs
    stim_cond_lfps,sample_rate = get_lfps_for_decoder(f, [electrode], stim_ids, site_id=site_id,
                                                      protocol_name=protocol_name, filter=filter, cutoff_freq=cutoff_freq,
                                                      post_stim_time=0.200)

    #ensure that stimulus amplitude envelopes are same length as LFPs
    for stim_id in stim_ids:
        env = stim_envelopes[stim_id]
        lfp = stim_cond_lfps[stim_id]
        nt = lfp.shape[-1]
        if len(env) != nt:
            new_env = np.zeros([nt])
            new_env[:len(env)] = env
            stim_envelopes[stim_id] = new_env
            del env
        #squeeze the stimulus conditioned LFP, because it's only one electrode
        stim_cond_lfps[stim_id] = lfp.squeeze()

    return stim_envelopes,stim_cond_lfps,sample_rate


def get_envelope_data_matrix(stim_envelopes, stim_cond_lfps, group_identities_to_use=None, encoder=False):

    if group_identities_to_use is None:
        #a group identity is a tuple that looks like (stim_id, trial_number), indicating that a given
        #trial number for a given stimulus should be used as data. If a list of group identities is
        #not specified, then we'll assume all stimuli and their trials should be used

        stim_ids = stim_envelopes.keys()
        ntrials = {stim_id:stim_cond_lfps[stim_id].shape[0] for stim_id in stim_ids}

        group_identities_to_use = list()
        for stim_id in stim_ids:
            for trial_num in range(ntrials[stim_id]):
                group_identities_to_use.append( (stim_id, trial_num) )
    else:
        stim_ids = np.unique([x[0] for x in group_identities_to_use])

    #compute total length of data vectors
    stim_lens = {stim_id:stim_cond_lfps[stim_id].shape[-1] for stim_id in stim_ids}
    total_lens = [stim_lens[stim_id] for stim_id,trial_num in group_identities_to_use]
    total_len = np.sum(total_lens)

    #construct empty data vectors
    X = np.zeros([total_len])
    y = np.zeros([total_len])

    #populate the data vectors with envelopes and LFPs
    group_index = np.zeros([total_len])
    group_identity = dict()
    index = 0
    group = 0
    for stim_id,trial_num in group_identities_to_use:
        #nt = stim_lens[stim_id]
        senv = stim_envelopes[stim_id]
        nt = min(stim_lens[stim_id], len(senv))
        end_index = index + nt
        X[index:end_index] = stim_cond_lfps[stim_id][trial_num, :nt]
        y[index:end_index] = stim_envelopes[stim_id][:nt]
        group_index[index:end_index] = group
        index = end_index
        group_identity[group] = (stim_id, trial_num)
        group += 1

    if encoder:
        Xcpy = copy.copy(X)
        X = y
        y = Xcpy

    X = X.reshape(len(X), 1)

    return X,y,group_index,group_identity


def get_labeled_stimuli(f, site_id=1, protocol_name='Call1', stim_types=['call', 'mlnoise', 'song']):

    #load up site
    site = Site(f)
    assert site.from_id(site_id)

    #load up protocol
    pcalls = [p for p in site.protocols if p.name == protocol_name]
    assert len(pcalls) == 1
    protocol = pcalls[0]

    stim_times = [(e.sound.id,e.start_time,e.end_time) for e in protocol.events if e.sound.stim_type in stim_types]
    stim_types = [(e.sound.stim_type,e.sound.call_id) for e in protocol.events if e.sound.stim_type in stim_types]

    etimes = np.array([e.end_time for e in protocol.events if e.sound.stim_type in stim_types])
    print '# of out-of-order points: %d' % np.diff(etimes <= 0.0).sum()

    #compute trial number for each stimulus
    nrepeats_map = {stim_id:0 for stim_id,start_time,end_time in stim_times}
    num_repeats = list()
    for k,(stim_id,start_time,end_time) in enumerate(stim_times):
        num_repeats.append(nrepeats_map[stim_id])
        nrepeats_map[stim_id] += 1
    num_repeats = np.array(num_repeats)

    #figure out the start and end times of the repeated random blocks
    block_ends = list()
    last_end_time = 0.0
    for k,tnum in enumerate(np.unique(num_repeats)):
        #get indices where stimulus has been presented tnum times
        block_stim_indices = np.where(num_repeats == tnum)[0]
        #block_start_times = np.array([stim_times[stim_index][1] for stim_index in block_stim_indices])
        #block_end_times = np.array([stim_times[stim_index][1] for stim_index in block_stim_indices])

        #find the last occurrance of a stimulus presented tnum times
        block_end_index = block_stim_indices.max()
        block_end_time = stim_times[block_end_index][2]

        block_ends.append( (last_end_time, block_end_time) )
        last_end_time = block_end_time

    #assign a block number to each stimulus presentation
    stim_blocks = list()
    for k,(stim_id,start_time,end_time) in enumerate(stim_times):
        nreps = num_repeats[k]
        block_list = [k for k,(stime,etime) in enumerate(block_ends) if end_time <= etime and end_time > stime]
        #print 'stim_id=%d, time=(%0.3f, %0.3f)' % (stim_id, start_time, end_time)
        #print 'len(block_list)=%d, nreps=%d' % (len(block_list), nreps)
        assert len(block_list) == 1
        block_num = block_list[0]
        stim_blocks.append(block_num)
    stim_blocks = np.array(stim_blocks)

    stim_ids = np.array([x[0] for x in stim_times])
    return stim_ids, np.array([(x[1], x[2]) for x in stim_times]), num_repeats, stim_blocks, block_ends


def plot_protocol_stats(f, site_id=1, protocol_name='Call1', stim_types=['call', 'mlnoise', 'song']):

    stim_ids, stim_times, num_repeats, stim_blocks, block_ends = get_labeled_stimuli(f, site_id, protocol_name)

    plt.figure()
    for stime,etime in block_ends:
        plt.axvline(etime, c='k', alpha=0.5)

    for k,(stime,etime) in enumerate(block_ends):
        #get start times for stims that have the correct and incorrect number of repeats for this block
        good_indices = (stim_blocks == k) & (num_repeats == k)
        bad_indices = (stim_blocks == k) & (num_repeats != k)
        plt.plot(stim_times[good_indices, 0], num_repeats[good_indices], 'ko')
        plt.plot(stim_times[bad_indices, 0], num_repeats[bad_indices], 'ro')

    plt.xlabel('Start Time')
    plt.ylabel('# of Repeats')
    plt.axis('tight')
    plt.suptitle('Site %d, %s' % (site_id, protocol_name))


def get_random_cv_groups(f, site_id=1, protocol_name='Call1', nfolds=5, stim_types=['song', 'noise', 'call']):
    """
        Generate training sets for N fold cross-validation that includes all trials.
    """

    stim_ids, stim_times, num_repeats, stim_blocks, block_ends = get_labeled_stimuli(f, site_id, protocol_name,
                                                                                     stim_types=stim_types)

    all_stim_ids = np.unique(stim_ids)
    np.random.shuffle(all_stim_ids)
    stim_groups = partition_set(all_stim_ids, nfolds)

    cv_groups = list()
    for k,stim_group in enumerate(stim_groups):
        training_ids = np.setdiff1d(all_stim_ids, stim_group)
        validation_ids = stim_group

        #append a (stim_id, trial) tuple for each training stimulus
        training_groups = list()
        for stim_id in training_ids:
            sindex = stim_ids == stim_id
            for block in stim_blocks[sindex]:
                training_groups.append( (stim_id, block) )

        #append a (stim_id, trial) tuple for each validation stimulus
        validation_groups = list()
        for stim_id in validation_ids:
            sindex = stim_ids == stim_id
            for block in stim_blocks[sindex]:
                validation_groups.append( (stim_id, block) )

        cv_groups.append( (training_groups, validation_groups) )

    return cv_groups


def get_temporal_cv_groups(f, site_id=1, protocol_name='Call1', num_training_blocks=3, nfolds=5, stim_types=['song']):

    stim_ids, stim_times, num_repeats, stim_blocks, block_ends = get_labeled_stimuli(f, site_id, protocol_name,
                                                                                     stim_types=stim_types)

    all_stim_ids = np.unique(stim_ids)
    np.random.shuffle(all_stim_ids)
    stim_groups = partition_set(all_stim_ids, nfolds)

    cv_sets = list()
    nblocks = len(block_ends)
    num_sets = nblocks - num_training_blocks + 1
    for k in range(num_sets):
        training_blocks = range(k, k+num_training_blocks)
        validation_blocks = np.setdiff1d(range(nblocks), training_blocks)
        #print 'training_blocks=',training_blocks
        #print 'validation_blocks=',validation_blocks

        for stim_holdout_ids in stim_groups:
            stim_training_ids = np.setdiff1d(all_stim_ids, stim_holdout_ids)
            training_groups = list()
            for tblock in training_blocks:
                good_indices = (num_repeats == tblock) & (stim_blocks == tblock)
                for stim_id in stim_ids[good_indices]:
                    if stim_id in stim_training_ids:
                        training_groups.append((stim_id, tblock))

            validation_groups = list()
            for vblock in validation_blocks:
                good_indices = (num_repeats == vblock) & (stim_blocks == vblock)
                for stim_id in stim_ids[good_indices]:
                    if stim_id in stim_holdout_ids:
                        validation_groups.append((stim_id, vblock))

            cv_sets.append((training_groups, validation_groups))

    return cv_sets


def partition_set(x, n, merge_extra=True):
    """
        Partition set x into n (mostly) equally-sized partitions.
    """
    psize = len(x) / n
    last_set_size = len(x) % n
    parts = list()
    for k in range(n):
        si = k*psize
        ei = si + psize
        parts.append( list(x[si:ei]) )

    if merge_extra and last_set_size > 0:
        for k in range(last_set_size):
            parts[k].append(x[psize*n + k])
    elif last_set_size > 0:
        parts.append(list(x[-last_set_size:]))

    return parts


def compare_baseline(f, output_dir1, output_dir2, mtype='encoder'):

    cc1 = list()
    cc2 = list()

    def get_cc(file_name):
        hf = h5py.File(file_name, 'r')
        ccs = np.array(hf['none'][mtype]['ccs'])
        hf.close()
        return ccs.mean()

    s = Site(f)
    for site in s.get_all():
        protocol = site.protocols[0]
        for electrode in site.electrodes:
            fname1 = os.path.join(output_dir1, 'encoder_decoder_Site%d_%s_e%d.h5' % (site.id, protocol.name, electrode.number))
            cc1.append(get_cc(fname1))

            fname2 = os.path.join(output_dir2, 'encoder_decoder_Site%d_%s_e%d.h5' % (site.id, protocol.name, electrode.number))
            cc2.append(get_cc(fname2))

    plt.figure()
    x = np.arange(-0.10, 1.1, 0.1)
    plt.plot(x, x, 'k-', alpha=0.5)
    plt.plot(cc1, cc2, 'ro')
    plt.xlabel(output_dir1)
    plt.ylabel(output_dir2)
    plt.axis('tight')
    plt.title(mtype)


def fit_encoder_and_decoder(f, electrode, site_id=1, protocol_name='Call1',
              filter=None, cutoff_freq=None, stim_type='song',
              cv_type='random',
              lambda1=1.0, lambda2=1.0, threshold=0.0, niters=1,
              gain_tau=0.0):


    sounds = find_common_stims(f)
    stim_ids = [s.id for s in sounds if s.stim_type == stim_type]

    stim_envelopes,stim_cond_lfps,sample_rate = get_decoder_data(f, electrode,
                                                                 site_id=site_id, protocol_name=protocol_name,
                                                                 filter=filter, cutoff_freq=cutoff_freq,
                                                                 stim_ids=stim_ids, stim_type=stim_type,
                                                                 gain_tau=gain_tau)

    cv_sets = None
    if cv_type == 'random':
        cv_sets = get_random_cv_groups(f, site_id, protocol_name, stim_types=[stim_type])
    elif cv_type == 'temporal':
        cv_sets = get_temporal_cv_groups(f, site_id, protocol_name, num_training_blocks=3, stim_types=[stim_type])
    else:
        print 'No such cross validation type \'%s\', valid options are \'random\' and \'temporal\'.' % cv_type

    nfolds = len(cv_sets)

    #initialize data structure to store encoder
    encoder_length = 0.250
    encoder_lags = range(0, int(encoder_length*sample_rate))

    encoder_filt = LinearFilter()
    encoder_filt.sample_rate = sample_rate
    encoder_filt.cv_sets = cv_sets
    encoder_filt.lags = encoder_lags
    encoder_filt.biases = np.zeros([nfolds])
    encoder_filt.filters = np.zeros([nfolds, len(encoder_lags)])
    encoder_filt.errs = np.zeros([nfolds])
    encoder_filt.etimes = np.zeros([nfolds])
    encoder_filt.ccs = np.zeros([nfolds])

    #initialize data structure to store decoder
    decoder_length = 0.250
    decoder_lags = range(-int(decoder_length*sample_rate), 1)
    
    decoder_filt = LinearFilter()
    decoder_filt.sample_rate = sample_rate
    decoder_filt.cv_sets = cv_sets
    decoder_filt.lags = decoder_lags
    decoder_filt.biases = np.zeros([nfolds])
    decoder_filt.filters = np.zeros([nfolds, len(decoder_lags)])
    decoder_filt.errs = np.zeros([nfolds])
    decoder_filt.etimes = np.zeros([nfolds])
    decoder_filt.ccs = np.zeros([nfolds])

    #train the models on each holdout set
    for k,(training_group,validation_group) in enumerate(cv_sets):

        #get training data
        X,y,group_index,group_identity = get_envelope_data_matrix(stim_envelopes, stim_cond_lfps,
                                                         group_identities_to_use=training_group, encoder=False)

        #train decoder
        stime = time.time()
        ico, filt = fit_single_model_lasso(X, y, group_index, decoder_lags, lambda1, lambda2, threshold, niters)
        etime = time.time() - stime

        #estimate performance on holdout set
        X,y,group_index,group_identity = get_envelope_data_matrix(stim_envelopes, stim_cond_lfps,
                                                         group_identities_to_use=validation_group, encoder=False)

        model_holdout_resp,holdout_err,holdout_cc = compute_model_performance_lasso(X, y, group_index, decoder_lags, ico)

        #save decoder data to data structure
        decoder_filt.biases[k] = ico.model.bias
        decoder_filt.filters[k, :] = filt
        decoder_filt.errs[k] = holdout_err
        decoder_filt.ccs[k] = holdout_cc
        decoder_filt.etimes[k] = etime

        print 'DECODER: k=%d, holdout_err=%0.1f, etime=%ds, len(active_set)=%d, cc=%0.2f' %\
              (k, holdout_err, int(etime), len(ico.active_set), holdout_cc)
        print ''

        #swap training data
        X,y,group_index,group_identity = get_envelope_data_matrix(stim_envelopes, stim_cond_lfps,
                                                         group_identities_to_use=training_group, encoder=True)

        #train encoder
        stime = time.time()
        ico, filt = fit_single_model_lasso(X, y, group_index, encoder_lags, lambda1, lambda2, threshold, niters)
        etime = time.time() - stime

        #estimate performance on holdout set
        X,y,group_index,group_identity = get_envelope_data_matrix(stim_envelopes, stim_cond_lfps,
                                                         group_identities_to_use=validation_group, encoder=True)

        model_holdout_resp,holdout_err,holdout_cc = compute_model_performance_lasso(X, y, group_index, encoder_lags, ico)

        #save encoder data to data structure
        encoder_filt.biases[k] = ico.model.bias
        encoder_filt.filters[k, :] = filt
        encoder_filt.errs[k] = holdout_err
        encoder_filt.ccs[k] = holdout_cc
        encoder_filt.etimes[k] = etime

        print 'ENCODER: k=%d, holdout_err=%0.1f, etime=%ds, len(active_set)=%d, cc=%0.2f' %\
              (k, holdout_err, int(etime), len(ico.active_set), holdout_cc)
        print ''
        print '--------------'
        print ''

    return decoder_filt, encoder_filt


def fit_single_model_lasso(X, y, group_index, lags, lambda1, lambda2, threshold, niters):
    fm = ConvolutionalInCrowdModel(X, y, lags=lags, bias=0.0, group_index=group_index)

    ico = InCrowd(fm, solver_params={'lambda1': lambda1, 'lambda2': lambda2}, max_additions_fraction=threshold)
    while not ico.converged and ico.num_iter < niters:
        ico.iterate()
    return ico, fm.get_filter(ico.x)


def compute_model_performance_lasso(X, y, group_index, lags, ico):
    holdout_fm = ConvolutionalInCrowdModel(X, y, lags=lags, bias=ico.model.bias, group_index=group_index)
    model_holdout_resp = holdout_fm.forward(ico.x)

    holdout_err = ((y - model_holdout_resp) ** 2).sum()
    holdout_cc_mat = np.corrcoef(y, model_holdout_resp)
    holdout_cc = holdout_cc_mat[0, 1]

    return model_holdout_resp,holdout_err,holdout_cc


def fit_gain_models(f, electrode, output_file, cv_type='random', site_id=1, protocol_name='Call1', stim_type='song', gain_taus=np.arange(0.0, 0.110, 0.010)):

    hf = h5py.File(output_file, 'a')

    mgrp = hf.create_group('gain_models')

    stime = time.time()
    for gain_tau in gain_taus:
        print '--------------------'
        print ''
        print 'Fitting gain %0.3f' % gain_tau
        print ''
        np.random.seed(12345678)
        decoder, encoder = fit_encoder_and_decoder(f, electrode, site_id=site_id, protocol_name=protocol_name,
                                                   filter=None, cutoff_freq=None, stim_type=stim_type,
                                                   cv_type='random', lambda1=1.0, lambda2=1.0, gain_tau=gain_tau)

        grp = mgrp.create_group('gain_%0.3f' % gain_tau)
        grp.attrs['filter'] = 'none'
        grp.attrs['cutoff_freq'] = 0.0
        grp.attrs['gain_tau'] = gain_tau

        egrp = grp.create_group('encoder')
        encoder.to_file(output_group=egrp, include_dataset=False)

        dgrp = grp.create_group('decoder')
        decoder.to_file(output_group=dgrp, include_dataset=False)

    etime = time.time() - stime
    print 'Total Elapsed Time: %0.1f min' % (etime / 60.0)

    hf.close()


def fit_filtered_models(f, electrode, output_file, cv_type='random', site_id=1, protocol_name='Call1', stim_type='song',
                        low_cutoff_freqs=np.arange(5.0, 100.0, 5.0), high_cutoff_freqs=np.arange(5.0, 100.0, 5.0),
                        gain_tau=0.0):

    hf = h5py.File(output_file, 'w')

    stime = time.time()
    #first run model with no filters
    decoder, encoder = fit_encoder_and_decoder(f, electrode, site_id=site_id, protocol_name=protocol_name,
                                               filter=None, cutoff_freq=None, stim_type=stim_type,
                                               cv_type=cv_type, lambda1=1.0, lambda2=1.0, gain_tau=gain_tau)

    grp = hf.create_group('none')
    grp.attrs['filter'] = 'none'
    grp.attrs['cutoff_freq'] = 0.0

    egrp = grp.create_group('encoder')
    encoder.to_file(output_group=egrp, include_dataset=False)

    dgrp = grp.create_group('decoder')
    decoder.to_file(output_group=dgrp, include_dataset=False)

    #run a model that's fit temporally
    decoder, encoder = fit_encoder_and_decoder(f, electrode, site_id=site_id, protocol_name=protocol_name,
                                               filter=None, cutoff_freq=None, stim_type=stim_type,
                                               cv_type='temporal', lambda1=1.0, lambda2=1.0, gain_tau=gain_tau)

    grp = hf.create_group('temporal')
    grp.attrs['filter'] = 'none'
    grp.attrs['cutoff_freq'] = 0.0

    egrp = grp.create_group('encoder')
    encoder.to_file(output_group=egrp, include_dataset=False)

    dgrp = grp.create_group('decoder')
    decoder.to_file(output_group=dgrp, include_dataset=False)

    #fit models with different high and low pass frequency cutoffs
    filters = {'low':low_cutoff_freqs,
               'high':high_cutoff_freqs}

    for filt,cutoff_freqs in filters.iteritems():
        for cutoff_freq in cutoff_freqs:

            print 'Fitting %spass filter with cutoff freq %d Hz' % (filt, cutoff_freq)

            decoder, encoder = fit_encoder_and_decoder(f, electrode, site_id=site_id, protocol_name=protocol_name,
                                                       filter=filt, cutoff_freq=cutoff_freq, stim_type=stim_type,
                                                       cv_type=cv_type, lambda1=1.0, lambda2=1.0, gain_tau=gain_tau)

            grp = hf.create_group('%s_%d' % (filt, cutoff_freq))
            grp.attrs['filter'] = filt
            grp.attrs['cutoff_freq'] = cutoff_freq
            egrp = grp.create_group('encoder')
            encoder.to_file(output_group=egrp, include_dataset=False)

            dgrp = grp.create_group('decoder')
            decoder.to_file(output_group=dgrp, include_dataset=False)

    etime = time.time() - stime
    print 'Total Elapsed Time: %0.1f min' % (etime / 60.0)

    hf.close()


def plot_cc_vs_freq(output_file, electrode, ax_encoder=None, ax_decoder=None, full_plot=False, site_id=None, protocol_name=None):
    hf = h5py.File(output_file, 'r')

    fdata = {'encoder':{'low':list(), 'high':list()},
             'decoder':{'low':list(), 'high':list()}
            }

    for key,grp in hf.iteritems():
        if key in ['none', 'temporal']:
            continue

        filt = grp.attrs['filter']
        cutoff_freq = grp.attrs['cutoff_freq']

        for model in ['encoder', 'decoder']:
            mgrp = grp[model]
            ccs = np.array(mgrp['ccs'])
            cc_mean = ccs.mean()

            fdata[model][filt].append( (cutoff_freq, cc_mean))

    grp = hf['none']
    encoder_ccs = np.array(grp['encoder']['ccs'])
    decoder_ccs = np.array(grp['decoder']['ccs'])

    none_data = {'encoder':encoder_ccs.mean(), 'decoder':decoder_ccs.mean()}

    axes = {'encoder':ax_encoder, 'decoder':ax_decoder}

    hf.close()

    mdata = dict()

    for model in ['encoder', 'decoder']:
        lpdata = np.array(sorted(fdata[model]['low'], key=operator.itemgetter(0)))
        hpdata = np.array(sorted(fdata[model]['high'], key=operator.itemgetter(0)))

        base_cc = none_data[model]

        if axes[model] is None:
            plt.figure()
        else:
            plt.sca(axes[model])

        cutoff_freqs = lpdata[:, 0]
        low_cc = lpdata[:, 1]
        high_cc = hpdata[:, 1]

        #lowdiff = np.diff(low_cc)
        #low_peak_index = lowdiff.argmin() + 1
        low_peak_index = low_cc.argmax()
        
        #highdiff = np.diff(high_cc)
        #high_peak_index = highdiff.argmin() + 1
        high_peak_index = high_cc.argmax()

        mdata[model] = {'cc':base_cc, 'highpass_peak':cutoff_freqs[high_peak_index], 'lowpass_peak':cutoff_freqs[low_peak_index]}
        
        plt.plot(0.0, base_cc, 'ko', ms=25.0)
        plt.plot(cutoff_freqs, low_cc, 'bo-', linewidth=3.0, ms=5.0)
        plt.plot(cutoff_freqs, high_cc, 'ro-', linewidth=3.0, ms=5.0)

        if full_plot:
            plt.legend(['Allpass', 'Lowpass', 'Highpass'])
            plt.title('%s Performance vs Cutoff Frequency' % model.capitalize())
            plt.xlabel('Cutoff Frequency (Hz)')
            plt.ylabel('Correlation Coefficient')
            plt.axis('tight')
            plt.ylim([0, 1.0])
        else:
            plt.legend(['E%d' % electrode])
            plt.axis('tight')

        plt.plot(cutoff_freqs[low_peak_index], low_cc[low_peak_index], 'gx', ms=10.0)
        plt.plot(cutoff_freqs[high_peak_index], high_cc[high_peak_index], 'gx', ms=10.0)


    return mdata


def plot_all_envelope_models(f, output_dir, site_id=1, protocol_name='Call1', figs_to_show=['cc_vs_freq', 'cc_temporal', 'plot_filters_over_time']):

    fig_funcs = {'cc_vs_freq':plot_cc_vs_freq, 'cc_temporal':plot_cc_vs_time, 'filters':plot_filters_over_time, 'strf':plot_strf}
    figs = dict()
    mtypes = ['encoder', 'decoder']

    for fig_name in figs_to_show:
        figs[fig_name] = dict()
        for mtype in mtypes:
            figs[fig_name][mtype] = plt.figure()
            plt.suptitle('%s: Site %d, %s' % (mtype.capitalize(), site_id, protocol_name))
            plt.subplots_adjust(bottom=0.05, top=0.95, right=0.95, left=0.05, wspace=0.25, hspace=0.25)

    site = Site(f)
    assert site.from_id(site_id)

    #get electrode geometry and coordinates
    geom = site.electrodes[0].geometry
    coords = geom.coordinates

    nrows = 8
    ncols = 4

    model_data = dict()
    for fig_name in fig_funcs.keys():
        model_data[fig_name] = list()

    for e in site.electrodes:
        ofile = os.path.join(output_dir, 'encoder_decoder_Site%d_%s_e%d.h5' % (site.id, protocol_name, e.number))
        if not os.path.exists(ofile):
            print 'Missing file: %s' % ofile
            continue

        row,col = np.nonzero(coords == e.number)
        sp = ncols*row + col + 1
        for fig_name in figs_to_show:
            encoder_fig = figs[fig_name]['encoder']
            decoder_fig = figs[fig_name]['decoder']
            ax_encoder = encoder_fig.add_subplot(nrows, ncols, sp)
            ax_decoder = decoder_fig.add_subplot(nrows, ncols, sp)
            mdata = fig_funcs[fig_name](ofile, e.number, ax_encoder=ax_encoder, ax_decoder=ax_decoder, site_id=site_id, protocol_name=protocol_name)

            model_data[fig_name].append(mdata)

    return model_data


def plot_envelope_ps(f, site_id=1, protocol_name='Call1', stim_type='song'):

    stim_envelopes,sample_rate = get_stim_representations(f, site_id=site_id, protocol_name=protocol_name, stim_type=stim_type)

    #fit the power spectrum of each amplitude envelope with a cubic spline
    all_splines = list()
    for stim_id,env in stim_envelopes.iteritems():
        env_fft = fft(env)
        env_fft_freq = fftfreq(len(env), d=1.0/sample_rate)
        #print 'env_fft.shape=',env_fft.shape
        #print 'env_fft_freq.shape=',env_fft_freq.shape
        freq_index = env_fft_freq >= 0.0
        env_ps = np.abs(env_fft[freq_index])

        #approximate the frequency spectrum with a cubic spline
        env_ps_spline = interp1d(env_fft_freq[freq_index], env_ps, kind='cubic')
        all_splines.append(env_ps_spline)

    #compute the interpolated power spectrum for each envelope
    max_freq = 30.0
    df = 0.05
    freq = np.arange(1.0, max_freq+df, df)
    all_ps = list()
    for k,env_ps_spline in enumerate(all_splines):
        env_ps = env_ps_spline(freq)
        all_ps.append(env_ps)

    #compute the mean and standard deviation of the power spectrum across envelopes
    all_ps = np.array(all_ps)
    env_ps_mean = all_ps.mean(axis=0)
    env_ps_std = all_ps.std(axis=0, ddof=1)

    #plot
    #rcParams.update({'font.size': 18})
    plt.figure()
    for ps in all_ps:
        plt.plot(freq, ps, '-', linewidth=2.0)
    plt.xlabel('Frequency (Hz)')
    plt.ylabel('Power')
    plt.title('Amplitude Envelope Power')
    plt.axis('tight')

    plt.figure()
    plt.errorbar(freq, env_ps_mean, yerr=env_ps_std, ecolor='#aaaaaa', capsize=0, color='k', linewidth=3, alpha=0.75)
    plt.xlabel('Frequency (Hz)')
    plt.ylabel('Power')
    plt.title('Average Amplitude Envelope Power')
    plt.axis('tight')


def plot_all_all_cc_vs_freq(f, output_dir='/auto/k8/fdata/mschachter/analysis/envelope_models'):

    mdata1 = plot_all_envelope_models(f, output_dir, site_id=1, protocol_name='Call1')
    mdata2 = plot_all_envelope_models(f, output_dir, site_id=2, protocol_name='Call2')
    mdata3 = plot_all_envelope_models(f, output_dir, site_id=3, protocol_name='Call3')
    mdata4 = plot_all_envelope_models(f, output_dir, site_id=4, protocol_name='Call1')
    mdata5 = plot_all_envelope_models(f, output_dir, site_id=5, protocol_name='Call2')

    all_ccs = {'encoder':list(), 'decoder':list()}
    all_highpass_peaks = {'encoder':list(), 'decoder':list()}
    all_lowpass_peaks = {'encoder':list(), 'decoder':list()}
    for mdata in [mdata1, mdata2, mdata3, mdata4, mdata5]:
        for x in mdata:
            for mtype in ['encoder', 'decoder']:
                all_ccs[mtype].append(x[mtype]['cc'])
                all_highpass_peaks[mtype].append(x[mtype]['highpass_peak'])
                all_lowpass_peaks[mtype].append(x[mtype]['lowpass_peak'])

    for mtype in ['encoder', 'decoder']:
        plt.figure()
        plt.subplot(3, 1, 1)
        plt.hist(all_ccs[mtype], bins=20)
        plt.xlabel('Correlation Coefficient')
        plt.axis('tight')

        plt.subplot(3, 1, 2)
        plt.hist(all_highpass_peaks[mtype], bins=20)
        plt.xlabel('Highpass Frequency Peak')
        plt.axis('tight')

        plt.subplot(3, 1, 3)
        plt.hist(all_lowpass_peaks[mtype], bins=20)
        plt.xlabel('Lowpass Frequency Peak')
        plt.axis('tight')

        plt.suptitle(mtype.capitalize())


def plot_cc_vs_time(ofile, electrode, ax_encoder=None, ax_decoder=None, site_id=None, protocol_name=None):

    hf = h5py.File(ofile, 'r')

    axes = {'encoder':ax_encoder, 'decoder':ax_decoder}

    grp = hf['temporal']
    for mtype in ['decoder', 'encoder']:
        #aggregate cross validated sets by the trials they're trained on
        groups_by_trials = dict()
        cv_grp = grp[mtype]['cv_sets']
        for key in cv_grp.keys():
            k = int(key[5:])
            training_indices = np.array(cv_grp[key]['training_indices'])
            unique_trials = np.unique(training_indices[:, 1])
            unique_trials.sort()
            ukey = ','.join([str(x) for x in unique_trials])
            if ukey not in groups_by_trials:
                groups_by_trials[ukey] = list()
            groups_by_trials[ukey].append(k)

        #aggregate correlation coefficients by training trials
        ccs = np.array(grp[mtype]['ccs'])
        ccs_by_trials = dict()
        for ukey,cc_index_list in groups_by_trials.iteritems():
            ccs_by_trials[ukey] = ccs[cc_index_list]

        key_order = groups_by_trials.keys()
        key_order.sort(key=operator.itemgetter(0))

        ax = axes[mtype]
        if ax is None:
            plt.figure()
            ax = plt.subplot(111)
        plt.sca(ax)
        boxplot_from_dict(ccs_by_trials, key_order, ax=ax)

        #plt.title(mtype)
        #plt.xlabel('Training Trials')
        #plt.ylabel('CC')
        plt.axis('tight')

    hf.close()
    return None


def plot_strf(ofile, electrode, ax_encoder=None, ax_decoder=None, site_id=None, protocol_name=None):

    #get performance of encoder trained on amplitude envelope
    hf = h5py.File(ofile, 'r')
    grp = hf['none']
    ccs = np.array(grp['encoder']['ccs'])
    env_cc = ccs.mean()
    hf.close()

    #get performance and strf from encoder trained on spectrogram
    root_dir,fname = os.path.split(ofile)
    spec_file = os.path.join(root_dir, '..', 'spec_models', 'encoder_spec_Site%d_%s_e%d.h5' % \
                                                            (site_id, protocol_name, electrode))
    hf = h5py.File(spec_file, 'r')

    #get performance
    ccs = np.array(hf['ccs'])
    spec_cc = ccs.mean()

    #get STRF and properties
    strfs = np.array(hf['filters'])
    strf = strfs.mean(axis=0)

    #compute frequency
    strf_freq = np.arange(300, 8000.0, strf.shape[0])

    #get lags
    sample_rate = hf['/'].attrs['sample_rate']
    lags = np.array(hf['lags'])
    strf_t = lags / sample_rate

    hf.close()

    #plot STRF
    absmax = np.abs(strf).max()
    plt.sca(ax_encoder)
    plot_spectrogram(strf_t, strf_freq, strf, ax=ax_encoder, vmin=-absmax, vmax=absmax, colorbar=None)
    plt.xlabel('')
    plt.ylabel('')
    tstr = '%d (%0.2f, %0.2f)' % (electrode, env_cc, spec_cc)
    plt.text(0.060, 4000.0, tstr)

    return None


def plot_filters_over_time(ofile, electrode, ax_encoder=None, ax_decoder=None, site_id=None, protocol_name=None):

    hf = h5py.File(ofile, 'r')

    axes = {'encoder':ax_encoder, 'decoder':ax_decoder}

    grp = hf['temporal']
    for mtype in ['decoder', 'encoder']:
        #aggregate cross validated sets by the trials they're trained on
        groups_by_trials = dict()
        cv_grp = grp[mtype]['cv_sets']
        for key in cv_grp.keys():
            k = int(key[5:])
            training_indices = np.array(cv_grp[key]['training_indices'])
            unique_trials = np.unique(training_indices[:, 1])
            unique_trials.sort()
            ukey = ','.join([str(x) for x in unique_trials])
            if ukey not in groups_by_trials:
                groups_by_trials[ukey] = list()
            groups_by_trials[ukey].append(k)

        #aggregate correlation coefficients by training trials
        sample_rate = grp[mtype].attrs['sample_rate']
        lags = np.array(grp[mtype]['lags'])
        filters = np.array(grp[mtype]['filters'])
        filters_by_trial = dict()
        for ukey,index_list in groups_by_trials.iteritems():
            filters_by_trial[ukey] = filters[index_list, :]

        key_order = groups_by_trials.keys()
        key_order.sort(key=operator.itemgetter(0))

        ax = axes[mtype]
        if ax is None:
            plt.figure()
            ax = plt.subplot(111)
        plt.sca(ax)
        nkeys = len(key_order)
        for k,ukey in enumerate(key_order):
            filts = filters_by_trial[ukey]
            n = float(k) / nkeys
            a = 1.0 - (float(k) / nkeys)*0.5
            clr = [n, 0.0, 1.0 - n]
            plt.plot(lags/sample_rate, filts.mean(axis=0), color=clr, alpha=a, linewidth=2.0)
            #plt.legend('E%d' % electrode)

        #plt.title(mtype)
        #plt.xlabel('Training Trials')
        #plt.ylabel('CC')
        plt.axis('tight')

    hf.close()
    return None


def plot_gain_model(ofile):

    hf = h5py.File(ofile, 'r')
    grp = hf['gain_models']

    model_cc = {'encoder':list(), 'decoder':list()}

    for gkey,ggrp in grp.iteritems():
        print 'gkey=%s' % gkey
        print ggrp.keys()
        gain = float(ggrp.attrs['gain_tau'])
        for mtype in model_cc.keys():
            mgrp = ggrp[mtype]
            ccs = np.array(mgrp['ccs'])
            model_cc[mtype].append( (gain, ccs.mean()) )

    hf.close()

    plt.figure()
    for k,(mtype,cclist) in enumerate(model_cc.iteritems()):
        cclist.sort(key=operator.itemgetter(0))
        cclist = np.array(cclist)
        plt.subplot(2, 1, k+1)
        plt.plot(cclist[:, 0], cclist[:, 1], 'ro')
        plt.xlabel('Gain Tau')
        plt.ylabel('CC')
        plt.title(mtype)
        plt.axis('tight')


def get_pertrial_performance(f, electrode, ofile):
    pass


if __name__ == '__main__':

    f = tables.open_file('/auto/k8/fdata/mschachter/tdt_continuous/GreBlu9508M/GreBlu9508M_tabled.h5', 'r')

    #stypes = ['song']
    #plot_protocol_stats(f, site_id=1, protocol_name='Call1', stim_types=stypes)
    #plot_protocol_stats(f, site_id=2, protocol_name='Call2', stim_types=stypes)
    #plot_protocol_stats(f, site_id=3, protocol_name='Call3', stim_types=stypes)
    #plot_protocol_stats(f, site_id=4, protocol_name='Call1', stim_types=stypes)
    #plot_protocol_stats(f, site_id=5, protocol_name='Call2', stim_types=stypes)

    site_id = 4
    protocol = 'Call1'
    electrode = 22
    output_dir = '/auto/k8/fdata/mschachter/analysis/envelope_models/'
    ofile = os.path.join(output_dir, 'encoder_decoder_Site%d_%s_e%d.h5' % (site_id, protocol, electrode))

    cutoff_freqs = range(1, 20) + range(20, 65, 5)

    decoder, encoder = fit_encoder_and_decoder(f, electrode, site_id=site_id, protocol_name=protocol,
                                               filter=None, cutoff_freq=None, stim_type='song',
                                               cv_type='random', lambda1=1.0, lambda2=1.0, gain_tau=0.0)

    #fit_filtered_models(f, electrode, ofile, cv_type='random',
    #                    high_cutoff_freqs=cutoff_freqs, low_cutoff_freqs=cutoff_freqs,
    #                    gain_tau=0.0)

    #ofile = '/tmp/encoder_decoder_Site%d_%s_e%d.h5' % (site_id, protocol, electrode)
    #fit_gain_models(f, electrode, ofile)
    #plot_gain_model(ofile)

    #rcParams.update({'font.size':22})
    #plot_envelope_ps(f, site_id=1, protocol_name='Call1', stim_type='song')

    #plot_all_all_cc_vs_freq(f)
    
    #plot_all_envelope_models(f, output_dir, site_id=1, protocol_name='Call1', figs_to_show=['filters'])
    #plot_all_envelope_models(f, output_dir, site_id=2, protocol_name='Call2', figs_to_show=['filters'])
    #plot_all_envelope_models(f, output_dir, site_id=3, protocol_name='Call3', figs_to_show=['filters'])
    #plot_all_envelope_models(f, output_dir, site_id=4, protocol_name='Call1', figs_to_show=['filters'])
    #plot_all_envelope_models(f, output_dir, site_id=5, protocol_name='Call2', figs_to_show=['filters'])

    #plot_all_envelope_models(f, output_dir, site_id=1, protocol_name='Call1', figs_to_show=['strf'])
    #plot_all_envelope_models(f, output_dir, site_id=2, protocol_name='Call2', figs_to_show=['strf'])
    #plot_all_envelope_models(f, output_dir, site_id=3, protocol_name='Call3', figs_to_show=['strf'])
    #plot_all_envelope_models(f, output_dir, site_id=4, protocol_name='Call1', figs_to_show=['strf'])
    #plot_all_envelope_models(f, output_dir, site_id=5, protocol_name='Call2', figs_to_show=['strf'])

    #plot_cc_vs_freq(ofile2)
    #plot_cc_vs_freq(ofile, electrode, full_plot=True)

    plt.show()

    f.close()
