import argparse
import operator
import os
from math import copysign

import numpy as np
from numpy.fft import fftfreq
from scipy.fftpack import hilbert,fft,ifft

from matplotlib.gridspec import GridSpec
from matplotlib.patches import ArrowStyle
import matplotlib.pyplot as plt
import matplotlib.cm as cm

import networkx as nx

import tables
from lasp.hht import HHT
from lasp.signal import another_hilbert

from neurotables.analysis.lfp.decomposition import get_bandpassed_lfp
from neurotables.analysis.lfp.electrode_graph import ElectrodeGraph
from neurotables.analysis.lfp.memd import IMFPlotter
from neurotables.analysis.lfp.waves import compute_wave_properties
from neurotables.objects.cells import Site
from neurotables.analysis.lfp.objects import *


class LFPDataSource(object):

    def __init__(self, f, site_id, protocol_name, t1, t2, low_freq=None, high_freq=30.0, load_data=True):

        self.f = f
        self.num_channels = 2

        #get site
        site = Site(f)
        assert site.from_id(site_id)
        self.site = site

        #get protocol
        plist = [p for p in site.protocols if p.name == protocol_name]
        assert len(plist) == 1
        self.protocol = plist[0]

        #get electrode
        elist = [e for e in self.site.electrodes]
        assert len(elist) == 32
        elist.sort(key=operator.attrgetter('number'))
        self.electrodes = elist

        #get coordinates
        electrode_geometry = self.site.electrodes[0].geometry
        self.coords = electrode_geometry.coordinates
        self.nrows,self.ncols = self.coords.shape

        #get the LFPs
        self.low_freq = low_freq
        self.high_freq = high_freq

        #peek into the first LFP to get the movie end time
        lfp_pr = self.electrodes[0].get_lfp_protocol_response(self.protocol.name)
        self.sample_rate = lfp_pr.lfp.sample_rate
        lfp = lfp_pr.waveform
        self.duration = len(lfp) / self.sample_rate
        del lfp

        #get anatomical location of each electrode
        regions = dict()
        for electrode in elist:
            reg = '?'
            if electrode.anatomy.__class__.__name__ == 'ElectrodeAnatomicalLocation':
                reg = electrode.anatomy.region
            regions[electrode.number] = reg
        self.regions = regions

        #make a matrix of text labels to put over movie
        text_mat = np.zeros([self.nrows, self.ncols], dtype='S40')
        enumbers = [e.number for e in self.electrodes]
        for enumber in enumbers:
            #get coordinates
            row,col = np.nonzero(self.coords == enumber)
            reg = self.regions[enumber]
            text_mat[row, col] = '%d\n%s' % (enumber, reg)
        self.electrode_labels = text_mat

        #don't load the data unless requested
        if not load_data:
            self.lfps = None
            self.absmax = None
            return

        #get LFPs
        start_index = int(t1*self.sample_rate)
        end_index = int(t2*self.sample_rate)
        lfps = np.zeros([self.nrows, self.ncols, end_index-start_index])
        lfp_min = np.inf
        lfp_max = -np.inf
        for electrode in self.electrodes:
            lfp,sample_rate = get_bandpassed_lfp(f, electrode.number,
                                                 site_id=site_id, protocol_name=protocol_name,
                                                 low_freq=self.low_freq, high_freq=self.high_freq)
            row,col = np.nonzero(self.coords == electrode.number)
            lfps[row, col, :] = lfp[start_index:end_index]
            lfp_min = min(lfp_min, lfps.min())
            lfp_max = max(lfp_max, lfps.max())

        #invert LFPs for color scale (so red is population spikes, blue is opposite polarity to population spikes)
        lfps = -lfps

        #pick a maximum display value that's not super crazy large, to improve range for color scale
        lfp_absmax = np.percentile(np.abs(lfps), 98)

        #split the left and right hemispheres up
        self.lfps = np.zeros([2, self.nrows, self.ncols/2, lfps.shape[-1]])
        self.lfps[0, :, :, :] = lfps[:, [0, 1], :]
        self.lfps[1, :, :, :] = lfps[:, [2, 3], :]
        self.absmax = lfp_absmax

    def get_data(self, t):
        return self.lfps[:, :, :, t].squeeze()

    def get_absmax(self):
        return [self.absmax, self.absmax]

    def get_labels(self, channel, t):
        return self.electrode_labels

    def plot_channel(self, t, channel, ax):

        plt.sca(ax)

        lfps = self.get_data(t)
        absmax = self.get_absmax()

        plt.imshow(lfps[channel, :, :].squeeze(), interpolation='nearest', aspect='auto', vmin=-absmax[channel], vmax=absmax[channel],
                           extent=[0, 2, 0, 8], origin='upper', cmap=cm.seismic)
        plt.xticks([])
        plt.yticks([])
        labels = self.get_labels(channel, t)
        for row in range(8):
            for col in [0, 1]:
                txt = labels[row, col]
                plt.text((col%2)+0.5, (7-row)+0.25, txt, horizontalalignment='center')


class HHTDataSource(LFPDataSource):

    def __init__(self, f, site_id, protocol_name, t1, t2, low_freq=None, high_freq=30.0, nmodes=2):
        LFPDataSource.__init__(self, f, site_id, protocol_name, t1, t2, low_freq=low_freq, high_freq=high_freq)

        self.num_channels = 2*nmodes

        #compute HHT of LFPs
        nt = self.lfps.shape[-1]
        self.imfs = np.zeros([self.num_channels, self.nrows, self.ncols/2, nt])

        for k in range(2):
            for row in range(self.nrows):
                for col in range(self.ncols / 2):
                    lfp = self.lfps[k, row, col, :]
                    lfp_hht = HHT(lfp, self.sample_rate, emd_max_modes=nmodes)
                    for j in range(nmodes):
                        self.imfs[k*nmodes + j, row, col, :] = lfp_hht.imfs[j].imf
                    del lfp_hht

        del self.lfps
        self.absmax = list()
        for k in range(self.num_channels):
            absmax = np.percentile(np.abs(self.imfs[k, :, :, :]), 98)
            self.absmax.append(absmax)

    def get_data(self, t):
        return self.imfs[:, :, :, t].squeeze()

    def get_absmax(self):
        return self.absmax

    def get_labels(self, channel, t):
        if channel in [0, 1]:
            return self.electrode_labels[:, [0, 1]]
        else:
            return self.electrode_labels[:, [2, 3]]


class MEMDDataSource(LFPDataSource):

    def __init__(self, f, imf_file, imf_number, t1, t2,
                 row_dist=50.0, col_dist=250.0, row_display_dist=50.0, col_display_dist=250.0):

        #read the IMFs from the file
        self.imf_number = imf_number
        imf_plotter = IMFPlotter.from_file(f, imf_file)
        self.t1 = t1
        self.t2 = t2
        self.site_id = imf_plotter.site_id
        self.protocol_name = imf_plotter.protocol_name
        self.sample_rate = imf_plotter.sample_rate
        self.memd_start_time = imf_plotter.start_time
        self.memd_end_time = imf_plotter.end_time
        self.index2electrode = list(imf_plotter.index2electrode)
        self.hemispheres = imf_plotter.hemispheres

        #determine start and end indices of IMFs for the start and end times given
        i1 = int((t1 - self.memd_start_time)*self.sample_rate)
        assert i1 >= 0
        i2 = i1 + int((t2 - t1)*self.sample_rate)
        assert i2 < imf_plotter.imfs.shape[-1]
        nt = i2 - i1

        #get IMF across electrodes, for the specified amount of time
        flat_imfs = imf_plotter.imfs[self.imf_number-1, :, i1:i2].squeeze()

        #compute a bunch of stuff
        imf_plotter.compute_complex_imfs(t1, t2, output_dir=None, plot=False)

        #get instantaneous frequency, amplitude, and phase
        flat_ifreq = imf_plotter.imf_ifreqs[self.imf_number-1, :, :nt]
        print flat_ifreq.shape[-1], flat_imfs.shape[-1]
        assert flat_ifreq.shape[-1] == flat_imfs.shape[-1]
        flat_amplitude = imf_plotter.imf_amplitudes[self.imf_number-1, :, :nt]
        flat_phase = imf_plotter.imf_phases[self.imf_number-1, :, :nt]

        #get the first PC projected component
        pca_proj = imf_plotter.imf_proj[self.imf_number-1, :nt]
        proj_amp = np.abs(pca_proj)
        proj_phase = np.angle(pca_proj)
        proj_uw_phase = np.unwrap(proj_phase)

        del imf_plotter

        #initialize the parent class
        LFPDataSource.__init__(self, f, self.site_id, self.protocol_name, t1, t2, low_freq=None, high_freq=None, load_data=True)
        self.num_channels = 4
        self.row_dist = row_dist
        self.col_dist = col_dist
        self.row_display_dist = row_display_dist
        self.col_display_dist = col_display_dist
        self.num_subplots = [1, 1, 1, 1] #the number of subplots each channel requires

        #ensure that the electrode count and number of rows/cols is correct given the IMF file
        self.nrows = 8
        self.ncols = len(self.hemispheres)*2
        self.num_electrodes = flat_imfs.shape[1]

        #zscore the IMFs
        flat_imf_mean = flat_imfs.mean(axis=1)
        flat_imf_std = flat_imfs.std(axis=1, ddof=1)
        flat_imfs = flat_imfs.T
        flat_imfs -= flat_imf_mean
        flat_imfs /= flat_imf_std
        flat_imfs = flat_imfs.T

        #compute the complex phase
        nt = flat_imfs.shape[-1]
        self.imf = np.zeros([self.nrows, self.ncols, nt])
        self.phase = np.zeros([self.nrows, self.ncols, nt])
        self.phase_unwrapped = np.zeros([self.nrows, self.ncols, nt])
        self.ifreq = np.zeros([self.nrows, self.ncols, nt])
        self.amplitude = np.zeros([self.nrows, self.ncols, nt])

        self.avg_freq = np.zeros([self.nrows, self.ncols])

        t = np.arange(nt) / self.sample_rate
        for row in range(self.nrows):
            for col in range(self.ncols):

                #get the electrode number that corresponds to the row and column
                col_in_coords = col
                if len(self.hemispheres) == 1 and self.hemispheres[0] == 'R':
                    col_in_coords += 2
                electrode_number = self.coords[row, col_in_coords]

                #get the index of the IMF for the electrode
                eindex = self.index2electrode.index(electrode_number)

                #get the IMF
                imf = flat_imfs[eindex, :].squeeze()
                self.imf[row, col, :] = imf

                #get the amplitude
                amp = flat_amplitude[eindex, :].squeeze()
                self.amplitude[row, col, :] = amp

                #get the instantaneous frequency
                ifreq = flat_ifreq[eindex, :].squeeze()
                self.ifreq[row, col, :] = ifreq
                
                #get the phase
                phase = flat_phase[eindex, :].squeeze()

                #unwrap the phase
                uw_phase = np.unwrap(phase)
                linear_freq,intercept = np.polyfit(t, uw_phase, deg=1)

                phase_resid = uw_phase - (linear_freq*t + intercept)

                nsp = 6
                plt.figure()
                plt.suptitle('E%d' % electrode_number)
                plt.subplot(nsp, 1, 1)
                plt.plot(t, imf, 'k-')
                plt.ylabel('IMF')
                plt.axis('tight')

                plt.subplot(nsp, 1, 2)
                plt.plot(t, amp, 'b-')
                plt.plot(t, proj_amp, 'b--')
                plt.ylabel('Amplitude')
                plt.axis('tight')

                plt.subplot(nsp, 1, 3)
                plt.plot(t, phase, 'r-')

                plt.ylabel('Phase')
                plt.axis('tight')
                plt.subplot(nsp, 1, 4)
                plt.plot(t, ifreq, 'g-')
                plt.ylabel('Inst. Freq.')
                plt.axis('tight')

                plt.subplot(nsp, 1, 5)
                plt.plot(t, phase_resid, 'm-')
                plt.axis('tight')
                plt.ylabel('Linear Phase')


                plt.subplot(nsp, 1, 6)
                plt.plot(t, uw_phase - proj_uw_phase, 'r-')
                plt.axis('tight')
                plt.ylabel('Carrier-Subtracted Phase')

                plt.show()

                self.phase[row, col, :] = phase_resid

        del flat_imfs
        del flat_ifreq
        del flat_amplitude
        del flat_phase

        nzindex = self.amplitude > 0.0
        self.log_amplitude = np.zeros_like(self.amplitude)
        self.log_amplitude[nzindex] = np.log(self.amplitude[nzindex])
        thresh = np.percentile(self.log_amplitude[nzindex], 10)

        self.log_amplitude[~nzindex] = self.log_amplitude.ravel().min()
        tindex = self.log_amplitude <= thresh
        self.log_amplitude[tindex] = thresh
        self.log_amplitude += -thresh

        #compute maxima and minima
        flat_imfs = self.imf.ravel()
        self.imf_min = np.percentile(flat_imfs, 3)
        self.imf_max = np.percentile(flat_imfs, 97)
        del flat_imfs

        flat_amps = self.amplitude.ravel()
        self.amp_max = np.percentile(flat_amps, 97)
        self.amp_min = 0.0
        del flat_amps

        flat_logamps = self.log_amplitude.ravel()
        self.log_amp_max = np.percentile(flat_logamps, 97)
        self.log_amp_min = 0.0
        del flat_logamps

        flat_ifreq = self.ifreq.ravel()
        self.ifreq_min = np.percentile(flat_ifreq, 2)
        self.ifreq_max = np.percentile(flat_ifreq, 98)
        #print 'ifreq_min=%f, ifreq_max=%f' % (self.ifreq_min, self.ifreq_max)

        #compute wave velocity and PGD
        """
        self.dt, self.dx, self.dy, self.vx, self.vy, self.pgd_x, self.pgd_y =\
                                    compute_wave_properties(self.phase, row_dist=self.row_dist, col_dist=self.col_dist)

        print 'vx.shape=',self.vx.shape
        print 'vy.shape=',self.vy.shape
        print 'pgd_x.shape=',self.pgd_x.shape
        print 'pgd_y.shape=',self.pgd_y.shape

        self.max_dx = np.percentile(np.abs(self.dx[:, self.lfp_channel, :]), 97)
        self.max_dy = np.percentile(np.abs(self.dy[:, self.lfp_channel, :]), 97)
        self.max_vx = np.percentile(np.abs(self.vx[:, self.lfp_channel, :]), 97)
        self.max_vy = np.percentile(np.abs(self.vy[:, self.lfp_channel, :]), 97)
        self.max_v = max(self.max_vx, self.max_vy)
        """

        #compute the time-varying phase difference
        self.num_electrodes = self.nrows*self.ncols
        self.phase_diff = np.zeros([self.num_electrodes, self.num_electrodes, self.phase.shape[-1]])
        self.phase_diff_ticks = np.zeros(self.num_electrodes)
        for k in range(self.num_electrodes):
            electrode = self.index2electrode[k]
            row,col = np.nonzero(self.coords == electrode)
            remapped_index = row*self.ncols + col
            self.phase_diff_ticks[remapped_index] = electrode

        for k in range(self.num_electrodes):
            for j in range(self.num_electrodes):
                if k == j:
                    continue
                electrode1 = self.index2electrode[k]
                electrode2 = self.index2electrode[j]
                row1,col1 = np.nonzero(self.coords == electrode1)
                row2,col2 = np.nonzero(self.coords == electrode2)
                #compute the phase difference between the two electrodes
                #pdiff = self.phase_unwrapped[row1, col1, :] - self.phase_unwrapped[row2, col2, :]
                pdiff = (self.phase[row1, col1, :] - self.phase[row2, col2, :]).squeeze()

                #map the linear electrode coordinates (like 1-16) to a sequence that keeps electrodes in the same
                #row next to eachother
                k_remapped = row1*self.ncols + col1
                j_remapped = row2*self.ncols + col2
                print 'electrodes=(%d, %d), e1=(%d,%d), e2=(%d,%d), new k=%d, j=%d' % \
                      (electrode1, electrode2, row1, col1, row2, col2, k_remapped, j_remapped)
                self.phase_diff[k_remapped, j_remapped, :] = pdiff

                """
                nsp = 3
                plt.figure()
                plt.subplot(nsp, 1, 1)
                plt.plot(self.imf[row1, col1, :].squeeze(), 'k-', linewidth=2.0)
                plt.plot(self.imf[row2, col2, :].squeeze(), 'r-', linewidth=2.0)
                plt.legend(['E%d' % (k+1), 'E%d' % (j+1)])
                plt.ylabel('IMFs')
                plt.axis('tight')

                plt.subplot(nsp, 1, 2)
                plt.axhline(0.0, c='k')
                plt.plot(self.phase[row1, col1, :].squeeze(), 'k-', linewidth=2.0)
                plt.plot(self.phase[row2, col2, :].squeeze(), 'r-', linewidth=2.0)
                plt.ylabel('Phase')
                plt.legend(['E%d' % (k+1), 'E%d' % (j+1)])
                plt.title('Avg. Freqs: %0.3f, %0.3f' % (self.avg_freq[row1, col1], self.avg_freq[row2, col2]))
                plt.axis('tight')

                #plt.subplot(nsp, 1, 3)
                #plt.plot(self.phase_unwrapped[row1, col1, :].squeeze(), 'k-', linewidth=2.0)
                #plt.plot(self.phase_unwrapped[row2, col2, :].squeeze(), 'r-', linewidth=2.0)
                #plt.legend(['E%d' % (k+1), 'E%d' % (j+1)])
                #plt.axis('tight')

                plt.subplot(nsp, 1, 3)
                plt.axhline(0.0, c='k')
                plt.plot(pdiff, 'b-', linewidth=2.0)
                plt.ylabel('Phase Diff')
                plt.axis('tight')

                plt.show()
                """

        self.phase_diff_mean = self.phase_diff.mean(axis=-1)
        self.phase_diff_max = self.phase_diff.max()
        self.phase_diff_min = self.phase_diff.min()
        self.phase_diff_absmax = max(np.abs(self.phase_diff_max), np.abs(self.phase_diff_min))

        """
        plt.figure()

        plt.subplot(3, 1, 1)
        plt.hist(self.phase.ravel(), bins=25)
        plt.xlabel('Phase')

        plt.subplot(3, 1, 2)
        plt.hist(self.phase_diff.ravel(), bins=25)
        plt.xlabel('Phase Difference')

        plt.subplot(3, 1, 3)
        im = plt.imshow(self.phase_diff_mean, interpolation='nearest', aspect='auto', origin='lower')
        tx = ['%d' % n for n in self.phase_diff_ticks]
        tx.reverse()
        plt.xticks(range(self.num_electrodes), tx)
        plt.yticks(range(self.num_electrodes), tx)
        plt.colorbar(im)
        plt.show()
        """

    def create_electrode_graph(self, t):

        #compute the xy location of each electrode
        electrode_location = np.zeros([self.nrows, self.ncols, 2])
        for row in range(self.nrows):
            for col in range(self.ncols):
                electrode_location[self.nrows-row-1, col, 0] = (col%2)*self.col_display_dist #x coordinate
                electrode_location[self.nrows-row-1, col, 1] = row*self.row_display_dist #y coordinate

        #map the undirected graph to a directed graph given the velocity directions
        dg=nx.DiGraph()
        #create nodes
        for row in range(self.nrows):
            for col in range(self.ncols):
                key=(row,col)
                dg.add_node(key,location=electrode_location[row,col].squeeze())

        #create horizontal edges
        for k in range(self.nrows):
            #get gradient
            grad=self.dx[k,self.lfp_channel,t-1]

            #get velocity
            v=self.vx[k,self.lfp_channel,t-1]

            #negative velocities goes right->left
            row=k
            col1=2*self.lfp_channel+int(v<0)
            col2=2*self.lfp_channel+int(v>=0)
            #create the connection
            dg.add_edge((row,col1),(row,col2),velocity=v,gradient=grad)

        #create vertical edges
        for col in range(self.ncols/2):
            for row in range(self.nrows-1):
                cindex=2*self.lfp_channel+col

                #get gradient
                grad=self.dy[row,cindex,t-1]

                #get velocity
                v=self.vy[row,cindex,t-1]

                if v>0:
                    #arrow should point upward
                    row1=row
                    row2=row+1
                else:
                    row1=row+1
                    row2=row

                #create the connection
                dg.add_edge((row1,col),(row2,col),velocity=v,gradient=grad)

        return dg

    def plot_channel(self, t, channel, ax):

        if t >= self.imf.shape[-1]:
            print 'Invalid time index! t=%d, self.imf.shape[-1]=%d' % (t, self.imf.shape[-1])
            return

        if channel == 0:
            plt.sca(ax)

            #plot the raw IMF
            imf = self.imf[:, :, t].squeeze()
            absmax = max(abs(self.imf_min), self.imf_max)
            plt.imshow(imf, interpolation='nearest', aspect='auto', vmin=-absmax, vmax=absmax,
                       extent=[0, 2, 0, 8], origin='upper', cmap=cm.seismic)
            plt.xticks([])
            plt.yticks([])
            labels = self.get_labels(channel, t)
            for row in range(8):
                for col in [0, 1]:
                    label_col = col
                    if len(self.hemispheres) == 1 and self.hemispheres[0] == 'R':
                        label_col += 2
                    txt = labels[row, label_col]
                    plt.text((col%2)+0.5, (7-row)+0.25, txt, horizontalalignment='center')
            plt.xlabel('IMF %d' % self.imf_number)

        elif channel == 1:
            plt.sca(ax)

            #plot the amplitude
            #amp = self.amplitude[:, :, t].squeeze()
            amp = self.log_amplitude[:, :, t].squeeze()

            plt.imshow(amp, interpolation='nearest', aspect='auto', vmin=self.log_amp_min, vmax=self.log_amp_max,
                       extent=[0, 2, 0, 8], origin='upper', cmap=cm.hot)
            plt.xticks([])
            plt.yticks([])
            labels = self.get_labels(channel, t)
            for row in range(8):
                for col in [0, 1]:
                    label_col = col
                    if len(self.hemispheres) == 1 and self.hemispheres[0] == 'R':
                        label_col += 2
                    txt = labels[row, label_col]
                    plt.text((col%2)+0.5, (7-row)+0.25, txt, horizontalalignment='center')
            plt.xlabel('Amplitude')

        elif channel == 2:
            plt.sca(ax)

            #plot the phase
            phase = self.phase[:, :, t].squeeze()

            plt.imshow(phase, interpolation='nearest', aspect='auto', vmin=-np.pi, vmax=np.pi,
                       extent=[0, 2, 0, 8], origin='upper', cmap=cm.hsv)
            plt.xticks([])
            plt.yticks([])
            labels = self.get_labels(channel, t)
            for row in range(8):
                for col in [0, 1]:
                    label_col = col
                    if len(self.hemispheres) == 1 and self.hemispheres[0] == 'R':
                        label_col += 2
                    txt = labels[row, label_col]
                    txt += "\n%0.3d" % int((180.0 / np.pi)*(phase[row, col] % 2*np.pi))
                    plt.text((col%2)+0.5, (7-row)+0.25, txt, horizontalalignment='center')
            plt.xlabel('Phase')

        elif channel == 3:
            plt.sca(ax)
            ax.set_axis_bgcolor('black')

            #plot the instantaneous frequency
            ifreq = self.ifreq[:, :, t].squeeze()
            #amp = self.amplitude[:, :, t].squeeze()
            amp = self.log_amplitude[:, :, t].squeeze()

            #create an image with alpha being the amplitude and the color being instantaneous frequency
            img = np.zeros([8, 2, 4], dtype='float32')

            #normalize instantaneous frequency for color
            ifreq_norm = (ifreq - self.ifreq_min) / (self.ifreq_max - self.ifreq_min)
            ifreq_norm[ifreq_norm > 1.0] = 1.0 #saturated values

            #normalize amplitude for alpha
            #amp /= self.log_amp_max
            #amp[amp > 1.0] = 1.0
            #amp[amp < 0.25] = 0.0
            amp = 1.0

            #set the rgba values for the image
            img[:, :, 0] = ifreq        #red
            img[:, :, 1] = 0.0          #green
            img[:, :, 2] = 1.0 - ifreq  #blue
            img[:, :, 3] = amp

            plt.imshow(img, interpolation='nearest', aspect='auto', extent=[0, 2, 0, 8], origin='upper')
            plt.xticks([])
            plt.yticks([])
            labels = self.get_labels(channel, t)
            for row in range(8):
                for col in [0, 1]:
                    label_col = col
                    if len(self.hemispheres) == 1 and self.hemispheres[0] == 'R':
                        label_col += 2
                    txt = labels[row, label_col]
                    txt += "\n%0.2dHz" % ifreq[row, col]
                    plt.text((col%2)+0.5, (7-row)+0.25, txt, horizontalalignment='center')
            plt.xlabel('Instantaneous Frequency')

        elif channel == 99:
            #plot phase difference matrix
            pdiff_mat = self.phase_diff[:, :, t].squeeze()

            im = plt.imshow(pdiff_mat, interpolation='nearest', aspect='auto',
                            vmin=self.phase_diff_min, vmax=self.phase_diff_max, origin='lower', cmap=cm.hsv)
            tx = ['%d' % n for n in self.phase_diff_ticks]
            tx.reverse()
            plt.xticks(range(self.num_electrodes), tx)
            plt.yticks(range(self.num_electrodes), tx)

            if len(self.hemispheres) > 1:
                plt.axhline(self.num_electrodes/2 - 0.5, color='k')
                plt.axvline(self.num_electrodes/2 - 0.5, color='k')
            #plt.colorbar(im)

        elif channel in [999]:
            plt.sca(ax)


            col_padding = self.col_display_dist / 4.0
            row_padding = self.row_display_dist / 4.0
            ax.set_xlim(-1.0*col_padding, self.col_display_dist + col_padding)
            ax.set_ylim(-1.0*row_padding, (self.nrows-1)*self.row_display_dist + row_padding)
            plt.xticks([])
            plt.yticks([])

            if t == 0:
                #don't plot any vectors
                return

            #create a networkx DiGraph with nodes representing the electrodes, and edge weights that correspond to
            #phase difference and velocity
            dg = self.create_electrode_graph(t)

            #draw the grid
            if channel == 3:
                self.draw_grid2d(dg, edge_value_name='gradient', ax=ax, vertical_max_value=self.max_dy, horizontal_max_value=self.max_dx)
            elif channel == 4:
                self.draw_grid2d(dg, edge_value_name='velocity', ax=ax, vertical_max_value=self.max_vy, horizontal_max_value=self.max_vx)

    def draw_grid2d(self, g, node_location_name='location', edge_value_name='weight', ax=None,
                    min_head_width = 5.0, max_head_width = 12.0, vertical_max_value=None, horizontal_max_value=None):
        """
            Draw a two-dimensional grid on a matplotlib axis given a networkx DirectedGraph object.

            g: A networkx directed graph. Each node should represent an electrode, with an attribute "location"
            describing it's position. Edges should connect each node, and should have an attribute "weight" that
            provides the connection weight.

            node_location_name: the name of the node attribute that describes a 2d electrode position in space.

            edge_value_name: the name of the edge attribute that describes the connection weight between two electrodes.
        """

        if ax is None:
            ax = plt.gca()

        #draw nodes
        for n in g.nodes():
            x,y = g.node[n][node_location_name]
            ax.plot_raw_imfs(x, y, 'ko')

        #draw edges
        for n1,n2 in g.edges():

            weight = g[n1][n2][edge_value_name]

            #get node coordinates
            x1,y1 = g.node[n1][node_location_name]
            x2,y2 = g.node[n2][node_location_name]

            #translate node coordinates into matplotlib arrow format
            x = x1
            dx = x2 - x1
            y = y1
            dy = y2 - y1

            #add padding to arrows
            if dx == 0:
                padding = np.abs(dy)*0.20
                is_up = int(y1 < y2) #does the edge go up or down?
                is_up_sign = 2*is_up - 1 # 1 if edge goes up, -1 if it goes down
                y += is_up_sign*padding # move origin up or down
                dy -= 2*is_up_sign*padding #reduce edge length
            elif dy == 0:
                padding = np.abs(dx)*0.20
                is_l2r = int(x1 < x2) # does the edge go left-to-right?
                is_l2r_sign = 2*is_l2r - 1 # 1 if edge goes left-to-right, -1 if right-to-left
                x += is_l2r_sign*padding #move origin to right or left
                dx -= 2*is_l2r_sign*padding #reduce edge length

            #calculate the size of the arrowhead based on magnitude
            row1,col1 = n1
            row2,col2 = n2
            max_value = None
            if  row1 != row2 and vertical_max_value is not None:
                max_value = vertical_max_value
            elif col1 != col2 and horizontal_max_value is not None:
                max_value = horizontal_max_value
            if max_value is not None:
                #normalize the display value
                wnorm = np.abs(weight) / max_value
            else:
                wnorm = np.abs(weight)
            arrow_size = min(max_head_width, wnorm*(max_head_width - min_head_width) + min_head_width)
            arrow_alpha = min(1.0, 0.5*wnorm + 0.45)

            #print '(%d,%d) to (%d,%d): w=%0.6f, max_value=%0.2f, wnorm=%0.2f' % (row1, col1, row2, col2, weight, max_value, wnorm)

            #draw the arrow
            ax.arrow(x, y, dx, dy, head_width=arrow_size, head_length=arrow_size, fc='k', ec='k', alpha=arrow_alpha)


class FlatLFPMovieRenderer(object):

    def __init__(self, data_source, framerate=15.0, edesc=''):
        self.data_source = data_source
        self.framerate = framerate
        self.edesc = edesc

    def render_movie(self, output_file, temp_dir='/tmp'):

        t1 = self.data_source.t1
        t2 = self.data_source.t2
        sample_rate = self.data_source.sample_rate

        #get the names and prefixes for files
        #root_dir,file_name = os.path.split(output_file)
        #base_name,filt_ext = os.path.splitext(file_name)
        base_name = 'lfp_movie'
        output_fmt2 = '%s_Site%d_%s_%sframe%%04d.png' % (base_name, site_id, protocol_name, self.edesc)

        #get the stimulus spectrogram
        stim_spec_t,stim_spec_f,stim_spec = self.data_source.protocol.compute_stimulus_spectrogram(t1, t2, spec_inc=1.0/self.data_source.sample_rate)

        #create a one-second wide stimulus placeholder
        spec_dt = stim_spec_t[1] - stim_spec_t[0]
        stim_win_len = 1.0
        stim_win_nlen = int(stim_win_len / spec_dt)
        if stim_win_nlen % 2 != 0:
            stim_win_nlen += 1
        stim_win = np.zeros([len(stim_spec_f), stim_win_nlen])

        start_index = int(t1*sample_rate)
        end_index = int(t2*sample_rate)
        nt = end_index - start_index
        print 'nt=%d, stim_spec_t.shape[-1]=%d' % (nt, stim_spec_t.shape[-1])

        if np.abs(nt - stim_spec_t.shape[-1]) > 1:
            print 'Some sort of problem! nt=%d, stim_spec_t.shape[-1]=%d' % (nt, stim_spec_t.shape[-1])
            return
        nt = min(nt, stim_spec_t.shape[-1])

        #render the LFPs
        for t in range(nt):

            output_fmt = '%s_Site%d_%s_%sframe%04d.png' % (base_name, site_id, protocol_name, self.edesc, t)
            ofile = os.path.join(temp_dir, output_fmt)
            if os.path.exists(ofile):
                print 'Skipping %s' % ofile
                continue

            treal = t / sample_rate
            #fill in the stimulus spectrogram window
            tspec = int(treal / spec_dt)
            stim_win[:, :] = 0.0
            middle_t = int(stim_win_nlen / 2.0)
            swin_t1 = 0
            swin_t2 = swin_t1 + stim_win_nlen
            spec_t1 = tspec - middle_t
            spec_t2 = tspec + middle_t
            #print 'treal=%0.3f, tspec=%d' % (treal, tspec)
            #print '(0) stim_win_nlen=%d, middle_t=%d, stim_spec.shape=%s' % (stim_win_nlen, middle_t, str(stim_spec.shape))
            #print '(1) swin_t1=%d, swin_t2=%d, spec_t1=%d, spec_t2=%d' % (swin_t1, swin_t2, spec_t1, spec_t2)

            #deal with edge effects
            if spec_t1 < 0:
                #adjust for left edge effects
                swin_t1 = np.abs(spec_t1)
                spec_t1 = 0

            stim_win_t = np.arange(stim_win.shape[1])*1e-3

            if spec_t2 > nt:
                #adjust for right edge effects
                #swin_t2 = middle_t + (spec_t2 - len(self.stim_spec_t))
                swin_t2 = stim_win.shape[1] - (spec_t2 - nt)
                spec_t2 = nt
            #print '(2) swin_t1=%d, swin_t2=%d, spec_t1=%d, spec_t2=%d' % (swin_t1, swin_t2, spec_t1, spec_t2)

            stim_win[:, swin_t1:swin_t2] = stim_spec[:, spec_t1:spec_t2]

            #make plot
            fig = plt.figure(figsize=(13.6, 11.0))
            plt.subplots_adjust(top=0.95, bottom=0.05, left=0.03, right=0.99, hspace=0.10)
            plt.suptitle('%04.3f s' % (treal+(start_index/sample_rate)))

            #plot the running stimulus spectrogram
            gs = GridSpec(4, 100)
            ax_spec = fig.add_subplot(gs[0, 5:96])
            plot_spectrogram(stim_win_t, stim_spec_f, stim_win, ax=ax_spec, colormap=cm.gist_yarg, colorbar=False)
            plt.xticks([])
            plt.yticks([])
            plt.xlabel('')
            plt.ylabel('')
            plt.axvline(middle_t*1e-3, color='b', alpha=0.5)

            padding = 2
            nchans = self.data_source.num_channels
            nsubplots = np.sum(self.data_source.num_subplots)
            space_available = 100
            space_per_chan = space_available / nsubplots
            #print 'nchans=%d, nsubplots=%d, space_available=%d, space_per_chan=%d' % (nchans, nsubplots, space_available, space_per_chan)

            gs_index = 0
            for k in range(nchans):
                #get number of subplots for this channel
                nsp_chan = self.data_source.num_subplots[k]

                #plot data from data source
                plot_width = space_per_chan - 2*padding
                gs_start = gs_index + padding
                gs_end = gs_start + space_per_chan*nsp_chan - padding
                #print 'k=%d, gs_start=%d, gs_end=%d' % (k, gs_start, gs_end)

                ax = plt.subplot(gs[1:, gs_start:gs_end])
                gs_index = gs_end
                self.data_source.plot_channel(t, k, ax)

            #write image to file
            #plt.show()
            print 'Saving frame to %s...' % ofile
            plt.savefig(ofile)

            plt.clf()
            plt.close('all')
            del fig

        #merge the frames into a movie
        input_fmt = os.path.join(temp_dir, output_fmt2)
        fps = self.framerate
        cmd = 'avconv -f image2 -r %d -i %s -c:v libx264 -preset slow -vf "scale=640:trunc(ow/a/2)*2" -crf 0 %s' % (fps, input_fmt, output_file)
        print cmd
        os.system(cmd)


if __name__ == '__main__':

    ap = argparse.ArgumentParser(description='Make LFP movies')
    ap.add_argument('--imf', type=int, required=True, help='The number of the IMF')
    argvals = ap.parse_args()

    f = tables.open_file('/auto/k8/fdata/mschachter/tdt_continuous/GreBlu9508M/GreBlu9508M_tabled.h5', 'r')

    site_id = 3
    protocol_name = 'Call3'
    hemis = ['L']

    imf_file = 'imfs_Site%d_%s_%s_start0.000000_end550.519746.h5' % (site_id, protocol_name, ','.join(hemis))
    imf_path = os.path.join('/auto/k8/fdata/mschachter/analysis/memd', imf_file)

    imf_number = argvals.imf
    name = 'imf%d' % imf_number
    output_file = '/tmp/lfp_movies/lfp_movie_Site%d_%s_%s.mp4' % (site_id, protocol_name, name)
    t1 = 121.0
    t2 = 141.0

    #ds = LFPDataSource(f, site_id, protocol_name, t1, t2, low_freq=None, high_freq=30.0)

    ds = MEMDDataSource(f, imf_path, imf_number, t1, t2)
    mrend = FlatLFPMovieRenderer(ds, framerate=15.0, edesc='imf%d' % imf_number)
    mrend.render_movie(output_file, temp_dir='/tmp/lfp_movies')

    f.close()

