import glob, os, h5py

import numpy as np
from scipy.interpolate import interp1d
import matplotlib.pyplot as plt

from lasp.hht import HHT,IMF
from lasp.signal import gaussian_stft
from lasp.sound import WavFile,plot_spectrogram
from neurotables.analysis.lfp.cross_coherence import ElectrodeComparator
from neurotables.analysis.lfp.objects import LFPEMD

from neurotables.objects.cells import Site


def compute_hht(lfp, protocol_name, output_file=None, sift_max_iter=100):

    presps = [pr for pr in lfp.protocol_responses if pr.protocol.name == protocol_name]
    if len(presps) == 0:
        print 'No such protocol response for protocol %s and LFP id of %d' % (protocol_name, lfp.id)

    sr = lfp.sample_rate
    pr = presps[0]

    hht = HHT(pr.waveform, sr, emd_resid_tol=1e-6, sift_max_iter=sift_max_iter, emd_max_modes=15)

    if output_file is not None:
        hf = h5py.File(output_file, 'w')

        #save EMD parameters
        emd_group = hf['/']
        emd_params = ['emd_max_modes', 'emd_resid_tol', 'sift_mean_tol', 'sift_stoppage_S', 'sift_max_iter',
                      'sift_remove_edge_effects', 'ensemble_num_samples', 'ensemble_noise_gain', 'hilbert_max_iter']
        for eparam in emd_params:
            emd_group.attrs[eparam] = getattr(hht, eparam)

        #save EMD output
        for k,imf in enumerate(hht.imfs):
            igrp = emd_group.create_group('IMF%d' % k)
            igrp['imf'] = imf.imf
            igrp['std'] = imf.std
            igrp['am'] = imf.am
            igrp['fm'] = imf.fm
            igrp['phase'] = imf.phase
            igrp['ifreq'] = imf.ifreq
        hf.close()


def aggregate_imfs(f, root_dir='/tmp', site_id=1, protocol='Call1', desc='iter100'):

    site = Site(f)
    assert site.from_id(site_id)

    for e in site.electrodes:
        fname = os.path.join(root_dir, 'hht_e%d_%s_%s.h5' % (e.number, protocol, desc))
        hf = h5py.File(fname, 'r')
        imfs = read_imfs(hf['/'])
        hf.close()
        print 'Integrating %s' % fname

        pr = e.get_lfp_protocol_response(protocol)
        lfp_emd = LFPEMD(f)
        lfp_emd.protocol_response_id = pr.id
        lfp_emd.save()

        imflen = len(imfs[0].imf)
        n = len(imfs)
        emd = np.zeros([n, imflen], dtype='float')
        for k,imf in enumerate(imfs):
            emd[k, :] = imf.imf
        lfp_emd.emd = emd


def create_sound(f, fname, t1=890.0, t2=910.0, output_dir='/tmp', cf1=587.33, cf2=1046.50, bw1=200.0, bw2=200.0, imf_index=4):

    ec = ElectrodeComparator.from_file(f, fname)

    imf1 = ec.pr1.emd.emd[imf_index, :]
    imf2 = ec.pr2.emd.emd[imf_index, :]

    i1 = int(t1 * ec.sample_rate)
    i2 = int(t2 * ec.sample_rate)
    d = i2 - i1
    tinterp = (np.arange(0, d) / ec.sample_rate)
    t = tinterp + t1

    #make FM signal
    dt = 1.0 / ec.sample_rate
    csr = 44100.0
    cdt = 1.0 / csr
    cn = int((t2 - t1 - 2*dt) / cdt)
    ct = np.arange(0, cn)*cdt

    print 'tinterp=(%0.4f, %0.4f), t=(%0.4f, %0.4f), ct=(%0.4f, %0.4f)' % (tinterp.min(), tinterp.max(), t.min(), t.max(), ct.min(), ct.max())
    #interpolate IMFs to resample
    ifunc1 = interp1d(tinterp, imf1[i1:i2], kind='linear')
    ifunc2 = interp1d(tinterp, imf2[i1:i2], kind='linear')

    #resample IMFs
    imf1_rs = ifunc1(ct)
    imf2_rs = ifunc2(ct)

    #normalize IMFs
    imf1_rs /= np.abs(imf1_rs).max()
    imf2_rs /= np.abs(imf2_rs).max()

    #create FM modulated signals and add them up
    fsum1 = np.cumsum(imf1_rs)
    fsum2 = np.cumsum(imf2_rs)
    cwave1 = np.sin(2*np.pi*ct*cf1 + bw1*fsum1)
    cwave2 = np.sin(2*np.pi*ct*cf2 + bw2*fsum2)
    w = cwave1 + cwave2

    #write to a .wav file
    ofname = os.path.join(output_dir, 'lfp_%d_%d.wav' % (ec.e1.number, ec.e2.number))
    wf = WavFile()
    wf.data = w
    wf.sample_rate = csr
    wf.depth = 1
    wf.to_wav(ofname, normalize=True)
    print 'Wrote file to %s' % ofname

    #make plots
    plt.figure()
    plt.plot(t, imf1[i1:i2]*1e6, 'r-', linewidth=2.0, alpha=0.75)
    plt.plot(t, imf2[i1:i2]*1e6, 'b-', linewidth=2.0, alpha=0.75)
    plt.legend(['E%d' % ec.e1.number, 'E%d' % ec.e2.number])
    plt.xlabel('Time (s)')
    plt.ylabel('Amplitude ($\mu$V)')
    plt.axis('tight')

    ec.plot_raw_imfs(t1, t2)




class RawFileComparator(object):
    """
        Compares the output of two different files created using compute_hht(...)
    """

    def __init__(self, f, enumber1, enumber2=None, desc1='iter100', desc2='iter500', protocol='Call1', root_dir='/tmp'):

        file_path1 = os.path.join(root_dir, 'hht_e%d_%s_%s.h5' % (enumber1, protocol, desc1))
        file_path2 = os.path.join(root_dir, 'hht_e%d_%s_%s.h5' % (enumber2, protocol, desc2))

        #read the IMFs from the files
        self.f = f
        hf1 = h5py.File(file_path1, 'r')
        self.imfs1 = read_imfs(hf1['/'])
        hf1.close()

        hf2 = h5py.File(file_path2, 'r')
        self.imfs2 = read_imfs(hf2['/'])
        hf2.close()

        #plot them
        print 'Files: %s and %s' % (file_path1, file_path2)
        print '# of IMFs for %s: %d' % (file_path1, len(self.imfs1))
        print '# of IMFs for %s: %d' % (file_path2, len(self.imfs2))

        self.nimfs = np.array([len(self.imfs1), len(self.imfs2)])

        if enumber2 is None:
            enumber2 = enumber1

        self.electrode_num1 = enumber1
        self.electrode_num2 = enumber2

        #get the Site
        s = Site(self.f)
        assert s.from_id(1)

        #get the Protocol
        plist = [p for p in s.protocols if p.name == protocol]
        assert len(plist) > 0
        self.protocol = plist[0]

        #get Electrodes
        self.electrode1 = s.get_electrode(self.electrode_num1)
        if self.electrode_num1 != self.electrode_num2:
            self.electrode2 = s.get_electrode(self.electrode_num2)
        else:
            self.electrode2 = self.electrode_num1

        #get the LFP for each electrode
        self.protocol_response1 = self.electrode1.get_lfp_protocol_response(self.protocol.name)
        self.protocol_response2 = self.electrode2.get_lfp_protocol_response(self.protocol.name)

        self.sample_rate = self.protocol_response1.lfp.sample_rate
        self.lfp1 = self.protocol_response1.waveform
        self.lfp2 = self.protocol_response2.waveform

        duration1 = len(self.imfs1[0].imf)
        duration2 = len(self.imfs2[0].imf)
        assert duration1 == duration2
        self.iduration = duration1
        self.duration = duration1 / self.sample_rate

        assert len(self.lfp1) == self.iduration and len(self.lfp2) == self.iduration

        #compute stimulus envelope
        self.stim_envelope = self.protocol.compute_stimulus_envelope(sample_rate=self.sample_rate, duration=self.duration)

    def plot(self, t1, t2):

        i1 = int(t1 * self.sample_rate)
        i2 = int(t2 * self.sample_rate)
        d = i2 - i1

        t = (np.arange(0, d) / self.sample_rate) + t1

        for k in range(1,5):
            #get the IMF data
            imf1 = self.imfs1[k]
            imf2 = self.imfs2[k]

            #get the stimulus envelope
            stim_env = self.stim_envelope[i1:i2]

            #comute spectrograms of IMFs
            winlen = 0.100
            spec_inc = 0.020
            imf_t1,imf_freq1,imf_spec1,imf_rms1 = gaussian_stft(imf1.imf[i1:i2], self.sample_rate, winlen, spec_inc, min_freq=0.0, max_freq=None)
            imf_t2,imf_freq2,imf_spec2,imf_rms2 = gaussian_stft(imf2.imf[i1:i2], self.sample_rate, winlen, spec_inc, min_freq=0.0, max_freq=None)

            #compute spectrograms of LFPs
            spec_inc = 0.020
            window_size = 0.100
            spec_t1,spec_freq1,spec1,spec_rms1 = self.protocol_response1.compute_spectrogram(spec_inc, window_size, min_freq=0.0, max_freq=100.0, t1=t1, t2=t2)
            spec_t2,spec_freq2,spec2,spec_rms2 = self.protocol_response2.compute_spectrogram(spec_inc, window_size, min_freq=0.0, max_freq=100.0, t1=t1, t2=t2)
            spec1 = 20*np.log10(spec1 / np.abs(spec1).max())
            spec2 = 20*np.log10(spec2 / np.abs(spec2).max())

            noverlap = int(0.005*self.sample_rate)
            nfft = int(spec_inc*self.sample_rate)

            legend = ['%d' % self.electrode_num1, '%d' % self.electrode_num2]

            #plot stimulus amplitude envelope
            fig = plt.figure()
            plt.subplots_adjust(bottom=0.04, top=0.99, right=0.99, left=0.04)
            fig.canvas.set_window_title('IMF %d' % k)
            plt.subplot(7, 1, 1)
            plt.plot(t, stim_env, 'k-')
            plt.ylabel('Stim Env.')
            plt.axis('tight')

            #plot LFPs
            plt.subplot(7, 1, 2)
            plt.plot(t, self.lfp1[i1:i2], 'k-', linewidth=2)
            plt.plot(t, self.lfp2[i1:i2], 'r-', linewidth=2, alpha=0.50)
            plt.ylabel('LFP')
            plt.axis('tight')

            #plot IMFs
            plt.subplot(7, 1, 3)
            plt.plot(t, imf1.imf[i1:i2], 'k-', linewidth=2)
            plt.plot(t, imf2.imf[i1:i2], 'r-', linewidth=2, alpha=0.50)
            plt.axis('tight')
            plt.ylabel('IMF')
            plt.legend(legend)

            #plot spectrograms of IMFs
            plt.subplot(7, 1, 4)
            plot_spectrogram(imf_t1, imf_freq1, imf_spec1, fmin=3.0, fmax=100.0)
            #plt.imshow(imf_spec1, extent=[imf_t1.min(), imf_t1.max(), imf_freq1.min(), imf_freq1.max()], interpolation='nearest', aspect='auto')
            #Pxx, freqs, bins, im = plt.specgram(imf1.imf[i1:i2], Fs=self.sample_rate, cmap=cm.gist_heat, NFFT=nfft, noverlap=noverlap)
            plt.axis('tight')
            plt.ylabel('IMF Spec %d' % self.electrode_num1)

            plt.subplot(7, 1, 5)
            plot_spectrogram(imf_t2, imf_freq2, imf_spec2, fmin=3.0, fmax=100.0)
            #plt.imshow(imf_spec2, extent=[imf_t2.min(), imf_t2.max(), imf_freq2.min(), imf_freq2.max()], interpolation='nearest', aspect='auto')
            #Pxx, freqs, bins, im = plt.specgram(imf2.imf[i1:i2], Fs=self.sample_rate, cmap=cm.gist_heat, NFFT=nfft, noverlap=noverlap)
            plt.axis('tight')
            plt.ylabel('IMF Spec %d' % self.electrode_num2)

            #plot LFP spectrogram
            plt.subplot(7, 1, 6)
            plot_spectrogram(spec1, spec_t1, spec_freq1, fmin=3.0, fmax=100.0)
            #plt.imshow(spec1, extent=[spec_t1.min(), spec_t1.max(), spec_freq1.min(), spec_freq1.max()], interpolation='nearest', aspect='auto')
            #Pxx, freqs, bins, im = plt.specgram(self.lfp1[i1:i2], Fs=self.sample_rate, cmap=cm.gist_heat, NFFT=nfft, noverlap=noverlap)
            plt.axis('tight')
            plt.ylabel('LFP Spec %d' % self.electrode_num1)

            plt.subplot(7, 1, 7)
            plot_spectrogram(spec2, spec_t2, spec_freq2, fmin=3.0, fmax=100.0)
            #plt.imshow(spec2, extent=[spec_t2.min(), spec_t2.max(), spec_freq2.min(), spec_freq2.max()], interpolation='nearest', aspect='auto')
            #Pxx, freqs, bins, im = plt.specgram(self.lfp2[i1:i2], Fs=self.sample_rate, cmap=cm.gist_heat, NFFT=nfft, noverlap=noverlap)
            plt.axis('tight')
            plt.ylabel('LFP Spec %d' % self.electrode_num2)

            #plot AM components
            """
            plt.subplot(6, 1, 4)
            plt.plot(t, imf1.am[i1:i2], 'k-', linewidth=2)
            plt.plot(t, imf2.am[i1:i2], 'r-', linewidth=2, alpha=0.50)
            plt.axis('tight')
            plt.ylabel('AM')
            plt.legend(legend)

            #plot FM components
            plt.subplot(6, 1, 5)
            plt.plot(t, imf1.fm[i1:i2], 'k-', linewidth=2)
            plt.plot(t, imf2.fm[i1:i2], 'r-', linewidth=2, alpha=0.50)
            plt.axis('tight')
            plt.ylabel('FM')
            plt.legend(legend)

            #plot instantaneous frequencies
            plt.subplot(6, 1, 6)
            plt.plot(t, imf1.ifreq[i1:i2], 'k-', linewidth=2)
            plt.plot(t, imf2.ifreq[i1:i2], 'r-', linewidth=2, alpha=0.50)
            plt.axis('tight')
            plt.ylabel('Inst. Freq.')
            plt.legend(legend)
            """


def read_imfs(group):
    imfs = list()
    for imf_key,imf_group in group.iteritems():
        imf = IMF()
        imf.imf = np.array(imf_group['imf'])
        imf.am = np.array(imf_group['am'])
        imf.fm = np.array(imf_group['fm'])
        imf.phase = np.array(imf_group['phase'])
        imf.ifreq = np.array(imf_group['ifreq'])
        imfs.append(imf)

    return imfs


def read_pairs(fname):
    pairs = list()
    f = open(fname, 'r')
    for ln in f.readlines():
        if len(ln) > 0:
            e1,e2 = [int(x.strip()) for x in ln.split(',')]
            pairs.append( (e1, e2) )
    return pairs


