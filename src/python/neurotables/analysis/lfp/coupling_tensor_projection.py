import os
import h5py
import operator
from matplotlib.gridspec import GridSpec

from sklearn.decomposition import RandomizedPCA
import tables

import numpy as np

import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D

from neurotables.analysis.lfp.coupling_tensor import LFPCouplingTensor
from neurotables.objects.cells import Site,Sound


def pca_projection(nmi2d_file='/auto/k8/fdata/mschachter/analysis/cc/nmi2d_Site1_Call1.h5', num_dim=3,
                   output_file='/auto/k8/fdata/mschachter/analysis/cc/pca_Site1_Call1.h5'):

    hf = h5py.File(nmi2d_file, 'r')
    C = np.array(hf['matrix'])
    hf.close()

    pca = RandomizedPCA(n_components=num_dim)
    pca.fit(C.T)

    X = pca.transform(C.T)
    del C

    hf = h5py.File(output_file, 'w')
    hf['matrix'] = X
    hf.close()

    del X
    del pca

def plot_trajectories(f, stim_id, site_id=1, protocol_name='Call1',
                      pca_dir='/auto/k8/fdata/mschachter/analysis/cc'):

    #get sound
    sound = Sound(f)
    assert sound.from_id(stim_id)

    #get site and protocol
    site = Site(f)
    assert site.from_id(site_id)

    sample_rate = site.electrodes[0].lfp.sample_rate

    pcalls = [p for p in site.protocols if p.name == protocol_name]
    assert len(pcalls) == 1
    protocol = pcalls[0]

    #load all the sound events for the given stimulus id
    sound_events = [se for se in protocol.events if se.sound.id == stim_id]
    sound_events.sort(key=operator.attrgetter('start_time'))

    #load the projected components from the file
    pca_file = os.path.join(pca_dir, 'pca_Site%d_%s.h5' % (site_id, protocol_name))
    hf = h5py.File(pca_file, 'r')
    X = np.array(hf['matrix'])
    hf.close()

    #get the max and min values across stimulus presentations
    xmax = -np.inf
    xmin = np.inf
    ymax = -np.inf
    ymin = np.inf
    zmax = -np.inf
    zmin = np.inf

    for k,se in enumerate(sound_events[:10]):
        si = int(se.start_time*sample_rate)
        ei = int(se.end_time*sample_rate)
        xmax = max(xmax, X[si:ei, 0].max())
        xmin = min(xmin, X[si:ei, 0].min())
        ymax = max(ymax, X[si:ei, 1].max())
        ymin = min(ymin, X[si:ei, 1].min())
        zmax = max(zmax, X[si:ei, 2].max())
        zmin = min(zmin, X[si:ei, 2].min())

    offset_before = int(0.750*sample_rate)
    offset_after = int(0.750*sample_rate)

    #plot a trajectory for each stimulus presentation
    fig = plt.figure(figsize=(16, 9))
    plt.subplots_adjust(bottom=0.01, top=0.99, right=0.99, left=0.01, wspace=0.01, hspace=0.05)
    gs = GridSpec(5, 2)
    for k,se in enumerate(sound_events[:10]):

        row = int(k/2)
        col = k % 2

        ax = fig.add_subplot(gs[row, col], projection='3d')

        si = int(se.start_time*sample_rate) - offset_before
        ei = int(se.end_time*sample_rate) + offset_after

        traj = X[si:ei, :]

        clrs = np.arange(ei-si, dtype='float') / (ei-si)

        p = ax.scatter(traj[:, 0], traj[:, 1], traj[:, 2], s=35.0, c=clrs, cmap=cm.jet, vmin=0.0, vmax=1.0, alpha=1.0)
        ax.set_xlim([xmin, xmax])
        ax.set_ylim([ymin, ymax])
        ax.set_zlim([zmin, zmax])

    title = '%d - %s (%s)' % (sound.id, sound.stim_type, sound.call_id)
    plt.suptitle(title)

if __name__ == '__main__':

    f = tables.open_file('/auto/k8/fdata/mschachter/tdt_continuous/GreBlu9508M/GreBlu9508M_tabled.h5', mode='r')

    stim_ids = [255, 197, 17, 277, 206, 285, 286, 174, 175]

    for sid in stim_ids:
        plot_trajectories(f, sid, site_id=3, protocol_name='Call3')
        plt.savefig('/auto/k8/fdata/mschachter/analysis/figures/pca/pca_proj_stim%d.png' % sid)

    f.close()
