import numpy as np

import operator

import tables
from neurotables.analysis.lfp.complex import get_complex_lfp

from neurotables.objects.cells import Site


def get_wave_properties(f, start_time=None, end_time=None, site_id=3, protocol_name='Call3', low_freq=0.0, high_freq=30.0):

    #get site
    site = Site(f)
    assert site.from_id(site_id)

    #get protocol
    plist = [p for p in site.protocols if p.name == protocol_name]
    assert len(plist) == 1
    protocol = plist[0]

    #get electrode
    elist = [e for e in site.electrodes]
    assert len(elist) == 32
    elist.sort(key=operator.attrgetter('number'))

    #get coordinates
    electrode_geometry = site.electrodes[0].geometry
    coords = electrode_geometry.coordinates

    #peak into first electrode to determine length of recording
    lfp_pr = elist[0].get_lfp_protocol_response(protocol.name)
    lfp = lfp_pr.waveform
    lfp_len = len(lfp)
    sample_rate = lfp_pr.lfp.sample_rate
    del lfp

    #identify start and end indices
    if start_time is None:
        start_time = 0.0
    if end_time is None:
        end_time = lfp_len/sample_rate
    si = int(start_time*sample_rate)
    ei = int(end_time*sample_rate)
    lfp_len = ei - si

    #get LFPs
    lfps = np.zeros([coords.shape[0], coords.shape[1], lfp_len], dtype='complex')
    for electrode in elist:
        row,col = np.nonzero(coords == electrode.number)
        z,sample_rate = get_complex_lfp(f, electrode.number, start_time=start_time, end_time=end_time,
                                        site_id=site_id, protocol_name=protocol_name,
                                        low_freq=low_freq, high_freq=high_freq)
        lfps[row, col, :] = z

    phase = np.unwrap(np.angle(lfps))
    print 'phase.shape=',phase.shape
    del lfps

    return compute_wave_properties(phase)


def compute_wave_properties(lfp_phase, row_dist=50.0, col_dist=250.0):

    row_dist = 50.0 #in microns
    col_dist = 250.0 #in microns

    #compute medial-lateral derivative, don't combine hemispheres
    dx_left = np.diff(lfp_phase[:, [0, 1], :], axis=1) / col_dist
    dx_right = np.diff(lfp_phase[:, [2, 3], :], axis=1) / col_dist
    dx = np.concatenate([dx_left, dx_right], axis=1)
    dy = np.diff(lfp_phase, axis=0) / row_dist
    dt = np.diff(lfp_phase, axis=-1)

    #print 'dt.shape=',dt.shape
    #print 'dx_left.shape=',dx_left.shape
    #print 'dx_right.shape=',dx_right.shape
    #print 'dx.shape=',dx.shape
    #print 'dy.shape=',dy.shape

    #compute velocity
    vx = dt[:, [0, 2], :] / dx[:, :, 1:]
    vy = dt[:-1, :, :] / dy[:, :, 1:]
    #print 'vx.shape=',vx.shape
    #print 'vy.shape=',vy.shape

    #compute PGD
    pgd_x = dx.mean(axis=0)
    pgd_y = dy.mean(axis=0)
    #print 'pgd_x.shape=',pgd_x.shape
    #print 'pgd_y.shape=',pgd_y.shape

    return dt, dx, dy, vx, vy, pgd_x, pgd_y




if __name__ == '__main__':

    f = tables.open_file('/auto/k8/fdata/mschachter/tdt_continuous/GreBlu9508M/GreBlu9508M_tabled.h5', 'r')

    site_id = 3
    protocol_name = 'Call3'

    start_time = 455.0
    end_time = 460.0

    dt, dx, dy, v_x,v_y,pgd_x, pgd_y = get_wave_properties(f, start_time=start_time, end_time=end_time,
                                                           site_id=site_id, protocol_name=protocol_name,
                                                           low_freq=5.0, high_freq=30.0)
