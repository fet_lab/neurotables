import argparse
from matplotlib.gridspec import GridSpec
from neurotables.analysis.lfp.cross_coherence import  *
from neurotables.analysis.lfp.electrode_graph import ElectrodeGraph,LFPMovieRenderer,ElectrodeGraphRenderer
from neurotables.analysis.lfp.stim_cond import preprocess_envelope
from neurotables.analysis.lfp.coupling_tensor import LFPCouplingTensor


class CircularElectrodeGraph(ElectrodeGraph):

    def __init__(self, num_rows=8, array_centers=[0.0, 600.0], radius=300.0):

        self.radius = radius
        ElectrodeGraph.__init__(self, num_arrays=2, num_sites_per_shank=1, num_rows=num_rows, num_cols=2,
                                array_locations=array_centers, site_depths=[0.0])

    def init_nodes_and_locations(self):
        self.X = np.zeros([self.num_arrays, self.num_sites_per_shank, self.num_rows, self.num_cols])
        self.Y = np.zeros([self.num_arrays, self.num_sites_per_shank, self.num_rows, self.num_cols])
        self.Z = np.zeros([self.num_arrays, self.num_sites_per_shank, self.num_rows, self.num_cols])

        num_electrodes_in_array = self.num_rows*self.num_cols
        dtheta = (2*np.pi) / (num_electrodes_in_array + 2)

        for k in range(self.num_arrays):
            for j in range(self.num_sites_per_shank):
                for row in range(self.num_rows):
                    for col in range(self.num_cols):

                        #specify the same x coordinate for all points on the circle
                        self.X[k, j, row, col] = self.array_locations[k]

                        #get linear index for row/col, goes from upper right down to lower right, to lower left, to upper left,
                        #represents coordinates on the circle like numbers do on a clock
                        linear_index = (1 - col)*self.num_rows + row + 1 + (1 - col)

                        #convert linear index to angle in polar coordinates
                        theta = linear_index*dtheta

                        #convert polar coordinates to cartesian coordinates
                        self.Y[k, j, row, col] = self.radius*np.sin(theta)
                        self.Z[k, j, row, col] = self.radius*np.cos(theta)

                        self.g.add_node( (k, j, row, col), location=[self.X[k, j, row, col], self.Y[k, j, row, col], self.Z[k, j, row, col]])

    def init_connections(self):
        #connect all nodes to eachother
        for node_id1 in self.g.nodes():
            for node_id2 in self.g.nodes():
                self.g.add_edge(node_id1, node_id2)

    def get_node_value_extrema(self, node_value_name, percentile=99):

        node_vals = np.array([self.g.node[node_id][node_value_name] for node_id in self.g.nodes()])
        absmax = np.percentile(np.abs(node_vals), percentile)
        node_max = absmax
        node_min = -absmax
        return node_min,node_max

    def get_node_locations_and_values(self, array_index, node_value_name):

        #fill the node value matrix with values
        L = np.zeros([self.num_arrays, self.num_sites_per_shank, self.num_rows, self.num_cols])

        for k in range(self.num_arrays):
            for j in range(self.num_sites_per_shank):
                for row in range(self.num_rows):
                    for col in range(self.num_cols):
                        if node_value_name in self.g.node[(k, j, row, col)]:
                            L[k, j, row, col] = self.g.node[(k, j, row, col)][node_value_name]

        X = self.X[:, array_index, :, :].ravel()
        Y = self.Y[:, array_index, :, :].ravel()
        Z = self.Z[:, array_index, :, :].ravel()
        V = L[:, array_index, :, :].ravel()

        return X,Y,Z,V

    def get_edge_locations_and_values(self, array_index, edge_value_name='weight'):

        X1 = list()
        Y1 = list()
        Z1 = list()
        X2 = list()
        Y2 = list()
        Z2 = list()
        W = list()

        edge_list = [((k1, j1, row1, col1),(k2, j2, row2, col2)) for (k1, j1, row1, col1),(k2, j2, row2, col2) in self.g.edges() if j1 == j2 and j1 == array_index]
        for (k1, j1, row1, col1),(k2, j2, row2, col2) in edge_list:
            node1 = (k1, j1, row1, col1)
            node2 = (k2, j2, row2, col2)
            if edge_value_name in self.g[node1][node2]:

                w = self.g[node1][node2][edge_value_name]
                x1,y1,z1 = self.X[k1, j1, row1, col1], self.Y[k1, j1, row1, col1], self.Z[k1, j1, row1, col1]
                x2,y2,z2 = self.X[k2, j2, row2, col2], self.Y[k2, j2, row2, col2], self.Z[k2, j2, row2, col2]

                X1.append(x1)
                Y1.append(y1)
                Z1.append(z1)

                X2.append(x2)
                Y2.append(y2)
                Z2.append(z2)

                W.append(w)

        return X1,Y1,Z1,X2,Y2,Z2,W

class CircularCoherenceMovieRenderer(object):


    def __init__(self, f, site_id=1, protocol_name='Call1',
                 tensor_dir='/auto/k8/fdata/mschachter/analysis/cc',
                 start_time=0.0, end_time=None, plot_3d=False,
                 zscore=True):

        self.f = f
        self.tensor_file = os.path.join(tensor_dir, 'lfp_tensor_Site%d_%s.h5' % (site_id, protocol_name))
        self.plot_3d = plot_3d

        #load up site
        self.site = Site(self.f)
        assert self.site.from_id(site_id)

        #load up protocol
        pcalls = [p for p in self.site.protocols if p.name == protocol_name]
        assert len(pcalls) == 1
        self.protocol = pcalls[0]
        self.protocol_response = self.protocol.lfp_protocol_responses[0]
        self.sample_rate = self.protocol_response.lfp.sample_rate

        #identify number of hemispheres
        hemispheres = np.unique([e.hemisphere for e in self.site.electrodes])
        nhemi = len(hemispheres)
        self.array_locations = np.arange(nhemi)*400.0

        #get electrode geometry and coordinates matrix
        e = self.site.electrodes[0]
        self.geometry = e.geometry
        self.coords = self.geometry.coordinates
        self.num_electrodes = len(self.site.electrodes)

        #create an electrode graph
        nrows,ncols = self.geometry.shape

        self.start_time = start_time
        self.end_time = end_time
        self.start_index = int(self.start_time*self.sample_rate)
        self.end_index = int(self.end_time*self.sample_rate)

        self.egraph = CircularElectrodeGraph(num_rows=nrows, array_centers=self.array_locations)
        self.renderer = ElectrodeGraphRenderer()

        #get log stimulus amplitude envelope
        self.stim_env_t,stim_env = self.protocol.compute_stimulus_envelope(sample_rate=self.sample_rate, t1=self.start_time, t2=self.end_time)
        self.stim_env = preprocess_envelope(stim_env, self.sample_rate, cutoff_freq=None, env_max=None, thresh=-50.0)
        movie_dur = min(self.end_index-self.start_index, len(self.stim_env))
        self.end_index = self.start_index + movie_dur
        self.stim_env = self.stim_env[:movie_dur]
        print 'start_index=%d, end_index=%d' % (self.start_index, self.end_index)
        print 'stim_env.shape=',self.stim_env.shape
        self.stim_env_nz = self.stim_env > self.stim_env.min()

        #get the stimulus spectrogram
        self.stim_spec_t,self.stim_spec_f,self.stim_spec = self.protocol.compute_stimulus_spectrogram(self.start_time, self.end_time)

        #read the LFP tensor object
        print 'Reading LFP tensor from %s' % self.tensor_file
        self.tensor = LFPCouplingTensor.from_file(self.f, self.tensor_file, t1=self.start_time, t2=self.end_time, zscore=zscore)

        edge_min = np.inf
        edge_max = -np.inf

        #get min and max
        self.edge_threshold = -np.inf
        for key in self.tensor.nmi.keys():
            nmi = self.tensor.nmi[key]
            nz = nmi >= 1.0
            nmi[nz] = np.log(nmi[nz])

            ethresh = np.percentile(nmi[nz], 10)
            self.edge_threshold = min(ethresh, self.edge_threshold)

            edge_min = min(nmi.min(), edge_min)
            edge_max = max(nmi.max(), edge_max)

        self.edge_threshold = edge_max*0.50

        self.edge_min = edge_min
        self.edge_max = edge_max

        self.node_min = -1.0
        self.node_max = 1.0

    def make_movie(self, output_dir='/tmp'):

        output_fmt = 'cc_Site%d_%s_frame%04d.png'
        output_fmt2 = 'cc_Site%d_%s_frame%%04d.png'
        #plot a frame for each segment of time, write to a file
        t = np.arange(self.end_index - self.start_index, dtype='int')/self.sample_rate

        #create a one-second wide stimulus placeholder
        spec_dt = self.stim_spec_t[1] - self.stim_spec_t[0]
        stim_win_len = 1.0
        stim_win_nlen = int(stim_win_len / spec_dt)
        stim_win = np.zeros([len(self.stim_spec_f), stim_win_nlen])

        for ti,treal in enumerate(t):

            #specify output file
            ofile = os.path.join(output_dir, output_fmt % (self.site.id, self.protocol.name,  ti))
            if os.path.exists(ofile):
                print 'Skipping %s, it already exists.' % ofile
                continue

            #set the node value of each graph equal to the LFP magnitude
            for k, j, row, col in self.egraph.g.nodes():
                #self.coords contains both hemispheres, but the graph is indexed by hemisphere,
                #so we need to adjust the column index corresponding to the electrode to
                #get the right hemisphere
                #ccol = col + 2*k
                #print 'k=%d, j=%d, row=%d, col=%d, ccol=%d' % (k, j, row, col, ccol)
                #enumber = self.coords[row, ccol]
                #eindex = self.electrode2index[enumber]
                #print 'enumber=%d, eindex=%d' % (enumber, eindex)
                #self.egraph.g.node[(k, j, row, col)]['v'] = lfp[j, eindex, ti]

                self.egraph.g.node[(k, j, row, col)]['v'] = 0.0

            #set each edge value to it's normalized PCA projected cross coherence value
            cc_mat = np.zeros([self.num_electrodes, self.num_electrodes])
            for (k1, j1, row1, col1),(k2, j2, row2, col2) in self.egraph.g.edges():
                ccol1 = col1 + 2*k1
                ccol2 = col2 + 2*k2
                e1 = self.coords[row1, ccol1]
                e2 = self.coords[row2, ccol2]
                if e1 == e2:
                    continue

                pval = None
                if (e1,e2) in self.tensor.nmi:
                    pval = self.tensor.nmi[(e1, e2)][ti]
                elif (e2,e1) in self.tensor.nmi:
                    pval = self.tensor.nmi[(e2, e1)][ti]

                if pval is not None:
                    self.egraph.g[(k1, j1, row1, col1)][(k2, j2, row2, col2)]['weight'] = pval
                    cc_mat[e1-1, e2-1] = pval
                    cc_mat[e2-1, e1-1] = cc_mat[e1-1, e2-1]

            #fill in the stimulus spectrogram window
            tspec = int(treal / spec_dt)
            stim_win[:, :] = 0.0
            middle_t = int(stim_win_nlen / 2.0)
            swin_t1 = 0
            swin_t2 = swin_t1 + stim_win_nlen
            spec_t1 = tspec - middle_t
            spec_t2 = tspec + middle_t
            #print '(0) stim_win_nlen=%d, middle_t=%d, stim_spec.shape=%s' % (stim_win_nlen, middle_t, str(self.stim_spec.shape))
            #print '(1) swin_t1=%d, swin_t2=%d, spec_t1=%d, spec_t2=%d' % (swin_t1, swin_t2, spec_t1, spec_t2)

            #deal with edge effects
            if spec_t1 < 0:
                #adjust for left edge effects
                swin_t1 = np.abs(spec_t1)
                spec_t1 = 0

            stim_win_t = np.arange(stim_win.shape[1])*1e-3

            if spec_t2 > len(self.stim_spec_t):
                #adjust for right edge effects
                #swin_t2 = middle_t + (spec_t2 - len(self.stim_spec_t))
                swin_t2 = stim_win.shape[1] - (spec_t2 - len(self.stim_spec_t))
                spec_t2 = len(self.stim_spec_t)
            #print '(2) swin_t1=%d, swin_t2=%d, spec_t1=%d, spec_t2=%d' % (swin_t1, swin_t2, spec_t1, spec_t2)

            stim_win[:, swin_t1:swin_t2] = self.stim_spec[:, spec_t1:spec_t2]

            #plt.figure()
            #plot_spectrogram(self.stim_spec_t, self.stim_spec_f, self.stim_spec)

            #make plot
            fig = plt.figure(figsize=(12, 8))

            actual_t = (float(ti) / self.sample_rate) + self.start_time
            plt.suptitle('%0.3f s' % actual_t)
            plt.subplots_adjust(top=0.95, bottom=0.05, left=0.03, right=0.99, hspace=0.03)

            #plot the running stimulus spectrogram
            gs = GridSpec(5, 1)
            ax_spec = fig.add_subplot(gs[0, 0])
            plot_spectrogram(stim_win_t, self.stim_spec_f, stim_win, ax=ax_spec, colormap=cm.gist_yarg, colorbar=False)
            plt.xticks([])
            plt.yticks([])
            plt.axvline(middle_t*1e-3, color='b', alpha=0.5)

            if self.plot_3d:
                #plot the circular electrodes with their connections
                ax_cc = fig.add_subplot(gs[1:, 0], projection='3d')

                print 'Rendering frame %d of %d to %s' % (ti, len(t), ofile)
                self.renderer.render(self.egraph, [ax_cc], node_value_min=self.node_min, node_value_max=self.node_max,
                                     edge_value_min=self.edge_min, edge_value_max=self.edge_max,
                                     edge_value_threshold=self.edge_threshold,
                                     output_file=ofile, draw_edges=True, fig=fig)

            else:
                print 'Rendering frame %d of %d to %s' % (ti, len(t), ofile)
                ax_cc = fig.add_subplot(gs[1:, 0])
                plt.imshow(cc_mat, interpolation='nearest', aspect='auto', vmin=self.edge_min, vmax=self.edge_max,
                           origin='lower')
                plt.axhline(self.num_electrodes/2 - 0.5, color='k')
                plt.axvline(self.num_electrodes/2 - 0.5, color='k')

                #plt.show()
                plt.savefig(ofile)

                plt.clf()
                plt.close('all')

            del ax_spec
            del ax_cc
            del fig

        #merge the frames into a movie
        mp4_file = os.path.join(output_dir, 'cc_Site%d_%s.mp4' % (self.site.id, self.protocol.name))
        input_fmt = os.path.join(output_dir, output_fmt2 % (self.site.id, self.protocol.name))
        fps = 30.0
        cmd = 'avconv -f image2 -r %d -i %s -c:v libx264 -preset slow -vf "scale=640:trunc(ow/a/2)*2" -crf 0 %s' % (fps, input_fmt, mp4_file)
        print cmd
        os.system(cmd)

        #create a .wav file for the amplitude envelope
        #wav_file = os.path.join(output_dir, 'stim_%d.wav' % stim_id)
        #self.envelope_to_sound(stim_id, output_file=wav_file, movie_fps=fps)

        #merge the video and audio
        #ofile = os.path.join(output_dir, 'stim_%d+audio.mp4' % stim_id)
        #cmd = 'avconv -i %s -i %s -c copy -c:a libmp3lame %s' % (mp4_file, wav_file, ofile)
        #os.system(cmd)


if __name__ == '__main__':

    #ap = argparse.ArgumentParser(description='Create a cross coherence movie')
    #argvals = ap.parse_args()

    f = tables.open_file('/auto/k8/fdata/mschachter/tdt_continuous/GreBlu9508M/GreBlu9508M_tabled.h5', mode='r')

    #renderer = CircularCoherenceMovieRenderer(f, site_id=2, protocol_name='Call2', start_time=742.0, end_time=760.0, tensor_dir='/auto/k8/fdata/mschachter/analysis/cc')
    renderer = CircularCoherenceMovieRenderer(f, site_id=2, protocol_name='Call2', start_time=840.0, end_time=858.0, tensor_dir='/auto/k8/fdata/mschachter/analysis/cc', zscore=True)
    renderer.make_movie(output_dir='/tmp/cc_movies')

    #renderer = CircularCoherenceMovieRenderer(f, site_id=1, protocol_name='Call1', start_time=406.550, end_time=410.0,
    #                                          tensor_dir='/tmp', plot_3d=True)
    #renderer.make_movie(output_dir='/tmp/cc_movies/3d')

    f.close()
