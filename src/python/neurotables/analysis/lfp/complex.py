import numpy as np
from matplotlib.gridspec import GridSpec

import matplotlib.pyplot as plt
import matplotlib.cm as cm
from scipy.fftpack import fft,fftfreq,hilbert

import tables
from lasp.hht import HHT
from lasp.sound import plot_spectrogram
from lasp.timefreq import gaussian_stft

from neurotables.analysis.lfp.decomposition import get_sample_rate, get_bandpassed_lfp, get_protocol_spectrogram


def get_complex_lfp(f, enumber, start_time=None, end_time=None, site_id=3, protocol_name='Call3', low_freq=0.0, high_freq=30.0):



    lfp,sample_rate = get_bandpassed_lfp(f, enumber, start_time=start_time, end_time=end_time,
                                         site_id=site_id, protocol_name=protocol_name, low_freq=low_freq, high_freq=high_freq)
    z = hilbert(lfp)
    return z,sample_rate


if __name__ == '__main__':

    f = tables.open_file('/auto/k8/fdata/mschachter/tdt_continuous/GreBlu9508M/GreBlu9508M_tabled.h5', 'r')

    site_id = 3
    protocol_name = 'Call3'

    sample_rate = get_sample_rate(f, site_id=site_id, protocol_name=protocol_name)

    view_size = 10.0
    start = 975.0
    duration = 10.0

    start_times = np.arange(start, start+duration, view_size)
    end_times = start_times + view_size

    enumber = 19

    #get LFP
    low_freq = 0.0
    high_freq = 120.0
    lfp_whole,sample_rate = get_bandpassed_lfp(f, enumber, site_id=site_id, protocol_name=protocol_name, low_freq=low_freq, high_freq=high_freq)

    for start,end in zip(start_times, end_times):

        si = int(start * sample_rate)
        ei = int(end * sample_rate)

        lfp = lfp_whole[si:ei]

        #compute spectrogram
        spec_t,spec_freq,spec,spec_rms = gaussian_stft(lfp, sample_rate, window_length=0.350, increment=1.0/sample_rate)
        spec_t += start

        #get stimulus spectrogram
        stim_spec_t,stim_spec_freq,stim_spec = get_protocol_spectrogram(f, start, end, site_id=site_id, protocol_name=protocol_name)

        #compute HHT
        lfp_hht = HHT(lfp, sample_rate)

        print '# of IMFs: %d' % len(lfp_hht.imfs)

        freq_index = (spec_freq >= low_freq) & (spec_freq <= high_freq)

        t = (np.arange(ei-si) / sample_rate) + start

        #make plots
        max_imfs = 4
        gs = GridSpec(5 + 2*max_imfs, 1)

        fig = plt.figure()
        plt.suptitle('E%d' % enumber)
        plt.subplots_adjust(bottom=0.04, top=0.97, right=0.99, left=0.04, wspace=0.10)
        ax = plt.subplot(gs[:1, 0])
        plot_spectrogram(stim_spec_t, stim_spec_freq, stim_spec, ticks=False, colormap=cm.gist_yarg, colorbar=False, ax=ax)

        ax = plt.subplot(gs[1:3, 0])
        plt.plot(t, lfp, 'k-', linewidth=2.0)
        plt.axhline(0.0, c='k')
        plt.axis('tight')

        ax = plt.subplot(gs[3:5, 0])
        plot_spectrogram(spec_t, spec_freq[freq_index], np.abs(spec[freq_index, :]), colormap=cm.afmhot_r, ax=ax, colorbar=False)
        plt.axis('tight')

        nimfs = min(max_imfs, len(lfp_hht.imfs))
        for k in range(nimfs):
            sp = (2*k)+5
            ax = plt.subplot(gs[sp:(sp+2), 0])
            imf = lfp_hht.imfs[k]
            #print 'len(t)=%d, len(imf)=%d' % (len(t), len(imf.imf))
            plt.plot(t, lfp, 'k--', alpha=0.7, linewidth=2.0)
            plt.plot(t, imf.imf, 'b-', linewidth=2.0)
            plt.axis('tight')

        if nimfs > 0:

            for k in range(nimfs):

                imf = lfp_hht.imfs[k]
                imf_ps = np.abs(fft(imf.imf))
                imf_freq = fftfreq(len(imf.imf), d=1.0/sample_rate)
                findex = (imf_freq >= low_freq) & (imf_freq <= high_freq)
                imf_ps = imf_ps[findex]

                nz = imf_ps > 0.0
                log_ps = np.zeros([len(imf_ps)])
                log_ps[nz] = 20*np.log10(imf_ps[nz])

                plt.figure()
                plt.subplots_adjust(bottom=0.04, top=0.97, right=0.99, left=0.04, wspace=0.10)
                plt.suptitle('IMF %d' % k)

                nsp = 4
                sp = 1
                imf = lfp_hht.imfs[k]

                ax = plt.subplot(nsp, 1, sp)
                #print 'len(t)=%d, len(imf)=%d' % (len(t), len(imf.imf))
                plt.plot(t, lfp, 'k--', alpha=0.7, linewidth=2.0)
                plt.plot(t, imf.imf, 'k-', linewidth=2.0)
                plt.xlabel('Time (s)')
                plt.axis('tight')

                ax = plt.subplot(nsp, 1, sp+1)
                #print 'len(t)=%d, len(imf)=%d' % (len(t), len(imf.imf))
                plt.plot(t, imf.imf, 'k--', alpha=0.7, linewidth=2.0)
                plt.plot(t, imf.amplitude, 'b-', linewidth=2.0)
                plt.ylabel('Amplitude')
                plt.xlabel('Time (s)')
                plt.axis('tight')

                ax = plt.subplot(nsp, 1, sp+2)
                #print 'len(t)=%d, len(imf)=%d' % (len(t), len(imf.imf))
                plt.plot(t, imf.imf, 'k--', alpha=0.7, linewidth=2.0)
                plt.plot(t, imf.phase, 'r-', linewidth=2.0)
                plt.ylabel('Phase (radians)')
                plt.xlabel('Time (s)')
                plt.axis('tight')

                ax = plt.subplot(nsp, 1, sp+3)
                #print 'len(t)=%d, len(imf)=%d' % (len(t), len(imf.imf))
                plt.plot(imf_freq[findex], imf_ps, 'g-', linewidth=2.0)
                plt.xlabel('Frequency (Hz)')
                plt.ylabel('Power')
                plt.axis('tight')

    plt.show()

    f.close()

