import operator
from scipy.fftpack import hilbert

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from sklearn.decomposition import PCA,FastICA,DictionaryLearning,SparseCoder
from spams import trainDL

import tables
import time

from lasp.signal import bandpass_filter, lowpass_filter
from lasp.sound import plot_spectrogram
from lasp.timefreq import gaussian_stft,postprocess_spectrogram

from neurotables.objects.cells import Site


def get_bandpassed_lfp(f, electrode_number, start_time=None, end_time=None, site_id=1, protocol_name='Call1', low_freq=4.0, high_freq=30.0):

    #get site
    site = Site(f)
    assert site.from_id(site_id)

    #get protocol
    plist = [p for p in site.protocols if p.name == protocol_name]
    assert len(plist) == 1
    protocol = plist[0]

    #get electrode
    elist = [e for e in site.electrodes if e.number == electrode_number]
    assert len(elist) == 1
    electrode = elist[0]

    #get LFP
    lfp_pr = electrode.get_lfp_protocol_response(protocol.name)
    lfp = lfp_pr.waveform
    sample_rate = lfp_pr.lfp.sample_rate

    #get start and end indices
    if start_time is None:
        start_time = 0.0
    if end_time is None:
        end_time = len(lfp)/sample_rate
    si = int(start_time*sample_rate)
    ei = int(end_time*sample_rate)
    lfp = lfp[si:ei]

    #bandpass filter LFP
    if low_freq is not None or high_freq is not None:
        if low_freq is not None and low_freq > 0.0:
            lfp = bandpass_filter(lfp, sample_rate, low_freq, high_freq)
        else:
            lfp = lowpass_filter(lfp, sample_rate, high_freq)

    #z-score LFP
    lfp -= lfp.mean()
    lfp /= lfp.std(ddof=1)

    return lfp,sample_rate


def get_sample_rate(f, site_id=1, protocol_name='Call1'):
    site = Site(f)
    assert site.from_id(site_id)

    #get protocol
    plist = [p for p in site.protocols if p.name == protocol_name]
    assert len(plist) == 1
    protocol = plist[0]

    #get LFP
    lfp_pr = site.electrodes[0].get_lfp_protocol_response(protocol.name)
    sample_rate = lfp_pr.lfp.sample_rate
    return sample_rate


def get_protocol_spectrogram(f, start_time, end_time, site_id=1, protocol_name='Call1'):

    site = Site(f)
    assert site.from_id(site_id)

    #get protocol
    plist = [p for p in site.protocols if p.name == protocol_name]
    assert len(plist) == 1
    protocol = plist[0]

    t,freq,full_spec = protocol.compute_stimulus_spectrogram(start_time, end_time, threshold=True, spec_inc=0.001, window_length=0.007)

    return t,freq,full_spec


def get_lfp_pcs(f, site_id=1, protocol_name='Call1'):

    #get site
    site = Site(f)
    assert site.from_id(site_id)

    #get protocol
    plist = [p for p in site.protocols if p.name == protocol_name]
    assert len(plist) == 1
    protocol = plist[0]

    #get electrode
    elist = [e for e in site.electrodes if e.hemisphere]
    assert len(elist) == 32
    elist.sort(key=operator.attrgetter('number'))
    sample_rate = None

    #get coordinates
    electrode_geometry = site.electrodes[0].geometry
    coords = electrode_geometry.coordinates

    #get LFPs
    lfps = list()
    for electrode in elist:
        lfp_pr = electrode.get_lfp_protocol_response(protocol.name)
        lfp = lfp_pr.waveform
        sample_rate = lfp_pr.lfp.sample_rate
        lfps.append(lfp)

    lfps = np.array(lfps)
    pca = PCA()
    stime = time.time()
    pca.fit(lfps.T)
    etime = time.time() - stime
    print 'PCA fit elapsed time: %d seconds' % etime

    #reshape pcs to fit electrode array shape
    pcs = np.zeros([32, 8, 4])
    for k,pc in enumerate(pca.components_):
        for j in range(32):
            row,col = np.nonzero(coords == j+1)
            pcs[k, row, col] = pc[j]

    #project LFPs to PC space
    lfp_proj = pca.transform(lfps.T)
    return pcs,lfp_proj


def get_lfp_ics(f, site_id=1, protocol_name='Call1'):

    #get site
    site = Site(f)
    assert site.from_id(site_id)

    #get protocol
    plist = [p for p in site.protocols if p.name == protocol_name]
    assert len(plist) == 1
    protocol = plist[0]

    #get electrode
    elist = [e for e in site.electrodes]
    assert len(elist) == 32
    elist.sort(key=operator.attrgetter('number'))
    sample_rate = None

    #get coordinates
    electrode_geometry = site.electrodes[0].geometry
    coords = electrode_geometry.coordinates

    #get LFPs
    lfps = list()
    for electrode in elist:
        lfp_pr = electrode.get_lfp_protocol_response(protocol.name)
        lfp = lfp_pr.waveform
        sample_rate = lfp_pr.lfp.sample_rate
        lfps.append(lfp)

    lfps = np.array(lfps)
    ica = FastICA()
    stime = time.time()
    ica.fit(lfps.T)
    etime = time.time() - stime
    print 'ICA fit elapsed time: %d seconds' % etime

    #reshape pcs to fit electrode array shape
    ics = np.zeros([32, 8, 2])
    #for k,ic in enumerate(ica.unmixing_matrix_):
    for k in range(32):
        ic = ica.components_[:, k]
        for j in range(16):
            row,col = np.nonzero(coords == j+1)
            ics[k, row, col] = ic[j]

    #project LFPs to PC space
    lfp_proj = ica.transform(lfps.T)
    return ics,lfp_proj


def get_lfp_sparse_dict(f, site_id=1, protocol_name='Call1', num_components=100, sparsity=0.10):

    #get site
    site = Site(f)
    assert site.from_id(site_id)

    #get protocol
    plist = [p for p in site.protocols if p.name == protocol_name]
    assert len(plist) == 1
    protocol = plist[0]

    #get electrode
    elist = [e for e in site.electrodes]
    assert len(elist) == 32
    elist.sort(key=operator.attrgetter('number'))
    sample_rate = None

    #get coordinates
    electrode_geometry = site.electrodes[0].geometry
    coords = electrode_geometry.coordinates

    #get LFPs
    lfps = list()
    for electrode in elist:
        lfp,sample_rate = get_bandpassed_lfp(f, electrode.number,
                                           site_id=site_id, protocol_name=protocol_name,
                                           low_freq=4.0, high_freq=30.0)
        lfps.append(lfp)

    lfps = np.array(lfps)
    print 'lfps.shape=',lfps.shape

    alpha = 1.0
    transform_alpha = 1
    fit_algorithm = 'lars'
    transform_algorithm = 'omp'
    num_nonzero_coefs = int(sparsity*num_components)
    print 'num_nonzero_coefs=%d' % num_nonzero_coefs
    dict_learner = DictionaryLearning(n_components=ncomps,
                                      alpha=alpha, transform_alpha=transform_alpha,
                                      fit_algorithm=fit_algorithm,
                                      transform_algorithm=transform_algorithm,
                                      transform_n_nonzero_coefs=num_nonzero_coefs,
                                      n_jobs=1
                                     )

    stime = time.time()
    dict_learner.fit(lfps.T)
    etime = time.time() - stime
    print 'Elapsed time for dictionary learning: %d seconds' % etime

    print 'components_.shape=',dict_learner.components_.shape

    #reshape the dictionary components
    comps = np.zeros([num_components, 8, 4])
    for k in range(num_components):
        comp = dict_learner.components_[k, :].squeeze()
        for j in range(32):
            row,col = np.nonzero(coords == j+1)
            comps[k, row, col] = comp[j]

    #obtain a sparse decomposition of the LFPs
    scoder = SparseCoder(dictionary=dict_learner.components_, transform_algorithm=transform_algorithm,
                         transform_n_nonzero_coefs=num_nonzero_coefs,
                         transform_alpha=transform_alpha)
    stime = time.time()
    lfp_proj = scoder.transform(lfps.T)
    etime = time.time() - stime
    print 'Elapsed time for sparse coding: %d seconds' % etime

    return comps,lfp_proj


if __name__ == '__main__':

    f = tables.open_file('/auto/k8/fdata/mschachter/tdt_continuous/GreBlu9508M/GreBlu9508M_tabled.h5', 'r')

    site_id = 3
    protocol_name = 'Call3'

    sample_rate = get_sample_rate(f, site_id=site_id, protocol_name=protocol_name)
    start = 850.0
    end = 890.0
    si = int(start * sample_rate)
    ei = int(end * sample_rate)

    stim_spec_t,stim_spec_freq,stim_spec = get_protocol_spectrogram(f, start, end, site_id=site_id, protocol_name=protocol_name)

    ncomps = 32
    nrows = 4
    ncols = 8

    for proj_type in ['SPARSE']:

        if proj_type == 'PCA':
            comps,lfp_proj = get_lfp_pcs(f, site_id=site_id, protocol_name=protocol_name)
        elif proj_type == 'ICA':
            comps,lfp_proj = get_lfp_ics(f, site_id=site_id, protocol_name=protocol_name)
        elif proj_type == 'SPARSE':
            ncomps = 200
            nrows = 10
            ncols = 20
            sparsity = 0.10
            comps,lfp_proj = get_lfp_sparse_dict(f, site_id=site_id, protocol_name=protocol_name,
                                                 num_components=ncomps, sparsity=sparsity)

        absmax = np.abs(comps).max()
        #normalize each projection by component
        for k in range(ncomps):
            kmax = np.abs(lfp_proj[:, k]).max()
            if kmax > 0.0:
                lfp_proj[:, k] /= kmax
            print 'k=%d, max=%0.6f' % (k, kmax)
        proj_absmax = np.abs(lfp_proj[si:ei, :]).max()

        #plot components
        plt.figure()
        plt.suptitle('%s Comps' % proj_type)
        for k in range(ncomps):
            pc = comps[k, :, :].squeeze()
            ax = plt.subplot(nrows, ncols, k+1)
            plt.imshow(pc, interpolation='nearest', aspect='auto', vmin=-absmax, vmax=absmax, cmap=cm.seismic)
            plt.axvline(1.5, c='k')
            plt.axis('tight')
            plt.xticks([])
            plt.yticks([])

        #plot projection
        plt.figure()
        plt.suptitle('%s Proj' % proj_type)
        ax = plt.subplot(2, 1, 1)
        plot_spectrogram(stim_spec_t, stim_spec_freq, stim_spec, ticks=False, colormap=cm.gist_yarg, colorbar=True, ax=ax)

        ax = plt.subplot(2, 1, 2)
        im = plt.imshow(lfp_proj.T[:, si:ei], interpolation='nearest', aspect='auto', vmin=-proj_absmax, vmax=proj_absmax, cmap=cm.seismic,
                        extent=[stim_spec_t.min(), stim_spec_t.max(), 0, ncomps], origin='upper')
        plt.colorbar(im)
        plt.axis('tight')
        plt.xticks([])
        plt.yticks([])

    """
    e1 = 22
    e2 = 19

    low_freq = 4.0
    high_freq = 30.0

    lfp1,sample_rate = get_low_freq_lfp(f, e1, site_id=site_id, protocol_name=protocol_name, low_freq=low_freq, high_freq=high_freq)
    lfp2,sample_rate = get_low_freq_lfp(f, e2, site_id=site_id, protocol_name=protocol_name, low_freq=low_freq, high_freq=high_freq)

    t = np.arange(len(lfp1)) / sample_rate

    spec_t1,spec_freq1,spec1,spec_rms1 = gaussian_stft(lfp1[si:ei], sample_rate, window_length=0.350, increment=1.0/sample_rate)
    spec_t2,spec_freq2,spec2,spec_rms2 = gaussian_stft(lfp2[si:ei], sample_rate, window_length=0.350, increment=1.0/sample_rate)

    phase_diff = np.angle(spec1) - np.angle(spec2)
    phase_diff_mean = phase_diff.mean(axis=1)
    phase_diff_std = phase_diff.std(axis=1, ddof=1)

    freq_index = (spec_freq1 >= low_freq) & (spec_freq1 <= high_freq)
    print 'frequencies=',spec_freq1[freq_index]

    plt.figure()

    ax = plt.subplot(4, 1, 1)
    plot_spectrogram(stim_spec_t, stim_spec_freq, stim_spec, ticks=False, colormap=cm.gist_yarg, colorbar=False, ax=ax)

    #plt.subplot(5, 1, 2)
    #plt.plot(t[si:ei], lfp1[si:ei], 'k-', linewidth=2.0)
    #plt.plot(t[si:ei], lfp2[si:ei], 'b-', linewidth=2.0, alpha=0.7)
    #plt.axis('tight')
    #plt.legend(['e%d' % e1, 'e%d' % e2])

    ax = plt.subplot(4, 1, 2)
    plot_spectrogram(spec_t1, spec_freq1[freq_index], np.abs(spec1[freq_index, :]), colormap=cm.afmhot_r, ax=ax, colorbar=False)
    plt.title('e%d' % e1)
    plt.xlabel('')

    ax = plt.subplot(4, 1, 3)
    plot_spectrogram(spec_t2, spec_freq2[freq_index], np.abs(spec2[freq_index, :]), colormap=cm.afmhot_r, ax=ax, colorbar=False)
    plt.title('e%d' % e2)
    plt.xlabel('')

    ax = plt.subplot(4, 1, 4)
    #plt.errorbar(spec_freq2[freq_index], phase_diff_mean[freq_index], yerr=phase_diff_std[freq_index], ecolor='#aaaaaa', capsize=0, color='k', linewidth=3, alpha=0.75)
    plot_spectrogram(spec_t2, spec_freq2[freq_index], phase_diff[freq_index, :], colormap=cm.afmhot_r, ax=ax, colorbar=False)
    plt.title('Phase Difference')
    plt.xlabel('')

    plt.show()
    """

    f.close()
