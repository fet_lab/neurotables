import tables

import numpy as np

import matplotlib.pyplot as plt
from matplotlib import cm

from lasp.plots import multi_plot
from lasp.sound import plot_spectrogram

from neurotables.analysis.lfp.fit_trial_envelope_models import get_stim_representations
from neurotables.objects.cells import Sound


def plot_all_stimuli(f):

    s = Sound(f)
    sounds = {sound.id:sound for sound in s.get_all()}

    #break stimuli into types
    stims_by_type = dict()
    stims_by_type['noise'] = [s.id for s in sounds.values() if s.stim_type == 'mlnoise']
    stims_by_type['song'] = [s.id for s in sounds.values() if s.stim_type == 'song']

    for cid in ['Ag', 'Be', 'DC', 'Di', 'LT', 'Ne', 'Te', 'Th']:
        stims_by_type[cid] = [s.id for s in sounds.values() if str(s.call_id) == cid]

    #compute spectrogram of each sound
    print 'Computing spectrograms...'
    sr = 1e3 #sample rate of 1kHz
    stim_specs,stim_t,spec_freq,sample_rate = get_stim_representations(f, stim_ids=[sid for sid in sounds.keys()], gain_tau=0.0,
                                                                       type='spectrogram', sample_rate=sr)

    #make plots by stimulus type
    print 'Making plots'
    for stype,stim_ids in stims_by_type.iteritems():
        plist = [{'id':sid, 'spec':stim_specs[sid], 'spec_t':stim_t[sid], 'spec_freq':spec_freq} for sid in stim_ids]
        multi_plot(plist, plot_stim_spec, title=stype, nrows=5, ncols=4, figsize=(20,12), output_pattern='/tmp/sounds_%s_p%%d.png' % stype)


def plot_stim_spec(pdata, ax):
    len = np.max(pdata['spec_t'])
    plot_spectrogram(pdata['spec_t'], pdata['spec_freq'], pdata['spec'], ticks=False, ax=ax, fmin=300.0, fmax=8000.0,
                     colorbar=False, colormap=cm.gist_yarg)
    plt.title(str(pdata['id']) + ('%dms' % int(len)))


if __name__ == '__main__':

    f = tables.open_file('/auto/k8/fdata/mschachter/tdt_continuous/GreBlu9508M/GreBlu9508M_tabled.h5', 'r')

    plot_all_stimuli(f)
    plt.show()

    f.close()











