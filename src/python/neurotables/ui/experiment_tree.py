import operator

from PyQt4.QtCore import *

from neurotables.objects.cells import Experiment


class ExperimentTreeItem(object):
    def __init__(self, name, parent=None):
        self.name = name
        self.parentItem = parent
        self.children = list()

    def appendChild(self, item):
        self.children.append(item)

    def child(self, row):
        return self.children[row]

    def childCount(self):
        return len(self.children)

    def columnCount(self):
        return 1

    def data(self, column):
        return self.name

    def parent(self):
        return self.parentItem

    def row(self):
        if self.parentItem:
            return self.parentItem.children.index(self)

        return 0


class ExperimentItem(ExperimentTreeItem):
    def __init__(self, experiment, parent=None):
        ExperimentTreeItem.__init__(self, experiment.name, parent)
        self.experiment = experiment

        #iterate through sites and append them as children
        for s in self.experiment.sites:
            si = SiteItem(s, self)
            self.appendChild(si)

        self.children.sort(key=operator.attrgetter('name'))


class SiteItem(ExperimentTreeItem):
    def __init__(self, site, parent=None):
        ExperimentTreeItem.__init__(self, '%s' % site.name, parent)
        self.site = site

        #iterate through the protocols and append them as children
        for p in self.site.protocols:
            pi = ProtocolItem(self.site, p, self)
            self.appendChild(pi)

        self.children.sort(key=operator.attrgetter('start_time'))


class ProtocolItem(ExperimentTreeItem):

    def __init__(self, site, protocol, parent=None):
        ExperimentTreeItem.__init__(self, '%s' % protocol.name, parent)
        self.site = site
        pname = protocol.name
        self.protocol = protocol
        self.start_time = self.protocol.start_time

        #iterate through the electrodes and append them as children
        for e in self.site.electrodes:
            ei = ElectrodeItem(e, self)
            self.appendChild(ei)

        self.children.sort(key=operator.attrgetter('number'))


class ElectrodeItem(ExperimentTreeItem):
    def __init__(self, electrode, parent=None):
        ExperimentTreeItem.__init__(self, 'Electrode %d' % electrode.number, parent)
        self.electrode = electrode
        self.number = electrode.number

        for c in self.electrode.cells:
            ci = CellItem(c, self)
            self.appendChild(ci)


class CellItem(ExperimentTreeItem):
    def __init__(self, cell, parent=None):
        ExperimentTreeItem.__init__(self, '%s' % cell.name, parent)
        self.cell = cell


class ResponseSetItem(ExperimentTreeItem):
    def __init__(self, cell, stim_class, parent=None):
        ExperimentTreeItem.__init__(self, '%s' % stim_class, parent)
        self.cell = cell
        self.stim_class = stim_class


class ExperimentTreeModel(QAbstractItemModel):
    def __init__(self, parent=None, lib_file_handle=None):
        QAbstractItemModel.__init__(self, parent)

        self.f = lib_file_handle

        self.rootItem = ExperimentTreeItem('The Experiment Tree')
        self.setupModelData(self.rootItem)

    def columnCount(self, parent):

        """
        if parent.isValid():
            return parent.internalPointer().columnCount()
        else:
            return self.rootItem.columnCount()
        """
        return 1

    def data(self, index, role):
        if not index.isValid():
            return None

        if role != Qt.DisplayRole:
            return None

        item = index.internalPointer()

        return item.data(index.column())

    def headerData(self, section, orientation, role):
        if orientation == Qt.Horizontal and role == Qt.DisplayRole:
            return 'The Experiment Tree'

        return None

    def index(self, row, column, parent):
        if not self.hasIndex(row, column, parent):
            return QModelIndex()

        if not parent.isValid():
            parentItem = self.rootItem
        else:
            parentItem = parent.internalPointer()

        childItem = parentItem.child(row)
        if childItem:
            return self.createIndex(row, column, childItem)
        else:
            return QModelIndex()

    def parent(self, index):
        if not index.isValid():
            return QModelIndex()

        childItem = index.internalPointer()
        parentItem = childItem.parent()

        if parentItem == self.rootItem:
            return QModelIndex()

        return self.createIndex(parentItem.row(), 0, parentItem)

    def rowCount(self, parent):
        if parent.column() > 0:
            return 0

        if not parent.isValid():
            parentItem = self.rootItem
        else:
            parentItem = parent.internalPointer()

        return parentItem.childCount()

    def setupModelData(self, parent):

        e = Experiment(self.f)
        elist = e.get_all()
        elist.sort(key=operator.attrgetter('name'))

        for experiment in elist:
            ei = ExperimentItem(experiment, parent)
            parent.appendChild(ei)
