import os

def get_root_dir():
    fpath = os.path.realpath(__file__)
    (cwdir, fname) = os.path.split(fpath)
    rdir = os.path.realpath(os.path.join(cwdir, '..', '..', '..', '..'))
    return rdir


def get_src_dir():
    return os.path.join(get_root_dir(), 'src')


def get_cache_dir():
    cdir = os.path.join(get_root_dir(), 'cache', 'images')
    if not os.path.exists(cdir):
        os.mkdir(os.path.join(get_root_dir(), 'cache'))
        os.mkdir(cdir)
    return cdir

SRC_DIR = get_src_dir()
ROOT_DIR = get_root_dir()


class Config(object):

    def __init__(self):
        self.properties = dict()


