import os
import logging
import cPickle
from PyQt4.QtCore import QSignalMapper

import tables

from PyQt4.QtGui import QFileDialog,QAction

from neurotables.ui.config import SRC_DIR,get_cache_dir,Config,get_root_dir

from neurotables.ui.experiment_tree import ExperimentTreeModel
from neurotables.ui.image_cache import ImageCache
from neurotables.ui.protocol_panel import ProtocolPanel

experiment_item_content_mappings = {'ProtocolItem':ProtocolPanel}


class AppController(object):

    def __init__(self):
        self.app = None
        self.current_file = None
        self.main_window = None

        logger = logging.getLogger()
        logger.setLevel(logging.DEBUG)
        console_handler = logging.StreamHandler()
        logger.addHandler(console_handler)
        self.logger = logger

        self.image_cache = ImageCache()
        self.load_config()

    def load_config(self):
        self.config_file_path = os.path.join(get_root_dir(), 'config.pickle')
        self.config = Config()
        if os.path.exists(self.config_file_path):
            cf = open(self.config_file_path, 'r')
            self.config.properties = cPickle.load(cf)
            cf.close()

    def load_recent_files(self):
        file_number = 0
        self.mapper = QSignalMapper(self.main_window)
        if 'recent_files' in self.config.properties:
            for rfile in self.config.properties['recent_files']:
                if os.path.exists(rfile):
                    action = QAction(self.main_window)
                    froot,fname = os.path.split(rfile)
                    action.setText(fname)
                    self.mapper.setMapping(action, file_number)
                    file_number += 1
                    action.triggered.connect(self.mapper.map)
                    self.main_window.menuRecentFiles.addAction(action)

            self.mapper.mapped.connect(self.recent_file_selected)

    def recent_file_selected(self, file_index):
        fname = self.config.properties['recent_files'][file_index]
        self.load_experiment_file(fname)

    def save_config(self):
        cf = open(self.config_file_path, 'w')
        cPickle.dump(self.config.properties, cf)
        cf.close()

    def get_qimage(self, image_id):
        return self.image_cache.get_qimage(image_id)

    def store_qimage(self, image_id, qimage):
        return self.image_cache.store_qimage(image_id, qimage)

    def load_experiment_file(self, file_name):
        f = tables.open_file(file_name, mode='a')
        self.logger.debug('Successfully opened file %s' % file_name)

        if self.current_file is not None:
            self.current_file.close()

        self.current_file = f
        self.reload()

        if 'recent_files' not in self.config.properties:
            self.config.properties['recent_files'] = list()
        if file_name not in self.config.properties['recent_files']:
            self.config.properties['recent_files'].append(file_name)
        self.save_config()

    def reload(self):

        etreeModel = ExperimentTreeModel(lib_file_handle=self.current_file)
        self.main_window.experimentTreeView.setExpandsOnDoubleClick(False)
        self.main_window.experimentTreeView.doubleClicked.connect(self.experiment_tree_double_clicked)
        self.main_window.experimentTreeView.setModel(etreeModel)
        #self.main_window.experimentTreeView.expandToDepth(4)

    def file_open(self):
        file_name = QFileDialog.getOpenFileName(self.main_window, caption='Open Cell Repository', filter='HDF5 Files (*.h5)')
        if file_name is not None:
            self.logger.debug('Opening filename %s' % file_name)
            self.load_experiment_file(str(file_name))

    def tab_close(self, index):
        cpane = self.main_window.tabWidget.widget(index)
        cpane.close()
        self.main_window.tabWidget.removeTab(index)

    def experiment_tree_double_clicked(self, index):
        view_item = index.internalPointer()
        #print 'treeDoubleClicked: index.column=%d, index.row=%d, data class=%s' % \
        #      (index.column(), index.row(), view_item.__class__.__name__)

        #create tab
        if view_item.__class__.__name__ in experiment_item_content_mappings:
            cp_class = experiment_item_content_mappings[view_item.__class__.__name__]
            cp_inst = cp_class(self, view_item, parent=self.main_window.tabWidget)
            self.main_window.tabWidget.addTab(cp_inst, cp_inst.name)
