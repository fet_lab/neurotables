import sys

from PyQt4.QtGui import QApplication, QMainWindow

from neurotables.ui.controller import *

from neurotables.ui.generated.main_window import *


class MainWindow(QMainWindow, Ui_MainWindow):

    def __init__(self, controller, parent=None):
        QMainWindow.__init__(self, parent)
        self.setupUi(self)

        self.controller = controller
        self.controller.main_window = self
        self.controller.load_recent_files()

        self.splitter.setSizes([200, 600])
        self.splitter.setStretchFactor(0, 1)
        self.splitter.setStretchFactor(1, 3)

        self.tabWidget.tabCloseRequested.connect(self.controller.tab_close)
        while self.tabWidget.count() > 0:
            self.tabWidget.removeTab(0)
        self.tabWidget.setTabsClosable(True)

        self.actionOpen.triggered.connect(self.controller.file_open)


def main():
    print 'Starting neurotables UI...'
    app = QApplication([])

    app_data = AppController()
    app_data.app = app

    main_window = MainWindow(app_data)
    main_window.show()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()

