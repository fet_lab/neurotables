# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'protocol_panel.ui'
#
# Created: Fri Sep 27 11:09:34 2013
#      by: PyQt4 UI code generator 4.9.3
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

class Ui_sitePanel(object):
    def setupUi(self, sitePanel):
        sitePanel.setObjectName(_fromUtf8("sitePanel"))
        sitePanel.resize(717, 557)
        self.gridLayout = QtGui.QGridLayout(sitePanel)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.groupBox_2 = QtGui.QGroupBox(sitePanel)
        self.groupBox_2.setStyleSheet(_fromUtf8("QGroupBox { \n"
"     border: 2px solid gray; \n"
"     border-radius: 3px; \n"
"    font-weight: bold;\n"
" } "))
        self.groupBox_2.setObjectName(_fromUtf8("groupBox_2"))
        self.displayLFPCheckBox = QtGui.QCheckBox(self.groupBox_2)
        self.displayLFPCheckBox.setGeometry(QtCore.QRect(30, 30, 97, 22))
        self.displayLFPCheckBox.setChecked(True)
        self.displayLFPCheckBox.setObjectName(_fromUtf8("displayLFPCheckBox"))
        self.displaySpikesCheckBox = QtGui.QCheckBox(self.groupBox_2)
        self.displaySpikesCheckBox.setGeometry(QtCore.QRect(30, 50, 97, 22))
        self.displaySpikesCheckBox.setChecked(True)
        self.displaySpikesCheckBox.setObjectName(_fromUtf8("displaySpikesCheckBox"))
        self.displayButton = QtGui.QPushButton(self.groupBox_2)
        self.displayButton.setGeometry(QtCore.QRect(410, 40, 98, 27))
        self.displayButton.setObjectName(_fromUtf8("displayButton"))
        self.gridLayout.addWidget(self.groupBox_2, 2, 0, 1, 2)
        self.groupBox = QtGui.QGroupBox(sitePanel)
        self.groupBox.setStyleSheet(_fromUtf8("QGroupBox { \n"
"     border: 2px solid gray; \n"
"     border-radius: 3px; \n"
"    font-weight: bold;\n"
" } "))
        self.groupBox.setObjectName(_fromUtf8("groupBox"))
        self.singleUnitCheckBox = QtGui.QCheckBox(self.groupBox)
        self.singleUnitCheckBox.setGeometry(QtCore.QRect(20, 30, 121, 22))
        self.singleUnitCheckBox.setChecked(True)
        self.singleUnitCheckBox.setObjectName(_fromUtf8("singleUnitCheckBox"))
        self.multiUnitCheckBox = QtGui.QCheckBox(self.groupBox)
        self.multiUnitCheckBox.setGeometry(QtCore.QRect(20, 50, 121, 22))
        self.multiUnitCheckBox.setChecked(False)
        self.multiUnitCheckBox.setObjectName(_fromUtf8("multiUnitCheckBox"))
        self.label_2 = QtGui.QLabel(self.groupBox)
        self.label_2.setGeometry(QtCore.QRect(20, 120, 66, 17))
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.lineEdit = QtGui.QLineEdit(self.groupBox)
        self.lineEdit.setGeometry(QtCore.QRect(80, 120, 113, 27))
        self.lineEdit.setObjectName(_fromUtf8("lineEdit"))
        self.noiseUnitCheckBox = QtGui.QCheckBox(self.groupBox)
        self.noiseUnitCheckBox.setGeometry(QtCore.QRect(20, 70, 121, 22))
        self.noiseUnitCheckBox.setChecked(False)
        self.noiseUnitCheckBox.setObjectName(_fromUtf8("noiseUnitCheckBox"))
        self.unsortedUnitCheckBox = QtGui.QCheckBox(self.groupBox)
        self.unsortedUnitCheckBox.setGeometry(QtCore.QRect(20, 90, 121, 22))
        self.unsortedUnitCheckBox.setChecked(False)
        self.unsortedUnitCheckBox.setObjectName(_fromUtf8("unsortedUnitCheckBox"))
        self.gridLayout.addWidget(self.groupBox, 1, 1, 1, 1)
        self.electrodesGroupBox = QtGui.QGroupBox(sitePanel)
        self.electrodesGroupBox.setStyleSheet(_fromUtf8("QGroupBox { \n"
"     border: 2px solid gray; \n"
"     border-radius: 3px; \n"
"    font-weight: bold;\n"
" } "))
        self.electrodesGroupBox.setObjectName(_fromUtf8("electrodesGroupBox"))
        self.electrodesAllRadioButton = QtGui.QRadioButton(self.electrodesGroupBox)
        self.electrodesAllRadioButton.setGeometry(QtCore.QRect(20, 30, 116, 22))
        self.electrodesAllRadioButton.setChecked(True)
        self.electrodesAllRadioButton.setObjectName(_fromUtf8("electrodesAllRadioButton"))
        self.electrodesRangeRadioButton = QtGui.QRadioButton(self.electrodesGroupBox)
        self.electrodesRangeRadioButton.setGeometry(QtCore.QRect(20, 50, 81, 22))
        self.electrodesRangeRadioButton.setObjectName(_fromUtf8("electrodesRangeRadioButton"))
        self.electrodesRangeLineEdit = QtGui.QLineEdit(self.electrodesGroupBox)
        self.electrodesRangeLineEdit.setGeometry(QtCore.QRect(100, 50, 113, 27))
        self.electrodesRangeLineEdit.setObjectName(_fromUtf8("electrodesRangeLineEdit"))
        self.label = QtGui.QLabel(self.electrodesGroupBox)
        self.label.setGeometry(QtCore.QRect(20, 90, 91, 17))
        self.label.setObjectName(_fromUtf8("label"))
        self.hemispheresRightCheckBox = QtGui.QCheckBox(self.electrodesGroupBox)
        self.hemispheresRightCheckBox.setGeometry(QtCore.QRect(120, 90, 97, 22))
        self.hemispheresRightCheckBox.setChecked(True)
        self.hemispheresRightCheckBox.setObjectName(_fromUtf8("hemispheresRightCheckBox"))
        self.hemispheresLeftCheckBox = QtGui.QCheckBox(self.electrodesGroupBox)
        self.hemispheresLeftCheckBox.setGeometry(QtCore.QRect(120, 110, 97, 22))
        self.hemispheresLeftCheckBox.setChecked(True)
        self.hemispheresLeftCheckBox.setObjectName(_fromUtf8("hemispheresLeftCheckBox"))
        self.gridLayout.addWidget(self.electrodesGroupBox, 1, 0, 1, 1)
        self.placeholderWidget = QtGui.QWidget(sitePanel)
        self.placeholderWidget.setObjectName(_fromUtf8("placeholderWidget"))
        self.gridLayout.addWidget(self.placeholderWidget, 0, 0, 1, 2)

        self.retranslateUi(sitePanel)
        QtCore.QMetaObject.connectSlotsByName(sitePanel)

    def retranslateUi(self, sitePanel):
        sitePanel.setWindowTitle(QtGui.QApplication.translate("sitePanel", "Form", None, QtGui.QApplication.UnicodeUTF8))
        self.groupBox_2.setTitle(QtGui.QApplication.translate("sitePanel", "Select Display Options:", None, QtGui.QApplication.UnicodeUTF8))
        self.displayLFPCheckBox.setText(QtGui.QApplication.translate("sitePanel", "LFP", None, QtGui.QApplication.UnicodeUTF8))
        self.displaySpikesCheckBox.setText(QtGui.QApplication.translate("sitePanel", "Spikes", None, QtGui.QApplication.UnicodeUTF8))
        self.displayButton.setText(QtGui.QApplication.translate("sitePanel", "Display!", None, QtGui.QApplication.UnicodeUTF8))
        self.groupBox.setTitle(QtGui.QApplication.translate("sitePanel", "Select Cells by Sort Type and Region:", None, QtGui.QApplication.UnicodeUTF8))
        self.singleUnitCheckBox.setText(QtGui.QApplication.translate("sitePanel", "Single Unit", None, QtGui.QApplication.UnicodeUTF8))
        self.multiUnitCheckBox.setText(QtGui.QApplication.translate("sitePanel", "Multi Unit", None, QtGui.QApplication.UnicodeUTF8))
        self.label_2.setText(QtGui.QApplication.translate("sitePanel", "Regions:", None, QtGui.QApplication.UnicodeUTF8))
        self.noiseUnitCheckBox.setText(QtGui.QApplication.translate("sitePanel", "Noise", None, QtGui.QApplication.UnicodeUTF8))
        self.unsortedUnitCheckBox.setText(QtGui.QApplication.translate("sitePanel", "Unsorted", None, QtGui.QApplication.UnicodeUTF8))
        self.electrodesGroupBox.setTitle(QtGui.QApplication.translate("sitePanel", "Select Electrodes by Number and Hemisphere:", None, QtGui.QApplication.UnicodeUTF8))
        self.electrodesAllRadioButton.setText(QtGui.QApplication.translate("sitePanel", "All", None, QtGui.QApplication.UnicodeUTF8))
        self.electrodesRangeRadioButton.setText(QtGui.QApplication.translate("sitePanel", "Range:", None, QtGui.QApplication.UnicodeUTF8))
        self.label.setText(QtGui.QApplication.translate("sitePanel", "Hemispheres:", None, QtGui.QApplication.UnicodeUTF8))
        self.hemispheresRightCheckBox.setText(QtGui.QApplication.translate("sitePanel", "Right", None, QtGui.QApplication.UnicodeUTF8))
        self.hemispheresLeftCheckBox.setText(QtGui.QApplication.translate("sitePanel", "Left", None, QtGui.QApplication.UnicodeUTF8))

