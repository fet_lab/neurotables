import operator

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

from PyQt4.QtGui import QWidget,QScrollArea, QImage
import time
from lasp.spikes import plot_raster
from neurotables.objects.cells import Sound,LFP,Cell,ElectrodeGeometry
from neurotables.objects.site_filter import SiteFilter
from neurotables.ui.content import ContentPanel

from neurotables.ui.events_widget import EventsWidget
from neurotables.ui.generated.protocol_panel import Ui_sitePanel
from neurotables.ui.image_cache import get_qimage_from_raw_data
from neurotables.ui.mpl_figure import MplFigure


class ProtocolPanel(ContentPanel,Ui_sitePanel):

    def __init__(self, controller, item, parent=None):

        QWidget.__init__(self, parent)
        Ui_sitePanel.__init__(self)

        self.setupUi(self)
        self.eventsWidgetHeight = 150.0
        self.eventsWidgetResolution = 150.0
        self.protocol = item.protocol
        self.site = item.site
        self.controller = controller
        self.eventsWidget = self.createEventsWidget()
        self.name = '%s-%s' % (self.site.name, self.protocol.name)

        #wire up the button
        self.displayButton.clicked.connect(self.searchAndDisplay)

        #remove placeholder widget from site panel
        ph = self.gridLayout.itemAtPosition(0, 0)
        self.gridLayout.removeItem(ph)
        del ph
    
        #create a scroll area to contain events widget
        widgetHeight = self.eventsWidget.thumbnailHeight+20
        scrollArea = QScrollArea()
        scrollArea.setFixedHeight(widgetHeight)
        scrollArea.setWidget(self.eventsWidget)

        #update the grid layout with the events widget
        self.gridLayout.addWidget(scrollArea, 0, 0, 1, 2)
        self.gridLayout.setRowStretch(0, 0.5)
        self.gridLayout.setRowStretch(1, 1.0)
        self.gridLayout.setRowStretch(2, 1.0)
        self.gridLayout.update()

    def createEventsWidget(self):
        """
            Create an EventsWidget object from list of sound events that occur in the protocol.
        """

        #get the list of events
        event_list = [e for e in self.protocol.events]
        event_list.sort(key=operator.attrgetter('start_time'))

        stime = time.time()
        #construct a QImage object for each sound
        sound_ids = np.unique([e.sound.id for e in event_list])
        sound_descriptions = dict()
        print '# of sound images to construct: %d' % len(sound_ids)
        img_by_sound = dict()
        for sid in sound_ids:

            #construct the sound and the description
            sound = Sound(self.protocol.f)
            sound.from_id(sid)
            desc = 'Type: %s #%d\nSubtype: %s\nSource: %s\n' % (sound.stim_type, sound.stim_number, sound.stim_source, sound.call_id)

            if hasattr(sound, "description"):
                if sound.description is not None:
                    desc += sound.description
            sound_descriptions[sid] = desc

            # check the image cache to see if the image is available
            image_id = 'Sound_%d.png' % sid
            if self.controller is not None:
                img = self.controller.get_qimage(image_id)
                if img is not None:
                    img_by_sound[sid] = img
                    continue

            #if nothing was found in the cache, create a qimage from the spectrogram of the sound
            img = self.getQImageFromSound(sound)
            img_by_sound[sound.id] = img

            #store the qimage in the cache for future use
            if self.controller is not None:
                self.controller.store_qimage(image_id, img)

        etime = time.time() - stime
        print 'Elapsed time to create images: %0.2fs' % etime

        #construct the list of events used in construction of the EventsWidget
        events = list()
        for e in event_list:
            eprops = dict()
            eprops['id'] = e.id
            eprops['start'] = e.start_time
            eprops['end'] = e.end_time
            eprops['image'] = img_by_sound[e.sound_id]
            eprops['bgcolor'] = '#ccc'
            eprops['description'] = sound_descriptions[e.sound.id]
            events.append(eprops)

        duration = np.maximum(self.protocol.end_time - self.protocol.start_time, self.protocol.events[-1].end_time)
        eventsWidget = EventsWidget(events, duration, thumbnailHeight=self.eventsWidgetHeight, resolution=self.eventsWidgetResolution, backgroundColor='#fff')
        return eventsWidget

    def getQImageFromSound(self,sound):
        #get image size and data
        imgWidth = sound.duration*self.eventsWidgetResolution
        imgHeight = self.eventsWidgetHeight
        imgData = np.flipud(sound.spectrogram.data)
        #rectify spectrogram futher
        imgData -= 50
        imgData[imgData < 0.0] = 0.0
        img = get_qimage_from_raw_data(imgData,imgWidth,imgHeight)
        return img

    def searchAndDisplay(self):

        #get electrode range
        criteria = dict()
        criteria['electrodes'] = 'ALL'
        if self.electrodesRangeRadioButton.isChecked():
            criteria['electrodes'] = self.electrodesRangeLineEdit.text()

        #get hemispheres
        criteria['hemisphere'] = list()
        if self.hemispheresRightCheckBox.isChecked():
            criteria['hemisphere'].append('R')
        if self.hemispheresLeftCheckBox.isChecked():
            criteria['hemisphere'].append('L')

        #get cell sort types
        criteria['sort'] = list()
        if self.singleUnitCheckBox.isChecked():
            criteria['sort'].append('single')
        if self.multiUnitCheckBox.isChecked():
            criteria['sort'].append('multi')
        if self.noiseUnitCheckBox.isChecked():
            criteria['sort'].append('noise')
        if self.unsortedUnitCheckBox.isChecked():
            criteria['sort'].append('unsorted')

        #get objects to display
        objects = list()
        if self.displayLFPCheckBox.isChecked():
            objects.append(LFP)
        if self.displaySpikesCheckBox.isChecked():
            objects.append(Cell)

        #do search
        sf = SiteFilter(self.site, self.protocol)
        found_objects = sf.find(objects, criteria)

        #create figure
        mfigs = self.createLfpAndCellFigures(found_objects, self.eventsWidget.selection[0], self.eventsWidget.selection[1])
        for hemi,mfig in mfigs.iteritems():
            mfig.popup_clicked()

    def createLfpAndCellFigures(self, objects, start_time, end_time):

        lfps = list()
        if 'LFP' in objects:
            lfps = objects['LFP']
        cells = list()
        if 'Cell' in objects:
            cells = objects['Cell']

        #one figure for each hemisphere
        #figs = {'L':MplFigure(self.controller, parent=self), 'R':MplFigure(self.controller, parent=self)}
        figs = dict()
        fig_titles = {'L':'Left Hemisphere', 'R':'Right Hemisphere'}
        duration = end_time - start_time

        #create a stimulus image
        selection_qimg = self.eventsWidget.get_selection_image()
        selection_qimg.save('/tmp/events_widget.png')
        stim_img = qimage2np(selection_qimg)

        if len(lfps) > 0:
            #separate lfps by hemisphere
            lfps_by_hemisphere = {'R':list(), 'L':list()}
            for lfp in lfps:
                lfps_by_hemisphere[lfp.lfp.electrode.hemisphere].append(lfp)

            for hemi in ['L', 'R']:
                if len(lfps_by_hemisphere[hemi]) == 0:
                    continue

                figs[hemi] = MplFigure(self.controller, parent=self)

                gs = gridspec.GridSpec(100, 2)

                #make LFP figure
                lfp_list = [(lfp,lfp.lfp.electrode.number) for lfp in lfps_by_hemisphere[hemi]]
                lfp_list.sort(key=operator.itemgetter(-1))
                mfig = figs[hemi]

                geom = lfps_by_hemisphere[hemi][0].lfp.electrode.geometry  # assume geometry is same object for all electrodes in the hemisphere
                coords = geom.coordinates

                sp_lfp_height = 6  # percent of vertical space a single LFP figure takes up
                sp_lfp_spacing = 1  # vertical space between LFPs
                sp_spec_height = 12  # percent of vertical space a the spectrogram plot takes up
                sp_spacing = 2

                sp_offset = 0

                stim_extent = (start_time, end_time, 375.0, 8000.0)
                gs_index = gs[sp_offset:(sp_offset+sp_spec_height), 0]
                ax = mfig.figure.add_subplot(gs_index)
                axim = ax.imshow(stim_img, aspect='auto', interpolation='none', extent=stim_extent)
                gs_index = gs[sp_offset:(sp_offset+sp_spec_height), 1]
                ax.set_xticks([])
                ax.set_yticks([])
                plt.axis('tight')
                ax = mfig.figure.add_subplot(gs_index)
                axim = ax.imshow(stim_img, aspect='auto', interpolation='none', extent=stim_extent)
                ax.set_xticks([])
                ax.set_yticks([])
                plt.axis('tight')

                #plot stimulus in each side of the figure
                sp_offset += sp_spec_height + sp_spacing

                for k,(lfp,enumber) in enumerate(lfp_list):

                    #get the grid coordinates for this LFP
                    row,col = np.nonzero(coords == enumber)

                    #determine the gridspec row and column from the grid coordinates
                    sp_col = col % 2
                    sp_row = (sp_lfp_height+sp_lfp_spacing)*row + sp_offset
                    gs_index = gs[sp_row:(sp_row+sp_lfp_height), sp_col]

                    sr = lfp.lfp.sample_rate
                    dt = 1.0 / sr

                    start_index = int(start_time / dt)
                    end_index = start_index + int(duration / dt) + 1
                    #print 'k=%d, nsp=%d, dt=%0.6f, duration=%0.2f, start_index=%d, end_index=%d' % (k, nsp, dt, duration, start_index, end_index)
                    t = np.arange(start_index, end_index, 1.0)*dt
                    s = lfp.waveform[start_index:end_index]
                    ax = mfig.figure.add_subplot(gs_index)
                    ax.plot_raw_imfs(t, s, 'k-')
                    ax.set_yticks([])
                    ax.set_xticks([])
                    ax.set_ylabel('%s%d' % (hemi, lfp.lfp.electrode.number))
                    ax.autoscale(tight=True)

                sp_offset += coords.shape[0]*(sp_lfp_height+sp_lfp_spacing) + sp_spacing
                sp_spike_height = 100 - sp_offset

                #make a list of cells on right and left depending on their column number
                left_cells = list()
                right_cells = list()
                clist = [c for c in cells if c.cell.electrode.hemisphere == hemi]
                for cell in clist:
                    #get grid coordinates of cell
                    enumber = cell.cell.electrode.number
                    row,col = np.nonzero(coords == enumber)
                    if col % 2 == 0:
                        left_cells.append( (cell, row) )
                    else:
                        right_cells.append( (cell, row) )

                left_cells.sort(key=operator.itemgetter(1))
                right_cells.sort(key=operator.itemgetter(1))

                cell_groups_by_electrode_right = dict()  # maps electrode numbers to cells for plot_raster
                for k,(c,row) in enumerate(right_cells):
                    enumber = '%d' % c.cell.electrode.number
                    if enumber not in cell_groups_by_electrode_right:
                        cell_groups_by_electrode_right[enumber] = list()
                    cell_groups_by_electrode_right[enumber].append(k)
                cell_groups_by_electrode_left = dict()  # maps electrode numbers to cells for plot_raster
                for k,(c,row) in enumerate(left_cells):
                    enumber = '%d' % c.cell.electrode.number
                    if enumber not in cell_groups_by_electrode_left:
                        cell_groups_by_electrode_left[enumber] = list()
                    cell_groups_by_electrode_left[enumber].append(k)

                #get the spike times for each cell
                left_spikes = list()
                for cell,row in left_cells:
                    ti = np.array(cell.spikes)
                    if len(ti.shape) > 0:
                        ti = ti[(ti >= start_time) & (ti <= end_time) ]
                    else:
                        ti = list()
                    left_spikes.append(ti)
                right_spikes = list()
                for cell,row in right_cells:
                    ti = np.array(cell.spikes)
                    if len(ti.shape) > 0:
                        ti = ti[(ti >= start_time) & (ti <= end_time) ]
                    else:
                        ti = list()
                    right_spikes.append(ti)

                #make a raster for each set of spikes
                gs_index_left = gs[sp_offset:(sp_offset+sp_spike_height-sp_spacing), 0]
                ax_left = mfig.figure.add_subplot(gs_index_left)
                plot_raster(left_spikes, ax=ax_left, duration=duration, bin_size=0.001, time_offset=start_time,
                            ylabel='Electrode', groups=cell_groups_by_electrode_left)
                gs_index_right = gs[sp_offset:(sp_offset+sp_spike_height-sp_spacing), 1]
                ax_right = mfig.figure.add_subplot(gs_index_right)
                plot_raster(right_spikes, ax=ax_right, duration=duration, bin_size=0.001, time_offset=start_time,
                            ylabel='Electrode', groups=cell_groups_by_electrode_right)

                #set figure title
                mfig.title = '%s - %s - %s - %0.1fs to %0.1f' % (self.site.name, self.protocol.name, fig_titles[hemi], start_time, end_time)
                mfig.figure.suptitle(mfig.title)

            return figs

        return None


bgra_dtype = np.dtype({'b': (np.uint8, 0),
                      'g': (np.uint8, 1),
                      'r': (np.uint8, 2),
                      'a': (np.uint8, 3)})


def qimage2np(qimage, dtype='array'):
    """
    Taken from http://kogs-www.informatik.uni-hamburg.de/~meine/software/vigraqt/qimage2ndarray.py:

        Convert QImage to np.ndarray.  The dtype defaults to uint8
        for QImage.Format_Indexed8 or `bgra_dtype` (i.e. a record array)
        for 32bit color images.  You can pass a different dtype to use, or
        'array' to get a 3D uint8 array for color images.
    """
    result_shape = (qimage.height(), qimage.width())
    temp_shape = (qimage.height(),
                  qimage.bytesPerLine() * 8 / qimage.depth())
    if qimage.format() in (QImage.Format_ARGB32_Premultiplied,
                           QImage.Format_ARGB32,
                           QImage.Format_RGB32):
        if dtype == 'rec':
            dtype = bgra_dtype
        elif dtype == 'array':
            dtype = np.uint8
            result_shape += (4, )
            temp_shape += (4, )
    elif qimage.format() == QImage.Format_Indexed8:
        dtype = np.uint8
    else:
        raise ValueError("qimage2np only supports 32bit and 8bit images")
    # FIXME: raise error if alignment does not match
    buf = qimage.bits().asstring(qimage.numBytes())
    result = np.frombuffer(buf, dtype).reshape(temp_shape)
    if result_shape != temp_shape:
        result = result[:,:result_shape[1]]
    if qimage.format() == QImage.Format_RGB32 and dtype == np.uint8:
        result = result[...,:3]
    return result
