import os
from PyQt4.QtGui import QImage,qRgb
from neurotables.ui.config import get_cache_dir


class ImageCache(object):
    """
        A simple filesystem based cache for images.
    """

    def __init__(self):
        self.cache_dir = get_cache_dir()
        self.cache = dict()

    def get_qimage(self, image_id):
        img_path = self.get_image_path(image_id)

        if image_id in self.cache:
            return self.cache[image_id]

        if os.path.exists(img_path):
            self.cache[image_id] = QImage(img_path)
            return self.cache[image_id]

        return None

    def store_qimage(self, image_id, qimage):
        img_path = self.get_image_path(image_id)
        if not qimage.save(img_path):
            print 'Could not save image to filesystem: %s' % img_path
            return False
        self.cache[image_id] = qimage
        return True

    def get_image_path(self, image_id):
        img_path = os.path.join(self.cache_dir, '%s' % image_id)
        return img_path


def get_qimage_from_raw_data(imgdata, width, height):
    """
        Takes a matrix of real-valued scalars and transforms it into a grayscale QImage of the specified width and height.
    """

    assert len(imgdata.shape) == 2  #enforce grayscale for now

    #construct the QImage object
    h,w = imgdata.shape
    img = QImage(w, h, QImage.Format_RGB32)

    #set the pixels
    max_val = imgdata.max()
    for row in range(h):
        for col in range(w):
            pxval = 1.0 - (imgdata[row, col] / max_val)
            if pxval > 0.0:
                pval = get_qrgb_from_grayscale(pxval)
                img.setPixel(col, row, pval)

    #rescale the image to the desired size
    return img.scaled(width, height)


def get_qrgb_from_grayscale(val):
    """
        Converts a number from 0 to 1 to a QRgb object representing a gray level between 0 and 255.
    """
    gval = int(val*255)
    return qRgb(gval, gval, gval)