import operator

from PyQt4.QtCore import pyqtSignal,Qt
from PyQt4.QtGui import QWidget,QScrollArea,QHBoxLayout,QLayout,QGraphicsScene,QGraphicsView,QBrush,QPen,QColor,QGraphicsRectItem,QImage, QPixmap, QRegion

from neurotables.ui.image_cache import get_qimage_from_raw_data


class EventsWidget(QWidget):

    selectionChanged = pyqtSignal(float, float)

    def __init__(self, events, duration, parent=None, resolution=10.0, thumbnailHeight=50, backgroundColor='#000'):
        """
            Initialize the events widget. Events should not overlap in time.

            events: A list of dictionaries, each dictionary represents an event, and each event and can have
                the following values:
                    "id" an integer id for the event (mandatory)
                    "start" the start time in seconds of the event (mandatory)
                    "end" the end time in seconds for the event (mandatory)
                    "image" a numpy matrix of values to be converted into an image that represents the event,
                        or an instance of QImage (optional)
                    "background-color" the background color if thumbnail is not specified
                    "description" the tooltip that's displayed when the mouse hovers over the image

            duration: the length of the event train in seconds

            resolution: the number of pixels per second in the display

            thumbnailHeight: the desired height of the thumbnails in widget.
        """

        QWidget.__init__(self, parent)

        self.thumbnailHeight = thumbnailHeight
        self.backgroundColor = backgroundColor

        #do some basic validation
        self.events = dict()
        for event in events:
            #do some basic validation
            if 'id' not in event:
                raise ValueError('event dictionary must contain an \'id\' value')
            if 'start' not in event:
                raise ValueError('event dictionary must contain a \'start\' value')
            if 'end' not in event:
                raise ValueError('event dictionary must contain a \'end\' value')
            self.events[event['id']] = event

        #sort the list of events
        elist = [(event['id'], event['start']) for event in events]
        elist.sort(key=operator.itemgetter(-1))
        self.event_ids = [e[0] for e in elist]

        #compute the total width in pixels
        self.resolution = resolution
        self.duration = duration
        self.totalWidth = self.duration*self.resolution

        #construct the graphics
        self.constructGraphics()
        self.selection = None

    def constructGraphics(self):

        #construct the graphics scene that we will draw on
        self.graphicsScene = MouseHandlingScene()
        self.graphicsScene.mouseMoveHandlers.append(self.mouseMove)
        self.graphicsScene.mousePressHandlers.append(self.mousePress)
        self.graphicsScene.mouseReleaseHandlers.append(self.mouseRelease)
        self.graphicsScene.mouseDoubleClickHandlers.append(self.mouseDoubleClick)

        #construct a list of QGraphicsItems representing events and the silence between them
        self.items = list()
        last_time = 0.0
        self.event2label = dict()
        for k,eid in enumerate(self.event_ids):
            e = self.events[eid]
            if e['start'] > last_time:
                #create a label for the silence preceding this event
                props = self.make_event_item(last_time, e['start'], id=None, bgcolor=self.backgroundColor)
                self.items.append(props)

            #now make the label for the event
            img = None
            if 'image' in e:
                img = e['image']
            if 'background-color' not in e:
                e['background-color'] = '#fff'
            desc = None
            if 'description' in e:
                desc = e['description']
            props = self.make_event_item(e['start'], e['end'], id=e['id'], bgcolor=e['background-color'], image=img, description=desc)
            self.event2label[e['id']] = props
            self.items.append(props)
            last_time = e['end']

        #create one last label for the silence following the last event
        props = self.make_event_item(last_time, self.duration, id=None, bgcolor=self.backgroundColor)
        self.items.append(props)

        #create a line used to highlight regions
        highlightColor = QColor('#0066ff')
        linePen = QPen(highlightColor)
        self.highlightLine = self.graphicsScene.addLine(0.0, 0.0, 0.0, self.thumbnailHeight, linePen)
        self.highlightLine.setVisible(False)

        #create text used to show the time at the highlight line
        self.highlightText = self.graphicsScene.addText('0.0')
        self.highlightText.setVisible(False)
        self.highlightText.setPos(0.0, 0.0)
        self.highlightText.setDefaultTextColor(highlightColor)

        #create rectangle used to highlight areas
        rectHighlightColor = QColor(int('0x00', 16), int('0x66', 16), int('0xff', 16), 100)
        self.highlightRect = self.graphicsScene.addRect(0.0, 0.0, 0.0, self.thumbnailHeight, linePen, QBrush(rectHighlightColor))
        self.highlightRect.setVisible(False)

        #set default highlight properties
        self.highlightProps = {'mouseDown':False, 'start':0.0}

        #create a main layout
        self.mainLayout = QHBoxLayout(self)
        self.mainLayout.setSpacing(0)
        self.mainLayout.setContentsMargins(0, 0, 0, 0)
        self.mainLayout.setSizeConstraint(QLayout.SetFixedSize)

        #create a QGraphicsView and add the scene to it
        self.graphicsView = QGraphicsView()
        self.graphicsView.setFixedWidth(int(self.resolution*self.duration))
        self.graphicsView.setFixedHeight(self.thumbnailHeight)
        self.graphicsView.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.graphicsView.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.graphicsView.setScene(self.graphicsScene)

        #add the view to the main layout
        self.mainLayout.addWidget(self.graphicsView)

    def make_event_item(self, start, end, id=None, bgcolor='#000', image=None, description=None):

        #compute width in pixels from time
        w = (end - start)*self.resolution
        t = 'silence'
        if id is not None:
            t = 'event'

        #create rectangle QGraphicsItem
        y = 0.0
        x = start*self.resolution

        #create properties
        props = {'type':t, 'start':start, 'end':end,
                 'start_px':start*self.resolution,
                 'end_px':end*self.resolution,
                 'id':id,
                 'image':image,
                 'description':description}

        #create rectangle
        brush = QBrush(QColor(bgcolor))
        pen = QPen(Qt.white)
        rect = EventGraphicsItem(x, y, w, self.thumbnailHeight, props)
        rect.setPen(pen)
        rect.setBrush(brush)
        self.graphicsScene.addItem(rect)

        props['item'] = rect
        return props

    def mouseMove(self, ev):
        #make the highlight line follow the mouse
        pos = ev.scenePos()
        x = pos.x()
        self.highlightLine.setLine(x, 0.0, x, self.thumbnailHeight)
        self.highlightLine.setVisible(True)

        #draw the time next to the highlight line
        t = x / self.resolution
        tstr = '%0.2f' % t
        self.highlightText.setVisible(False)
        self.highlightText.setPos(x, self.thumbnailHeight/5.0)
        self.highlightText.setPlainText(tstr)
        self.highlightText.setVisible(True)

        #if the mouse is being clicked and dragged, show the highlight rectangle
        if self.highlightProps['mouseDown']:
            xstart = self.highlightProps['start']
            w = x - xstart
            self.highlightRect.setRect(xstart, 0.0, w, self.thumbnailHeight)
            self.highlightRect.setVisible(True)

    def mousePress(self, ev):
        if ev.button() == Qt.LeftButton:
            self.selection = None
            self.highlightProps['mouseDown'] = True
            self.highlightProps['start'] = ev.scenePos().x()

    def mouseRelease(self, ev):
        if ev.button() == Qt.LeftButton:
            self.highlightProps['mouseDown'] = False
            self.highlightProps['end'] = ev.scenePos().x()
            tstart = self.highlightProps['start'] / self.resolution
            tend = self.highlightProps['end'] / self.resolution
            self.selection = (tstart, tend)
            self.selectionChanged.emit(tstart, tend)

    def get_selection_image(self):
        """
            Creates an image out of the selection, useful for quick plotting of the selected stimulus history.
        """
        if self.selection is not None:
            x_start = self.selection[0] * self.resolution
            x_end = self.selection[1] * self.resolution
            w = x_end - x_start
            h = self.thumbnailHeight
            src_region = QRegion(x_start, 0.0, w, h)
            img = QImage(w, h, QImage.Format_ARGB32)
            self.render(img, sourceRegion=src_region)
            return img

    def mouseDoubleClick(self, ev):
        pass


class EventGraphicsItem(QGraphicsRectItem):

    def __init__(self, x, y, w, h, props):
        self.props = props
        self.image = None
        QGraphicsRectItem.__init__(self, x, y, w, h)
        self.abspos = (x, y)
        #print 'id=%s, type=%s, x=%d, y=%d, w=%d, h=%d' % (str(props['id']), props['type'], x, y, w, h)

        #if the event has an image, plot it
        if self.props['image'] is not None:
            if type(self.props['image']) == QImage:
                self.image = self.props['image']
                if self.image.height() != h or self.image.width() != w:
                    self.image = self.image.scaled(w, h)
            else:
                self.image = get_qimage_from_raw_data(props['image'], w, h)
        #set tooltip
        if 'description' in props:
            d = props['description']
            if d is not None:
                self.setToolTip(d)

    def paint(self, painter, option, widget=None):

        QGraphicsRectItem.paint(self, painter, option, widget)
        if self.image is not None:
            painter.drawImage(self.abspos[0], 0, self.image)

    def mousePressEvent(self, ev):
        if ev.button() == Qt.RightButton:
            if self.props['id'] is not None:
                print 'Right click: id=%d' % self.props['id']


class MouseHandlingScene(QGraphicsScene):

    def __init__(self):
        QGraphicsScene.__init__(self)
        self.mouseMoveHandlers = list()
        self.mousePressHandlers = list()
        self.mouseReleaseHandlers = list()
        self.mouseDoubleClickHandlers = list()

    def mousePressEvent(self, ev):
        [mph(ev) for mph in self.mousePressHandlers]
        QGraphicsScene.mousePressEvent(self, ev)

    def mouseMoveEvent(self, ev):
        [mmh(ev) for mmh in self.mouseMoveHandlers]
        QGraphicsScene.mouseMoveEvent(self, ev)

    def mouseReleaseEvent(self, ev):
        [mrh(ev) for mrh in self.mouseReleaseHandlers]
        QGraphicsScene.mouseReleaseEvent(self, ev)

    def mouseDoubleClickEvent(self, ev):
        [mdch(ev) for mdch in self.mouseDoubleClickHandlers]
        QGraphicsScene.mouseDoubleClickEvent(self, ev)


def create_events_widget_layout(eventsWidget):

    widgetHeight = eventsWidget.thumbnailHeight+20

    scrollArea = QScrollArea()
    scrollArea.setFixedHeight(widgetHeight)
    scrollArea.setWidget(eventsWidget)

    layout = QHBoxLayout()
    layout.setSpacing(0)
    layout.setContentsMargins(0, 0, 0, 0)
    layout.addWidget(scrollArea)

    container = QWidget()
    container.setLayout(layout)
    #container.setFixedHeight(widgetHeight)

    return container
