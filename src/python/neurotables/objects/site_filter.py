from neurotables.objects.cells import LFP,Cell


class SiteFilter(object):

    def __init__(self, site, protocol):
        self.site = site
        self.protocol = protocol

    def find(self, objects=[LFP, Cell], criteria=dict()):
        """
            Does a search on the specified objects to make sure they adhere to the given criteria.
        """

        #first find the relevant electrodes first
        electrodes = self.site.electrodes
        if 'electrodes' in criteria:
            hemisphere = None
            if 'hemisphere' in criteria:
                hemisphere = criteria['hemisphere']
            electrodes = self.filter_electrodes(electrodes, criteria['electrodes'], hemisphere)

        #from the electrodes, pull LFPs and cells
        found_objects = dict()
        if LFP in objects:
            lfps = [e.lfp for e in electrodes]
            found_objects['LFP'] = list()
            for lfp in lfps:
                found_objects['LFP'].extend([lfppr for lfppr in lfp.protocol_responses if lfppr.protocol_id == self.protocol.id])

        if Cell in objects:
            cells = list()
            for clist in [e.cells for e in electrodes]:
                for c in clist:
                    cells.extend([cpr for cpr in c.protocol_responses if cpr.protocol_id == self.protocol.id])
            if 'sort' in criteria:
                cells = self.filter_cells(cells, criteria['sort'])
            found_objects['Cell'] = cells

        return found_objects

    def filter_cells(self, cells, sort_criteria):
        return [c for c in cells if c.cell.sort_type in sort_criteria]

    def filter_electrodes(self, electrodes, range_criteria, hemisphere_criteria=None):

        #filter based on hemisphere
        if hemisphere_criteria is not None:
            electrodes = [e for e in electrodes if e.hemisphere in hemisphere_criteria]
        if range_criteria == 'ALL':
            return electrodes

        #parse the range string and come up with a list of electrode numbers
        good_electrodes = list()
        rstrs = range_criteria.split(',')
        for rstr in rstrs:
            if '-' in rstr:
                s,e = rstr.split('-')
                good_electrodes.extend(range(int(s), int(e)+1))
            else:
                good_electrodes.append(int(rstr))

        #filter based on electrode number
        electrodes = [e for e in electrodes if e.number in good_electrodes]
        return electrodes


