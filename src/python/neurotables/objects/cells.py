import copy
import operator
from matplotlib import cm
import tables
from lasp.signal import resample_signal
from lasp.spikes import plot_raster
from lasp.timefreq import gaussian_stft
from tables_extension import TablesBackedObject, TablesBackedRow, IdCol, tj_relations,DatasetCol

import numpy as np

import matplotlib.pyplot as plt
import matplotlib.cm as cm

from lasp.sound import plot_spectrogram, spectral_envelope


class Subject(TablesBackedObject):

    class Row(TablesBackedRow):
        name = tables.StringCol(60)
        sex = tables.StringCol(1)
        age = tables.Int32Col()


class Experiment(TablesBackedObject):
    ''' Returns an experiment object. Properties specified here can be auto-completed. Other properties include: sites, subject.'''

    class Row(TablesBackedRow):
        name = tables.StringCol(60)
        subject_id = IdCol()


class Site(TablesBackedObject):

    class Row(TablesBackedRow):
        experiment_id = IdCol()
        name = tables.StringCol(100)
        rdepth = tables.Int32Col()
        ldepth = tables.Int32Col()

    def get_electrode(self, number):
        elist = [e for e in self.electrodes if e.number == number]
        if len(elist) > 0:
            return elist[0]
        return None


class Protocol(TablesBackedObject):

    class Row(TablesBackedRow):

        name = tables.StringCol(60)
        site_id = IdCol()
        timezone = tables.StringCol(20)
        start_time = tables.Float64Col()  # unix timestamp
        end_time = tables.Float64Col()    # unix timestamp
        stimulus_envelope = DatasetCol({'compute':'compute_stimulus_envelope'})

    def __init__(self, file_handle):
        TablesBackedObject.__init__(self, file_handle)
        self.envelope_sample_rate = 1.0 / 0.020

    def compute_stimulus_envelope(self, sample_rate=None, t1=None, t2=None):

        if t1 is None:
            t1 = 0.0
        if t2 is None:
            protocol_duration = self.end_time - self.start_time  # protocol duration in seconds
            t2 = protocol_duration
        duration = t2 - t1

        if sample_rate is None:
            sample_rate = self.envelope_sample_rate

        #get all events that occur in interval of interest
        events = [e for e in self.events if e.start_time >= t1 and e.start_time <= t2]
        events.sort(key=operator.attrgetter('start_time'))

        nbins = int(np.ceil(duration * sample_rate))
        print 't1=%0.0f, t2=%0.0f, duration=%0.2f, sample_rate=%0.2f, nbins=%d' % (t1, t2, duration, sample_rate, nbins)
        env = np.zeros(nbins, dtype='float')
        for e in events:
            #compute stimulus envelope
            senv = e.sound.envelope
            #downsample
            tenv,senv = resample_signal(senv, e.sound.sample_rate, sample_rate)
            #get rid of any weird negative values
            senv[senv < 0.0] = 0.0

            sbin = int((e.start_time-t1)*sample_rate)
            ebin = min(sbin + len(senv), len(env))
            dbin = ebin - sbin
            print '\te.start_time=%0.2f, e.end_time=%0.2f, sbin=%d, ebin=%d, len(senv)=%d' % (e.start_time, e.end_time, sbin, ebin, len(senv))

            #nbins_stim = (e.end_time - e.start_time)*sample_rate
            #stim_dur = e.end_time - e.start_time
            #print 'sound.id=%d, sbin=%d, ebin=%d, nbins=%d, senv.shape=(%s), nbins_stim=%d, stim_dur=%0.6f, tenv.max()=%0.6f' % (e.sound.id, sbin, ebin, nbins, str(senv.shape), nbins_stim, stim_dur, tenv.max())
            env[sbin:ebin] = senv[:dbin]

        tenv = (np.arange(len(env)) / sample_rate) + t1

        return tenv,env

    def plot(self, start_time, end_time):

        t_env = np.arange(len(self.stimulus_envelope)) / self.envelope_sample_rate
        env_index = (t_env >= start_time) & (t_env <= end_time)

        plt.figure()
        plt.subplot(3, 1, 1)
        plt.plot(t_env[env_index], self.stimulus_envelope[env_index], 'g-')
        plt.xlabel('Time (s)')
        plt.ylabel('Envelope Amplitude')

        plt.subplot(3, 1, 2)
        #plot LFPs

    def compute_stimulus_spectrogram(self, start_time, end_time, threshold=True, spec_inc=0.001, window_length=0.007):

        events = [e for e in self.events if e.start_time >= start_time and e.start_time <= end_time]
        events.sort(key=operator.attrgetter('start_time'))

        if len(events) == 0:
            nt = int((end_time - start_time) / spec_inc)
            t = np.arange(nt)*spec_inc
            freq = np.array([0.0, 1000.0])
            return t,freq,np.zeros([2, nt])

        #compute the spectrogram of each sound
        specs=list()

        for e in events:
            sr=e.sound.sample_rate
            s=e.sound.waveform
            t,freq,timefreq,rms=gaussian_stft(s,sr,window_length,spec_inc)
            specs.append((t,freq,np.abs(timefreq),e))

        #construct the full spectrogram
        freq=specs[0][1]
        tlen=int((end_time-start_time)/spec_inc)
        full_spec=np.zeros([len(freq),tlen])
        for t,freq,timefreq,e in specs:
            nz=timefreq>0.0
            logspec=np.zeros_like(timefreq)
            logspec[nz]=20*np.log10(timefreq[nz])
            logspec/=np.abs(logspec).max()

            if threshold:
                thresh = np.percentile(logspec.ravel(), 10)
                logspec[logspec < thresh] = thresh
                logspec -= thresh

            si=int((e.start_time-start_time)/spec_inc)
            ei=min(full_spec.shape[1],si+len(t))
            eei=ei-si

            #print 'full_spec.shape[1]=%d, len(t)=%d, si=%d, ei=%d, eei=%d' % (full_spec.shape[1], len(t), si, ei, eei)

            full_spec[:,si:ei]=logspec[:,:eei]

        t = np.arange(full_spec.shape[1])*spec_inc + start_time

        return t,freq,full_spec

    def plot_spectrograms(self, start_time, end_time, threshold=None, min_freq=300.0, max_freq=8000.0, colormap=cm.afmhot_r, electrode_num=None, lfp_min_freq=30.0, lfp_max_freq=100.0):
        """
            Make plots of spectrograms
        """

        t,freq,full_spec = self.compute_stimulus_spectrogram(start_time, end_time, threshold)

        nsp = 1 + (electrode_num is not None)
        plt.figure()
        plt.subplot(nsp, 1, 1)
        plot_spectrogram(t, freq, full_spec, fmin=min_freq, fmax=max_freq, colormap=colormap, colorbar=False)

        if electrode_num is not None:
            e = [e for e in self.site.electrodes if e.number == electrode_num]
            if len(e) > 0:
                electrode = e[0]
                pr = electrode.get_lfp_protocol_response(self.name)
                spec_inc = 0.067
                winsize = 0.100
                lfp_t,lfp_freq,lfp_spec,lfp_rms = pr.compute_spectrogram(spec_inc, winsize, min_freq=lfp_min_freq, max_freq=lfp_max_freq, t1=start_time, t2=end_time)

                plt.subplot(nsp, 1, 2)
                plot_spectrogram(lfp_t, lfp_freq, lfp_spec, fmin=lfp_min_freq, fmax=lfp_max_freq, colormap=cm.jet, colorbar=False)
                plt.axis('tight')


class ElectrodeGeometry(TablesBackedObject):

    class Row(TablesBackedRow):
        name = tables.StringCol(100)
        shape = tables.Float32Col(shape=(2,), dflt=[0, 0])
        coordinates = DatasetCol()

        # Should include angle of left and right arrays, as well as spacing between columns and rows


class ElectrodeData(TablesBackedObject):

    class Row(TablesBackedRow):
        site_id = IdCol()
        geometry_id = IdCol()
        number = tables.Int32Col()
        hemisphere = tables.StringCol(1)

    def get_lfp_protocol_response(self, protocol_name):
        plist = [pr for pr in self.lfp.protocol_responses if pr.protocol.name == protocol_name]
        if len(plist) > 0:
            plist[0].from_id(plist[0].id)
            return plist[0]
        return None


class LFP(TablesBackedObject):

    class Row(TablesBackedRow):
        electrode_id = IdCol()
        sample_rate = tables.Float32Col()


class LFPProtocolResponse(TablesBackedObject):

    class Row(TablesBackedRow):
        lfp_id = IdCol()
        protocol_id = IdCol()
        waveform = DatasetCol()

    def compute_spectrogram(self, increment, window_length, min_freq, max_freq=None, t1=0.0, t2=None, return_phase=False):

        sr = self.lfp.sample_rate
        if t2 is None:
            t2 = float(len(self.waveform)) / sr
        lfp_t = np.arange(len(self.waveform)) / sr
        lfp_index = (lfp_t >= t1) & (lfp_t <= t2)

        if return_phase is True:
            t,freq,spec,spec_phase,rms = gaussian_stft(self.waveform[lfp_index], sr, window_length, increment, min_freq=min_freq, max_freq=max_freq, return_phase=return_phase)
            t += t1
            return t, freq, spec, spec_phase, rms
            
        else:
            t,freq,spec,rms = gaussian_stft(self.waveform[lfp_index], sr, window_length, increment, min_freq=min_freq, max_freq=max_freq, return_phase=return_phase)
            t += t1
            return t,freq,spec,rms


class Cell(TablesBackedObject):

    class Row(TablesBackedRow):
        electrode_id = IdCol()
        name = tables.StringCol(60, dflt='')
        region = tables.StringCol(25, dflt='')
        subregion = tables.StringCol(25, dflt='')
        sort_type = tables.StringCol(25, dflt='')
        extra_info = tables.StringCol(150, dflt='')

        spike_shape_mean = DatasetCol()
        spike_shape_std = DatasetCol()

    def get_extra_info(self):
        if self.extra_info is not None:
            einfo = dict()
            propstrs = self.extra_info.split(';')
            for nvpair in propstrs:
                name,val = nvpair.split('=')
                einfo[name] = val
            return einfo
        return None


class CellProtocolResponse(TablesBackedObject):

    class Row(TablesBackedRow):
        cell_id = IdCol()
        protocol_id = IdCol()
        spikes = DatasetCol()

        
class SoundEvent(TablesBackedObject):

    class Row(TablesBackedRow):
        start_time = tables.Float32Col()
        end_time = tables.Float32Col()
        sound_id = IdCol()
        protocol_id = IdCol()


class Sound(TablesBackedObject):

    class Row(TablesBackedRow):
        md5 = tables.StringCol(64)
        sample_rate = tables.Float32Col()
        duration = tables.Float32Col()
        stim_class = tables.StringCol(50)
        stim_type = tables.StringCol(50)
        stim_number = tables.Int32Col()
        stim_source = tables.StringCol(50, dflt='')
        call_id = tables.StringCol(10, dflt='')
        call_gender = tables.StringCol(1, dflt='')
        call_age = tables.StringCol(10, dflt='')
        description = tables.StringCol(500, dflt='')
        original_wavfile = tables.StringCol(100, dflt='')
        waveform = DatasetCol()

    def from_md5(self, md5):
        se_loaded = self.from_prop('md5', md5)
        if se_loaded:
            return se_loaded
        return self.from_prop('md5', md5.lower())

    def from_number(self, num):
        se_loaded = self.from_prop('number', num)
        return se_loaded

    @property
    def envelope(self):
        return spectral_envelope(self.waveform, self.sample_rate)

    def compute_spectrogram(self, window_length=0.007, increment=0.001, min_freq=300.0, max_freq=8000.0, nstd=6, log=True, thresh_percentile=10):
        """
            Compute a spectrogram
        """
        x = self.waveform
        spec_t, spec_f, spec, spec_rms = gaussian_stft(x, self.sample_rate, window_length=window_length, increment=increment, min_freq=min_freq, max_freq=max_freq, nstd=nstd)

        spec = np.abs(spec)
        if log:
            nz = spec > 0.0
            spec[nz] = 20*np.log10(spec[nz])
            nzmin = spec[nz].min()
            spec[~nz] = nzmin

            thresh = np.percentile(spec[nz], thresh_percentile)
            print 'thresh=%f, nzmin=%f' % (thresh, nzmin)
            spec[spec < thresh] = thresh

        return spec_t, spec_f, spec

    def plot(self, db_rectify=None):

        if self.waveform is None:
            return

        x = self.waveform
        t = np.arange(len(x))*(1.0 / self.sample_rate)

        nsp = 2 + (self.spectrogram is not None)

        plt.figure()
        plt.subplot(nsp, 1, 1)
        plt.plot(t, x, 'k-')
        plt.xlabel('Time (s)')
        plt.axis('tight')

        if self.envelope is not None:
            plt.subplot(nsp, 1, 2)
            plt.plot(t, self.envelope, 'g-', linewidth=2.0)
            plt.xlabel('Time (s)')
            plt.ylabel('Amplitude Envelope')
            plt.axis('tight')

        if self.spectrogram is not None:
            spec = self.spectrogram
            sdata = copy.copy(spec.data)
            if db_rectify is not None:
                sdata += db_rectify
                sdata[sdata < 0.0] = 0.0

            spec_t = np.arange(sdata.shape[1])*(1.0 / spec.sample_rate)
            ax = plt.subplot(nsp, 1, 3)
            plot_spectrogram(spec_t, spec.frequency, sdata, ax=ax, ticks=True, colorbar=False, colormap=cm.afmhot_r)
            plt.axis('tight')


class SoundConditionedLFPResponse(TablesBackedObject):

    class Row(TablesBackedRow):
        lfp_protocol_response_id = IdCol()
        sound_id = IdCol()
        psth = DatasetCol({'compute':'compute_psth'})

    def trials(self):
        if not hasattr(self, '_trials'):
            self._trials = None
        if self._trials is not None:
            return self._trials

        if self.lfp_protocol_response is not None and self.sound is not None:
            events = [e for e in self.sound.events if e.protocol_id == self.lfp_protocol_response.protocol_id]
            events.sort(key=operator.attrgetter('start_time'))
            dt = 1.0 / self.lfp_protocol_response.lfp.sample_rate
            lfpwave = self.lfp_protocol_response.waveform
            trials = list()

            duration = np.min([e.sound.duration for e in events])
            for event in events:
                start_index = int(event.start_time / dt)
                end_index = start_index + int(duration / dt)
                trials.append(lfpwave[start_index:end_index])
                
            self._trials = trials
        return self._trials

    def compute_psth(self):
        if self.trials() is not None:
            pass

        return np.mean(self.trials(), axis=0)

    # Need to rework this. The second subplot should show the lfp spectrogram for this stimulus.
    #def plot(self):
    #    
    #    spec = self.sound.spectrogram
    #    spec_t = np.arange(spec.data.shape[1])*(1.0 / spec.sample_rate)
    #    psth = self.psth
    #    psth_t = np.arange(len(psth)) * 0.001

    #    plt.figure()
    #    ax = plt.subplot(3, 1, 1)
    #    plot_spectrogram(spec_t, spec.frequency, spec.data, ax=ax, ticks=True)
    #    plt.axis('tight')

    #    ax = plt.subplot(3, 1, 2)
    #    plot_raster(self.trials, ax=ax, bin_size=0.001)
    #    plt.axis('tight')

    #    ax = plt.subplot(3, 1, 3)
    #    ax.plot(psth_t, psth, 'k-')
    #    ax.set_xlabel('Time (s)')
    #    ax.set_ylabel('Pr[spike]')
    #    plt.axis('tight')


class SoundConditionedCellResponse(TablesBackedObject):

    class Row(TablesBackedRow):
        cell_protocol_response_id = IdCol()        
        sound_id = IdCol()
        psth = DatasetCol({'compute':'compute_psth'})

    def trials(self):
        if not hasattr(self, '_trials'):
            self._trials = None
        if self._trials is not None:
            return self._trials

        if self.cell_protocol_response is not None and self.sound is not None:
            events = [e for e in self.sound.events if e.protocol_id == self.cell_protocol_response.protocol_id]
            events.sort(key=operator.attrgetter('start_time'))

            trials = list()
            for event in events:
                start_time = event.start_time
                end_time = event.end_time
                spike_index = (self.cell_protocol_response.spikes >= start_time) & (self.cell_protocol_response.spikes <= end_time)
                if spike_index.sum() > 0:
                    ti = self.cell_protocol_response.spikes[spike_index]
                    ti -= start_time  # center at stimulus onset
                    trials.append(ti)
                else:
                    trials.append(list())
            self._trials = trials
        return self._trials

    def compute_psth(self):
        if self.trials() is None:
            raise Exception('There are no spike trials for SoundConditionedCellResponse with id %d, cannot construct PSTH' % self.id)

        bin_rate = 1000
        nbins = int(self.sound.duration*bin_rate)
        scnts = np.zeros(nbins, dtype='int32')

        num_trials = len(self.trials())
        for stimes in self.trials():
            stimes = np.array(stimes, dtype='float32')
            if len(stimes.shape) > 0:
                stimes *= 1000
                stimes = stimes.astype('int32')
                sindx = stimes[(stimes >=0) & (stimes < nbins)]
                scnts[sindx] += 1
        nscnts = scnts.astype('float32') / num_trials
        return nscnts

    def plot(self):

        spec = self.sound.spectrogram
        spec_t = np.arange(spec.data.shape[1])*(1.0 / spec.sample_rate)
        psth = self.psth
        psth_t = np.arange(len(psth)) * 0.001

        plt.figure()
        ax = plt.subplot(3, 1, 1)
        plot_spectrogram(spec_t, spec.frequency, spec.data, ax=ax, ticks=True)
        plt.axis('tight')

        ax = plt.subplot(3, 1, 2)
        plot_raster(self.trials(), ax=ax, bin_size=0.001)
        plt.axis('tight')

        ax = plt.subplot(3, 1, 3)
        ax.plot_raw_imfs(psth_t, psth, 'k-')
        ax.set_xlabel('Time (s)')
        ax.set_ylabel('Pr[spike]')
        plt.axis('tight')


class PreprocessedSound(TablesBackedObject):

    class Row(TablesBackedRow):
        sound_id = IdCol()
        sample_rate = tables.Float32Col()
        data = DatasetCol()

    def from_sound_id(self, id):
        return self.from_prop('sound_id', id)


class Spectrogram(PreprocessedSound):

    class Row(PreprocessedSound.Row):
        low_freq = tables.Float32Col()
        high_freq = tables.Float32Col()
        freq_spacing = tables.Float32Col()
        nstd = tables.Int32Col()

        rms = DatasetCol()
        frequency = DatasetCol()


class LyonsCochleagram(PreprocessedSound):

    class LyonsCochleagramRow(PreprocessedSound.Row):
        agc = tables.BoolCol()
        differ = tables.BoolCol()
        tau = tables.Float32Col()
        low_freq = tables.Float32Col()
        high_freq = tables.Float32Col()
        earq = tables.Float32Col()
        step = tables.Float32Col()


tj_relations.one2many(Subject, 'experiments', Experiment, 'subject_id', 'subject')
tj_relations.one2many(Experiment, 'sites', Site, 'experiment_id', 'experiment')

tj_relations.one2many(Site, 'electrodes', ElectrodeData, 'site_id', 'site')
tj_relations.one2many(Site, 'protocols', Protocol, 'site_id', 'site')

tj_relations.one2many(Protocol, 'events', SoundEvent, 'protocol_id', 'protocol')
tj_relations.one2many(Protocol, 'cell_protocol_responses', CellProtocolResponse, 'protocol_id', 'protocol')
tj_relations.one2many(Protocol, 'lfp_protocol_responses', LFPProtocolResponse, 'protocol_id', 'protocol')

tj_relations.one2many(ElectrodeData, 'lfp', LFP, 'electrode_id', 'electrode', one2one=True)
tj_relations.one2many(ElectrodeData, 'cells', Cell, 'electrode_id', 'electrode')
tj_relations.one2many(ElectrodeGeometry, 'electrodes', ElectrodeData, 'geometry_id', 'geometry')

tj_relations.one2many(LFP, 'protocol_responses', LFPProtocolResponse, 'lfp_id', 'lfp')
tj_relations.one2many(Cell, 'protocol_responses', CellProtocolResponse, 'cell_id', 'cell')

tj_relations.one2many(CellProtocolResponse, 'sound_responses', SoundConditionedCellResponse, 'cell_protocol_response_id', 'cell_protocol_response')
tj_relations.one2many(LFPProtocolResponse, 'sound_responses', SoundConditionedLFPResponse, 'lfp_protocol_response_id', 'lfp_protocol_response')

tj_relations.one2many(Sound, 'events', SoundEvent, 'sound_id', 'sound')
tj_relations.one2many(Sound, 'cell_protocol_responses', SoundConditionedCellResponse, 'sound_id', 'sound')
tj_relations.one2many(Sound, 'lfp_protocol_responses', SoundConditionedLFPResponse, 'sound_id', 'sound')

tj_relations.one2many(Sound, 'spectrogram', Spectrogram, 'sound_id', 'sound', one2one=True)
tj_relations.one2many(Sound, 'cochleagram', LyonsCochleagram, 'sound_id', 'sound', one2one=True)
