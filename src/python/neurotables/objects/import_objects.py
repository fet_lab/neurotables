import hashlib
import calendar
from dateutil import parser
import os
import datetime

import h5py
import tables

from scipy.io import wavfile
from lasp.sound import spectrogram

from cells import *


def import_stims(f, stims_group):

    stims_by_number = dict()

    for stim_key,stim_group in stims_group.iteritems():
        tdt_wavfile = str(stim_group.attrs['tdt_wavfile'])
        [samp_rate, data] = wavfile.read(tdt_wavfile)

        sint = 1.0 / samp_rate
        duration = len(data)*sint
        stim_md5 = md5_for_file(tdt_wavfile)

        sound = Sound(f)       
        #if sound.from_md5(stim_md5):
            #print 'Stimulus from wavfile %s already exists with md5: %s! This is probably weird. other file=%s' % (tdt_wavfile, stim_md5, sound.original_wavfile)
            #continue

        sound.md5 = stim_md5
        sound.duration = duration
        sound.stim_class = stim_group.attrs['stim_class']
        sound.stim_type = stim_group.attrs['type']
        sound.sample_rate = samp_rate
        sound.stim_number = int(stim_group.attrs['number'])
        sound.original_wavfile = str(stim_group.attrs['tdt_wavfile'])

        if 'source' in stim_group.attrs:
            sound.stim_source = str(stim_group.attrs['source'])
        if 'callid' in stim_group.attrs:
            sound.call_id = str(stim_group.attrs['callid'])
        if 'sex' in stim_group.attrs:
            sound.call_gender = str(stim_group.attrs['sex'])
        if 'callerAge' in stim_group.attrs:
            sound.call_age = str(stim_group.attrs['callerAge'])

        sound.save()
        sound.waveform = data
        stims_by_number[sound.stim_number] = sound

    return stims_by_number


def create_sound_conditioned_responses(f):
    """
        For all the cells and lfps in file, identify the sounds that are present during their
        recordings and associate them with a SoundConditionedResponse object.
    """

    p = Protocol(f)
    for protocol in p.get_all():
        print 'Creating conditioned responses for protocol %s' % protocol.name

        #aggregate events by sound id
        print '\tGetting all sounds for this protocol'
        sound_ids = np.unique([se.sound.id for se in protocol.events])

        #connect sounds to cell        
        for k,sound_id in enumerate(sound_ids):
            print '\tConstructing conditioned responses for sound %d, %d to go' % (sound_id, len(sound_ids)-k-1)
            #get all cell responses
            for cr in protocol.cell_protocol_responses:
                scr = SoundConditionedCellResponse(f)
                scr.sound_id = sound_id
                scr.cell_protocol_response_id = cr.id
                scr.save()
                #print '\t\tComputing PSTH for cell %s, protocol %s, sound %d, sort type %s' % (cr.cell.name, cr.protocol.name, sound_id, cr.cell.sort_type)

            #get all lfp responses
            for lfpr in protocol.lfp_protocol_responses:
                scr = SoundConditionedLFPResponse(f)
                scr.sound_id = sound_id
                scr.lfp_protocol_response_id = lfpr.id
                scr.save()
                #print '\t\tComputing PSTH for LFP on electrode %d, protocol %s, sound %d' % (lfpr.lfp.electrode.number, lfpr.protocol.name, sound_id)



def create_spectrograms(f):

    s = Sound(f)
    for sound in s.get_all():
        t,freq,spec,rms = spectrogram(sound.waveform, sound.sample_rate, 1000.0, 125.0, min_freq=375.0, max_freq=8000.0)
        sgram = Spectrogram(f)
        sgram.sound_id = sound.id
        sgram.sample_rate = 1000.0
        sgram.low_freq = 375.0
        sgram.high_freq = 8000.0
        sgram.freq_spacing = 125.0
        sgram.nstd = 6
        sgram.save()
        sgram.data = spec
        sgram.frequency = freq
        sgram.rms = rms


def read_sort_file(sort_file):
    sort_map = dict()
    hf_sorted = h5py.File(sort_file, 'r')
    for site_key, site_group in hf_sorted['/'].iteritems():
        site_protocol = str(site_group.attrs['protocol'])  # the protocol is ignored, spikes are supposed to be sorted across protocols
        site_name = str(site_group.attrs['site'])
        site_ldepth = int(site_group.attrs['ldepth'])
        site_rdepth = int(site_group.attrs['rdepth'])
        print '\tImporting sorted site %s...' % site_key

        #make an entry in the sort map for the site if none exists
        site_name_and_depth = (site_name, site_rdepth, site_ldepth)
        if site_name_and_depth not in sort_map:
            sort_map[site_name_and_depth] = dict()
            #iterate through each electrode in the site and make an entry for the cells corresponding to that electrode
        for estr, egroup in site_group.iteritems():
            enumber = int(estr)
            sort_map[site_name_and_depth][enumber] = list()
            for cell_name, cell_group in egroup.iteritems():
                cprops = dict()
                cprops['name'] = cell_name
                cprops['sort_file_name'] = cell_group.attrs['sort_file_name']
                cprops['sort_type'] = cell_group.attrs['sort_type']
                cprops['spike_ids'] = np.array(cell_group['spike_ids'])
                cprops['sort_code'] = int(cell_group.attrs['sort'])
                if cprops['sort_code'] == -1:
                    cprops['sort_code'] = 0
                sort_map[site_name_and_depth][enumber].append(cprops)
    hf_sorted.close()
    return sort_map


def import_stim_data(f, protocol, site_group, stims_by_number):
    stim_data = np.array(site_group['stim_data'])
    for stim_num, start_time, end_time in stim_data:
        if int(stim_num) in stims_by_number:
            stim_num = int(stim_num)
            s = stims_by_number[stim_num]
            se = SoundEvent(f)
            se.protocol_id = protocol.id
            se.sound_id = s.id
            se.start_time = start_time
            se.end_time = end_time
            se.save()


def import_all(unsorted_file, output_file, sort_file=None):
    """
        The same as import_all but does not require spike sorting. It pulls all
        required data from the unsorted file instead
    """
    f = tables.open_file(output_file, mode='a')
    hf_unsorted = h5py.File(unsorted_file, 'r')
    
    bird_name = str(hf_unsorted.attrs['bird_name'])
    bird_gender = str(hf_unsorted.attrs['bird_gender'])

    #import stimuli
    print 'Importing Stimuli...'
    stims_by_number = import_stims(f, hf_unsorted['stims'])

    #create bird
    print 'Creating new subject named %s...' % bird_name
    subject = Subject(f)
    subject.name = bird_name
    subject.gender = bird_gender
    subject.age = 0
    subject.save()

    #create experiment
    #may need to be expanded to incorporate multiple experiments   
    root_dir,file_name = os.path.split(unsorted_file)
    tank_name,ext = file_name.split('.')
    print 'Importing data from tank named %s...' % tank_name
    experiment = Experiment(f)
    experiment.name = tank_name
    experiment.subject_id = subject.id
    experiment.save()
    
    sites_by_name_and_depth = dict()
    electrodes_by_site = dict()
    spike_shapes_by_site = dict()

    sort_map = dict()
    #if a sort file is specified, open it, and construct a hierarchical list of sites and cells
    if sort_file is not None:
        print 'Importing sort data from %s' % sort_file
        sort_map = read_sort_file(sort_file)

    #loop over all sites and protocols
    for site_key,site_group in hf_unsorted['sites'].iteritems():
        print 'Importing site and protocol named %s...' % site_key
        lfp_group = site_group['lfp']
        spikes_group = site_group['spikes']
        
        site_name = site_group.attrs['site']
        site_protocol = site_group.attrs['protocol']
        site_rdepth = int(site_group.attrs['rdepth'])
        site_ldepth = int(site_group.attrs['ldepth'])
        start_date = site_group.attrs['start_date']
        end_date = site_group.attrs['end_date']

        start_date_py = parser.parse(start_date)
        end_date_py = parser.parse(end_date)

        start_date = datetime.datetime.strptime(str(start_date), '%Y-%m-%d %H:%M:%S')
        end_date = datetime.datetime.strptime(str(end_date), '%Y-%m-%d %H:%M:%S')

        site_name_and_depth = (site_name, site_rdepth, site_ldepth)

        #create site specific objects if this is a new site
        if site_name_and_depth not in sites_by_name_and_depth:
            print 'Creating a new site named %s...' % site_name
            #create site object
            site = Site(f)
            site.experiment_id = experiment.id
            site.name = site_name
            site.timezone = 'PST'
            site.rdepth = site_rdepth
            site.ldepth = site_ldepth
            site.start_date = calendar.timegm(start_date_py.timetuple())
            site.end_date = calendar.timegm(end_date_py.timetuple())
            site.save()

            #create electrode objects
            electrode_map = dict()
            spikes_map = dict()
            for enum in lfp_group.keys():
                enum = int(enum)
                if enum in electrode_map:
                    print 'Electrode %d is represented twice for LFP group in site %s. This is probably weird.' % (enum, site_name)
                    e = electrode_map[enum]
                else:
                    e = ElectrodeData(f)
                    e.site_id = site.id
                    e.number = enum
                    e.geometry_id = 0  # adjust this if we implement electrode geometries
                    e.save()
                electrode_map[enum] = e

                #create lfp object
                lfp = LFP(f)
                lfp.electrode_id = e.id
                lfp.sample_rate = float(lfp_group.attrs['sample_rate'])
                lfp.save()

                #create cells
                if sort_file is None:
                    #create multi-unit object
                    cell = Cell(f)
                    cell.name = '%s_L%dR%d_e%d_1' % (site_name, site_ldepth, site_rdepth, e.number)
                    cell.electrode_id = e.id
                    cell.sort_type = 'multi'
                    cell.save()
                    spikes_map[cell.id] = {'sum': list(), 'squared': list(), 'count': list()}

                else:
                    #get the cells that have been sorted for this site/electrode combination, create objects for them
                    if e.number in sort_map[site_name_and_depth]:
                        cell_list = sort_map[site_name_and_depth][e.number]
                        for cmap in cell_list:
                            cell = Cell(f)
                            cell.name = cmap['name']
                            cell.electrode_id = e.id
                            cell.sort_type = str(cmap['sort_type'])
                            cell.extra_info = 'sort_file=%s;sort_code=%d' % (cmap['sort_file_name'], cmap['sort_code'])
                            cell.save()
                            spikes_map[cell.id] = {'sum': list(), 'squared': list(), 'count': list()}
                    else:
                        print '\tNo cells found for electrode %d' % e.number

            spike_shapes_by_site[site_name_and_depth] = spikes_map
            electrodes_by_site[site_name_and_depth] = electrode_map
            sites_by_name_and_depth[site_name_and_depth] = site
            
        else:
            #the site has already been created
            site = sites_by_name_and_depth[site_name_and_depth]
            electrode_map = electrodes_by_site[site_name_and_depth]
            spikes_map = spike_shapes_by_site[site_name_and_depth]
            
        #create a protocol
        print '\tImporting protocol data named %s...' % site_protocol
        protocol = Protocol(f)
        protocol.name = site_protocol
        protocol.site_id = site.id
        protocol.start_time = calendar.timegm(start_date.utctimetuple())
        protocol.end_time = calendar.timegm(end_date.utctimetuple())
        protocol.save()        

        #import LFP data for this site and protocol        
        for enum,electrode in electrode_map.iteritems():
            print '\tImporting LFPs for electrode %d' % enum
            lfppr = LFPProtocolResponse(f)
            lfppr.lfp_id = electrode.lfp.id
            lfppr.protocol_id = protocol.id
            lfppr.save()
            lfppr.waveform = np.array(lfp_group['%d' % enum])

        #get the snippets for this site and protocol
        snippets = np.array(site_group['snippets'])

        #aggregate electrodes and their sorts
        electrode_sorts = dict()
        for unit_key,unit_group in spikes_group.iteritems():
            enumber,usort_code = [int(x) for x in unit_key.split(',')]
            if enumber not in electrode_sorts:
                electrode_sorts[enumber] = list()
            electrode_sorts[enumber].append(usort_code)

        #import the units for this site and protocol
        for electrode_num,esorts in electrode_sorts.iteritems():
            print '\tImporting spikes for electrode %d...' % electrode_num
            electrode = electrode_map[electrode_num]

            #get spike ids and times merged across sorts for this electrode
            spike_ids,spike_times = get_merged_spikes(spikes_group, electrode_num, esorts)
            print '\tFound %d spike times, %d spike ids' % (len(spike_times), len(spike_ids))

            #build a map between spike ids and spike times
            spike_id2time = dict()
            for sid,stime in zip(spike_ids, spike_times):
                spike_id2time[sid] = stime

            for cell in electrode.cells:  # for unsorted data, there is only one unit per electrodes
                #create the cell object
                cellpr = CellProtocolResponse(f)
                cellpr.protocol_id = protocol.id
                cellpr.cell_id = cell.id
                cellpr.save()

                #set the spikes for the cell
                if sort_file is None:
                    cellpr.spikes = np.array(spike_times)
                    unit_snippets = snippets[list(np.array(spike_ids)-1), :]
                else:
                    #find the corresponding cell properties in sort_map
                    clist = list()
                    einfo = cell.get_extra_info()
                    for c in [c for c in sort_map[site_name_and_depth][electrode_num] if c['name'] == cell.name]:
                        if c['sort_code'] == int(einfo['sort_code']):
                            clist.append(c)

                    if len(clist) == 0:
                        print '\tCannot find cell corresponding to name %s and sort code %d' % (cell.name, einfo['sort_code'])
                        continue
                    assert len(clist) == 1
                    cmap = clist[0]
                    cell_spike_ids = cmap['spike_ids']
                    cell_spike_times = list()
                    nmissing = 0
                    for sid in cell_spike_ids:
                        if sid not in spike_id2time:
                            #print "Missing spike id for cell %s: %d" % (cell.name, sid)
                            nmissing += 1
                        else:
                            cell_spike_times.append(spike_id2time[sid])
                    if nmissing > 0:
                        print '\tCell %s is missing %d spikes' % (cell.name, nmissing)
                    cell_spike_times.sort()
                    print '\t\tSetting %d spikes for cell %s, protocol %s, sort code %d, sort type %s' % \
                          (len(cell_spike_times), cellpr.cell.name, cellpr.protocol.name,  int(einfo['sort_code']), cellpr.cell.sort_type)
                    cellpr.spikes = np.array(cell_spike_times)
                    unit_snippets = snippets[[x-1 for x in cell_spike_ids], :]

                #accumulate snippet data
                spikes_map[cell.id]['sum'].append(unit_snippets.sum(axis=0))
                spikes_map[cell.id]['squared'].append((unit_snippets**2).sum(axis=0))
                spikes_map[cell.id]['count'].append(len(spike_ids))

        #clean up
        del snippets

        #import stim data
        print '\tImporting stim data for this site...'
        import_stim_data(f, protocol, site_group, stims_by_number)

    #aggregate snippet data for each unit
    print 'Aggregating snippet data for each unit...'
    for spike_map in spike_shapes_by_site.values():
        for cell_id, spike_dict in spike_map.iteritems():
            c = Cell(f)
            c.from_id(cell_id)
            count = float(np.sum(spike_dict['count']))
            if count > 1:
                spike_shape_mean = np.sum(spike_dict['sum'], axis=0) / count
                c.spike_shape_mean = spike_shape_mean
                c.spike_shape_std = ((count / count - 1) * (np.sum(spike_dict['squared'], axis=0) / count - spike_shape_mean ** 2)) ** .5

    hf_unsorted.close()

    print 'Creating spectrograms...'
    create_spectrograms(f)

    print 'Creating conditioned responses...'
    create_sound_conditioned_responses(f)

    f.close()


def get_merged_spikes(spikes_group, electrode_number, esorts):
    spike_ids = list()
    spike_times = list()
    for sort_code in esorts:
        gname = '%d,%d' % (electrode_number, sort_code)
        spike_ids.extend(np.array(spikes_group[gname]['spike_ids']))
        spike_times.extend(np.array(spikes_group[gname]['spike_times']))
    return spike_ids,spike_times


def md5_for_file(file_name, block_size=2**20):
    f = open(file_name, 'r')
    md5 = hashlib.md5()
    while True:
        data = f.read(block_size)
        if not data:
            break
        md5.update(data)
    f.close()
    return md5.hexdigest()


def get_electrode_data(f):

    left_electrodes = raw_input('Enter range of electrodes on the LEFT hemisphere (start-finish):')
    right_electrodes = raw_input('Enter range of electrodes on the RIGHT hemisphere (start-finish):')

    lstart,lend = [int(x) for x in left_electrodes.split('-')]
    left_electrodes = range(lstart, lend+1)
    rstart,rend = [int(x) for x in right_electrodes.split('-')]
    right_electrodes = range(rstart, rend+1)

    geom = ElectrodeGeometry(f)
    geom.name = raw_input('Please enter the name/type of the electrode array:')
    #ncols = int(raw_input('Please enter the number of electrodes in the grid that run LEFT to RIGHT:'))
    #nrows = int(raw_input('Please enter the number of electrodes in the grid that run CAUDAL to ROSTRAL:'))
    #geom.shape = [nrows, ncols]
    nrows = 8
    ncols = 4
    geom.shape = [nrows, ncols]
    geom.save()

    ecoords = np.zeros(geom.shape, dtype='int')

    e = ElectrodeData(f)
    elist = e.get_all()
    electrode_numbers = np.unique([e.number for e in elist])
    electrode_numbers.sort()
    for edata in elist:
        #set the hemisphere for the electrode
        if edata.number in left_electrodes:
            edata.hemisphere = 'L'
        if edata.number in right_electrodes:
            edata.hemisphere = 'R'
        edata.geometry_id = geom.id
        edata.save()

    tr_right = int(raw_input('Enter the top right (most rostral) electrode number in the RIGHT hemisphere: '))
    tr_left = int(raw_input('Enter the top right (most rostral) electrode number in the LEFT hemisphere: '))

    #construct a template for electrode numbers that makes sense for an 8x2 array
    coord_template = (np.arange(0, 16) + 1).reshape(ncols/2, nrows).transpose()
    coord_template[:, 1] = coord_template[::-1, 1]

    #set the grid coordinates for electrodes in the right hemisphere
    right_offset = (tr_right > 16)*16
    if tr_right % 16 == 1:
        ecoords[:, [2, 3]] = np.fliplr(coord_template) + right_offset
    elif tr_right % 16 == 9:
        ecoords[:, [2, 3]] = np.flipud(coord_template) + right_offset
    else:
        raise Exception('It does not make sense for the top right electrode in the right hemisphere to be electrode %d' % tr_right)

    #set the grid coordinates for electrodes in the left hemisphere
    left_offset = (tr_left > 16)*16
    if tr_left % 16 == 1:
        ecoords[:, [0, 1]] = np.fliplr(coord_template) + left_offset
    elif tr_left % 16 == 9:
        ecoords[:, [0, 1]] = np.flipud(coord_template) + left_offset
    else:
        raise Exception('It does not make sense for the top right electrode in the left hemisphere to be electrode %d' % tr_left)

    #save the grid coordinate data
    geom.coordinates = ecoords
