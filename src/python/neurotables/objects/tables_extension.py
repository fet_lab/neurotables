import inspect
import numpy as np
import tables

ONE2MANY = 1
MANY2ONE = 2
ALL_RELATION_TYPES = [ONE2MANY, MANY2ONE]

IdCol = tables.UInt32Col


class One2ManyRelation(object):
    def __init__(self, parent_class, attr_name, child_class, id_colname, one2one=False):
        self.parent_class = parent_class
        self.child_class = child_class
        self.id_colname = id_colname
        self.attr_name = attr_name
        self.table_name = None
        self.table = None
        self.one2one = one2one

    def load(self, inst):
        #get child class table
        if self.table is None:
            group_name = self.child_class.__name__
            self.table_name = '%s/meta' % group_name
            try:
                self.table = inst.f.get_node(inst.f.root, self.table_name)
            except tables.NoSuchNodeError:
                if self.one2one:
                    return None
                return []

        #query table
        parent_id = inst.id
        vals = []
        for row in self.table.read_where('%s == %d' % (self.id_colname, parent_id)):
            c = self.child_class(inst.f)
            c.from_row(row)
            vals.append(c)

        if self.one2one and len(vals) > 0:
            return vals[0]

        return vals

class Many2OneRelation(object):
    def __init__(self, child_class, attr_name, id_colname, parent_class):
        self.parent_class = parent_class
        self.child_class = child_class
        self.id_colname = id_colname
        self.attr_name = attr_name
        self.table_name = None
        self.table = None

    def load(self, inst):
        #get parent class table
        if self.table is None:
            group_name = self.parent_class.__name__
            self.table_name = '%s/meta' % group_name
            try:
                self.table = inst.f.get_node(inst.f.root, self.table_name)
            except tables.NoSuchNodeError:
                return None

        #query table
        parent_id = getattr(inst, self.id_colname)
        rows = [row for row in self.table.read_where('id == %d' % parent_id)]
        if len(rows) > 1:
            raise Exception('More than one parent object of class %s, id=%d found for %s.%s' % \
                            self.parent_class.__name__, parent_id, self.child_class.__name__, self.attr_name)
        if len(rows) == 0:
            return None

        pinst = self.parent_class(inst.f)
        pinst.from_row(rows[0])

        return pinst


class TablesBackedRelations(object):
    def __init__(self):
        self.relation_types = {}
        self.relations = {}
        for rt in ALL_RELATION_TYPES:
            self.relations[rt] = {}

    def has_relation(self, cls, attr_name):
        type_key = self.get_type_key(cls, attr_name)
        return type_key in self.relation_types

    def load_relation(self, inst, attr_name):
        type_key = self.get_type_key(inst.__class__, attr_name)
        if type_key not in self.relation_types:
            raise Exception('No relation for class %s and attribute name %s' % (inst.__class__.__name__, attr_name))
        rt = self.relation_types[type_key]
        rkey = self.get_relation_key(rt, inst.__class__, attr_name)
        rel = self.relations[rt][rkey]

        return rel.load(inst)


    def one2many(self, parent_class, attr_name, child_class, id_colname, backproj_attr_name=None, one2one=False):
        type_key = self.get_type_key(parent_class, attr_name)
        self.relation_types[type_key] = ONE2MANY
        rel = One2ManyRelation(parent_class, attr_name, child_class, id_colname, one2one=one2one)
        rel.key = self.get_relation_key(ONE2MANY, parent_class, attr_name)
        self.relations[ONE2MANY][rel.key] = rel

        if backproj_attr_name is not None:
            type_key = self.get_type_key(child_class, backproj_attr_name)
            self.relation_types[type_key] = MANY2ONE
            rel = Many2OneRelation(child_class, backproj_attr_name, id_colname, parent_class)
            rel.key = self.get_relation_key(MANY2ONE, child_class, backproj_attr_name)
            self.relations[MANY2ONE][rel.key] = rel

    def get_relation_key(self, rel_type, parent_class, attr_name):
        if rel_type in [ONE2MANY, MANY2ONE]:
            return '%s.%s' % (parent_class.__name__, attr_name)
        return None

    def get_type_key(self, cls, attr_name):
        return '%s.%s' % (cls.__name__, attr_name)


tj_relations = TablesBackedRelations()


class DatasetCol(object):

    def __init__(self, properties=dict()):
        self.properties = properties


class TablesBackedRow(object):
    id = IdCol()


class TablesBackedObject(object):

    def __init__(self, file_handle):

        #construct the group name to be the same name as the subclass
        class_name = self.__class__.__name__
        self.group_name = class_name

        self.dataset_properties = dict()

        #dynamically create a row class from static member variables
        self.row_class = self.introspect_and_create_row_class()

        self.f = file_handle
        self.data_group = None
        try:
            self.group = self.f.get_node(self.f.root, self.group_name)
        except tables.NoSuchNodeError:
            self.group = self.f.createGroup(self.f.root, self.group_name)
        try:
            self.table = self.f.get_node(self.group, 'meta')
        except tables.NoSuchNodeError:
            self.table = self.f.createTable(self.group, 'meta', self.row_class)
        try:
            self.objects_group = self.f.get_node(self.group, 'objects')
        except tables.NoSuchNodeError:
            self.objects_group = self.f.createGroup(self.group, 'objects')

        for cname in self.row_class.columns.keys():
            setattr(self, cname, None)

    def introspect_and_create_row_class(self):
        """
            Get the static column attributes for this class and dynamically construct
            a row class with those attributes.
        """

        filter_func = lambda x: not inspect.ismethod(x) and \
                                not inspect.isbuiltin(x) and \
                                not inspect.isroutine(x)

        #go through the member variables of the class object, and find
        #ones whose type name ends in "Col"
        internal_row_class = self.__class__.Row
        col_vals = dict()
        for mname,mval in inspect.getmembers(internal_row_class, filter_func):
            tname = type(mval).__name__
            if tname.endswith('Col') and mname not in col_vals:
                if tname == 'DatasetCol':
                    self.add_dataset_property(mname, mval.properties)
                else:
                    col_vals[mname] = mval

        row_class_name = '%s.%sRow' % (self.__module__, self.__class__.__name__)
        #create dynamic class
        row_class = type(row_class_name, (tables.IsDescription, ), col_vals)
        return row_class

    def delete(self):
        if self.id is None:
            return
        row_index = self.get_row_index()
        self.table.removeRows(row_index)
        self.table.flush()
        dgname = get_data_group_name(self.id)
        dg = getattr(self.objects_group, dgname)
        dg._f_remove(recursive=True)

    def get_all(self):
        olist = []
        for row in self.table.iterrows():
            o = self.__class__(self.f)
            o.from_row(row)
            olist.append(o)
        return olist

    def get_dataset(self, attr_name):
        member_attr_name = '_%s' % attr_name
        props = self.dataset_properties[attr_name]
        if getattr(self, member_attr_name) is None and self.data_group is not None:
            try:
                ds = self.f.get_node(self.data_group, attr_name)
                setattr(self, member_attr_name, ds.read())
            except tables.NoSuchNodeError:
                if 'compute' in props:
                    cfunc_name = props['compute']
                    cfunc = getattr(self, cfunc_name)
                    fval = cfunc()
                    self.set_dataset(attr_name, fval)
                else:
                    print 'No such dataset %s found' % attr_name

        return getattr(self, member_attr_name)


    def set_dataset(self, attr_name, val):
        member_attr_name = '_%s' % attr_name
        if self.id is None:
            self.save()
        try:
            props = self.dataset_properties[attr_name]
            if 'type' not in props or props['type'] != 'VLArray':
                ds = self.f.createArray(self.data_group, attr_name, val)
            else:
                if props['type'] == 'VLArray':
                    ds = self.f.createVLArray(self.data_group, attr_name, tables.Float32Atom())
                for v in val:
                    if len(v.shape) == 0:
                        v = []
                    ds.append(np.array(v).squeeze())
        except tables.ClosedNodeError:
            print 'Cannot set %s, it already exists!' % attr_name
        setattr(self, member_attr_name, ds.read())

    def add_dataset_property(self, attr_name, props=dict()):
        member_attr_name = '_%s' % attr_name
        setattr(self, member_attr_name, None)
        self.dataset_properties[attr_name] = props

    def from_id(self, id):
        return self.from_prop('id', id)

    def from_prop(self, attr_name, attr_value):
        rows = self._prop_query(attr_name, attr_value)
        if len(rows) == 0:
            return False
        if len(rows) > 1:
            raise Exception('%d rows found with %s=%s in table /%s/meta' % (len(rows), attr_name, str(attr_value), self.group_name))
        self.from_row(rows[0])
        return True

    def from_name(self, name):
        return self.from_prop('name', name)


    def list_by_prop(self, attr_name, attr_value):

        rows = self._prop_query(attr_name, attr_value)
        if len(rows) == 0:
            return []

        obj_list = []
        for r in rows:
            o = self.__class__(self.f)
            o.from_row(r)
            obj_list.append(o)
        return obj_list

    def _prop_query(self, attr_name, attr_value):
        try:
            if type(attr_value).__name__ in ['str', 'string_', 'unicode']:
                qstr = '%s == \'%s\'' % (attr_name, attr_value)
            else:
                qstr = '%s == %d' % (attr_name, attr_value)
        except TypeError as te:
            print '[from_prop] type \'%s\' not understood!' % type(attr_value).__name__
            return False
        rows = [row for row in self.table.read_where(qstr)]
        return rows


    def from_row(self, row):
        for cname in self.row_class.columns.keys():
            cval = None
            try:
                cval = row[cname]
            except:
                pass
            setattr(self, cname, cval)
        self.data_group = self.f.get_node(self.objects_group, get_data_group_name(self.id))

    def get_row(self):
        rows = [r for r in self.table.where('id == %d' % self.id)]
        if len(rows) > 1:
            raise Exception('[save] %d rows found with id %d in table /%s/meta' % (len(rows), id, self.group_name))
        if len(rows) == 0:
            raise Exception('[save] No row with id %d in table /%s/meta' % (self.id, self.group_name))
        return rows[0]

    def get_row_index(self):
        rcoords = self.table.getWhereList('id == %d' % self.id)
        if len(rcoords) == 0:
            raise Exception('[save] No row with id %d in table /%s/meta' % (self.id, self.group_name))
        if len(rcoords) > 1:
            raise Exception('[save] %d rows found with id %d in table /%s/meta' % (len(rcoords), id, self.group_name))
        row_index = rcoords[0]
        return row_index

    def save(self):
        if self.id is None:
            self.id = get_next_table_id(self.table)
            row = self.table.row
            for cname in self.row_class.columns.keys():
                row[cname] = getattr(self, cname)
            row.append()
            self.data_group = self.f.createGroup(self.objects_group, get_data_group_name(self.id))
        else:
            #get row index in table
            row_index = self.get_row_index()

            #get table description to figure out column positions
            tdesc = self.table.description._v_colObjects

            #overwrite column values with new values
            row = [None]*len(tdesc)
            for cname in self.row_class.columns.keys():
                col = tdesc[cname]
                col_index = col._v_pos
                cval = getattr(self, cname)
                row[col_index] = cval
                #print 'Set %s=%s for id=%d, col_index=%d' % (cname, cval, self.id, col_index)
            #print row
            #self.table.modifyRows(start=row_index, rows=[row])
            self.table[row_index] = tuple(row)

        self.table.flush()

    def __getattr__(self, attr_name):
        if tj_relations.has_relation(self.__class__, attr_name):
            setattr(self, attr_name, tj_relations.load_relation(self, attr_name))
            return getattr(self, attr_name)
        elif attr_name in self.dataset_properties:
            return self.get_dataset(attr_name)
        else:
            raise AttributeError('%s instance has no attribute \'%s\'' % (self.__class__.__name__, attr_name))

    def __setattr__(self, attr_name, value):
        if attr_name in ['dataset_properties', 'group_name', 'row_class']:
            #prevents some wicked recursion
            object.__setattr__(self, attr_name, value)
            return
        if attr_name in self.dataset_properties:
            props = self.dataset_properties[attr_name]
            if 'compute' in props:
                raise Exception('Property %s is read-only!' % attr_name)
            self.set_dataset(attr_name, value)
        else:
            object.__setattr__(self, attr_name, value)

    def update_columns(self):
        """ Add a new column to an already-existing table. Can be expensive! Code
            modified from: http://www.pytables.org/trac-bck/browser/PyTablesPro/trunk/examples/add-column.py.
            Assumes the new column is already defined in the TableObject's TableRow class.
        """

        #get description of table
        descr = self.table.description._v_colObjects

        #find if there are any new columns
        new_cols = {}
        for cname,ctype in self.row_class.columns.iteritems():
            if cname not in descr:
                new_cols[cname] = ctype

        if len(new_cols) == 0:
            return

        #create a new table to overwrite old
        table2 = self.f.createTable(self.group, 'meta2', self.row_class)

        #fill in the rows
        for i in xrange(self.table.nrows):
            table2.row.append()

        #flush rows to disk
        table2.flush()

        #copy columns of source table to destination
        for col in descr:
            destcols = getattr(table2.cols, col)
            destcols[:] = getattr(self.table.cols, col)[:]

        #remove the original table
        self.table.remove()

        #change the name of the table copy
        table2.move(self.group, 'meta')
        self.table = table2


def get_data_group_name(id):
    return '_%d' % id

def get_next_table_id(tbl):
    ids = np.array([row['id'] for row in tbl.iterrows()])
    if len(ids) == 0:
        return 1
    return ids.max()+1


def get_class( kls ):
    parts = kls.split('.')
    module = ".".join(parts[:-1])
    m = __import__( module )
    for comp in parts[1:]:
        m = getattr(m, comp)
    return m