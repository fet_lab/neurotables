import argparse
import os
import h5py
import joblib
import operator

import numpy as np

import matplotlib.pyplot as plt
import matplotlib.cm as cm

from slurm_tools import SlurmBot

import tables
import time
from lasp.sound import plot_spectrogram

from lasp.timefreq import gaussian_stft,postprocess_spectrogram
from neurotables.objects.cells import Site


def compute_timefreq(f, electrode_num, site_id, protocol_name, output_file=None):

    stime = time.time()

    s = Site(f)
    assert s.from_id(site_id)
    assert len(s.electrodes) > 0
    assert electrode_num-1 < len(s.electrodes)

    #get an electrode
    elist = [e for e in s.electrodes if e.number == electrode_num]
    assert len(elist) == 1
    e = elist[0]
    lfp_pr = e.get_lfp_protocol_response(protocol_name)
    assert lfp_pr is not None

    sr = lfp_pr.lfp.sample_rate

    #zscore the LFP
    lfp = lfp_pr.waveform
    lfp -= lfp.mean()
    lfp /= lfp.std(ddof=1)

    #compute the spectrogram
    window_sizes = [0.050, 0.100, 0.200, 0.400, 0.800]
    increment = 0.025

    output_vals = list()
    for win_size in window_sizes:
        t,freq,tf,rms = gaussian_stft(lfp, sr, win_size, increment)
        output_vals.append( (t, freq, tf) )

    #persist the data to a file
    if output_file is not None:
        hf = h5py.File(output_file, 'w')
        hf.attrs['site_id'] = site_id
        hf.attrs['protocol'] = protocol_name
        hf.attrs['electrode'] = electrode_num
        for k,(t,freq,tf) in enumerate(output_vals):
            wgrp = hf.create_group('window_%d' % k)
            wgrp.attrs['window_size'] = window_sizes[k]
            wgrp.attrs['increment'] = increment
            wgrp['t'] = t
            wgrp['freq'] = freq
            wgrp['tf'] = tf
        hf.close()

    etime = time.time() - stime
    print 'Elapsed time: %d seconds' % etime


def run_all_local(hdf5_cell_file='/auto/k8/fdata/mschachter/tdt_continuous/GreBlu9508M/GreBlu9508M_tabled.h5',
                   output_dir='/auto/k8/fdata/mschachter/analysis/lfp_spectrogram', num_cpus=4):

    #run all electrodes across sites and protocols
    f = tables.open_file(hdf5_cell_file, mode='r')
    s = Site(f)
    for site in s.get_all():
        enumbers = [e.number for e in site.electrodes]
        for protocol in site.protocols:
            for enumber in enumbers:
                print 'Running site %d, protocol %s, electrode %d' % (site.id, protocol.name, enumber)
                output_file = os.path.join(output_dir, 'lfp_spectrogram_Site%d_%s_e%d.h5' % (site.id, protocol.name, enumber))
                compute_timefreq(f, enumber, site.id, protocol.name, output_file=output_file)


def run_all_slurm(hdf5_cell_file='/auto/k8/fdata/mschachter/tdt_continuous/GreBlu9508M/GreBlu9508M_tabled.h5',
                  ve_script='/auto/fhome/mschachter/virtualenv_cluster/pyslurm_ve.sh',
                  source_dir='/auto/fhome/mschachter/tlab/neurotables/src/python',
                  output_dir='/auto/k8/fdata/mschachter/analysis/lfp_spectrogram',
                  log_dir='/auto/k8/fdata/mschachter/analysis/lfp_spectrogram/log'):


    #get all Site and Protocol information from the cell file
    protocols_by_site = dict()
    f = tables.open_file(hdf5_cell_file, mode='r')
    s = Site(f)
    for site in s.get_all():
        protocols_by_site[site.id] = [p.name for p in site.protocols]
    f.close()

    #make a flattened list of site,protocol combinations
    site_protocols = list()
    for site_id,protocols in protocols_by_site.iteritems():
        for pname in protocols:
            site_protocols.append( (site_id, pname) )

    #generate a list of electrodes to be run for each job
    nelectrodes = 32
    ncpus_per_job = 2
    njobs_per_combo = nelectrodes / ncpus_per_job
    electrodes_per_job = list()
    for k in range(njobs_per_combo):
        e1 = k*2
        e2 = e1 + 1
        electrodes_per_job.append( (e1, e2) )

    sb = SlurmBot()
    job_list = list()

    for site_id,protocol in site_protocols:
        for e1,e2 in electrodes_per_job:
            args = list()
            args.append(ve_script)
            args.append(source_dir)
            args.append('neurotables/jobs/lfp/timefreq.py')
            args.append('--cell_file')
            args.append(hdf5_cell_file)
            args.append('--site_id')
            args.append('%d' % site_id)
            args.append('--protocol_name')
            args.append(protocol)
            args.append('--output_dir')
            args.append(output_dir)

            args.append('--electrodes')
            args.append('%d,%d' % (e1,e2))

            log_file = os.path.join(log_dir, 'lfp_spectrogram_Site%d_%s_e%d_e%d_%%j.log' % \
                                             (site_id, protocol, e1, e2))
            sparams = dict()

            #sparams['extra_code'] = 'sbcast -C %s %s' % (f.filename, tmp_f_path)
            sparams['partition'] = 'all'
            sparams['out'] = log_file
            sparams['err'] = log_file
            sparams['cpus'] = 2
            sparams['mem'] = '8000M'

            job_list.append( (args, sparams) )

    for args,sparams in job_list:
        sb.add(args, sparams)
    num_jobs = len(sb.jobs)
    print 'Running %d jobs' % num_jobs
    sb.run()
    return job_list


def visualize_output(site_id, protocol_name, electrode_num, t1, t2, output_dir='/auto/k8/fdata/mschachter/analysis/lfp_spectrogram', edesc=''):

    output_file = os.path.join(output_dir, 'lfp_spectrogram_Site%d_%s_e%d%s.h5' % (site_id, protocol_name, electrode_num, edesc))
    if not os.path.exists(output_file):
        print 'Missing output file: %s' % output_file
        return

    print 'Reading file %s' % output_file

    #verify the contents of hte flie
    hf = h5py.File(output_file, 'r')

    site_id_from_file = hf.attrs['site_id']
    assert site_id_from_file == site_id

    protocol_name_from_file = hf.attrs['protocol']
    assert protocol_name_from_file == protocol_name

    electrode_num_from_file = hf.attrs['electrode']
    assert electrode_num_from_file == electrode_num

    #sort a list of window sizes
    win_sizes = list()
    for key in hf.keys():
        win_sizes.append( (key, hf[key].attrs['increment'], hf[key].attrs['window_size']) )
    win_sizes.sort(key=operator.itemgetter(-1))

    #make plots for each window size
    plt.figure()
    nsp = len(win_sizes)
    for k,(key,inc,win_size) in enumerate(win_sizes):
        spec_t = np.array(hf[key]['t'])
        spec_f = np.array(hf[key]['freq'])
        spec_tf = np.array(hf[key]['tf'])

        si = int(t1 / inc)
        ei = int(t2 / inc)

        #plot power spectrum
        ax = plt.subplot(nsp, 2, k*2 + 1)
        tf_slice = spec_tf[:, si:ei]
        log_power_spec = postprocess_spectrogram(np.abs(tf_slice))
        plot_spectrogram(spec_t[si:ei], spec_f, log_power_spec, ax=ax, colorbar=False, colormap=cm.afmhot_r)
        plt.xlabel('')
        ax.yaxis.set_label_position("right")
        plt.ylabel('%dms' % int(win_size*1000))

        #plot phase
        ax = plt.subplot(nsp, 2, k*2 + 2)
        phase = np.angle(tf_slice)
        plot_spectrogram(spec_t[si:ei], spec_f, phase, ax=ax, colorbar=False, colormap=cm.afmhot_r)
        plt.xlabel('')

    hf.close()


if __name__ == '__main__':

    #to use multiple cores
    #os.system("taskset -p 0xff %d" % os.getpid())

    ap = argparse.ArgumentParser(description='Compute the spectrogram of an LFP.')
    ap.add_argument('--cell_file', type=str, required=True, help='The HDF5 file containing cell information.')
    ap.add_argument('--site_id', type=int, required=True, help='The Site ID')
    ap.add_argument('--protocol_name', type=str, required=True, help='The name of the protocol')
    ap.add_argument('--electrodes', type=str, required=True, help='Comma separated list of electrode pairs (e.g. 1,2,3,4,...)')
    ap.add_argument('--output_dir', type=str, required=True, help='The output directory')
    ap.add_argument('--edesc', type=str, required=False, default=None, help='Extra text to append to the output file name.')
    argvals = ap.parse_args()

    edesc = ''
    if argvals.edesc is not None:
        edesc = '_%s' % argvals.edesc

    def pfunc(electrode_num):
        f = tables.open_file(argvals.cell_file, 'r')
        output_file = os.path.join(argvals.output_dir, 'lfp_spectrogram_Site%d_%s_e%d%s.h5' % (argvals.site_id, argvals.protocol_name, electrode_num, edesc))
        compute_timefreq(f, electrode_num, argvals.site_id, argvals.protocol_name, output_file=output_file)

    electrodes = [int(x) for x in argvals.electrodes.split(',')]
    print '# of jobs: %d' % len(electrodes)
    print electrodes
    p = joblib.Parallel(n_jobs=len(electrodes), verbose=5)
    jobs = list()
    for enumber in electrodes:
        d = joblib.delayed(pfunc)
        jobs.append(d(enumber))
    p(jobs)
