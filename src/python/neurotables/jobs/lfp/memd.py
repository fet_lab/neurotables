import argparse
import os
import h5py
import joblib
import tables

import numpy as np
from slurm_tools import SlurmBot

from neurotables.analysis.lfp.memd import run_memd,IMFPlotter
from neurotables.objects.cells import Site


def split_site_into_segments(f, site_id=1, protocol_name='Call1', njobs=10, overlap=10.0):
    """
        MEMD is kind of expensive, so to speed things up, we split a single site into
        overlapping time segments, and run MEMD on each one separately.

        Args:
            f (pytables file object): an open file handle to the pytables file
            site_id (int): id of the site
            protocol_name (str): name of the protocol to apply MEMD to
            jobs (int): the number of jobs to run in parallel, the number of segments to split the protocol response into
            overlap (float): time in seconds that segments overlap, used to deal with end effects

        Returns:

    """

    site = Site(f)
    assert site.from_id(site_id)

    #peek into the first LFP to get the recording end time
    lfp_pr = site.electrodes[0].get_lfp_protocol_response(protocol_name)
    sample_rate = lfp_pr.lfp.sample_rate
    lfp = lfp_pr.waveform
    duration = len(lfp) / sample_rate

    time_per_segment = duration / njobs
    print 'duration=%0.2fs, time_per_segment=%0.2f' % (duration, time_per_segment)

    #determine start and end time of each segment
    segment_times = list()
    for k in range(njobs):
        start_time = k*time_per_segment
        if k > 0:
            start_time -= overlap
        end_time = min(duration, (k+1)*time_per_segment)
        segment_times.append( (start_time, end_time) )

    return segment_times


def merge_segments(f, site_id=1, protocol_name='Call1', hemispheres=['L'], njobs=10, overlap=10.0, output_dir='/auto/k8/fdata/mschachter/analysis/memd'):

    seg_times = split_site_into_segments(f, site_id=site_id, protocol_name=protocol_name, njobs=njobs, overlap=overlap)

    hemi_str = ','.join(hemispheres)
    output_files = list()

    lowest_start_time = np.inf
    highest_end_time = -np.inf
    imfp = None

    for start_time,end_time in seg_times:
        output_file = os.path.join(output_dir, 'imfs_Site%d_%s_%s_start%0.6f_end%0.6f.h5' % \
                                                       (site_id, protocol_name, hemi_str,
                                                        start_time, end_time))
        if not os.path.exists(output_file):
            print 'Missing file %s, breaking!' % output_file
            break
        imfp = IMFPlotter.from_file(f, output_file, load_data=False)
        lowest_start_time = min(lowest_start_time, imfp.start_time)
        highest_end_time = max(highest_end_time, imfp.end_time)

        output_files.append(output_file)
    print 'lowest_start_time=%0.6f, highest_end_time=%0.6f' % (lowest_start_time, highest_end_time)

    #construct a matrix to hold all the data
    nelectrodes = len(imfp.index2electrode)
    nimfs = imfp.nimfs
    t1 = int(lowest_start_time*imfp.sample_rate)
    t2 = int(highest_end_time*imfp.sample_rate)
    d = t2 - t1
    imfs = np.zeros([nimfs, nelectrodes, d])

    for k,ofile in enumerate(output_files):
        print 'Reading file %s...' % ofile

        imf_plotter = IMFPlotter.from_file(f, ofile)
        si = int(imf_plotter.start_time*imf_plotter.sample_rate)
        ei = int(imf_plotter.end_time*imf_plotter.sample_rate)

        fnimfs,fnelectrodes,fnt = imf_plotter.imfs.shape
        assert fnimfs == nimfs
        assert fnelectrodes == nelectrodes
        assert fnt == ei - si

        if k > 0:
            #find the time that the overlap ends
            overlap_size = int(overlap*imf_plotter.sample_rate)
        else:
            overlap_size = 0

        #get the non-overlapping portion of the IMF
        imfs[:, :, (si+overlap_size):ei] = imf_plotter.imfs[:, :, overlap_size:]

        if k > 0:
            #linearly blend the overlapping segments from this output file and the previous one
            right_blend = np.arange(overlap_size, dtype='float') / (overlap_size-1) # goes from 0 to 1
            left_blend = right_blend[::-1]                                          # goes from 1 to 0
            print 'k=%d, overlap_size=%d, si=%d, ei=%d' % (k, overlap_size, si, ei)

            imfs[:, :, si:(si+overlap_size)] = left_blend*imfs[:, :, si:(si+overlap_size)] + right_blend*imf_plotter.imfs[:, :, :overlap_size]

        del imf_plotter

    #write the data to an output file
    output_file = os.path.join(output_dir, 'imfs_Site%d_%s_%s.h5' % (imfp.site_id, imfp.protocol_name, ','.join(imfp.hemispheres)))
    print 'Writing to file %s' % output_file

    hf = h5py.File(output_file, 'w')
    hf.attrs['start_time'] = 0.0
    hf.attrs['end_time'] = highest_end_time

    hf.attrs['site_id'] = imfp.site_id
    hf.attrs['protocol_name'] = imfp.protocol_name

    hf.attrs['num_samples'] = imfp.num_samples
    hf.attrs['num_noise_channels'] = imfp.num_noise_channels
    hf.attrs['nimfs'] = imfp.nimfs
    hf.attrs['hemispheres'] = imfp.hemispheres
    hf.attrs['index2electrode'] = imfp.index2electrode
    hf.attrs['resolution'] = imfp.resolution
    hf.attrs['max_iterations'] = imfp.max_iterations
    hf.attrs['sample_rate'] = imfp.sample_rate

    hf['imfs'] = imfs
    hf.close()


def find_missing(f, site_id, protocol_name, hemispheres=['L'], njobs=10, overlap=10.0, output_dir='/auto/k8/fdata/mschachter/analysis/memd'):

    seg_times = split_site_into_segments(f, site_id=site_id, protocol_name=protocol_name, njobs=njobs, overlap=overlap)

    hemi_str = ','.join(hemispheres)

    for start_time,end_time in seg_times:
        output_file = os.path.join(output_dir, 'imfs_Site%d_%s_%s_start%0.6f_end%0.6f.h5' % \
                                                       (site_id, protocol_name, hemi_str,
                                                        start_time, end_time))

        missing_files = list()
        if not os.path.exists(output_file):
            missing_files.append(output_file)
        else:
            print '%s exists.' % output_file

        for mf in missing_files:
            print '%s is missing!' % mf


def run_single(hdf5_cell_file='/auto/k8/fdata/mschachter/tdt_continuous/GreBlu9508M/GreBlu9508M_tabled.h5',
             pyslurm='/auto/k8/mschachter/cluster/pyslurm.sh',
             source_dir='/auto/fhome/mschachter/tlab/neurotables/src/python',
             output_dir='/auto/k8/fdata/mschachter/analysis/memd',
             log_dir='/auto/k8/fdata/mschachter/analysis/memd/log',
             site_id=1,
             protocol='Call1'):

    pass

def run_jobs(hdf5_cell_file='/auto/k8/fdata/mschachter/tdt_continuous/GreBlu9508M/GreBlu9508M_tabled.h5',
             pyslurm='/auto/k8/mschachter/cluster/pyslurm.sh',
             source_dir='/auto/fhome/mschachter/tlab/neurotables/src/python',
             output_dir='/auto/k8/fdata/mschachter/analysis/memd',
             log_dir='/auto/k8/fdata/mschachter/analysis/memd/log',
             site_id=1,
             protocol='Call1'):

    f = tables.open_file(hdf5_cell_file, 'r')

    njobs = 10
    seg_times = split_site_into_segments(f, site_id=site_id, protocol_name=protocol, njobs=njobs, overlap=10.0)

    #hemis = [ ['L'], ['R'], ['L', 'R'] ]
    hemis = [ 'L', 'R', 'L,R' ]
    job_list = list()
    #group segment times into pairs
    for hemispheres in hemis:
        for k in range(njobs/2):
            si = 2*k
            ei = si + 2
            stimes = seg_times[si:ei]
            start_times = ','.join(['%0.6f' % x[0] for x in stimes])
            end_times = ','.join(['%0.6f' % x[1] for x in stimes])

            args = list()
            args.append(pyslurm)
            args.append(source_dir)
            args.append('neurotables/jobs/lfp/memd.py')
            args.append('--cell_file')
            args.append(hdf5_cell_file)
            args.append('--site_id')
            args.append('%d' % site_id)
            args.append('--protocol_name')
            args.append(protocol)
            args.append('--output_dir')
            args.append(output_dir)
            args.append('--hemispheres')
            args.append(hemispheres)
            args.append('--start_times')
            args.append(start_times)
            args.append('--end_times')
            args.append(end_times)
            args.append('--num_samples')
            args.append('100')
            args.append('--nimfs')
            args.append('7')
            args.append('--rseed')
            args.append('12345678')

            log_file = os.path.join(log_dir, 'slurm_memd_Site%d_%s_%s_%%j.log' % (site_id, protocol, hemispheres))
            sparams = dict()

            #sparams['extra_code'] = 'sbcast -C %s %s' % (f.filename, tmp_f_path)
            sparams['partition'] = 'all'
            sparams['out'] = log_file
            sparams['err'] = log_file
            sparams['cpus'] = 2
            sparams['mem'] = '15400M'

            job_list.append( (args, sparams) )

    sb = SlurmBot()
    for args,sparams in job_list:
        sb.add(args, sparams)
    num_jobs = len(sb.jobs)
    print 'Running %d jobs' % num_jobs
    sb.run()

    f.close()


if __name__ == '__main__':

    ap = argparse.ArgumentParser(description='Compute the multi-variate empirical mode decomposition (MEMD).')
    ap.add_argument('--cell_file', type=str, required=True, help='The HDF5 file containing cell information.')
    ap.add_argument('--site_id', type=int, required=True, help='The Site ID')
    ap.add_argument('--protocol_name', type=str, required=True, help='The name of the protocol')
    ap.add_argument('--hemispheres', type=str, required=True, help='Semi-colon separated list of hemispheres (L, R)')
    ap.add_argument('--output_dir', type=str, required=True, help='The output directory')
    ap.add_argument('--start_times', type=str, required=True, default=None, help='Comma separated list of start times')
    ap.add_argument('--end_times', type=str, required=True, default=None, help='Comma separated list of end times')
    ap.add_argument('--nimfs', type=int, required=False, default=6, help='The # of IMFs ')
    ap.add_argument('--num_noise_channels', type=int, required=False, default=0, help='The # of noise channels to use (for Noise-assisted MEMD) ')
    ap.add_argument('--resolution', type=float, required=False, default=1.0, help='The resolution used as stopping criteria')
    ap.add_argument('--max_iterations', type=int, required=False, default=30, help='The maximum number of iterations for sifting')
    ap.add_argument('--num_samples', type=int, required=False, default=100, help='The # of random projections used to compute the mean envelope.')
    ap.add_argument('--rseed', type=int, required=False, default=12345678, help='Random seed to use (defaults to 12345678)')

    argvals = ap.parse_args()

    start_times = [float(s) for s in argvals.start_times.split(',')]
    end_times = [float(s) for s in argvals.end_times.split(',')]
    hemispheres = argvals.hemispheres.split(',')
    print 'start_times=',start_times
    print 'end_times=',end_times

    def pfunc(start_time, end_time):
        f = tables.open_file(argvals.cell_file, 'r')

        output_file = os.path.join(argvals.output_dir, 'imfs_Site%d_%s_%s_start%0.6f_end%0.6f.h5' % \
                                                       (argvals.site_id, argvals.protocol_name, argvals.hemispheres,
                                                        start_time, end_time))
        print 'Running MEMD, output file: %s' % output_file

        imfs,index2electrode,sample_rate = run_memd(f, argvals.site_id, argvals.protocol_name, hemispheres=hemispheres,
                                                    start_time=start_time, end_time=end_time, output_file=output_file,
                                                    nimfs=argvals.nimfs, num_noise_channels=argvals.num_noise_channels,
                                                    num_samps=argvals.num_samples, resolution=argvals.resolution,
                                                    max_iterations=argvals.max_iterations, rseed=argvals.rseed)
        f.close()


    print '# of jobs: %d' % len(start_times)

    p = joblib.Parallel(n_jobs=len(start_times), verbose=1)
    jobs = list()
    for start_time,end_time in zip(start_times, end_times):
        d = joblib.delayed(pfunc)
        jobs.append(d(start_time, end_time))
    p(jobs)
