import argparse
import os
import tables
import joblib

import numpy as np

from slurm_tools import SlurmBot
from neurotables.analysis.lfp.fit_trial_envelope_models import fit_filtered_models


def run_all(hdf5_cell_file='/auto/k8/fdata/mschachter/tdt_continuous/GreBlu9508M/GreBlu9508M_tabled.h5',
            ve_script='/auto/fhome/mschachter/virtualenv_cluster/pyslurm_ve.sh',
            source_dir='/auto/fhome/mschachter/tlab/neurotables/src/python',
            output_dir='/auto/k8/fdata/mschachter/analysis/envelope_models',
            log_dir='/auto/k8/fdata/mschachter/analysis/envelope_models/log',
            site_id=1,
            protocol='Call1',
            electrode_ids=None):

    epairs = list()
    if electrode_ids is None:
        num_electrodes = 32
        for k in range(num_electrodes/2):
            epairs.append( (k*2 + 1, k*2 + 2))
    else:
        epairs = electrode_ids

    print 'Total # of jobs to run per band: %d' % len(epairs)

    sb = SlurmBot()
    job_list = list()

    for e1,e2 in epairs:

        args = list()
        args.append(ve_script)
        args.append(source_dir)
        args.append('neurotables/jobs/lfp/fit_trial_envelope_models.py')
        args.append('--cell_file')
        args.append(hdf5_cell_file)
        args.append('--site_id')
        args.append('%d' % site_id)
        args.append('--protocol_name')
        args.append(protocol)
        args.append('--output_dir')
        args.append(output_dir)
        args.append('--electrodes')
        args.append('%d,%d' % (e1,e2))

        log_file = os.path.join(log_dir, 'slurm_envelope_models_e%d_e%d_Site%d_%s_%%j.log' % \
                                         (e1, e2, site_id, protocol))
        sparams = dict()

        #sparams['extra_code'] = 'sbcast -C %s %s' % (f.filename, tmp_f_path)
        sparams['partition'] = 'all'
        sparams['out'] = log_file
        sparams['err'] = log_file
        sparams['cpus'] = 2
        sparams['mem'] = '10000M'

        job_list.append( (args, sparams) )

    for args,sparams in job_list:
        sb.add(args, sparams)
    num_jobs = len(sb.jobs)
    print 'Running %d jobs' % num_jobs
    sb.run()
    return job_list


if __name__ == '__main__':

    #to use multiple cores
    #os.system("taskset -p 0xff %d" % os.getpid())

    ap = argparse.ArgumentParser(description='Compute the cross-coherence between two electrodes')
    ap.add_argument('--cell_file', type=str, required=True, help='The HDF5 file containing cell information.')
    ap.add_argument('--site_id', type=int, required=True, help='The Site ID')
    ap.add_argument('--protocol_name', type=str, required=True, help='The name of the protocol')
    ap.add_argument('--electrodes', type=str, required=True, help='Comma separated list of electrodes')
    ap.add_argument('--output_dir', type=str, required=True, help='The output directory')
    argvals = ap.parse_args()

    def pfunc(electrode):
        f = tables.open_file(argvals.cell_file, 'r')
        ofile = os.path.join(argvals.output_dir, 'encoder_decoder_Site%d_%s_e%d.h5' % \
                                                 (argvals.site_id, argvals.protocol_name, electrode))
        cutoff_freqs = range(1, 20) + range(20, 65, 5)
        fit_filtered_models(f, electrode, ofile, site_id=argvals.site_id, protocol_name=argvals.protocol_name, cv_type='random', high_cutoff_freqs=cutoff_freqs, low_cutoff_freqs=cutoff_freqs)

    np.random.seed(12345)

    electrodes = [int(x) for x in argvals.electrodes.split(',')]
    print '# of jobs: %d' % len(electrodes)
    print electrodes
    p = joblib.Parallel(n_jobs=len(electrodes), verbose=5)
    jobs = list()
    for enumber in electrodes:
        #pfunc(e1, e2)
        d = joblib.delayed(pfunc)
        jobs.append(d(enumber))
    p(jobs)
