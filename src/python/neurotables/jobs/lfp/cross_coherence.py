import argparse
import os
import h5py

import numpy as np
try:
    from sklearn.decomposition import PCA
except ImportError:
    from scikits.learn.decomposition import PCA

from slurm_tools import SlurmBot
import tables
import joblib
from neurotables.analysis.lfp.cross_coherence import compute_cc, get_cc_output_file_name,ElectrodeComparator, get_all_pair_files


if __name__ == '__main__':

    #to use multiple cores
    #os.system("taskset -p 0xff %d" % os.getpid())

    ap = argparse.ArgumentParser(description='Compute the cross-coherence between two electrodes')
    ap.add_argument('--cell_file', type=str, required=True, help='The HDF5 file containing cell information.')
    ap.add_argument('--spec_window_size', type=float, required=True, help='The window size of the spectrogram')
    ap.add_argument('--coherence_window_size', type=float, required=True, help='The window size of the cross coherence')
    ap.add_argument('--site_id', type=int, required=True, help='The Site ID')
    ap.add_argument('--protocol_name', type=str, required=True, help='The name of the protocol')
    ap.add_argument('--electrode_pairs', type=str, required=True, help='Semi-colon separated list of electrode pairs (e.g. 1,2;1,15;6,22)')
    ap.add_argument('--output_dir', type=str, required=True, help='The output directory')
    ap.add_argument('--t1', type=float, required=False, default=None, help='The start time')
    ap.add_argument('--t2', type=float, required=False, default=None, help='The end time')
    ap.add_argument('--num_shuffles', type=int, required=False, default=0, help='The number of shuffles for computing the coherence floor, or 0 if no floor should be computed.')
    ap.add_argument('--edesc', type=str, required=False, default=None, help='Extra text to append to the output file name.')
    argvals = ap.parse_args()

    def pfunc(e1, e2):
        f = tables.open_file(argvals.cell_file, 'r')

        compute_cc(f, argvals.site_id, argvals.protocol_name, e1, e2, argvals.output_dir,
                   spec_window_size=argvals.spec_window_size, coherence_window_size=argvals.coherence_window_size,
                   t1=None, t2=None, edesc=None, num_shuffles=argvals.num_shuffles)

    epairs = [(int(x[0]),int(x[1])) for x in [s.split('x') for s in argvals.electrode_pairs.split('a')]]
    print '# of jobs: %d' % len(epairs)
    print epairs
    p = joblib.Parallel(n_jobs=len(epairs), verbose=5)
    jobs = list()
    for e1,e2 in epairs:
        #pfunc(e1, e2)
        d = joblib.delayed(pfunc)
        jobs.append(d(e1, e2))
    p(jobs)


def run_all(hdf5_cell_file='/auto/k8/fdata/mschachter/tdt_continuous/GreBlu9508M/GreBlu9508M_tabled.h5',
            ve_script='/auto/fhome/mschachter/virtualenv_cluster/pyslurm_ve.sh',
            source_dir='/auto/fhome/mschachter/tlab/neurotables/src/python',
            output_dir='/auto/k8/fdata/mschachter/analysis/cc',
            log_dir='/auto/k8/fdata/mschachter/analysis/cc/log',
            pairs_to_use=None,
            site_id=1,
            protocol='Call1'):

    num_electrodes = 32
    if pairs_to_use is None:
        all_pairs = dict()
        for i in range(num_electrodes):
            for j in range(num_electrodes):
                if i == j:
                    continue
                p = [i+1, j+1]
                p.sort()
                all_pairs[tuple(p)] = True
        all_pairs = all_pairs.keys()
    else:
        all_pairs = pairs_to_use

    print 'Total # of jobs to run per band: %d' % len(all_pairs)

    pairs_per_job = 2
    job_pairs = list()
    njobs = len(all_pairs) / 2
    for k in range(njobs):
        m = k*2
        if m == len(all_pairs) - 1:
            job_pairs.append(all_pairs[m])
        else:
            job_pairs.append(all_pairs[m:m+pairs_per_job])

    sb = SlurmBot()
    job_list = list()

    for epairs in job_pairs:

        args = list()
        args.append(ve_script)
        args.append(source_dir)
        args.append('neurotables/jobs/lfp/cross_coherence.py')
        args.append('--spec_window_size')
        args.append('0.100')
        args.append('--coherence_window_size')
        args.append('0.200')
        args.append('--cell_file')
        args.append(hdf5_cell_file)
        args.append('--site_id')
        args.append('%d' % site_id)
        args.append('--protocol_name')
        args.append(protocol)
        args.append('--output_dir')
        args.append(output_dir)
        args.append('--num_shuffles')
        args.append('150')

        args.append('--electrode_pairs')
        if len(epairs) > 1:
            pstr = '%s' % 'a'.join(['x'.join([str(y) for y in x]) for x in epairs])
        else:
            pstr = '%s' % 'a'.join(str(x) for x in epairs[0])
        args.append(pstr)

        log_file = os.path.join(log_dir, 'slurm_cross_coherence_Site%d_%s_%s_%%j.log' % \
                                         (site_id, protocol, pstr))
        sparams = dict()

        #sparams['extra_code'] = 'sbcast -C %s %s' % (f.filename, tmp_f_path)
        sparams['partition'] = 'all'
        sparams['out'] = log_file
        sparams['err'] = log_file
        sparams['cpus'] = 2
        sparams['mem'] = '15400M'

        job_list.append( (args, sparams) )

    for args,sparams in job_list:
        sb.add(args, sparams)
    num_jobs = len(sb.jobs)
    print 'Running %d jobs' % num_jobs
    sb.run()
    return job_list


def run_pca_all(site_id=1, protocol_name='Call1', num_electrodes=32, edesc=None,
                cell_file='/auto/k8/fdata/mschachter/tdt_continuous/GreBlu9508M/GreBlu9508M_tabled.h5',
                output_dir='/auto/k8/fdata/mschachter/analysis/cc'):

    all_files = get_all_pair_files(site_id=site_id, protocol_name=protocol_name, num_electrodes=num_electrodes, edesc=edesc)
    f = tables.open_file(cell_file, 'r')

    if edesc is not None:
        edesc = '_%s' % edesc

    hf = h5py.File(os.path.join(output_dir, 'pca_Site%d_%s%s.h5' % (site_id, protocol_name, edesc)), 'w')

    print 'Total of %d pairs to be processed...' % len(all_files)

    for e1,e2,ofile in all_files:
        print 'Reading file %s, doing PCA...' % ofile
        ec = ElectrodeComparator.from_file(f, ofile, root_dir=output_dir)
        if ec is None:
            return
        ctf = ec.ctimefreq
        nzctf = ctf > 0.0
        ctf[nzctf] = np.log(ctf[nzctf])
        ctf[~nzctf] = ctf[nzctf].min()

        pca = PCA(n_components=len(ec.cfreq))
        pca.fit(ctf.transpose())

        grp = hf.create_group('%d,%d' % (e1, e2))
        grp['pcs'] = pca.components_
        grp['explained_variance'] = pca.explained_variance_ratio_
        hf.flush()
        del ec

    f.close()
    hf.close()


def check_pairs(site_id=1, protocol_name='Call1', num_electrodes=32, edesc=None, output_dir='/auto/k8/fdata/mschachter/analysis/cc'):

    all_files = get_all_pair_files(site_id=site_id, protocol_name=protocol_name, num_electrodes=num_electrodes,
                                   edesc=edesc)

    missing_pairs = list()
    for e1,e2,ofile in all_files:
        ofile_full = os.path.join(output_dir, ofile)
        if not os.path.exists(ofile_full):
            print 'File %s is missing!' % ofile_full
            missing_pairs.append( (e1, e2) )

    return missing_pairs


def fix_ec_files(site_id=1, protocol_name='Call1', output_dir='/auto/k8/fdata/mschachter/analysis/cc'):

    pca_file = os.path.join(output_dir, 'pca_Site%d_%s.h5' % (site_id, protocol_name))

    hf = h5py.File(pca_file, 'r')
    for key in hf.keys():
        e1,e2 = [int(x) for x in key.split(',')]
        pcs = np.array(hf[key]['pcs'])
        evar = np.array(hf[key]['explained_variance'])

        pair_file = get_cc_output_file_name(e1, e2, site_id, protocol_name, None)
        print 'fixing file %s' % pair_file
        ecf = h5py.File(os.path.join(output_dir, pair_file), 'a')
        cgrp = ecf['cross_coherence']
        cgrp['principle_components'] = pcs
        cgrp['explained_variance_ratio'] = evar
        ecf.close()

    hf.close()


def export_pca(site_id=1, protocol_name='Call1', num_electrodes=32, output_dir='/auto/k8/fdata/mschachter/analysis/cc',
               edesc=None, topn=6):

    all_files = get_all_pair_files(site_id=site_id, protocol_name=protocol_name, num_electrodes=num_electrodes,
                                   edesc=edesc)

    output_file = os.path.join(output_dir, 'pca_proj_Site%d_%s.h5' % (site_id, protocol_name))
    ohf = h5py.File(output_file, 'a')

    for e1, e2, ofile in all_files:

        opath = os.path.join(output_dir, ofile)
        print 'Reading file %s' % opath

        key = '%d,%d' % (e1, e2)
        if key in ohf.keys():
            print 'Already written!'
            continue

        hf = h5py.File(opath, 'r')
        cgrp = hf['cross_coherence']
        pcs = np.array([cgrp['principle_components']]).squeeze()
        ctf = np.array([cgrp['timefreq']]).squeeze()

        nzctf = ctf > 0.0
        ctf[nzctf] = np.log(ctf[nzctf])
        ctf[~nzctf] = ctf[nzctf].min()

        print 'ctf.shape=',ctf.shape
        print 'pcs.shape=',pcs.shape

        #project timefreq onto it's principle components
        proj_pcs = np.dot(ctf.transpose(), pcs[:, :topn])
        hf.close()
        del ctf
        del pcs

        print 'proj_pcs.shape=',proj_pcs.shape

        ohf[key] = proj_pcs
        ohf.flush()
        del proj_pcs

    ohf.close()
