import os
import operator

import tables

from lasp.incrowd import fast_conv
from lasp.signal import power_spectrum

from neurotables.analysis.lfp.fit_envelope import *
from neurotables.analysis.lfp.stim_cond import get_lfp_tensors_by_stim
from neurotables.objects.cells import Site


def fit_encoder(stim_envelopes, stim_averaged_lfps, sample_rate, output_file,
                filter_len=0.500, lambda1=5.0, lambda2=5.0, nfolds=3):

    #specify the filter length and lags
    nlags = int(filter_len*sample_rate) + 1
    lags = np.arange(0, nlags, 1, dtype='int')

    #fit the encoding model
    lf = fit_envelope_lfp_model(stim_envelopes, stim_averaged_lfps, sample_rate, lags,
                                lambda1=lambda1, lambda2=lambda2, threshold=0.0, niters=25, nfolds=nfolds,
                                encoder=True)
    lf.to_file(output_file, include_dataset=False)
    #lf.plot()
    #plt.show()


def run_all(cell_file='/auto/k8/fdata/mschachter/tdt_continuous/GreBlu9508M/GreBlu9508M_tabled.h5',
            lambda1=5.0, lambda2=5.0, filter_len=0.300, nfolds=5,
            output_dir='/auto/k8/fdata/mschachter/analysis/stim_env_encoder',
            site_ids=[1, 2, 3, 4, 5], electrode_ids=range(32), edesc='', check_missing=False):

    for site_id in site_ids:
        for electrode_id in electrode_ids:
            if check_missing:
                output_file = os.path.join(output_dir, 'env_encoder_site%d_electrode%d%s.h5' % (site_id, electrode_id+1, edesc))
                if not os.path.exists(output_file):
                    print 'No file for site %d and electrode %d' % (site_id, electrode_id+1)
            else:
                run(cell_file, site_id, electrode_id+1, output_dir, lambda1, lambda2, filter_len, nfolds)


def run(cell_file, site_id, electrode, output_dir, lambda1, lambda2, filter_len, nfolds, edesc=''):
    
    f = tables.open_file(cell_file, mode='r')
    site_ids,sample_rate,stim_envelopes,lfp_tensors_by_stim,electrodes_indices = get_lfp_tensors_by_stim(f)
    f.close()

    site_to_use = [site_id]
    electrodes_to_use = [electrode-1]
    index2indices,lfp_matrices_by_stim = get_stim_averaged_lfps(lfp_tensors_by_stim, site_to_use, electrodes_to_use)

    stim_envelopes_list = list()
    stim_averaged_lfps = list()
    stim_ids = stim_envelopes.keys()
    for sid in stim_ids:
        lmat = lfp_matrices_by_stim[sid]
        stim_env = stim_envelopes[sid]
        print 'sid=%d, stim_env.shape=%s, lmat.shape=%s' % (sid, str(stim_env.shape), str(lmat.shape))
        stim_envelopes_list.append(stim_env)
        stim_averaged_lfps.append(lmat)

    output_file = os.path.join(output_dir, 'env_encoder_site%d_electrode%d%s.h5' % (site_id, electrode, edesc))

    fit_encoder(stim_envelopes_list, stim_averaged_lfps,
                sample_rate, output_file, lambda1=lambda1, lambda2=lambda2,
                filter_len=filter_len, nfolds=nfolds)


if __name__ == '__main__':

    """
    ap = argparse.ArgumentParser(description='Compute an encoding filter that maps the stimulus amplitude envelope to an LFP')
    ap.add_argument('--cell_file', type=str, required=True, help='The HDF5 file containing cell information.')
    ap.add_argument('--site_id', type=int, required=True, help='The Site ID')
    ap.add_argument('--lambda1', type=float, default=1.0, help='LASSO L1 Regularization weight')
    ap.add_argument('--lambda2', type=float, default=1.0, help='LASSO L2 Regularization weight')
    ap.add_argument('--filter_len', type=float, default=0.500, help='Length of filter in seconds')
    ap.add_argument('--env_cutoff_freq', type=float, default=50.0, help='Cutoff frequency for low pass filter of envelope')
    ap.add_argument('--nfolds', type=int, default=5, help='Number of folds for cross validation')
    ap.add_argument('--electrode', type=int, required=True, help='Comma separated list of electrode pairs (e.g. 1,2,3,4,...)')
    ap.add_argument('--output_dir', type=str, required=True, help='The output directory')
    ap.add_argument('--edesc', type=str, required=False, default=None, help='Extra text to append to the output file name.')
    argvals = ap.parse_args()

    edesc = ''
    if argvals.edesc is not None:
        edesc = '_%s' % argvals.edesc

    run(argvals.cell_file, argvals.site_id, argvals.electrode, argvals.output_dir, argvals.lambda1, argvals.lambda2,
        argvals.filter_len, argvals.nfolds, argvals.env_cutoff_freq, edesc)
    """

    #cell_file = '/auto/k8/fdata/mschachter/tdt_continuous/GreBlu9508M/GreBlu9508M_tabled.h5'
    #run(cell_file, site_id=1, electrode=7, output_dir='/tmp', lambda1=0.5, lambda2=0.5, filter_len=0.150, nfolds=5)

    #run_all()

    #stim_type = 'call'
