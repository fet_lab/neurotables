import argparse
import os
import subprocess
import tables
import joblib

import numpy as np

from slurm_tools import SlurmBot
from neurotables.analysis.lfp.fit_trial_spec_models import fit_spectrogram_encoder

def run_all_local(hdf5_cell_file='/auto/k8/fdata/mschachter/tdt_continuous/GreBlu9508M/GreBlu9508M_tabled.h5',
                  source_dir='/auto/fhome/mschachter/tlab/neurotables/src/python',
                  output_dir='/auto/k8/fdata/mschachter/analysis/spec_models',
                  site_id=1,
                  protocol='Call1',
                  electrode_ids=None):

    electrode_ids = range(32)

    for electrode_id in electrode_ids:

        electrode = electrode_id + 1

        args = list()
        args.append('python')
        args.append('neurotables/jobs/lfp/fit_trial_spec_models.py')
        args.append('--cell_file')
        args.append(hdf5_cell_file)
        args.append('--site_id')
        args.append('%d' % site_id)
        args.append('--protocol_name')
        args.append(protocol)
        args.append('--output_dir')
        args.append(output_dir)
        args.append('--electrode')
        args.append('%d' % electrode)

        print '---------------------'
        print 'Running for electrode %d' % electrode
        print ' '.join(args)

        subprocess.check_call(args)


def run_all(hdf5_cell_file='/auto/k8/fdata/mschachter/tdt_continuous/GreBlu9508M/GreBlu9508M_tabled.h5',
            ve_script='/auto/fhome/mschachter/virtualenv_cluster/pyslurm_ve.sh',
            source_dir='/auto/fhome/mschachter/tlab/neurotables/src/python',
            output_dir='/auto/k8/fdata/mschachter/analysis/spec_models',
            log_dir='/auto/k8/fdata/mschachter/analysis/spec_models/log',
            site_id=1,
            protocol='Call1',
            electrode_ids=None):

    if electrode_ids is None:
        electrode_ids = range(32)

    print 'Total # of jobs to run: %d' % len(electrode_ids)

    sb = SlurmBot()
    job_list = list()

    for electrode_id in electrode_ids:

        electrode = electrode_id + 1

        args = list()
        args.append(ve_script)
        args.append(source_dir)
        args.append('neurotables/jobs/lfp/fit_trial_spec_models.py')
        args.append('--cell_file')
        args.append(hdf5_cell_file)
        args.append('--site_id')
        args.append('%d' % site_id)
        args.append('--protocol_name')
        args.append(protocol)
        args.append('--output_dir')
        args.append(output_dir)
        args.append('--electrode')
        args.append('%d' % electrode)

        log_file = os.path.join(log_dir, 'slurm_spec_models_e%d_Site%d_%s_%%j.log' % \
                                         (electrode, site_id, protocol))
        sparams = dict()

        #sparams['extra_code'] = 'sbcast -C %s %s' % (f.filename, tmp_f_path)
        sparams['partition'] = 'all'
        sparams['out'] = log_file
        sparams['err'] = log_file
        sparams['cpus'] = 2
        sparams['mem'] = '15000M'

        job_list.append( (args, sparams) )

    for args,sparams in job_list:
        sb.add(args, sparams)
    num_jobs = len(sb.jobs)
    print 'Running %d jobs' % num_jobs
    sb.run()
    return job_list


if __name__ == '__main__':

    #to use multiple cores
    #os.system("taskset -p 0xff %d" % os.getpid())

    ap = argparse.ArgumentParser(description='Compute the cross-coherence between two electrodes')
    ap.add_argument('--cell_file', type=str, required=True, help='The HDF5 file containing cell information.')
    ap.add_argument('--site_id', type=int, required=True, help='The Site ID')
    ap.add_argument('--protocol_name', type=str, required=True, help='The name of the protocol')
    ap.add_argument('--electrode', type=int, required=True, help='The electrode to fit')
    ap.add_argument('--output_dir', type=str, required=True, help='The output directory')
    argvals = ap.parse_args()

    electrode = argvals.electrode
    np.random.seed(12345)

    f = tables.open_file(argvals.cell_file, 'r')
    ofile = os.path.join(argvals.output_dir, 'encoder_spec_Site%d_%s_e%d.h5' % \
                         (argvals.site_id, argvals.protocol_name, electrode))

    encoder_filt = fit_spectrogram_encoder(f, electrode, site_id=argvals.site_id, protocol_name=argvals.protocol_name,
                                           cv_type='random', opt_params={'solver':'ridge', 'alpha':1.0})

    encoder_filt.to_file(output_file=ofile, include_dataset=False)

    f.close()
